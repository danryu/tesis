<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
class Tw_msg
{

	private $from = '+16125675628';
    public function __construct()  
    { 
        $this->CI =& get_instance();
        $this->CI->load->library('twilio');
    } 
    public function send($var)
    {
    	
    	$message=NULL;
    	$from=$this->from;
		if(isset($var['type'])){$type=$var['type'];}else{$type="a#s5g";}
		if(isset($var['pass'])){$pass=$var['pass'];}else{$pass=NULL;}
		if(isset($var['to'])){$to='+'.$var['to'];}else{$to='+56972363570';}		
		switch ($type) 
		{
			case 'password':
				$message="Se ha creado una cuenta en Apple's Teacher, su contraseña es: ".$pass;
				break;
			case 'new_pass':
				$message="Se ha generado una nueva contraseña para su cuenta Apple's Teacher: ".$pass;
				break;
			case 'new_alert':
				$message=$this->tw_alert_mensaje();
				break;
			case 'dead':									//BORRAR
				$message=$this->tw_alert_dead();
				break;
			default:
				$message=NULL;
				break;    
		}
		if($message!=NULL)
		{
			$response = $this->CI->twilio->sms($from, $to, $message);
			if($response->IsError)
			{
				$r=array('bool'=>FALSE,'msg'=>$response->ErrorMessage);
				return $r;
			}
			else
			{
				$r=array('bool'=>TRUE,'msg'=>'Mensaje enviado correctamente');
				return $r;
			}
		}
		else
		{
			$r=array('bool'=>FALSE,'msg'=>'Error al generar mensaje');
			return $r;
		}
	}
    private function tw_pass($pass)
    {
    	$texto="Se ha creado una cuenta en Apple\'s Teacher, su contraseña es:".$pass;
    	return $texto;
    }
    private function tw_alert_dead()
    {
    	$texto="Su hijo murio, por favor retire la bolsa del colegio";
    	return $texto;
    }
    private function tw_alert_mensaje($id=NULL)
    {
    	$texto="Tiene una nueva alerta, por favor revisar su bandeja de entrada";
    	return $texto;
    }
    private function tw_alert($pass)
    {
    	$texto="Se ha creado una cuenta en Apple\'s Teacher, su contraseña es:".$pass;
    	return $texto;
    }

}
        
	
	?>