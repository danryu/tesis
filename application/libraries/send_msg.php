<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
/** 
 * Layouts Class. PHP5 only. 
 * 
 */
class Send_msg
{ 
	private $CI; 
	public function __construct()  
      { 
        $this->CI =& get_instance(); 
        $this->CI->load->helper('url');
        $this->CI->load->library('ion_auth');  
        $this->CI->load->library('twilio');
		$this->CI->load->library('ion_auth');
		$this->CI->load->library('tw_msg');
		$this->CI->load->library('Mandrill');
		$this->CI->load->library('security_lib');
		$this->CI->load->helper('app');
		$this->CI->load->model('app_model');
      } 

	function twitter($Id_twitter_User=NULL,$Mensaje)
	{
		$this->CI->config->load('twitter');
		$this->CI->load->library('twitteroauth');
		$consumer=$this->CI->config->item('twitter_consumer_token');
		$consumer_secret=$this->CI->config->item('twitter_consumer_secret');
		$token=$this->CI->config->item('twitter_access_token');
		$token_secret=$this->CI->config->item('twitter_access_secret');
		$connection = $this->CI->twitteroauth->create(	$consumer, $consumer_secret,$token,$token_secret);
		if($Id_twitter_User!=NULL)
		{
			$options = array("user_id" => $Id_twitter_User, "text" => $Mensaje);
			$result = $connection->post('direct_messages/new', $options);
			if(!isset($result->errors))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	function SMS($telefono,$Mensaje)
	{

	}
	function Mandril($email,$Mensaje,$titulo,$subject)
	{		
		$this->CI->load->library('Mandrill');
		$mandrill_ready = NULL;
		try 
		{
		    $this->CI->mandrill->init($this->CI->config->item('mandrill_key'));
		    $mandrill_ready = TRUE;
		} 
		catch(Mandrill_Exception $e) 
		{
		    $mandrill_ready = FALSE;
		    return false;
		}
		if( $mandrill_ready ) 
		{
			$var=array('type'=>'creator','title'=>$titulo,'mensaje'=>$Mensaje);
		    $body=$this->CI->mandrill->msg_create($var);		   
		    $email = array(
		        'html' => $body,// 'text' => '<h1>Ejemplo</h1>',//$body,
		        'subject' => $subject, 'from_email' => 'applesteacher2015@gmail.com',
		        'from_name' => 'Apple\'s Teacher','to' => array(array('email' => $email)));
		    $result = $this->CI->mandrill->messages_send($email);
		    return true;
		}
	}
	function twitter_Masivo($twitter,$Mensaje)
	{
		if(isset($twitter) && isset($Mensaje))
		{
			$Id_bueno=array();
			$this->CI->config->load('twitter');
			$this->CI->load->library('twitteroauth');
			$consumer=$this->CI->config->item('twitter_consumer_token');
			$consumer_secret=$this->CI->config->item('twitter_consumer_secret');
			$token=$this->CI->config->item('twitter_access_token');
			$token_secret=$this->CI->config->item('twitter_access_secret');
			$connection = $this->CI->twitteroauth->create(	$consumer, $consumer_secret,$token,$token_secret);
			foreach ($twitter as $t) 
			{
				
				if($t->Id_twitter!=NULL)
				{
					if($t->Id_twitter>0)
					{
						$Id_bueno[]=$t->Id_twitter;
					}
				}
			}
			if(count($Id_bueno)>0)
			{
				for($i=0;$i<count($Id_bueno);$i++)
				{

					$options = array("user_id" => $Id_bueno[$i], "text" => $Mensaje);
					$result = $connection->post('direct_messages/new', $options);
					if(isset($result->errors)){ return false;}
				}
				return true;
			}
			else
			{ return false;}			
		}
		else
		{ return false;}		
	}
	function SMS_Masivo($Mensaje,$to)
	{
		//$to ='+56972363570';
		$from = '+16125675628';
		if($to!=null)
		{
			if($Mensaje!=NULL)
			{
				foreach($to as $t) 
				{
					$num=$t->cellphone;
					if(strlen($num)>=8)
					{
						$num='+'.$num;
						$response = $this->CI->twilio->sms($from, $num, $Mensaje);
						if($response->IsError)
						{
							$r=array('bool'=>FALSE,'msg'=>$response->ErrorMessage);
						}
						else
						{
							$r=array('bool'=>TRUE,'msg'=>'Mensaje enviado correctamente');
							
						}
					}
					
				}	
			}
			else
			{
				$r=array('bool'=>FALSE,'msg'=>'Error al generar mensaje');
				return $r;
			}
		}
		else
		{
			$r=array('bool'=>FALSE,'msg'=>'No se han encontrado numeros telefonicos');
			return $r;
		}
		
	}
	function SMS_Masivo_nota($to)
	{
		//$to ='+56972363570';
		$from = '+16125675628';
		if($to!=null)
		{
				foreach($to as $t) 
				{
					$Mensaje='Su alumno ha obtenido un '.$t->Nota.' en '.$t->Nombre;
					$num=$t->cellphone;
					if(strlen($num)>=8)
					{
						$num='+'.$num;
						$response = $this->CI->twilio->sms($from, $num, $Mensaje);
						if($response->IsError)
						{
							$r=array('bool'=>FALSE,'msg'=>$response->ErrorMessage);
						}
						else
						{
							$r=array('bool'=>TRUE,'msg'=>'Mensaje enviado correctamente');
							
						}
					}
					
				}	
			
		}
		else
		{
			$r=array('bool'=>FALSE,'msg'=>'No se han encontrado numeros telefonicos');
			return $r;
		}
		
	}
	function Mandril_Masivo($objeto_email,$Mensaje,$titulo,$subject)
	{
		$array_email=array();
		foreach ($objeto_email as $e) 
		{
			$array_email[]=array('email' => $e->email);
		}
		$this->CI->load->library('Mandrill');
		$mandrill_ready = NULL;
		if(count($array_email)>0)
		{
			try 
			{
			    $this->CI->mandrill->init($this->CI->config->item('mandrill_key'));
			    $mandrill_ready = TRUE;
			} 
			catch(Mandrill_Exception $e) 
			{
			    $mandrill_ready = FALSE;
			    return false;
			}
			if( $mandrill_ready ) 
			{
				$var=array('type'=>'creator','title'=>$titulo,'mensaje'=>$Mensaje);
			    $body=$this->CI->mandrill->msg_create($var);		   
			    $email = array(
			        'html' => $body,// 'text' => '<h1>Ejemplo</h1>',//$body,
			        'subject' => $subject, 'from_email' => 'applesteacher2015@gmail.com',
			        'from_name' => 'Apple\'s Teacher','to' => $array_email);
			    $result = $this->CI->mandrill->messages_send($email);
			    return true;
			}
		}
		else
		{
			return false;
		}
	}
}
?>