<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
/** 
 * Dashboard | Carga dinamica
 * 
 */
class Micurso
{ 
	private $CI; 
	public function __construct()  
    { 
        $this->CI =& get_instance(); 
        $this->CI->load->helper('url');
        $this->user_log=$this->CI->ion_auth->user()->row();   
    } 
    private function set_name($nombre,$apellido)
    {
        $nombre=preg_split("/[\s,]+/",$nombre);
        $apellido=preg_split("/[\s,]+/",$apellido);
       	return $nombre[0].' '.$apellido[0]; 
    }
    function Mis_Alumnos($alumnos)
    {
    	if($alumnos['bool'])
        {
            $tabla="";
            $lista_alumnos="";
            $i=0;
            foreach ($alumnos['query'] as $q) 
            {
               	$i++;                
                $lista_alumnos.='  <tr>
                                    <td>'.$i.'</td>
                                    <td>'.$q->username.'</td>
                                    <td>'.$this->set_name($q->first_name,$q->last_name).'</td>
                                    <td>'.$q->email.'</td>
                                    <td>
                                        <div class="btn-group">
                                            <button title="" data-original-title="" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                                &nbsp; <i class="fa fa-gear"></i> Acciones &nbsp;
                                                <i class="fa fa-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu opciones-alumno">
                                                <li><a class="hand ver-ficha" id="'.$q->Id.'"><i class="fa fa-eye"></i> Ver Ficha</a></li>
                                                <li><a class="hand anotaciones" id="'.$q->Id.'"><i class="fa fa-pencil-square-o"></i> Anotaciones</a></li>
                                                <li><a class="hand" href="/tesis/profesor/informe_notas/'.$q->Id.'"><i class="fa fa-check-circle"></i> Notas</a></li>
                                            </ul>
                                        </div>
                                    </td>                                                     
                    </tr>';
            }
            $tabla='<div style="margin-top: 25px; margin-bottom: 70px">
                        <table id="tabla_alumnos_lista" class="table table-striped">
                            <thead>
                                <tr>
                                    <th><label style="color:#ffffff">#</label></th>
                                    <th><label style="color:#ffffff">Run</label></th>
                                    <th><label style="color:#ffffff">Nombre</label></th>
                                    <th><label style="color:#ffffff">Email</label></th>
                                    <th><label style="color:#ffffff">opciones</label></th>
                                </tr>
                            </thead>
                            <tbody>
                                '.$lista_alumnos.'
                            </tbody>
                        </table>
                    </div>';
             return $tabla;
        }
    }

   	function Mis_Apoderados($apoderados) 
   	{
        if($apoderados['bool'])
        {
            $tabla="";
            $lista_alumnos="";
            $i=0;
            foreach ($apoderados['query'] as $q) 
            {
                $i++;
                $lista_alumnos.='  <tr>
                                        <td>'.$i.'</td>
                                        <td>'.$q->username.'</td>
                                        <td>'.set_name($q->first_name,$q->last_name).'</td>
                                        <td>'.$q->email.'</td>
                                        <td>'.$q->cellphone.'</td>
                                        <td>'.$q->phone.'</td>
                                    </tr>';
            }
            $tabla='<div style="margin-top: 25px; margin-bottom: 30px">
                        <table id="tabla_apoderado_listado" class="table table-hover">
                            <thead>
                                <th style="color:#ffffff">#</th>
                                <th style="color:#ffffff">Run</th>
                                <th style="color:#ffffff">Nombre</th>
                                <th style="color:#ffffff">Correo Electrónico</th>
                                <th style="color:#ffffff">Telefono Celular</th>
                                <th style="color:#ffffff">Telefono Fijo</th>
                            </thead>
                            <tbody>
                                '.$lista_alumnos.'
                            </tbody>
                        </table>
                    </div>';
                                                    
        } 
        return $tabla;
    }
   	function Mis_Reuniones($reuniones)
   	{
   		if($reuniones['bool'])
        {
            $i=1;
            $horas="";
            
            foreach ($reuniones['query'] as $q) 
            {
                $explode = explode('-',$q->Fecha);
            $dia = $explode[2];
            $mes = $explode[1];
            $año = $explode[0];
            $fecha = $año.'/'.$mes.'/'.$dia ;                    
            $fecha = date('d/m/Y',strtotime($fecha));
                $horas.='<tr>
                            <td>'.$i.'</td><td>'.$q->Motivo.'</td><td>'.$fecha.'</td><td>'.$q->Hora.'</td>
                            <td>
                                <div style="width:50px">
                                    <div class="btn-group">
                                        <button title="" data-original-title="" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                            &nbsp; <i class="fa fa-gear"></i> Acciones &nbsp;
                                            <i class="fa fa-caret-down"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-toggle="modal" data-target="#mostrar_reunion'.$q->Id.'" data-backdrop="false"><i class="fa fa-eye"></i> Ver</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#editar_reunion'.$q->Id.'" data-backdrop="false"><i class="fa fa-pencil-square-o"></i> Editar</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#eliminar_reunion'.$q->Id.'" data-backdrop="false"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
                                        </ul>
                                    </div>
                                    '.$this->modal_reunion_show($q).$this->modal_reunion_editar($q).$this->modal_reunion_eliminar($q).'
                                </div>
                            </td>
                        </tr>';
                $i++;
            }
            	$tabla='<div style="margin-top: 25px; margin-bottom: 70px;">
                        <table id="tabla_reuniones_lista" class="table table-striped">
                            <thead>
                                <th> <label style="color:#ffffff"> # </label></th>
                                <th> <label style="color:#ffffff"> Motivo </label></th>
                                <th> <label style="color:#ffffff"> Fecha </label></th>
                                <th> <label style="color:#ffffff"> Hora </label></th>
                                <th> <label style="color:#ffffff"> Opciones </label></th>
                            </thead>
                            <tbody>  
                                '.$horas.'                                      
                            </tbody>
                        </table></div>';
        }
        else
        {
        	$tabla='No hay tablas registradas';
        }                                
   		   $code=' <table class="table table-striped" >
                        <tr>
                        	<td><p style="margin:5px 0 5px">Nueva Reunión</p></td>
                            <td><input id="new_reunion_motivo" class="form-control" type="text" placeholder="Motivo" value=""></td>
                        	<td><input id="new_reunion" class="form-control date-picker" type="text" placeholder="Fecha"  name="reunion_input" value=""></td>
                        	<td><input id="mask-time" size="4" type="text"  placeholder="Hora" class="form-control"></td>
                        	<td><button id="btn_new_reunion" class="btn btn-success" >Guardar</button></td>
                        </tr>
                </table>
                <div id="tabla_reuniones">
                    '.$tabla.'
                </div>';
        return $code;
   	}
    function modal_reunion_show($q)
    {
        $explode = explode('-',$q->Fecha);
        $dia = $explode[2];
        $mes = $explode[1];
        $año = $explode[0];
        $fecha = $año.'/'.$mes.'/'.$dia ;                    
        $fecha = date('d/m/Y',strtotime($fecha));
        $code='<div id="mostrar_reunion'.$q->Id.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">x</button>
                                <h4 class="modal-title" id="mostrar_reunion'.$q->Id.'"><i class="fa fa-eye"></i> Ver Reunión</h4>
                            </div>
                            <div class="modal-body">
                                <table width="100%">
                                    
                                    <tr style="background-color:withe">
                                        <td width="50%" style="background-color:white;">
                                            <h4>Fecha</h4>
                                            '.$fecha.'
                                        </td>
                                        <td width="50%" style="background-color:white;">
                                            <h4>Hora</h4>
                                            '.$q->Hora.'
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" width="100%"><h4>Motivo</h4>'.$q->Motivo.'</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>';
        return $code;
    }
    function modal_reunion_editar($q)
    {
        //$fecha=str_replace('-', '/', $q->Fecha);
        $explode = explode('-',$q->Fecha);
        $dia = $explode[2];
        $mes = $explode[1];
        $año = $explode[0];
        $fecha = $año.'/'.$mes.'/'.$dia ;                    
        $fecha = date('d/m/Y',strtotime($fecha));
        $code='<div id="editar_reunion'.$q->Id.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">x</button>
                                <h4 class="modal-title" id="myModalLabel3"><i class="fa fa-pencil-square-o"></i> Editar Reunión</h4>
                            </div>
                            <div class="modal-body">
                            <p style="padding-bottom:10px; border-bottom: 1px dotted #575757"><strong>
                                <i class="glyphicon glyphicon-trash"></i> Editar una Reunión:</strong></br>
                                    Para editar el registro seleccionado, haga click en el boton editar de la parte inferior de esta ventana. </p>

                            <table id="datatable-table" class="table table-striped" >
                                    <tr>
                                        <td style="background-color:white;"><input id="motivo_reunion_'.$q->Id.'" class="form-control" type="text" placeholder="Motivo" value="'.$q->Motivo.'"></td>
                                        <td style="background-color:white;"><input id="fecha_reunion_'.$q->Id.'" class="form-control date-picker" type="text" placeholder="Fecha"  name="reunion_input" value="'.$fecha.'"></td>
                                        <td style="background-color:white;"><input id="hora_reunion_'.$q->Id.'" size="4" type="text"  placeholder="Hora" class="form-control" value="'.$q->Hora.'"></td>
                                    </tr>
                            </table>
                            <p style="padding-bottom:2px; border-bottom: 1px dotted #575757">&nbsp;</p>                                                                
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger edit_re" id="'.$q->Id.'"><i class="fa fa-pencil-square-o"></i> Editar</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancelar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div>
                </div>';
        return $code;
    }
    function modal_reunion_eliminar($q)
    {
        $explode = explode('-',$q->Fecha);
        $dia = $explode[2];
        $mes = $explode[1];
        $año = $explode[0];
        $fecha = $año.'/'.$mes.'/'.$dia ;                    
        $fecha = date('d/m/Y',strtotime($fecha));
        $code='<div id="eliminar_reunion'.$q->Id.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">x</button>
                                                            <h4 class="modal-title" id="myModalLabel3"><i class="glyphicon glyphicon-trash"></i> Eliminar Reunión</h4>
                                                        </div>

                                                            <div class="modal-body">
                                                                <p style="padding-bottom:10px; border-bottom: 1px dotted #575757"><strong><i class="glyphicon glyphicon-trash"></i> Eliminar un Registro:</strong></br>
                                                                Para eliminar el registro seleccionado, haga click en el boton Eliminar de la parte inferior de esta ventana. 
                                                                Recuerde que eliminará de manera permanente los datos guardados.</p>

                                                                <table width="100%" >
                                    
                                                                    <tr style="background-color:withe">
                                                                        <td width="50%" style="background-color:white;">
                                                                            <h4>Fecha</h4>
                                                                            '.$fecha.'
                                                                        </td>
                                                                        <td width="50%" style="background-color:white;">
                                                                            <h4>Hora</h4>
                                                                            '.$q->Hora.'
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" width="100%"><h4>Motivo</h4>'.$q->Motivo.'</td>
                                                                    </tr>
                                                                </table>
                                                                <p style="padding-bottom:10px; border-bottom: 1px dotted #575757">&nbsp;</p>
                                                                <div class="alert alert-danger">
                                                                    <p><strong><i class="fa fa-warning"></i> ¡Cuidado!</strong></br>
                                                                        Está apunto de Eliminar de manera permanente información, la cual podría ser de importancia para el colegio, 
                                                                        asegúrese de verificar los datos antes eliminar un registro. Al ejecutar está acción está tomando la total responsabilidad del acto.  
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-danger del_re" id="'.$q->Id.'"><i class="glyphicon glyphicon-trash"></i> Eliminar</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancelar</button>
                                                            </div>
                                                    </div><!-- /.modal-content -->
                    </div>
                </div>';
        return $code;
    }

   	function Mi_Curso($curso,$asignaturas,$alumnos,$apoderado,$reunion,$notas,$set_nota)
   	{
   		if($curso['bool'])
   		{
   			$curso=$curso['query']->Grado.' '.$curso['query']->Letra;
	   		$code='	<div class="col-md-9">
		                <section class="widget widget-tabs">
		                    <header>
		                        <ul class="nav nav-tabs">
		                            <li class="active">
		                                <a href="#alumno" data-toggle="tab">Alumnos</a>
		                            </li>
		                            <li>
		                                <a href="#apoderado" data-toggle="tab">Apoderados</a>
		                            </li>
		                            <li>
		                                <a href="#reunion" data-toggle="tab">Reuniones</a>
		                            </li>
                                    <li>
                                        <a href="#notas" data-toggle="tab">Calificaciones</a>
                                    </li>
		                        </ul>
		                    </header>
		                    <div class="body tab-content">
				                <div id="alumno" class="tab-pane active clearfix">
				                    <div class="body">
				                        <h3><i class="eicon-graduation-cap"></i> Alumnos: °'.$curso.'</h3> 
				                        '.$this->Mis_Alumnos($alumnos).'
				                	</div>
				                </div>
				                <div id="apoderado" class="tab-pane">
				                    <div class="body">
				                        <h3><i class="eicon-users"></i> Apoderados: °'.$curso.'</h3> 
				                        '.$this->Mis_Apoderados($apoderado).'
				                    </div>
				                </div>
				                <div id="reunion" class="tab-pane">
				                    <div class="body">
				                        <h3><i class="eicon-book"></i> Resumen: °'.$curso.'</h3> 
				                        '.$this->Mis_Reuniones($reunion).'
				                    </div>
				                </div>
                                <div id="notas" class="tab-pane">
                                        '.$this->Mis_Notas($notas,$alumnos,$asignaturas,$set_nota).'
                                </div>
				            </div>
		                </section>
		            </div>
		    <div class="col-md-3">
                <section class="widget widget-tabs">
                    <header>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#reuniones" data-toggle="tab">Reuniones</a>
                            </li>
                            <li>
                                <a href="#asignaturas" data-toggle="tab">Asignatura</a>
                            </li>
                        </ul>
                    </header>
                    <div class="body tab-content">
                        <div id="reuniones" class="tab-pane active clearfix">
                            '.$this->Mis_Reuniones_lateral($reunion).'
                        </div>
                        <div id="asignaturas" class="tab-pane">
                       		'.$this->Mis_Asignaturas($asignaturas).'
                        </div>
                    </div>
                </section>
            </div>';
		    return $code;
		}
  	}
   	function Notas_alumno($n)
   	{
   		if($asignaturas['bool'])
    	{
    		$acordion='';
    		foreach ($asignaturas['query'] as $q) 
    		{
    			$count=1;
    			if($notas['bool'])
    			{
    				$fila='';
    				$suma=0;
    				$notas_total=0;
    				foreach ($set_notas['query'] as $s) 
    				{
    					if($s->Asignatura_Id===$q->Asignatura_Id)
		    			{
	    					if($s->Registrada)
	    					{	    						
	    						foreach ($notas['query'] as $n) 
			    				{  
			    					if($n->set_nota_Id==$s->set_nota_Id)
			    					{                          
			    						$pro_curso='-';
                                        if(isset($promedio['bool']))
                                        {
                                            $_promedio_curso=0;
                                            $cont_pro_curso=0;
                                            foreach ($promedio['query'] as $p) 
                                            {                                                
                                                if($n->set_nota_Id==$p->set_nota_Id)
                                                {
                                                    $_promedio_curso+=$p->Nota;
                                                    $cont_pro_curso++;
                                                }
                                            }
                                            if($cont_pro_curso!=0)
                                            {
                                                $_promedio_curso=$_promedio_curso/$cont_pro_curso;
                                            }

                                            if($_promedio_curso<4)
                                            {
                                                $pro_curso='<label style="color:red">'.$_promedio_curso.'</label>';
                                            }
                                            else
                                            {
                                                $pro_curso=$_promedio_curso;
                                            }
                                        }			    						
						    			if($n->Nota<4)
						    			{
						    				$Nota='<label style="color:red">'.$n->Nota.'</label>';
						    			}
						    			else
						    			{
						    				$Nota=$n->Nota;
						    			}
			    						$fila.='<tr><td>'.$count.'</td><td>'.$n->Tipo.'</td><td>'.$n->Fecha.'</td><td>'.$Nota.'</td><td>'.$pro_curso.'</td></tr>';
			    						$suma+=$n->Nota;
			    						$notas_total++;
			    						$count++;
			    					}
			    				}  
	    					}
	    					else
	    					{
	    						$fila.='<tr><td>'.$count.'</td><td>'.$s->Tipo.'</td><td>'.$s->Fecha.'</td><td> - </td><td> - </td></tr>';
	    						$count++;
	    					}
	    				}
    				}   
    				if($notas_total!=0)
    				{
    					$suma=$suma/$notas_total;
                        if($suma<4)
                        {
                            
                        }

    					$promedio_fin='Promedio: '.$suma;
    				} 			
    				else
    				{
    					$promedio_fin='No hay evaluaciones registradas';
    				}	
    				if($count===1)
		    		{
		    			$fila='<tr><td colspan="5" style="text-align:center"> No hay evaluaciones registradas</td></tr>';
		    		} 	     							
    			}
    			else
    			{
    				$fila='<tr><td colspan="5" style="text-align:center"> No hay registro de evaluaciones</td></tr>';
    				$promedio_fin='No hay evaluaciones registradas';
    			}

    			$tabla='<table class="table table-bordered" style="text-align:center;background-color: white;">
                                <thead>
                                <tr>
                                    <th style="text-align:center">N°</th>
                                    <th style="text-align:center">Evaluación</th>
                                    <th style="text-align:center">Fecha</th>
                                    <th style="text-align:center">Nota</th>
                                    <th style="text-align:center">Promedio Nota Curso</th>
                                </tr>
                                </thead>
                                <tbody>
                                '.$fila.'                                
                                </tbody>
                            </table>    ';
    			$acordion.='<div class="panel">
                                <div class="panel-heading">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#asg'.$q->Asignatura_Id.'">
                                        '.$q->Nombre.' <label style="float:right">'.$promedio_fin.'</label>
                                    </a>
                                </div>
                                <div id="asg'.$q->Asignatura_Id.'" class="panel-collapse collapse">
                                    <div class="panel-body">
                                       '.$tabla.'
                                    </div>
                                </div>
                            </div>';
    		}
    		$acordion;
    	}
    	else
    	{
    		$body='No hay asignaturas registradas';
    	}
   	}
   	function Mis_Reuniones_lateral($reuniones)
   	{
   		if($reuniones['bool'])
        {
            $i=1;
            $horas="";
            foreach ($reuniones['query'] as $q) 
            {
                $explode = explode('-',$q->Fecha);
        $dia = $explode[2];
        $mes = $explode[1];
        $año = $explode[0];
        $fecha = $año.'/'.$mes.'/'.$dia ;                    
        $fecha = date('d/m/Y',strtotime($fecha));
                $horas.='<tr><td>'.$i.'</td><td>'.$q->Motivo.'</td><td>'.$fecha.'</td><td>'.$q->Hora.'</td></tr>';
                $i++;
            }
            $tabla='<table style="margin-left: -6px" class="table table-striped">
                        <thead>
                            <tr>
                                <th><label style="color:#ffffff">#</th>
                                <th><label style="color:#ffffff">Motivo</label></th>
                                <th><label style="color:#ffffff">Fecha</label></th>
                                <th><label style="color:#ffffff">Hora</label></th>
                            </tr>
                        </thead>
                        <tbody>  
                            '.$horas.'                                      
                        </tbody>
                    </table>';
        }
        else
        {
        	$tabla='No hay registros';
        } 
        $code='    <h5 class="tab-header"><i class="fa fa-calendar"></i> Proximas Reuniones</h5>
                        <ul class="news-list news-list-no-hover">
                            '.$tabla.'                                
                        </ul>';    
        return $code;
   	}
    function Mis_Notas($notas,$alumnos,$asignaturas,$set_nota)
    {
        if($alumnos['bool'] && $asignaturas['bool'])
        {
            $body='';
            $first=true;
            $menu_as='';            
            $tab='';
            foreach ($asignaturas['query'] as $as) 
            {

                if($first){$first_class='class="active"';$first_tab='active clearfix';$first=false;}else{$first_class='';$first_tab=''; }
                $menu_as.=' <li '.$first_class.'>
                                <a href="#as'.$as->Id_Asi.'" data-toggle="tab">'.$as->Nombre.'</a>
                            </li>';
                
                if($as->Cantidad_Notas>0)
                {
                    $menu_sn='';
                    foreach ($set_nota['query'] as $sn) 
                    { 
                     
                        if($as->Id_Cha == $sn->Id_Cha)
                        {
                              $menu_sn.='<td>'.$sn->Tipo.'</td>';
                        }                        
                    }
                    $fila='';
                    foreach ($alumnos['query'] as $al) 
                    {                        
                        $fila_notas='';
                        $ing=0;
                        foreach ($notas['query'] as $no) 
                        {
                            if($as->Id_Cha === $no->Id_Cha && $al->Id_Alumno === $no->Alumno_Id)
                            {
                                if($no->Nota<4)
                                {
                                    $fila_notas.='<td><span class="badge badge-danger">'.$no->Nota.'</span></td>';
                                }
                                else
                                {
                                    $fila_notas.='<td>'.$no->Nota.'</td>';
                                }
                                
                                $ing++;
                            }                            
                        }
                        $ing=$as->Cantidad_Notas-$ing;
                        $td='';
                        for($i=0;$i<$ing;$i++)
                        {
                            $td.='<td></td>';
                        }

                        $fila.='<tr><td>'.set_name($al->first_name,$al->last_name).'</td>'.$fila_notas.$td.'</tr>';
                        $fila_notas='';
                    }
                    $tabla='<table id="lista_'.$as->Nombre.'" class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <td style="width:200px;">Alumnos</td>
                                        '.$menu_sn.'
                                    </tr>
                                </thead>
                                <tbody>
                                '.$fila.'
                                </tbody>           
                            </table>';
                }
                else
                {
                    $tabla='<h3> No hay Evaluaciones registradas </h3>';
                }
                $tab.=' <div id="as'.$as->Id_Asi.'"" class="tab-pane '.$first_tab.'">
                            <div class="body">
                                '.$tabla.'                    
                            </div>
                        </div>';
            }
            $body=' <section class="widget widget-tabs">
                        <header>
                            <ul class="nav nav-tabs">
                            '.$menu_as.'
                            </ul>
                        </header>
                        <div class="body tab-content">
                            '.$tab.'
                        </div>
                    </section>';
            
            return $body;
        }
        else
        {
            $body='    <h5 class="tab-header"><i class="fa fa-group"></i> Calificaciones</h5>
                        <ul class="news-list news-list-no-hover">
                            No se han encontrado registros                            
                        </ul>';
            return $body;
        }
    }
   	function Mis_Asignaturas($asignatura)
   	{
   		if($asignatura['bool'])
        {
            $lista_=NULL;
            foreach ($asignatura['query'] as $q) 
            {
                $lista_.='  <ul class="news-list">
                                <li>                                                   
                                    <div class="news-item-info" style="margin-left: 10px">
	                                    <div class="name"><b>'.set_name($q->first_name,$q->last_name).'</b></div>
	                                    <div class="position">'.$q->email.'</div>     
	                                    <div class="position">'.$q->Nombre.'</div>                                                    
                                    </div>
                                </li>
                            </ul>';
            }
            
        }
        else
        {
        	$lista_='No hay registros';
        }
        $lista='    <h5 class="tab-header"><i class="fa fa-group"></i> Lista de profesores</h5>
                        <ul class="news-list news-list-no-hover">
                            '.$lista_.'                                
                        </ul>';
        return $lista;

   	}

}