<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
/** 
 * Dashboard | Carga dinamica
 * 
 */
class Academia
{ 
	private $CI; 
	public function __construct()  
    { 
        $this->CI =& get_instance(); 
        $this->CI->load->helper('url');
        $this->user_log=$this->CI->ion_auth->user()->row();   
    }  
    private function set_name($nombre,$apellido)
    {
        $nombre=preg_split("/[\s,]+/",$nombre);
        $apellido=preg_split("/[\s,]+/",$apellido);
       	return $nombre[0].' '.$apellido[0]; 
    }
    public function content_tab($notas,$asignaturas,$set_notas,$promedio,$hojaVida,$horario,$bloques,$activo)
    {
    	$_acn=false;
    	$_achv=false;
    	$_ach=false;
    	switch ($activo) 
    	{
    		case 'notas':
    			$_acn=true;
    			break;
    		case 'hojavida':
    			$_achv=true;
    			break;
    		case 'horario':
    			$_ach=true;
    			break;
    		default:
    			$_acn=true;
    			break;
    	}
    	$code='<div class="body tab-content">
                  '.$this->notas($notas,$asignaturas,$promedio,$set_notas,$_acn).
                   	$this->hojaVida($hojaVida,$_achv).
                   	$this->horario($horario,$bloques,$_ach).'
                </div>';
        $tabs='	<div class="col-md-8">
                	<section class="widget widget-tabs">'.
                		$this->header($activo).$code.'
                	</section>
            	</div>';
        return $tabs;
    }
    public function header($select=NULL,$type=NULL)
    {
    	$active='class="active"';
    	$_achdv='';
    	$_acNotas='';
    	$_acHorario='';
    	switch ($select) 
    	{
    		case 'notas':
    			$_acNotas=$active;
    			break;
    		case 'hojavida':
    			$_achdv=$active;
    			break;
    		case 'horario':
    			$_acHorario=$active;
    			break;
    		default:
    			$_acNotas=$active;
    			break;
    	}    	
    	$code='<header>
                    <ul class="nav nav-tabs">
                    	<li '.$_acNotas.'>
                            <a href="#tab_notas" data-toggle="tab">Evaluaciones</a>
                        </li>
                        <li '.$_achdv.'>
                            <a href="#tab_hdv" data-toggle="tab">Hoja de Vida</a>
                        </li>
                        <li '.$_acHorario.'>
                            <a href="#tab_horario" data-toggle="tab">Horario</a>
                        </li>
                    </ul>
                </header>';
        return $code;
    }
    private function tab($body,$id_tab,$activo)
    {
    	$active='';
    	if(is_bool($activo)){if($activo){$active='active clearfix';}}
    	if(is_null($id_tab)){$id_tab='sin_id';}
    	$code='<div id="'.$id_tab.'" class="tab-pane '.$active.'">
               '.(string)$body.'     
               </div>';
        return $code;
    }
    public function notas($notas,$asignaturas,$promedio,$set_notas,$active=null)
    {
    	if($asignaturas['bool']) 
    	{
    		$acordion='';
    		foreach ($asignaturas['query'] as $q) 
    		{
    			$count=1;
    			if($notas['bool'])
    			{
    				$fila='';
    				$suma=0;
    				$notas_total=0;
    				foreach ($set_notas['query'] as $s) 
    				{
    					if($s->Asignatura_Id===$q->Asignatura_Id)
		    			{
	    					if($s->Registrada)
	    					{	    						
	    						foreach ($notas['query'] as $n) 
			    				{  
			    					if($n->set_nota_Id==$s->set_nota_Id)
			    					{                          
			    						$pro_curso='-';
                                        if(isset($promedio['bool']))
                                        {
                                            $_promedio_curso=0;
                                            $cont_pro_curso=0;
                                            foreach ($promedio['query'] as $p) 
                                            {                                                
                                                if($n->set_nota_Id==$p->set_nota_Id)
                                                {
                                                    $_promedio_curso+=$p->Nota;
                                                    $_promedio_curso=number_format($_promedio_curso,1);
                                                    $cont_pro_curso++;
                                                }
                                            }
                                            if($cont_pro_curso!=0)
                                            {
                                                $_promedio_curso=$_promedio_curso/$cont_pro_curso;
                                                $_promedio_curso=number_format($_promedio_curso,1);
                                            }

                                            if($_promedio_curso<4)
                                            {
                                                $pro_curso='<label style="color:red">'.$_promedio_curso.'</label>';
                                            }
                                            else
                                            {
                                                $pro_curso=$_promedio_curso;
                                            }
                                        }			    						
						    			if($n->Nota<4)
						    			{
						    				$Nota='<label style="color:red">'.$n->Nota.'</label>';
						    			}
						    			else
						    			{
						    				$Nota=$n->Nota;
						    			}
			    						$fila.='<tr><td>'.$count.'</td><td>'.$n->Tipo.'</td><td>'.$n->Fecha.'</td><td>'.$Nota.'</td><td>'.$pro_curso.'</td></tr>';
			    						$suma+=$n->Nota;
			    						$notas_total++;    
			    						$count++;
			    					}
			    				}  
	    					}
	    					else
	    					{
	    						$fila.='<tr><td>'.$count.'</td><td>'.$s->Tipo.'</td><td>'.$s->Fecha.'</td><td> - </td><td> - </td></tr>';
	    						$count++;
	    					}
	    				}
    				}   
    				if($notas_total!=0)
    				{
    					$suma=$suma/$notas_total;
                        $suma=number_format($suma,1);
                        if($suma<4)
                        {
                            $promedio_fin='Promedio: <label style="color:red">'.$suma.'</label>';
                        }
                        else
                        {
                            $promedio_fin='Promedio: '.$suma;
                        }

    					
    				} 			
    				else
    				{
    					$promedio_fin='No hay evaluaciones registradas';
    				}	
    				if($count===1)
		    		{
		    			$fila='<tr><td colspan="5" style="text-align:center"> No hay evaluaciones registradas</td></tr>';
		    		} 	     							
    			}
    			else
    			{
    				$fila='<tr><td colspan="5" style="text-align:center"> No hay registro de evaluaciones</td></tr>';
    				$promedio_fin='No hay evaluaciones registradas';
    			}

    			$tabla='<table class="table table-bordered" style="text-align:center;background-color: white;">
                                <thead>
                                <tr>
                                    <th style="text-align:center">N°</th>
                                    <th style="text-align:center">Evaluación</th>
                                    <th style="text-align:center">Fecha</th>
                                    <th style="text-align:center">Nota</th>
                                    <th style="text-align:center">Promedio Nota Curso</th>
                                </tr>
                                </thead>
                                <tbody>
                                '.$fila.'                                
                                </tbody>
                            </table>    ';
    			$acordion.='<div class="panel">
                                <div class="panel-heading">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#asg'.$q->Asignatura_Id.'">
                                        '.$q->Nombre.' <label style="float:right">'.$promedio_fin.'</label>
                                    </a>
                                </div>
                                <div id="asg'.$q->Asignatura_Id.'" class="panel-collapse collapse">
                                    <div class="panel-body">
                                       '.$tabla.'
                                    </div>
                                </div>
                            </div>';
    		}
    		$body='<header>
                       	<h4>
                            <i class="fa fa-check-square-o"></i>
                            evaluaciones
                        </h4>
                    </header>'.$acordion;
    	}
    	else
    	{
    		$body='No hay asignaturas registradas';
    	}
        return $this->tab($body,'tab_notas',$active);
    }
    public function hojaVida($hojaVida,$active=null)
    {
    	if($hojaVida['bool'])
    	{
    		$fila='';
    		$count=1;
    		foreach ($hojaVida['query'] as $q) 
    		{
    			if($q->Tipo)
    			{
    				$Tipo='<span class="label label-success">Positiva</span>';
    			}
    			else
    			{
    				$Tipo='<span class="label label-danger">Negativa</span>';
    			}
    			$Texto=$q->Descripcion;
    			$Fecha=$q->Date;
    			$fila.='<tr>
                        	<td>'.$count.'</td>
                        	<td>'.$Tipo.'</td>
                            <td>'.$q->Descripcion.'</td>
                            <th>'.$this->set_name($q->NombreProfesor,$q->ApellidoProfesor).'</td>                            
                            <td>'.$q->Date.'</td>
                        </tr>';
                $count++;
    		}
    	}
    	else
    	{
    		$fila='<tr><td colspan="5" style="text-align:center"> No se encontraron anotaciones</td></tr>';
    	}
    	$body='
                        <header>
                            <h4>
                                <i class="fa fa-list-alt"></i>
                                Hoja de Vida
                            </h4>
                        </header>
                        <div class="body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Profesor</th>
                                    <th>Fecha</th>
                                </tr>
                                </thead>
                                <tbody>
                                '.$fila.'                                
                                </tbody>
                            </table>                            
                        </div>';
        return $this->tab($body,'tab_hdv',$active);
    }
    public function horario($horario,$bloques,$active=null)
    {
        
        $_day=array('Lunes','Martes','Miercoles','Jueves','Viernes');
        $_horario='';
       
        $semana = array();
        $_i=0;
        $_t='';
        if($horario['bool'])
        { 
            $len=count($horario['query']);
            $_len=count($bloques['query']);
            foreach ($bloques['query'] as $b) 
            {           
                $_i++;
                $_fila='';
                for($j=0;$j<=4;$j++)
                {   $i=0;                 
                    foreach ($horario['query'] as $h) 
                    {
                        $i++;
                        if($h->Hora_Inicio==$b->Hora_Inicio && $h->Hora_Fin==$b->Hora_Fin)
                        {   
                            if($h->Dia===$_day[$j])
                            {
                                $_fila.='<td>'.$h->Nombre.'</td>';
                                break;
                            } 
                        }                       
                    }
                    if($i===$len && !($_i == $_len && $j ==4))
                    {

                        $_fila.='<td> - </td>'; 
                         //&nbsp;   
                    } 
                }
                   
                $_horario.='<tr><td>'.$b->Hora_Inicio.' - '.$b->Hora_Fin.'</td>'.$_fila.'</tr>';        
            }            
         $_horario='<table class="table table-bordered" style="text-align:center;">
                        <thead>
                            <tr>
                                <th style="text-align:center">Hora</th>
                                <th style="text-align:center">Lunes</th>
                                <th style="text-align:center">Martes</th>
                                <th style="text-align:center">Miercoles</th>
                                <th style="text-align:center">Jueves</th>
                                <th style="text-align:center">Viernes</th>
                            </tr>
                        </thead>
                        <tbody>
                          '.$_horario.'                                
                        </tbody>
                    </table>    ';
        }
        else
        {
            $_horario='<h1>No hay horario registrado</h1>';
        }
    	$body=' <header>
                    <h4>
                        <i class="fa fa-calendar"></i>
                        Horario de Clases
                    </h4>
                </header>
                    '.$_horario;

        return $this->tab($body,'tab_horario',$active);
    }
    public function docentes($docentes,$jefe,$active=null)
    {
        if($active) $active=' active clearfix';
        else $active='';

        if($docentes['bool'])
        {   
            $div='';
            foreach ($docentes['query'] as $q) 
            {
                $div.=' <li>
                            <div class="news-item-info">
                                <div class="name"><a>'.$this->set_name($q->first_name,$q->last_name).'</a></div>
                                <div class="position">'.$q->materia.'</div>
                                <div class="time"><a>'.$q->email.'</a></div>
                            </div>
                        </li>';
            }
            $body='<h5 class="tab-header"><i class="fa fa-group"></i> Profesores</h5>
                            <ul class="news-list">
                            '.$div.'
                            </ul>';
        }
        else
        {
            $body='<h5 class="tab-header"><i class="fa fa-group"></i> Profesores</h5>
                            <ul class="news-list">
                            <li>
                            <div class="news-item-info">
                                <div class="name">No se encontraron profesores</div>
                            </div>
                        </li>';
        }
        if($jefe['bool'])
        {
             $_body='<h5 class="tab-header"><i class="fa fa-group"></i> Profesor Jefe</h5>
                            <ul class="news-list"> <li>
                            <div class="news-item-info">
                                <div class="name">'.$this->set_name($jefe['query']->first_name,$jefe['query']->last_name).'</div>
                                <div class="time"><a>'.$jefe['query']->email.'</a></div>
                            </div>
                        </li> </ul>';
        }
        else
        {
            $_body='<h5 class="tab-header"><i class="fa fa-group"></i> Profesor Jefe </h5>
                            <ul class="news-list">
                            <li>
                            <div class="news-item-info">
                                <div class="name">No se encontraron profesores</div>
                            </div>
                        </li>';
        }

        $code=' <div id="tab_docentes" class="tab-pane'.$active.'">
                    '.$_body.$body.'
                </div>';
        return $code;
    }
    public function content_pr($docentes,$jefe,$reuniones,$active=null)
    {
        $active_1=false;
        $active_2=false;
        switch ($active) 
        {
            case 'profesores':
                $active_1=true;
                break;
            case 'reuniones':
                $active_2=true;
                break;
            default:
                $active_1=true;
                break;
        }
        $code='<div class="col-md-4">
                <section class="widget widget-tabs">
                    '.$this->header_2($active).'
                    <div class="body tab-content">
                        '.$this->docentes($docentes,$jefe,$active_1).
                        $this->reuniones($reuniones,$active_2).'
                    </div>
                </section>
            </div>';
        return $code;
    }
    public function reuniones($reuniones,$active=null)
    {
        if($reuniones['bool'])
        {
            $div='';
            foreach ($reuniones['query'] as $q) 
            {
                $div.=' <li>
                            <div class="news-item-info">
                                <div class="name">'.$q->Motivo.'</div>
                                <div class="position">'.$q->Fecha.'</div>
                            </div>
                        </li>';
            }

            
        }
        else
        {
            $div='No hay reuniones registradas';
        }
        $body='<h5 class="tab-header"><i class="fa fa-group"></i>Reuniones Registradas</h5>
                        <ul class="news-list">
                            '.$div.'
                        </ul>';
        $body=' <div id="tab_reuniones" class="tab-pane'.$active.'">
                    '.$body.'
                </div>';
        return $body;
    }
    public function header_2($select=NULL)
    {
        $active='class="active"';
        $_achdv='';
        $_acNotas='';
        $_acHorario='';
        switch ($select) 
        {
            case 'profesores':
                $_acNotas=$active;
                break;
            case 'reuniones':
                $_achdv=$active;
                break;
            default:
                $_acNotas=$active;
                break;
        }       
        $code='<header>
                    <ul class="nav nav-tabs">
                        <li '.$_acNotas.'>
                            <a href="#tab_docentes" data-toggle="tab">Profesores</a>
                        </li>
                        <li '.$_achdv.'>
                            <a href="#tab_reuniones" data-toggle="tab">Reuniones</a>
                        </li>
                    </ul>
                </header>';
        return $code;
    }
    public function container($tab_1,$tab_2)
    {
        $content='  <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Academia <small>Información Estudiante</small></h2>
                        </div>
                    </div>
                    <div class="row">
                        '.$tab_1.$tab_2.'
                    </div>';
        return $content;
    }
}