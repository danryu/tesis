<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
  class Security_lib 
  { 
    private $min_password=6;
  	public function __construct()  
    { 
    	$this->CI =& get_instance(); 
    } 
    public function generator()
    {
    	$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		  $pass = "";
			for($i=0;$i<$this->min_password;$i++) 
			{
				$pass .= substr($str,rand(0,62),1);
			}
    	return $pass;
    }
    public function cryp_pass($pass,$digito=7)
    {

      if(trim(strlen($pass))>=$this->min_password)
      {
        $set_salt = './1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $salt = sprintf('$2a$%02d$', $digito);
        for($i = 0; $i < 22; $i++)
        {
          $salt .= $set_salt[mt_rand(0, 22)];
        }
        $final_pass =crypt($pass, $salt);
        return $final_pass;
      }
    }
    public function verify_pass($pass,$passBD)
    {
        if( crypt($pass, $passBD) == $passBD) 
        {
          return true;
        }
        else
        {
          return false;
        }
    }
    public function issetUser($Usuario)
    {        
        if( $Usuario <=0 || $Usuario == NULL)
        {
            redirect('auth/login');
        }
    }
    //CAMBIAR ENLACES
    public function selectiRol($rol_id)
    {     
      

          switch ($rol_id) 
          {
            case 0:
              redirect('administracion'); //ejemplo //ADMINISTRACION
              break;
            case 1:
              redirect('administracion'); //ADMINISTRADOR
               break;
            case 2:
              redirect('apoderado');//PROFESOR
              break;
            case 3:
              redirect('alumno');//ALUMNO
              break;      
            case 4:
              redirect('profesor');//APODERADO
              break;
            case 5:
              redirect('administracion/apoderado');//PROFESOR JEFE
              break;
          }
        
    }
    public function valid_rut($run)
    {
      
    }


  }


?>
