<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
  class Alert 
  { 
  	public function __construct()  
    { 
    	$this->CI =& get_instance(); 
    }
    public function show($data)
    {
      $icon='<i class="fa fa-check"></i>';
      $title='';
      $mensaje='';
      $tipo='success';
      if(isset($data['type']))
      {
        switch ($data['type']) 
        {
          case 'primary':
            $tipo=$data['type'];
            $title='';
            $icon='';
            break;
          case 'success':
            $tipo=$data['type'];
            $title='Felicidades!';
            $icon='<i class="fa fa-check"></i>';
            break;
          case 'info':
            $tipo=$data['type'];
            $title='Atención!';
            $icon='fa fa-info-circle';
            break;
          case 'warning':
            $tipo=$data['type'];
            $title='Advertencia!';
            $icon='<i class="fa fa-bell-o"></i>';
            break;
          case 'danger':
            $tipo=$data['type'];
            $title='Ups! Encontramos un error!';
            $icon='<i class="fa fa-ban"></i>';
            break;
          default:
            $tipo='success';
            $title='Felicidades!';
            $icon='<i class="fa fa-check"></i>';
            break;
        }
      }
      if(isset($data['title'])){$title=$data['title'];}
      if(isset($data['icon'])){$icon='<i class="'.$data['icon'].'"></i>';}
      if(isset($data['text'])){$mensaje=$data['text'];}
      $alerta='<div class="alert alert-'.$tipo.'">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>'.$icon.' '.$title.'</strong> '.$mensaje.'
              </div>';
      return $alerta;
    }
  }