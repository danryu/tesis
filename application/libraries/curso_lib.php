<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
/** 
 * Layouts Class. PHP5 only. 
 * 
 */
class Curso_lib 
{ 

	public function ver_anotacion($var)
	{
		$nombre_alumno=NULL;
		$class=NULL;
		$tipo=NULL;
		$text=NULL;
		// TRUE = POSITIVA ** FALSE = NEGATIVA
		if(isset($var["tipo"])){if(is_bool($var["tipo"])){$tipo=$var["tipo"];}}	
		if(isset($var["description"])){if(!is_null($var["description"])){$description=$var["description"];}}
		if(isset($var["nombre_alumno"])){if(!is_null($var["nombre_alumno"])){$nombre_alumno=$var["nombre_alumno"];}}
		//PROXIMAMENTE CONFIGURADO EN BD
		if(isset($var["nombre_profesor"])){if(!is_null($var["nombre_profesor"])){$nombre_alumno=$var["nombre_profesor"];}}
		if($tipo && $tipo!=null)
		{
			$class="btn-primary";
			$text="Positiva";
		}
		else
		{
			$class="";
			$text="Negativa";
		}
		$base='	<legend class="section"></legend>
				<div class="controls form-group">
                   	<textarea id="description" rows="3" name="text" class="form-control" disable="disable"></textarea>
                    <span class="help-block">Las anotaciones no se pueden borrar o editar</span>
                </div>
                <div class="controls form-group">
                    <div class="btn-group" data-toggle="buttons">                                        
                        <label class="btn '.$class.' btn-inverse btn-sm" data-toggle-class="btn-primary" data-toggle-passive-class="btn-inverse">
                            <input type="checkbox"> '.$text.'
                        </label>
                    </div>
               	</div>';
		return $base;
		
	}
}
?>