<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
/** 
 * Layouts Class. PHP5 only. 
 * 
 */
class Layout { 

      private $CI;  
      private $title_for_layout ="Apple's Teacher"; 
      private $title_separator = ' | '; 
      private $head_for_layout = NULL;
      private $_rol=NULL;
      private $_id_page=NULL;
      private $extra_js=NULL;
      private $count_panel=0;
      private $user_log=NULL;
      private $url_browser=NULL;
      private $template='bootstrap';
      //private $Id_UserLogin=NULL;
      //private $set_menu_curso=NULL;
      private $_nombre=NULL;
      public function __construct()  
      { 
        $this->CI =& get_instance(); 
        $this->CI->load->helper('url');
        $this->CI->load->library('ion_auth');  
        $this->user_log=$this->CI->ion_auth->user()->row();   
      }   
      public function set_template($template) 
      { 
        $this->template = $template ; 
      }       
      public function set_title($title) 
      { 
        $this->title_for_layout = $title.$this->title_separator.$this->title_for_layout ; 
      } 
      private function set_menu_curso()
      {         
        $this->CI->load->model('profesor_model');
        $this->CI->load->library('ion_auth');
        $code=NULL;
        $url=base_url();
        $id=$this->CI->ion_auth->get_profesor_id();
        $cursos=$this->CI->profesor_model->load_cursos($id);
        if($cursos['bool'])
        {
          foreach ($cursos['query'] as $q) 
          {
            $code.='<li><a class="hand" onClick="location.href=\''.$url.'profesor/curso/'.$q->Id.'\'">'.$q->Grado.'°'.$q->Letra.'</a></li>';
          }
          return $code;
        }
        else
        {
          return '<li><a href="#">No hay cursos</a></li>';
        }
        //$this->set_menu_curso = ; 
      } 
       private function set_menu_profe_jefe()
      {         
        $this->CI->load->model('profesor_model');
        $this->CI->load->library('ion_auth');
        $code=NULL;
        $url=base_url();
        $Id_User=$this->CI->ion_auth->get_user_id(); 
        $cursos=$this->CI->profesor_model->load_cursos_jefe($Id_User);
        if($cursos['bool'])
        {
          $cursos=$cursos['query'];
          //var_dump($cursos);
          foreach ($cursos as $q) 
          {
              $code.='<li><a class="hand" onClick="location.href=\''.$url.'profesor/mi_curso/'.$q->Id.'\'">'.$q->Grado.'°'.$q->Letra.'</a></li>';
              
          }
          return $code;
        }
        else
        {
          return '<li><a href="#">No hay cursos</a></li>';
        }
        //$this->set_menu_curso = ; 
      } 
      public function set_head($link)
      {
        $this->head_for_layout=$link;
      }
      public function set_specific_js($_extra,$theme=NULL)
      {
          $this->CI->load->helper('url');
          $base_url=base_url();
          $n=NULL;
          if(is_null($theme))
          {
            $theme=$this->template;
          }
          if($_extra!==NULL)
          {
            foreach ($_extra as $url_extra ) 
            {
                $n=$n.'<script src="'.$base_url.'assets/'.$theme.'/'.$url_extra.'"></script>';               
            }
            $this->extra_js=$n;
          }
      }
      private function get_url($n=NULL)
      {
        $ruta = uri_string().'';
        $sp = explode('/', $ruta);
        if($n!=null)
        {
          $lower = strtolower($sp[$n]);
          return $lower;
        }
        else
        {
          $lower = strtolower($sp[0]);
          $this->url_browser=$lower;
        }        
      }

      //************ SELECCIONAR PAGINA ***********
        public function set_page($id_page)
        {
            if(is_numeric($id_page) && $id_page>=0)
            {
                $this->_id_page=$id_page;
            }
        }
        private function set_name()
        {
          $user=$this->user_log;                                      
          $nombre=$user->first_name;
          $nombre=preg_split("/[\s,]+/",$nombre);
          $apellido=$user->last_name;
          $apellido=preg_split("/[\s,]+/",$apellido);
          $this->_nombre=$nombre[0].' '.$apellido[0]; 
        }
        public function set_page_rol($id_rol)
        {
            if(!is_numeric($id_rol) && !is_null($id_rol))
            {
                $this->_rol=$id_rol;
            }
            else
            {
              $this->_rol="default";
            }
        }
      //************ SELECCIONAR PAGINA FIN ***********
  public function view($view_name, $params = array(), $layout = 'layout_main') 
  {   
    $_headDefault="";
    $head=NULL;
    if ($this->title_for_layout !== NULL){ 
      $separated_title_for_layout =  $this->title_for_layout; 
    } 
    if ($this->head_for_layout !== NULL){        
        $head=$this->head_for_layout;
    }
    $this->set_name();
    $this->get_url();
    //************ SELECCIONAR PAGINA *********** FALTA TERMINAR
      if($this->_rol !== NULL && $this->_id_page !== NULL )
      {
        switch($this->_rol)
        {
          case 'root':
                  $menu=$this->set_menu_root($this->_id_page);
                  break;
          case 'apoderado':
                  $menu=$this->set_menu_apoderado($this->_id_page);
                  break;
          case 'profesor':
                  $menu=$this->set_menu_profesor($this->_id_page);
                  break;
          case 'alumno':
                  $menu=$this->set_menu_alumno($this->_id_page);
                  break;
          default:
                  $menu=$this->set_menu_demo();
                  break;
        }
      }
      else
      {
          $menu=$this->set_menu_demo(); //
      }
    //************ SELECCIONAR PAGINA FIN ***********
    $view_content = $this->CI->load->view($view_name, $params, TRUE); 
    $_headDefault=$this->headDefault().$_headDefault;
    $header=$this->set_header();
    $finalcode=$this->finalCode();
    $this->CI->load->view('layout/' . $layout, array( 
      'content_for_layout' => $view_content, 
      'title_for_layout' => $separated_title_for_layout,
      'menu_for_layout'=>$menu,
      'headdefault_for_layout'=>$_headDefault,
      'header_for_layout'=>$header,
      'head_for_layout' => $head,
      'finalCode_for_layout' => $finalcode)); 
  }      
  public function add_include($path, $prepend_base_url = TRUE) 
  {
        if ($prepend_base_url) 
        { 
          $this->CI->load->helper('url'); // Load this just to be sure 
          $this->file_includes[] = base_url() . $path; 
        } 
        else
        { 
          $this->file_includes[] = $path; 
        } 
      
        return $this; // This allows chain-methods 
  }   
  public function print_includes() 
  {
            // Initialize a string that will hold all includes 
            $final_includes = ''; 
          
            foreach ($this->includes as $include) 
            { 
              // Check if it's a JS or a CSS file 
              if (preg_match('/js$/', $include)) 
              { 
                // It's a JS file 
                $final_includes .= '<script type="text/javascript" src="' . $include . '"></script>'; 
              } 
              elseif (preg_match('/css$/', $include)) 
              { 
                // It's a CSS file 
                $final_includes .= '<link href="' . $include . '" rel="stylesheet" type="text/css" />'; 
              } 
          
              return $final_includes; 
            } 
  } 
  public function headDefault()
  {       

        $this->CI->load->helper('url');
        $url=base_url().'assets/'.$this->template.'/';
        $urlCI='assets/'.$this->template.'/';        
        $urlJ=base_url().$urlCI; //<script src="'.$urlJ.'lib/alert/sweet-alert.js"></script>
        $list='
        <script src="'.$urlJ.'lib/jquery/jquery-2.0.3.min.js"></script>
        <script src="'.$urlJ.'lib/alert/sweet-alert.min.js"></script>
        <link href="'.$url.'css/application.min.css" rel="stylesheet">
        <link href="'.$url.'css/loaders.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="'.$url.'lib/alert/sweet-alert.css"">
        <link href="'.$url.'lib/styles/metro/notify-metro.css" rel="stylesheet">
        <link rel="shortcut icon" href="'.$url.'img/favicon.png" type="image/png>
        <link rel="stylesheet" href="maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta charset="utf-8">';
        return $list;
  }
  //************ SELECCIONAR PAGINA  ***********
        public function set_menu_demo()
        {
              $this->CI->load->helper('url');
              $url=base_url();
              $list=null;
              $ruta = uri_string().'';
              $sp = explode('/', $ruta);
              $lower = strtolower($sp[0]);
              if($lower == "administracion")
              {
                  $list='<ul class="side-nav">'.
                  $this->single_menu('Home','fa fa-home','administracion').
                  $this->single_menu('Alumnos','fa fa-user','administracion/alumno').
                  $this->single_menu('Profesor','fa fa-user','administracion/profesor').
                  $this->single_menu('Apoderados','fa fa-user','administracion/apoderado').
                  $this->single_menu('Asignaturas','fa fa-book','administracion/asignatura').
                  $this->single_menu('Cursos','fa fa-graduation-cap','administracion/curso').
                  $this->single_menu('Bloques','fa fa-square-o','administracion/bloque').
                  //$this->single_menu('Home','fa fa-home','administracion').
                  $this->panel_menu(array('name'=>'Asignar','icon'=>'fa fa-plus-square',
                                      array('url'=>'administracion/profesor_asignatura','label'=>'Profesor / Asignatura'),
                                      array('url'=>'administracion/curso_asignatura','label'=>'Asignatura / Cursos'),
                                      array('url'=>'administracion/curso_alumno','label'=>'Curso / Alumno'),
                                      array('url'=>'administracion/horario','label'=>'Horario'))).'</ul>';
              }
              if($lower == "alumno")
              {
                  $list='<ul id="side-nav" class="side-nav">'.
                          $this->single_menu('Home','fa fa-home','alumno').
                          $this->single_menu('Academia','fa fa-folder-open','alumno/academia').
                          $this->single_menu('Mi Curso','fa fa-folder-open','alumno/mis_cursos').
                          $this->single_menu('Inbox','fa fa-inbox','alumno/inbox').
                          $this->single_menu('Perfil','fa fa-user','alumno/perfil').'</ul>';
              }
              if($lower == "profesor")
              {
                $menu_curso=$this->set_menu_curso();
                $menu_curso_jefe=$this->set_menu_profe_jefe();
                $list=' <ul id="side-nav" class="side-nav">'.$this->single_menu('Home','fa fa-home','profesor').'
                          <li class="panel">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#side-nav" href="#special-collapse"><i class="fa fa-book"></i> <span class="name">Cursos</span></a>
                              <ul id="special-collapse" class="panel-collapse collapse">
                                '.$menu_curso.'
                              </ul>
                          </li>
                          <li class="panel">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#side-nav" href="#mis_cursos"><i class="fa fa-list-alt"></i><span class="name">Mis Cursos</span></a>
                              <ul id="mis_cursos" class="panel-collapse collapse">
                                '.$menu_curso_jefe.'
                              </ul>
                          </li>'.
                          $this->single_menu('Inbox','fa fa-inbox','profesor/inbox').
                          $this->single_menu('Perfil','fa fa-user','profesor/perfil').'
                        </ul>';
              }
              if($lower== "apoderado")
              {
                $list='<ul id="side-nav" class="side-nav">'.
                  $this->single_menu('Home','fa fa-home','apoderado').
                  $this->single_menu('Academia','fa fa-folder-open','apoderado/academia').
                  $this->single_menu('Inbox','fa fa-inbox','apoderado/inbox').
                  $this->single_menu('Perfil','fa fa-user','apoderado/perfil').'</ul>';
              }
              return $list;
        }
      //menu
      private function panel_menu($panel)
      {
          //************ DEFAULT ****************
            $href_panel="nn".$this->count_panel."-collapse";
            $this->count_panel++;
            $label="Sin Nombre";
            $icon_panel="fa fa-exclamation-triangle";
            $url=base_url();
            $url_line=$url."ups";
            $label_line="Sin Nombre";
            $list_panel="";
          //********** FIN DEFAULT **************
          if(is_array($panel))
          {
            //********** FIRST *************
            if(isset($panel['name']))
            {              
                $label=$panel['name'];
                $l_min=strtolower($label);
                if(strlen($l_min)>2){$href_panel=$l_min[0].$l_min[1].$l_min[2]."-collapse";}   
            }
            if(isset($panel['icon']))
            {
              $icon_panel=$panel['icon'];
            }  
            $first='<a class="accordion-toggle collapsed hand" data-toggle="collapse" data-parent="#side-nav" href="#'.$href_panel.'">
                      <i class="'.$icon_panel.'"></i> <span class="name">'.$label.'</span>
                    </a>';
            //********** LIST *************
            foreach ($panel as $key => $value) 
            {
              if($key!=="name" && $key!=="icon")
              {                
                if(isset($value['url']))
                {
                  $url_line=$url.$value['url'];  
                }
                if(isset($value['label']))
                {
                  $label_line=$value['label'];
                }                
                $line='<li><a class="hand" onClick="location.href=\''.$url_line.'\'">'.$label_line.'</a></li>';
                $list_panel.=$line;
              }
            }
            $all_panel='<li class="panel">
                          '.$first.'                  
                          <ul id="'.$href_panel.'" class="panel-collapse collapse">
                            '.$list_panel.'
                          </ul>
                        </li>';
            return $all_panel;
          }
          else
          {
            return false;
          }
      }
      private function single_menu($label,$icon,$url,$active=NULL)
      {          
        
        if(!(is_null($label) && is_null($icon) && is_null($url)))
        {
          if(is_null($active)){if(is_bool($active)){if($active){$active='class="active"';}else{$active='';}}}
          $url_menu=base_url("/".$url);
          $list=' <li '.$active.'>
                    <a class="hand" onClick="location.href=\''.$url_menu.'\'"><i class="'.$icon.'"></i><span class="name">'.$label.'</span></a>
                  </li>';
          return $list;
        }          
      }
  //************ SELECCIONAR PAGINA FIN ***********
    //HEADER
    public function set_header()
    {
          $this->CI->load->helper('url');
          $this->CI->load->library('ion_auth');  
          $url=base_url().'assets/bootstrap/';
          $alumnos=$this->header_list_alumnos();
          $mensajes=$this->header_mensaje();
          $twitter=$this->header_twitter();
          $nombre=$this->_nombre;
          //***************************
              $ruta = uri_string().'';
              $sp = explode('/', $ruta);
              $lower = strtolower($sp[0]);
          //****************************
          $code=' <div class="navbar">
                      <ul class="nav navbar-nav navbar-right pull-right">                            
                          '.$twitter.$mensajes.$alumnos.'    
                          <li class="divider"></li>                          
                          <li class="hidden-xs dropdown">
                              <a href="#" title="Account" id="account" class="dropdown-toggle" data-toggle="dropdown">
                                  <i class="fa fa-user"></i>
                              </a>
                              <ul id="account-menu" class="dropdown-menu account" role="menu">
                                  <li role="presentation" class="account-picture">
                                      '.$nombre.'
                                  </li>
                                  <li role="presentation">
                                      <a href="'.base_url().$lower.'/perfil"" class="link">
                                          <i class="fa fa-user"></i>
                                          Mi Perfil
                                      </a>
                                  </li>
                              </ul>
                          </li>
                          <li class="visible-xs">
                              <a href="#" class="btn-navbar" data-toggle="collapse" data-target=".sidebar" title="">
                                  <i class="fa fa-bars"></i>
                              </a>
                          </li>
                          <li class="hidden-xs"><a href="'.base_url().'auth/logout"><i class="fa fa-sign-out"></i></a></li>
                      </ul>
                  </div>';
          return $code;
    }
    private function header_list_alumnos()
    {
        $url=base_url().'assets/bootstrap/';
        $this->CI->load->model('apoderado_model');
        $this->CI->load->library('ion_auth');
        $id_apoderado=$this->CI->session->userdata('apoderado_id');
        if($id_apoderado!=null) 
        {        
            $cantidad="0";   
            $pupilos=" No hay pupilos registrados ";         
            $alumnos=$this->CI->apoderado_model->get_alumnos_and_cursos($id_apoderado);
            if($alumnos['bool'])
            {   
                $cantidad=$alumnos['cantidad'];
                $alumnos=$alumnos['query'];
                $pupilos=NULL;
                
                foreach ($alumnos as $r) 
                {          
                                                      
                    $nombre=$r->first_name;
                    $nombre=preg_split("/[\s,]+/",$nombre);
                    $apellido=$r->last_name;
                    $apellido=preg_split("/[\s,]+/",$apellido);
                    $nombre=$nombre[0].' '.$apellido[0]; 
                    $curso=$r->Grado."°".$r->Letra;
                    $pupilos.='<li role="presentation">
                                <a class="message idalumno" id="'.$r->Alumno_Id.'">                             
                                  <div class="key"> <i class="eicon-use"></i></div>
                                  <img src="'.$url.'img/user.png" alt="">
                                  <div class="details" >
                                      <div class="sender">'.$nombre.'</div>
                                      <div class="text">'.$curso.'</div>
                                  </div>
                                </a>
                              </li>';
                }
                
            }            
            $more=' <li role="presentation">
                      <a href="#" class="text-align-center see-all">
                        Ver mis pupilos <i class="fa fa-arrow-right"></i>
                      </a>
                    </li>';
            $codigo='<li class="dropdown">
                                  <a href="#" title="Alumnos" id="messages"
                                     class="dropdown-toggle"
                                     data-toggle="dropdown">
                                      <i class="fa fa-group"></i>
                                      <span class="count">'.$cantidad.'</span>
                                  </a>
                                  <ul id="messages-menu" class="dropdown-menu messages" role="menu">                                  
                                      '.$pupilos.'
                                  </ul>
                              </li>';
          return $codigo; 
        }
    }
    private function header_mensaje()
    {
        $url=base_url().'assets/bootstrap/';
        $url_inbox=base_url().$this->url_browser.'/inbox';
        $this->CI->load->model('app_model');
        $Id_User=$this->CI->ion_auth->get_user_id(); 
        $query=$this->CI->app_model->inbox_load($Id_User);
        $mensaje=NULL;
        if($query['bool'])
        {
          $nuevos=$query['n_new'];
          $nmensajes=count($query['query']);
          $query=$query['query'];
          if($nmensajes>5){$nmensajes=5;}
          $i=0;
          foreach ($query as $q) 
          {
            if($i==$nmensajes){break;}
            $Titulo=$q->Titulo;
            if($q->visto)
            {
              $icono='<span class="label label-default"><i class="fa fa-check-square-o"></i></span>';
              $title='Visto';
            }
            else
            {
              $icono='<span class="label label-danger"><i class="fa fa-envelope"></i></span>';
              $title='Nuevo mensaje';
            }
            $mensaje.=' <li role="presentation">
                          <a href="'.$url_inbox.'/'.$q->Id.'" class="support-ticket">
                            <div class="picture" title="'.$title.'">
                                '.$icono.'
                            </div>
                            <div class="details">
                                  '.$Titulo.'
                            </div>
                          </a>
                        </li>';
            $i++;
          }
        }
        else
        {
          $nuevos=0;
        }
        $codigo=' <li class="dropdown">
                    <a href="#" title="Messages" id="messages" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-comments"></i>
                      <span class="count">'.$nuevos.'</span>
                    </a>
                    <ul id="messages-menu" class="dropdown-menu messages" role="menu">
                      '.$mensaje.'                    
                      <li role="presentation">
                        <a href="'.$url_inbox.'" class="text-align-center see-all">
                          Ver todos los mensajes <i class="fa fa-arrow-right"></i>
                        </a>
                      </li>
                    </ul>
                  </li>';
        return $codigo;  
    }
    private function header_recordar()
    {
      $codigo='<li class="dropdown">
                <a href="#" title="8 support tickets"
                   class="dropdown-toggle"
                   data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="count">8</span>
                </a>
                <ul id="support-menu" class="dropdown-menu support" role="menu">
                    <li role="presentation">
                        <a href="#" class="support-ticket">
                            <div class="picture">
                                <span class="label label-important"><i class="fa fa-bell-o"></i></span>
                            </div>
                            <div class="details">
                                Check out this awesome ticket
                            </div>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#" class="support-ticket">
                            <div class="picture">
                                <span class="label label-warning"><i class="fa fa-question-circle"></i></span>
                            </div>
                            <div class="details">
                                "What is the best way to get ...
                            </div>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#" class="support-ticket">
                            <div class="picture">
                                <span class="label label-success"><i class="fa fa-tag"></i></span>
                            </div>
                            <div class="details">
                                This is just a simple notification
                            </div>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#" class="support-ticket">
                            <div class="picture">
                                <span class="label label-info"><i class="fa fa-info-circle"></i></span>
                            </div>
                            <div class="details">
                                12 new orders has arrived today
                            </div>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#" class="support-ticket">
                            <div class="picture">
                                <span class="label label-important"><i class="fa fa-plus"></i></span>
                            </div>
                            <div class="details">
                                One more thing that just happened
                            </div>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#" class="text-align-center see-all">
                            See all tickets <i class="fa fa-arrow-right"></i>
                        </a>
                    </li>
                </ul>
            </li>';
        return $codigo;
    }
    private function header_twitter()
    {
      $user=$this->user_log; 
      $txt='Desconectado';
      $url='';
      $color_conectado='';
      if($user->Id_twitter!=null)
      {  
        $twitter=$this->CI->session->userdata('twitter_screen_name');
        if($twitter!=null)
        {
          $color_conectado='style="color:#55ACEE"';
          $txt='Conectado: '.$twitter;
        }
          
          $code=' <li id="estado_twitter">
                    <a title="'.$txt.'" id="twitter" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-twitter fa-2x hand" '.$color_conectado.' ></i>
                    </a>
                  </li>';
          return $code;
      }
    }
    //HEADER
    public function finalCode()
    {
          $this->CI->load->helper('url');
          $urlCI="assets/bootstrap/";        
          $url=base_url().$urlCI;
          //$url="http://tesis.jampsur.com/bootstrap/";
          $specific=NULL;
          if(!is_null($this->extra_js))
          {
            $specific=' <!-- page specific -->'.$this->extra_js;
          }
          /*
             
          */
          $code='<!-- jquery and friends -->              
              <script src="'.$url.'lib/jquery/jquery-2.0.3.min.js"></script>
              <script src="'.$url.'lib/jquery-pjax/jquery.pjax.js"></script>

              <!-- jquery plugins -->
              <script src="'.$url.'lib/jquery-maskedinput/jquery.maskedinput.js"></script>
              <script src="'.$url.'lib/parsley/parsley.js"> </script>
              <script src="'.$url.'lib/icheck.js/jquery.icheck.js"></script>
              <script src="'.$url.'lib/select2.js"></script>
              <script src="'.$url.'lib/jquery.autogrow-textarea.js"></script>
              <script src="'.$url.'lib/jquery-ui-1.10.3.custom.js"></script>
              <script src="'.$url.'lib/jquery.slimscroll.js"></script>
              
              <!--backbone and friends -->            
                            <script src="'.$url.'lib/backbone/underscore-min.js"></script>
                            <script src="'.$url.'lib/backbone/backbone-min.js"></script>
                            <script src="'.$url.'lib/backbone/backbone-pageable.js"></script>
                            <script src="'.$url.'lib/backgrid/backgrid.js"></script>
                            <script src="'.$url.'lib/backgrid/backgrid-paginator.js"></script>
              <!-- bootstrap default plugins -->
              <script src="'.$url.'lib/bootstrap/transition.js"></script>
              <script src="'.$url.'lib/bootstrap/collapse.js"></script>
              <script src="'.$url.'lib/bootstrap/alert.js"></script>
              <script src="'.$url.'lib/bootstrap/tooltip.js"></script>
              <script src="'.$url.'lib/bootstrap/popover.js"></script>
              <script src="'.$url.'lib/bootstrap/button.js"></script>
              <script src="'.$url.'lib/bootstrap/dropdown.js"></script>
              <script src="'.$url.'lib/bootstrap/modal.js"></script>
              <script src="'.$url.'lib/bootstrap/tab.js"> </script>

              <!-- bootstrap custom plugins -->

              <script src="'.$url.'lib/bootstrap-datepicker.js"></script>
              <script src="'.$url.'lib/bootstrap-select/bootstrap-select.js"></script>
              <script src="'.$url.'lib/wysihtml5/wysihtml5-0.3.0_rc2.js"></script>
              <script src="'.$url.'lib/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
              <script src="'.$url.'lib/bootstrap-switch.js"></script>
              <script src="'.$url.'lib/bootstrap-colorpicker.js"></script>

              <!-- basic application js-->
              <script src="'.$url.'lib/notify.js"></script>
              <script src="'.$url.'lib/styles/metro/notify-metro.js"></script>
              <script src="'.$url.'js/app.js"></script>
              <script src="'.$url.'js/settings.js"></script>    

              '.$specific;
          return $code;
    }
}