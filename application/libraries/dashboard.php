<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
/** 
 * Dashboard | Carga dinamica
 * 
 */
class Dashboard
{ 
	private $CI; 
    private $soy_url='';
	public function __construct()  
    { 
        $this->CI =& get_instance(); 
        $this->CI->load->helper('url');
        $this->user_log=$this->CI->ion_auth->user()->row(); 
        $this->soy_url=$this->CI->session->userdata('soy');  
    }  
    public function anotaciones($alumno)
    {
    	$tbody='<tr><td colspan="3"style="text-align:center">No hay anotaciones registradas</td></tr>';
    	if(!is_null($alumno))
    	{
            $count_an=0;
            $tbody='';
            foreach ($alumno as $q) 
            {
                if($count_an<3)
                {
                    $count_an++;
                    if($q->Tipo)
                    {
                        $Tipo='<span class="label label-success">Positiva</span>';
                    }
                    else
                    {
                        $Tipo='<span class="label label-danger">Negativa</span>';
                    }
                    $tbody.='<tr>
                            <td>'.$Tipo.'</td>
                            <td>'.$this->cortar($q->Descripcion,30).'</td>                               
                            </tr>';
                }
                else
                {
                    break;
                }
            }
    	}

    	$body='	<table class="table table-striped no-margin sources-table">
                    <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>Descripcion</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tbody.'
                   	</tbody>
				</table>';
		$data = array(	'icon'=>'eicon-graduation-cap',
						'title'=>'Hoja de vida',
						'more'=>'/'.$this->soy_url.'/academia/hojavida',
						'body'=>$body);
		return $this->base($data);
    }    
    public function notas($alumno)
    {
        $tbody='';
        $count=0;
        if(!is_null($alumno))
        {
            foreach ($alumno as $q) 
            {
                if($count!=3)
                {
                    $tbody.=' <tr>
                                <td>'.$q->Nombre.'</td>
                                <td>'.$q->Nota.'</td>
                            </tr>';
                    $count++;
                }               
            }           
        }     
        else
        {
            $tbody='<tr><td colspan="3"style="text-align:center">No hay Reuniones registradas</td></tr>';
        }
        $body=' <table class="table table-striped no-margin sources-table">
                    <thead>
                        <tr>
                            <th>Asginatura</th>
                            <th>Nota</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tbody.'
                    </tbody>
                </table>';
        $data = array(  'icon'=>'eicon-graduation-cap',
                        'title'=>'Notas',
                        'more'=>'/'.$this->soy_url.'/academia/notas',
                        'body'=>$body);
        return $this->base($data);
    }
    public function prox_pruebas($alumno)
    {
        $tbody='';
        $count=0;
        if(!is_null($alumno))
        {
            foreach ($alumno as $q) 
            {
                if($count!=3)
                {
                    
                        $tbody.=' <tr>
                                <td>'.$this->cortar($q->Tipo).'</td>
                                <td>'.$q->Nombre.'</td>
                                <td>'.date("d-m-Y", strtotime($q->Fecha)).'</td>
                            </tr>';
                    $count++;
                    
                }               
            }           
        }     
        else
        {
            $tbody='<tr><td colspan="3"style="text-align:center">No se encontraron evaluaciones</td></tr>';
        }

        $body=' <table class="table table-striped no-margin sources-table">
                    <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>Asignatura</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tbody.'
                    </tbody>
                </table>';
        $data = array(  'icon'=>'fa fa-check-circle-o',
                        'title'=>'Evaluaciones',
                        'more'=>'/'.$this->soy_url.'/academia/notas',
                        'body'=>$body);
        return $this->base($data);
    }
    public function cortar($texto,$largo=NULL)
    {
        if ($largo == NULL)
            $largo = 50;
        $mini = substr(strip_tags($texto), 0, $largo);
        if (strlen(strip_tags($texto)) > $largo)
            $mini .= '...';
        return $mini;
    }
    public function reuniones($alumno)
    {
    	$tbody='';
        $count=0;
    	if(!is_null($alumno))
    	{
            foreach ($alumno as $q) 
            {
                if($count!=3)
                {
                    $tbody.=' <tr>
                                <td>'.$this->cortar($q->Motivo).'</td>
                                <td>'.date("d-m-Y", strtotime($q->Fecha)).'</td>
                            </tr>';
                    $count++;
                }               
            }    		
    	}     
        else
        {
            $tbody='<tr><td colspan="3"style="text-align:center">No hay Reuniones registradas</td></tr>';
        }

    	$body='	<table class="table table-striped no-margin sources-table">
                    <thead>
                        <tr>
                            <th>Motivo</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tbody.'
                   	</tbody>
				</table>';
		$data = array(	'icon'=>'eicon-clock',
						'title'=>'Reuniones',
						'more'=>'/'.$this->soy_url.'/academia/reuniones',
						'body'=>$body);
		return $this->base($data);
    }
    public function notificaciones()
    {
        $icon='';
        $text='Ejemplo';
        $fecha='';
        $notify='   <section class="feed-item">
                        <div class="icon pull-left">
                            <i class="'.$icon.'"></i>
                        </div>
                        <div class="feed-item-body">
                            <div class="text">
                                '.$text.'
                            </div>
                            <div class="time pull-left">
                                '.$fecha.'
                            </div>
                        </div>
                    </section>';
        $code='<section class="widget large" style="height:390px">                
                    <header>
                        <h4>
                            <i class="eicon-share"></i>
                            Notificaciones
                        </h4>
                        <div class="actions">
                            <button class="btn btn-transparent btn-xs">Mostrar Todas <i class="fa fa-arrow-down"></i></button>
                        </div>
                    </header>
                    <div class="body">
                            <a class="twitter-timeline"  href="https://twitter.com/applesteacher" data-widget-id="661947514304745472">Tweets por el @applesteacher.</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                   
                    </div>                
                </section>';
        return $code;
    }
    public function base($data)
    {
        $url='http://127.0.0.1/tesis';
    	$icon='fa fa-bell';
    	$title='Alertas';
    	$more='';
    	$body='No hay nuevos registros';
    	if(isset($data))
    	{
    		if(is_array($data))
    		{
    			if(isset($data['icon'])){$icon=$data['icon'];}
    			if(isset($data['title'])){$title=$data['title'];}
    			if(isset($data['more'])){$more='<button style="background:rgb(102, 102, 102)" class="btn btn-transparent btn-xs" onclick="location.href=\''.$url.$data['more'].'\'">Mostrar Todos  <i class="fa fa-plus"></i></button>';}
    			if(isset($data['body'])){$body=$data['body'];}
    		}
    	}
    	$code='<section class="widget">
                    <header>
                        <h4>
                            <i class="'.$icon.'"></i> '.$title.'  
                        </h4>
                        <div class="actions">
							<small>Ultimos 3 registros</small>
							'.$more.'
						</div>
                        
                    </header>
                    <div class="body">
                        '.$body.'
                    </div>
                </section>';
        return $code;
    }
    public function stat_apoderado($reuniones,$notas,$pruebas)
    {
        $sub_reuniones='Proxima Reunión';        
        
        $sub_prueba='Proxima Evaluación';
        if(!is_null($notas))
        {
            $promedio=0;
            $_notas=0;
            foreach ($notas as $n) 
            {
                $promedio+=$n->Nota;
                $_notas++;
            }
            if($_notas!=0)
            {
                $promedio=$promedio/$_notas;
                if($promedio<4)
                {
                    $promedio='<div class="big-text" style="color:#F14C4C">'.number_format($promedio,1).'</div>';
                }
                else
                {
                    $promedio='<div class="big-text">'.number_format($promedio,1).'</div>';
                }
            }
            else
            {
                $promedio='<div class="big-text" style="font-size:16px">Sin Registro</div>';
            }
        }
        else
        {
            $promedio='<div class="big-text" style="font-size:16px">Sin Registro</div>';
        }

        if(!is_null($reuniones))
        {
            foreach ($reuniones as $r) 
            {
                $reuniones=date("d-m-Y", strtotime($r->Fecha));
                break;
            }
            if($reuniones!=NULL)
            {
                $especial='';
                $hoy=date("d-m-Y");
                if($reuniones==$hoy)
                {
                    $sub_reuniones="Hoy Reunión!!";
                    $especial='color:red;';
                }   
                $reuniones='<div class="big-text" style="font-size:26px;'.$especial.'">'.$reuniones.'</div>';
            }
            else
            {
                $reuniones='<div class="big-text" style="font-size:16px">Sin Registro</div>';
                
            }
        }
        else
        {
            $reuniones='<div class="big-text" style="font-size:16px">Sin Registro</div>';
        }

        if(!is_null($pruebas))
        {
            foreach ($pruebas as $p) 
            {
                $fecha=date("d-m-Y", strtotime($p->Fecha));
                break;
            }
            if($fecha!=NULL)
            {
                $especial_p='';
                $hoy=date("d-m-Y");
                if($fecha==$hoy)
                {
                    $sub_prueba="Hoy Prueba!!";
                    $especial_p='color:red;';
                }   
                $prueba='<div class="big-text" style="font-size:26px;'.$especial_p.'">'.$fecha.'</div>';
            }   
        }
        else
        {
            $prueba='<div class="big-text" style="font-size:16px">Sin Registro</div>';
        }
        $code='     <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="box">
                                '.$reuniones.'
                            <div class="description">
                                <i class="fa fa-calendar"></i> '.$sub_reuniones.'
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="box">
                            <div class="big-text">
                                '.$prueba.'
                            </div>
                            <div class="description">
                                <i class="fa fa-calendar"></i>
                                '.$sub_prueba.'
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="box">
                            <div class="big-text">
                                '.$promedio.'
                            </div>
                            <div class="description">
                                <i class="fa fa-user"></i>
                                Promedio Actual
                            </div>
                        </div>
                    </div>';
        return $code;
    } 



    //********************* PROFESOR *************************

    public function profe_evaluaciones($pruebas)
    {
        $tbody='';
        $count=0;
        if($pruebas['bool'])
        {
            foreach ($pruebas['query'] as $q) 
            {
                $fecha=date("d-m-Y", strtotime($q->Fecha));
                if($count!=3)
                {
                    if($fecha>date("d-m-Y"))
                    {
                        $tbody.=' <tr>
                                <td>'.$q->Grado.'° '.$q->Letra.'</td><td>'.$this->cortar($q->Nombre,5).'</td>
                                <td>'.$this->cortar($q->Tipo,4).'</td>
                                
                                <td>'.date("d-m-Y", strtotime($q->Fecha)).'</td>
                            </tr>';
                        $count++;
                    }
                    
                }               
            }           
        }     
        else
        {
            $tbody='<tr><td colspan="4"style="text-align:center">No hay Reuniones registradas</td></tr>';
        }

        $body=' <table class="table table-striped no-margin sources-table">
                    <thead>
                        <tr>
                            <th>Curso</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tbody.'
                    </tbody>
                </table>';
        $data = array(  'icon'=>'eicon-clock',
                        'title'=>'Pruebas Cursos Jefe',
                        'body'=>$body);
        return $this->base($data);
    }
    public function prox_prueba_materia($pruebas)
    {
        $tbody='';
        $count=0;
        if($pruebas['bool'])
        {
            foreach ($pruebas['query'] as $q) 
            {
                $fecha=date("d-m-Y", strtotime($q->Fecha));
                if($count!=3)
                {
                    if($fecha>date("d-m-Y"))
                    {
                        
                        $tbody.=' <tr>
                                <td>'.$q->Grado.'° '.$q->Letra.'</td><td>'.$this->cortar($q->Nombre,5).'</td>
                                <td>'.$this->cortar($q->Tipo,4).'</td>
                                
                                <td>'.date("d-m-Y", strtotime($q->Fecha)).'</td>
                            </tr>';
                        $count++;
                        
                    }
                    
                }               
            }           
        }     
        else
        {
            $tbody='<tr><td colspan="4"style="text-align:center">No hay Reuniones registradas</td></tr>';
        }

        $body=' <table class="table table-striped no-margin sources-table">
                    <thead>
                        <tr>
                            <th>Curso</th>
                            <th>Asignatura</th>
                            <th>Tipo</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tbody.'
                    </tbody>
                </table>';
        $data = array(  'icon'=>'eicon-clock',
                        'title'=>'Pruebas Cursos',
                        'body'=>$body);
        return $this->base($data);
    }
    public function debe_prueba_materia($pruebas)
    {
        $tbody='';
        $count=0;
        if($pruebas['bool'])
        {
            foreach ($pruebas['query'] as $q) 
            {
                $fecha=date("d-m-Y", strtotime($q->Fecha));
                if($count!=3)
                {
                    if($fecha<date("d-m-Y"))
                    {
                        $diferencia=date("d-m-Y")-$fecha;

                        $fecha=date("d-m-Y")-date("d-m-Y", strtotime($q->Fecha));
                        $tbody.=' <tr>
                                <td>'.$q->Grado.'° '.$q->Letra.'</td><td>'.$this->cortar($q->Nombre,5).'</td>
                                <td>'.$this->cortar($q->Tipo,4).'</td>
                                
                                <td>'.$diferencia.' días</td>
                            </tr>';
                        $count++;
                    }
                    
                }               
            }           
        }     
        else
        {
            $tbody='<tr><td colspan="4"style="text-align:center">No hay Reuniones registradas</td></tr>';
        }

        $body=' <table class="table table-striped no-margin sources-table">
                    <thead>
                        <tr>
                            <th>Curso</th>
                            <th>Asignatura</th>
                            <th>Tipo</th>
                            <th>Retrazo</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tbody.'
                    </tbody>
                </table>';
        $data = array(  'icon'=>'eicon-clock',
                        'title'=>'Pruebas realizadas',
                        'body'=>$body);
        return $this->base($data);
    }
    public function profe_jefe_reuniones($reuniones)
    {
        $tbody='';
        $count=0;
        if($reuniones['bool'])
        {
            foreach ($reuniones['query'] as $q) 
            {
                if($count!=3)
                {
                    $fecha=date("d-m-Y", strtotime($q->Fecha));
                    if($fecha>date("d-m-Y"))
                    {
                        $tbody.=' <tr>
                                <td>'.$q->Grado.'° '.$q->Letra.'</td>
                                <td>'.$this->cortar($q->Motivo).'</td>
                                
                                <td>'.date("d-m-Y", strtotime($q->Fecha)).'</td>
                            </tr>';
                        $count++;
                    }
                    
                }               
            }           
        }     
        else
        {
            $tbody='<tr><td colspan="3"style="text-align:center">No hay Reuniones registradas</td></tr>';
        }

        $body=' <table class="table table-striped no-margin sources-table">
                    <thead>
                        <tr>
                            <th>Curso</th>
                            <th>Motivo</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tbody.'
                    </tbody>
                </table>';
        $data = array(  'icon'=>'eicon-clock',
                        'title'=>'Reuniones',
                        'body'=>$body);
        return $this->base($data);
    }
    public function profe_jefe_ultimas_notas()
    {
        return '';
    }
    public function profe_jefe_ultimas_anotaciones()
    {
        return '';
    }
    public function profe_jefe_promedio_curso()
    {
        return '';
    }


}