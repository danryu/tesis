<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
  class App 
  { 
    private $min_password=6;
  	public function __construct()  
    { 
    	$this->CI =& get_instance(); 
        $this->CI->load->library('Mandrill');
    }
    
    function mandrill($correo,$mensaje,$title)
    {
        try
        {
            $emails = NULL;
            $type   = NULL;
            if($type!=NULL){$type="Users";}
            if(!isset($mensaje) || $mensaje == NULL){$r = array('bool' => false, 'msg'=> 'Falta Mensaje'); return $r;}
            //if(!isset($query) || $query == NULL){$r = array('bool' => false, 'msg'=> 'Falta Correos'); return $r;}          
            $email = array('email' => $correo);
            $var=array('mensaje'=>$mensaje,'email'=>$email,'type'=>'creator','title'=>$title);
            $enviado=$this->send_mandrill($var);         
            if($enviado)
            {
                $r = array('bool' => true, 'msg'=> 'Mensajes Enviado a'.$type);
                return $r; 
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'Error al enviar mensajes a'.$type);
                return $r; 
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }    
    }
    private function send_mandrill($var)
    {
        $mensaje=$var['mensaje'];
        $title=$var['title'];
        $email=$var['email'];
        $type=$var['type'];

        $mandrill_ready = NULL;
        try 
        {
            $this->CI->mandrill->init($this->CI->config->item('mandrill_key'));;
            $mandrill_ready = TRUE;

        } 
        catch(Mandrill_Exception $e) 
        {
            $mandrill_ready = FALSE;  
        }
        if( $mandrill_ready ) 
        {
            $var=array('type'=>$type,'mensaje'=>$mensaje,'title'=>$title);
            $body=$this->CI->mandrill->msg_create($var);           
            $email = array(
                'html' => $body,//'text' => '<h1>Ejemplo</h1>',//$body,
                'subject' => 'Apples\'s Teacher', 'from_email' => 'applesteacher2015@gmail.com',
                'from_name' => 'Apples\'s Teacher','to' => array($email));
            $result = $this->CI->mandrill->messages_send($email);
            //var_dump($result);
            //echo "Enviado a ".$email['to'][0]['email'];
            return true;
        }
        else
        {
            return false;
        }
    }

    public function get_alumno_view()
    {
        $user_id = $this->session->userdata('alumno_view');
        if (!empty($user_id))
        {
            return $user_id;
        }
        return null;
    }
    public function get_alumno_id()
    {
        if($this->CI->session->userdata('alumno_view')!=null)
        {
            $user_id = $this->CI->session->userdata('alumno_view')->Alumno_Id;
            if (!empty($user_id))
            {
                return $user_id;
            }
        }
        
        return null;
    }
    public function get_mis_alumnos()
    {
        $user_id = $this->session->userdata('mis_alumnos');
        if (!empty($user_id))
        {
            return $user_id;
        }
        return null;
    }

}
?>