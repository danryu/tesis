<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
/** 
 * Layouts Class. PHP5 only. 
 * 
 */
class Whatsapp { 

 	

	public function __construct()  
    { 
    	$this->CI =& get_instance();     	
    } 
	function send_wsp($var)
	{	
		$password=$this->CI->config->item('Wsp_pass');
    	$username=$this->CI->config->item('Wsp_num');
    	echo $password."----".$username;
		$to=NULL;
		$message=NULL;
		$msg=NULL;
		if(isset($var["numero"]))
		{			
			if(!is_null($var["numero"]) || $var["numero"]=="")
			{ 
				$to=trim($var["numero"]);
				$msg.=" ".$to;
				if(strpos($to,'+')!==false)
				{
					$to=str_replace("+","",$to);					
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		if(isset($var["mensaje"]))
		{
			if(!is_null($var["mensaje"]) || $var["mensaje"]=="")
			{ 
				$message=trim($var["mensaje"]);
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		try
		{ 

			require_once 'src_wsp/whatsprot.class.php';			
			$w = new WhatsProt($username, 0, "Tesis", true); 
			$w->connect();
			$w->loginWithPassword($password);
			echo $w;	 
			$w->SendPresenceSubscription($to); 
			$w->sendMessage($to,$message); 
			return true;
		}
		catch(Exception $e)
		{
			echo "error=>".$e;

			return false;
		}
	}
	
}
?>