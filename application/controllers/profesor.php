<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Profesor extends CI_Controller {
 

    function __construct()
    {
        parent::__construct();
       
        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->model('app_model');
        $this->load->helper('app');
        $this->load->model('profesor_model');
         $this->load->model('apoderado_model');
        $this->load->model('alumno_model');
        //$this->load->library('ion_auth');
        $this->load->helper('url');
        $this->load->helper('profesor');
       // $this->load->helper('apoderado');
        $this->load->library(array('security_lib','ion_auth'));
        $this->load->model(array('administracion_model','ion_auth_model')); 
        $this->verificar();
        /* ------------------ */ 
        $this->soy();

        $this->load->library('dashboard');
        $this->load->library('micurso');
        $this->load->library('layout');      
    }
    private function soy()
    {
        if(!$this->session->userdata('soy'))
        {
            $this->session->set_userdata('soy','profesor');
        }
        else
        {
            $this->session->unset_userdata('soy');
            $this->session->set_userdata('soy','profesor');
        }
    }
    private function verificar()
    {
        $email = $this->session->userdata('identity');
        $codigo = $this->ion_auth_model->get_activication_code($email);
        $valor = $codigo['query'];
        if($valor == NULL || $valor == "")
        {
            redirect('auth/change_password');
        }
        else
        {
            $group = 4;
            if (!$this->ion_auth->in_group($group,$this->ion_auth->get_user_id()))
            {
                redirect('auth/ver_nuevo');
            }
        }
    }
    public function index()
    {
        $this->verificar();
        $data['dashboard']=$this->dashboard_generator();
        $this->layout->set_specific_js(array('lib/icheck.js/jquery.icheck.js','lib/sparkline/jquery.sparkline.js','lib/jquery-ui-1.10.3.custom.js','lib/jquery.slimscroll.js','lib/nvd3/lib/d3.v2.js','lib/nvd3/nv.d3.custom.js','lib/nvd3/src/models/scatter.js','lib/nvd3/src/models/axis.js','lib/nvd3/src/models/legend.js','lib/nvd3/src/models/multiBar.js','lib/nvd3/src/models/multiBarChart.js','lib/nvd3/src/models/line.js','lib/nvd3/src/models/lineChart.js','lib/nvd3/stream_layers.js','lib/backbone/backbone.localStorage-min.js','js/index.js','lib/fullcalendar/fullcalendar.js','lib/jquery-ui-1.10.3.custom.js','js/forms-elemets.js','js/calendar.js'));
        $this->layout->view('profesor/main',$data);
    }
    private function dashboard_generator()
    {
        $_reuniones='';
        $_notas='';
        $_stat='';
        $_pruebas='';
        $id=$this->ion_auth->get_Profesor_Id();
        
        // PROFE JEFE
        $reuniones= $this->profesor_model->get_all_reuniones($id);  
        $reuniones=$this->dashboard->profe_jefe_reuniones($reuniones);

        $pruebas=$this->profesor_model->get_all_evaluaciones($id);    
        $pruebas=$this->dashboard->profe_evaluaciones($pruebas);
        // FIN PROFE JEFE

        //PROFE NORMAL
        $prox_pruebas=$this->profesor_model->get_prox_prueba($id);    
        $prox_pruebas=$this->dashboard->prox_prueba_materia($prox_pruebas);

        $debe_pruebas=$this->profesor_model->get_debe_prueba($id);    
        $debe_pruebas=$this->dashboard->debe_prueba_materia($debe_pruebas);
        //FIN PROFE NORMAL


            $_notificacion=$this->dashboard->notificaciones();
         //   $_stat=$this->dashboard->stat_apoderado($reuniones,$notas,$pruebas);
            $code=' <div class="row">
                        <div class="col-md-12">
                            <h2 class="page-title">Profesor <small>Plataforma educativa</small></h2>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-8">
                            '.$_stat.'
                            <div class="col-md-6">
                                '.$prox_pruebas.'
                                '.$debe_pruebas.'
                            </div>  
                            <div class="col-md-6">
                                '.$reuniones.'
                                '.$pruebas.'
                            </div>    
                        </div>
                        <div class="col-md-4">       
                            '.$_notificacion.'
                        </div>              
                    </div>  ';
            return $code;
    }
    
    
    public function create_message()
    {
        $this->verificar();
        if($this->input->post('title')==NULL || $this->input->post('title')=="")
        {
            $this->layout->set_specific_js(array('js/forms-article.js'));
            $id=$this->ion_auth->get_user_id(); 
            $var=array('Id_User'=>$id);
            $cursos=$this->app_model->load_curso($var);
            if($cursos['bool'])
            {
                $cursos=$cursos['query'];
                if(isset($cursos['bool_profe'])){if($cursos['bool_profe']){$datos['alumnos']=$cursos['query_profe'];}}
                if(isset($cursos['bool_jefe'])){if($cursos['bool_jefe']){$datos['apoderados']=$cursos['query_jefe'];}}
            }
            $datos['rango']=4;            
            $cursos_jefe=$this->profesor_model->load_cursos_jefe($id);
            $this->layout->view('app/create_message',$datos);

        }       
        else
        {
            $mensaje= array(    'Titulo' =>$this->input->post('title'), 
                                'Contenido' =>$this->input->post('text'),
                                'Fecha' =>date("Y-m-d"));
            $origen_destino=array(  'alumnos' =>$this->input->post('alumnos'),
                                    'apoderado' =>$this->input->post('apoderados'),
                                    'nivel' =>$this->input->post('status'));
            $datos = array('mensaje' =>$mensaje,'od'=>$origen_destino);
            var_dump($datos);
          //  $this->enviar_message($datos);
        }   
    }
    public function enviar_message($datos)
    {
        //Guardar mensajes

        $Id_mensaje=0;
        $lvl=1;
        $Id_origen=$this->ion_auth->get_user_id();  
        if(isset($datos['mensaje']))
        {
            $datos_mensaje=$datos['mensaje'];
            $save=$this->app_model->save_alert($datos_mensaje);
            if($save['bool'])
            {
                $Id_mensaje=$save['id'];
            }
            else
            {
                echo $save['msg'].'<br>'.$save['e'];
            }
        }
        if(isset($datos['od']))
        {
            
            $datos_od=$datos['od'];         
            if($Id_mensaje!==0)
            {
                $mensaje=$datos['mensaje']['Contenido'];
                $title=$datos['mensaje']['Titulo'];
                if(isset($datos_od['alumnos']))
                {                   
                    $alumnos=$datos_od['alumnos'];
                    if(count($alumnos)>0 && $alumnos != false)
                    {
                        $lvl=1;
                        foreach ($alumnos as $q) 
                        {
                            $save_OD=$save=$this->app_model->save_OD($q,$Id_origen,$Id_mensaje,'alumno',$lvl,$mensaje,$title);
                        }
                    }
                }               
                if(isset($datos_od['apoderado']))
                {
                    if(isset($datos_od['nivel']) &&  $datos_od['nivel'] != NULL)
                    {
                        $lvl=$datos_od['nivel'];
                        if($lvl!=1 && $lvl!=2 && $lvl!=3 && $lvl!=4)
                        {
                            $lvl=1;
                        }
                    }
                    $apoderado=$datos_od['apoderado'];
                    if(count($apoderado)>0 &&  $apoderado != false)
                    {
                        foreach ($apoderado as $q) 
                        {
                            $save_OD=$save=$this->app_model->save_OD($q,$Id_origen,$Id_mensaje,'apoderado',$lvl,$mensaje,$title);
                        }
                    }
                }
                redirect('profesor/inbox');

            }
            else
            {
                echo "no funca";
            }
        }
        
        //Enviar mensajes
    }
    public function crear_prueba()
    {
        $datos=NULL;
        $this->layout->set_specific_js(array('lib/jquery.bootstrap.wizard.js','js/wizard.js'));
        $Id_User=$this->ion_auth->get_user_id(); 
        $Id_User=36;//BORRAR AL TERMINAR DE PROBAR
        $asignaturas = $this->profesor_model->load_cursos($Id_User);
        if($asignaturas['bool'])
        {
            $datos['asg_query']=$asignaturas['query'];
        }
        


        //$this->loadAsg();
        $this->layout->view('profesor/cursos/crear_prueba',$datos);
    }
    public function curso($Id_C,$Id_Nota=NULL)
    {
        $this->verificar();

        try
        {
            $this->layout->set_specific_js(array('js/loader.js','lib/nvd3/lib/d3.v2.js','lib/nvd3/nv.d3.custom.js','lib/nvd3/src/models/scatter.js','lib/nvd3/src/models/axis.js','lib/nvd3/src/models/legend.js','lib/nvd3/src/models/multiBar.js','lib/nvd3/src/models/multiBarChart.js','lib/nvd3/src/models/line.js','lib/nvd3/src/models/lineChart.js','lib/nvd3/stream_layers.js','js/index.js','lib/icheck.js/jquery.icheck.js','lib/sparkline/jquery.sparkline.js','lib/jquery-ui-1.10.3.custom.js','lib/jquery.slimscroll.js','js/forms-article.js','js/forms-article.js','lib/vendor/jquery.ui.widget.js','lib/vendor/http_blueimp.github.io_JavaScript-Templates_js_tmpl.js','lib/vendor/http_blueimp.github.io_JavaScript-Load-Image_js_load-image.js','lib/vendor/http_blueimp.github.io_JavaScript-Canvas-to-Blob_js_canvas-to-blob.js','lib/jquery.iframe-transport.js','lib/jquery.fileupload.js','lib/jquery.fileupload-fp.js','lib/jquery.fileupload-ui.js','js/forms-elemets.js','lib/jquery.blockUI.js'));
            
            if(isset($Id_C))
            {
                if($Id_C>0)
                {
                    $Id_U=$this->ion_auth->get_user_id();
                    $verificar_curso=$this->profesor_model->verificar_curso($Id_U,$Id_C);
                    if($verificar_curso['bool'])
                    {

                        $Id_Profesor=$this->profesor_model->get_Profesor_Id($Id_U);
                        if($Id_Profesor['bool']){$Id_Profesor=$Id_Profesor['Id'];}
                        $datos=NULL;
                        $asignaturas=$this->profesor_model->get_asignaturas($Id_U,$Id_C);
                        if($asignaturas['bool']){$datos['query_asg']=$asignaturas['query'];}
                        $apoderados=$this->profesor_model->get_apoderados_curso($Id_C);
                        if($apoderados['bool']){$datos['query_apo']=$apoderados['query'];}
                        $alumnos=$this->profesor_model->get_alumnos_curso($Id_C);
                        if($alumnos['bool']){$datos['query_alu']=$alumnos['query'];}
                        $lista_foros=$this->profesor_model->get_foros_curso($Id_U,$Id_C);
                        if($lista_foros['bool']){$datos['query_foros']=$lista_foros['query'];}    

                        $set_notas_cursos=$this->profesor_model->get_set_notas_curso($Id_Profesor,$Id_C);
                        if($set_notas_cursos['bool']){$datos['set_notas_cursos']=$set_notas_cursos['query'];}  

                        $notas_cursos=$this->profesor_model->get_notas_curso($Id_Profesor,$Id_C);
                        if($notas_cursos['bool']){$datos['notas_cursos']=$notas_cursos['query'];}

                        $reglamento_notas=$this->profesor_model->get_reglamento_notas();
                        if($reglamento_notas['bool']){$datos['reglamento_notas']=$reglamento_notas['query'];}  
                        $tipos_evaluaciones=$this->profesor_model->get_tipos_evaluaciones();
                        if($tipos_evaluaciones['bool']){$datos['tipos_evaluaciones']=$tipos_evaluaciones['query'];}  

                        $notas_alumnos=$this->profesor_model->get_notas_alumnos($Id_Profesor,$Id_C);
                        if($notas_alumnos['bool']){$datos['query_notas']=$notas_alumnos['query'];}

                        $datos['Id_C']=$Id_C;
                        $this->session->unset_userdata('curso');
                        $this->session->set_userdata('curso',$Id_C);
                        
                        $this->layout->view('profesor/cursos/cursos',$datos);
                    }
                    else
                    {
                       redirect('/');
                    }
                }
                else
                {
                    if(is_numeric($Id_Nota)&&!is_null($Id_Nota))
                    {
                        $this->registrar_notas($Id_Nota);
                    }
                    else
                    {
                        redirect('/');
                    }                    
                }
            }
            else
            {
                redirect('profesor/index');
            }
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'Error al verificar curso');
            echo $r;
        }
    }
    private function registrar_notas($Id_Set_Nota)
    {
        if(isset($Id_Set_Nota))
        {
            if($Id_Set_Nota>0)
            {
                $Id_Profesor=$this->ion_auth->get_profesor_id(); 
                $Id_C=$this->profesor_model->get_curso_set_nota($Id_Set_Nota,$Id_Profesor);
                if($Id_C['bool'])
                {
                    $datos['Id_C']=$Id_C['Id'];
                    $datos['fecha_actual']=$Id_C['Fecha'];

                    $set_notas_cursos=$this->profesor_model->get_set_notas_curso($Id_Profesor,$datos['Id_C']);       
                    if($set_notas_cursos['bool']){$datos['set_notas_cursos']=$set_notas_cursos['query'];}

                
                    $datos['se_puede_registrar']=true;
                    foreach ($datos['set_notas_cursos'] as $q) 
                    {               
                   
                        if(!$q->Registrada) 
                        {   
                            if($datos['fecha_actual']>$q->Fecha)
                            {
                                $datos['se_puede_registrar']=false;
                            }
                        }
                    }

                    $alumnos=$this->profesor_model->get_alumnos_curso($datos['Id_C']);
                    if($alumnos['bool']){$datos['alumnos']=$alumnos['query'];}

                    $notas_cursos=$this->profesor_model->get_notas_curso($Id_Profesor,$datos['Id_C']);
                    if($notas_cursos['bool']){$datos['notas_cursos']=$notas_cursos['query'];}    

                    $datos['url_regresar']=base_url('profesor/curso/'.$datos['Id_C']);
                    $datos['Id_set']=$Id_Set_Nota;
                    $this->layout->view('profesor/cursos/registrar_notas',$datos);
                }
                else
                {
                    redirect('profesor/index');
                }  
            }
            else
            {
                redirect('profesor/index');
            }            
        }
        else
        {
            redirect('profesor/index');
        }      
    }
    public function save_notas()
    {
        try
        {
            $Id_U=$this->ion_auth->get_user_id();
            $Id_Profesor=$this->ion_auth->get_profesor_id();        
            if($this->input->post('Id_Set') && $this->input->post('Notas'))
            {
                $Id_Set=$this->input->post('Id_Set');
                if($Id_Set>0)
                {
                    $notas=$this->input->post('Notas'); 
                    $Id_C=$this->profesor_model->get_curso_set_nota($Id_Set,$Id_Profesor);
                    if($Id_C['bool'])
                    {
                        $Id_C=$Id_C['Id'];
                        $alumnos=$this->profesor_model->get_alumnos_curso($Id_C);
                        if($alumnos['bool']){$can_alumno=$alumnos['Cantidad'];$alumnos=$alumnos['query'];}
                        if($can_alumno===count($notas))
                        {
                            $rows_alumnos = array(); 
                            $i=0;
                            foreach ($alumnos as $q) 
                            {
                                $rows_alumnos[]= array( 'Alumno_Id' => $q->Id_Alumno,
                                                        'Set_nota_Id'=>$Id_Set,
                                                        'hide'=>false,
                                                        'Nota'=>$notas[$i]);
                                $i++;
                            }
                            $save=$this->profesor_model->save_notas($rows_alumnos);
                            if($save['bool'])
                            {
                                $revisado=$this->profesor_model->revisado_set_nota($Id_Set);
                                $Apoderados=$this->profesor_model->get_apoderados_curso_nota($Id_C,$Id_Set);
                                
                                if($Apoderados['bool'])
                                {
                                    $Apoderados=$Apoderados['query'];
                                    $this->load->library('send_msg');
                                    $this->send_msg->Mandril_Masivo($Apoderados,'Se han registrado nuevos resultados','Resultado Evaluaciones','Resultado');
                                    $this->send_msg->twitter_Masivo($Apoderados,'Nuevos resultados de evaluaciones, revise el sistema');
                                    $this->send_msg->SMS_Masivo_nota($Apoderados);
                                    echo json_encode(array("bool"=>true,"msg"=>"Se ha guardado y notificado correctamente"));
                                }
                                else
                                {
                                   echo json_encode(array("bool"=>true,"msg"=>"Error de sistema: No se han encontrado apoderados registrados"));
                                }
                                
                            }
                            else
                            {
                                echo json_encode(array("bool"=>false,"msg"=>"Error de sistema: No se han guardado correctamente las evaluaciones"));
                            }                        
                        }
                        else
                        {
                            echo json_encode(array("bool"=>false,"msg"=>"Inconsistencia en la cantidad de alumnos registrados"));
                        }
                    }
                    else
                    {
                        echo json_encode(array("bool"=>false,"msg"=>"No puede registrar resultados de evaluaciones a al curso seleccionado"));
                    }
                    
                }
                else
                {
                    echo json_encode(array("bool"=>false,"msg"=>"El identificador del registro no es válido"));
                }
                
            }
            else
            {
                echo json_encode(array("bool"=>false,"msg"=>"No se ha logrado identificar el curso o las notas para registrar"));
            }
        }
        catch(Exception $e)
        {
             echo json_encode(array("bool"=>false,"msg"=>"Ha ocurrido un problema mientras se registraba, intente más tarde"));
        }     
    }
    public function loadAsg()
    {
        
        $options = "";
        if($this->input->post('cursos'))
        {
            $curso = $this->input->post('cursos');
            $Id_User=$this->ion_auth->get_user_id(); 
            $Id_User=36;//BORRAR AL TERMINAR
            $asignaturas = $this->profesor_model->load_asg($Id_User,$curso);
            if($asignaturas['bool'])
            {
                $asg=$asignaturas['query'];
                foreach($asg as $r)
                {
                    echo '<option value="'.$r->Id.'">'.$r->Nombre.'</option>';
                }
            }    
            else
            {
                echo '<option value="0">No hay asignaturas para este curso</option>';
            }
        }    
    }
    public function create_foro() // ALERTAS LISTAS
    {
        if($this->input->post('cha_Id') && $this->input->post('titulo') && $this->input->post('comentario'))
        {
            $cha_Id = $this->input->post('cha_Id');          
            if(is_numeric($cha_Id) && $cha_Id > 0)
            {
                $Titulo = (string)$this->input->post('titulo');
                $User_Id=$this->ion_auth->get_user_id();
                $Comentario =(string)$this->input->post('comentario');
                $foro = array('Curso_has_Asignatura_Id' => $cha_Id, 'Comentario'=>$Comentario,'Titulo'=>$Titulo);
                $query=$this->profesor_model->create_foro($foro,$User_Id);
                if($query['bool'])
                {
                    $new_list=$lista_foros=$this->update_foro($cha_Id);                                     
                    if($new_list['bool']) 
                    {             
                        echo json_encode(array("bool"=>true,"list"=>$new_list['list']));
                    }
                    else{echo json_encode(array("bool"=>false,"msg"=>"Error de sistema: recargue la página por favor"));}
                }else{echo json_encode(array("bool"=>false,"msg"=>"Error de sistema: No se ha logrado guardar el foro"));}                
            }else{echo json_encode(array("bool"=>false,"msg"=>"Error de sistema: No se ha logrado identificar la Asignatura"));}
        }else{echo json_encode(array("bool"=>false,"msg"=>"Error de sistema: No se ha logrado identificar el titulo, el comentario o la asignatura"));}
    }
    private function update_foro($Id)
    {
        $Id_U=$this->ion_auth->get_user_id();
        $new_list=$lista_foros=$this->profesor_model->get_foros_asg($Id_U,$Id);
        if($new_list['bool'])
        {
            $list=NULL;
            $count_foro=0;
            $code=null;
            foreach ($new_list['query'] as $q) 
            {
                if($q->Id_cha==$Id)
                {
                    if($q->Estado)
                    {
                         $Estado='<span class="badge badge-success">Abierto</span>';
                    }
                    else
                    {
                        $Estado='<span class="badge badge-danger">Cerrado</span>';
                    }
                    $list.='<tr>
                                <td>'.($count_foro+1).'</td>
                                <td><a href="/tesis/profesor/foro/'.$q->Id.'">'.$q->Titulo.'</a></td>
                                <td class="hidden-xs-portrait">'.$q->Comentario.'</td>
                                <td>'.$q->Fecha.'</td>
                                <td class="hidden-xs-portrait">'.$Estado.'</td>
                            </tr>'; 
                    $count_foro++;
                }
                
            }
            if($count_foro>0)
            {
                $count_foro=0;
                $code=' <table id="tabla_foro_'.$Id.'"" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Titulo</th>
                                <th class="hidden-xs-portrait">Comentario</th>
                                <th>Fecha</th>
                                <th class="hidden-xs-portrait">Estado</th>
                            </tr>
                            </thead>
                            <tbody>'.$list.'</tbody>
                        </table>';
            }
            $r = array('bool' => true, 'list'=> $code);
                    return $r;

        }        
        else
        {
            $r = array('bool' => false, 'msg'=> $error);
            return $r;
        }
    }
    public function foro($Id_foro)
    {
        $datos=NULL;
        $fail=false;
        $this->layout->set_specific_js(array('js/loader.js','js/forms-elemets.js','js/forms-article.js'));
        if(isset($Id_foro))
        {
            if(is_numeric($Id_foro) && 0 < $Id_foro)
            {
                $Id_U=$this->ion_auth->get_user_id();
                $respuesta=$this->profesor_model->verificar_profesor_foro($Id_U,$Id_foro);
                if($respuesta['bool'])
                {
                    $foro=$this->profesor_model->get_foro($Id_foro);   
                    if($foro['bool'])                 
                    {
                        $datos['profesor'] = true;              
                        $datos['first_foro'] = $foro['query'];
                        $respuestas=$this->profesor_model->get_respuesta_foro($Id_foro); 

                        if($respuestas['bool'])
                        {
                            $datos['respuestas_foro'] = $respuestas['query'];
                        }
                        $datos['url']="profesor";
                        $this->layout->view('app/foro',$datos); 

                    }else{$fail=true;}                                                      
                }else{$fail=true;}
            }else{$fail=true;}
        }else{$fail=true;}
        if($fail)
        {
            if($this->session->userdata("curso"))
            {
                 redirect('profesor/curso/'.$this->session->userdata("curso")); 
            }
            else
            {
                redirect('/');
            }
            
        }
    }    
    public function change_state_foro()
    {
        if($this->ion_auth->get_user_id())
        {
             $User_Id=$this->ion_auth->get_user_id();
             if($this->input->post('Id'))
             {
                $Id_foro=$this->input->post('Id');
                $save=$this->profesor_model->verificar_profesor_foro($User_Id,$Id_foro);
                if($save['bool'])
                {
                    $cambio=$this->profesor_model->change_state_foro($Id_foro);
                    if($cambio['bool'])
                    {
                        if($cambio['Estado'])
                        {
                            $btn='<button style="width:100%" type="button" class="btn btn-danger btn-lg .change_state" data-placement="top" data-original-title=".btn .btn-info">
                                    &nbsp;<span><i class="fa fa-comments fa-lg"></i> Cerrar Foro</span>&nbsp;
                                </button>';
                        }
                        else
                        {
                            $btn='<button style="width:100%" id="change_state" type="button" class="btn btn-success btn-lg .change_state" data-placement="top" data-original-title=".btn .btn-info">
                                    &nbsp;<span><i class="fa fa-comments fa-lg"></i> Abrir Foro</span>&nbsp;
                                </button>';
                        }
                        echo json_encode(array("bool"=>true,"btn"=>$btn));
                    }
                    else{echo json_encode(array("bool"=>false));}
                }
                else
                {
                    echo json_encode(array("bool"=>false));
                }
             }
             else
             {echo json_encode(array("bool"=>false));}            
             
        }
        else
        {
            echo json_encode(array("bool"=>false));
        }
    }
    public function save_comentario()
    {
        try
        {
            $Id_foro = $this->input->post('id_foro');
            if(isset($Id_foro))
            {
                if(is_numeric($Id_foro) && $Id_foro>0)
                {

                    $User_Id=$this->ion_auth->get_user_id();
                    $comentario = (string)$this->input->post('comentario');
                    $save=$this->profesor_model->save_comentario_foro($Id_foro,$User_Id,$comentario);    
                                    
                    if($save['bool'])
                    {
                        $list=$this->update_comentarios($Id_foro);
                        if($list['bool'])
                        {
                            echo json_encode(array("bool"=>true,"respuestas"=>$list['code']));
                        }
                        else
                        {
                            echo json_encode(array("bool"=>false,'msg'=>"Error interno de sistema: No se encontraron respuestas"));
                        }
                    }
                    else
                    {
                        echo json_encode(array("bool"=>false,'msg'=>"Error interno de sistema: no se guardó su respuesta"));
                    }
                }
                else
                {
                    echo json_encode(array("bool"=>false,'msg'=>"Error en el identificador del Foro"));
                }
            }
            else
            {
                 echo json_encode(array("bool"=>false,'msg'=>"Error interno de sistema, intente mas tarde"));
            }
        }
        catch(Exception $e)
        {
             echo json_encode(array("bool"=>false,'msg'=>"Error interno de sistema, intente mas tarde"));
        }
    }
    private function update_comentarios($Id_foro)
    {
        if(isset($Id_foro))
        {
            if(is_numeric($Id_foro))
            {
                $foro=$this->profesor_model->get_foro($Id_foro);   
                if($foro['bool'])                 
                {
                    $Id_U=$this->ion_auth->get_user_id();
                    $respuesta=$this->profesor_model->verificar_profesor_foro($Id_U,$Id_foro);
                    if($respuesta['bool'])
                    {
                        $respuestas=$this->profesor_model->get_respuesta_foro($Id_foro);
                        if($respuestas['bool'])
                        {
                            $dt="left";
                            $code=null;
                            $Titulo_foro=$foro['query']->Titulo; 
                            foreach ($respuestas['query'] as $q) 
                            {
                                    $nombre=$q->first_name;
                                    $nombre=preg_split("/[\s,]+/",$nombre);
                                    $apellido=$q->last_name;
                                    $apellido=preg_split("/[\s,]+/",$apellido);
                                    $name=$nombre[0].' '.$apellido[0]; 
                                    if($q->Id_User===$Id_U){$type_user="Profesor";}else{$type_user="Alumno";}
                                    $msn_dir="";
                                    $img_dir=" pull-left";                                    
                                    $code.=' <section class="search-result">                            
                                                <header><h4>Re:'.$Titulo_foro.'</h4></header>
                                                <div id="chat" class="chat">
                                                    <div id="chat-messages" class="chat-messages">
                                                        <div class="chat-message">
                                                            <div class="sender'.$img_dir.'">
                                                                <div class="icon">
                                                                    <img src="'.base_url().'assets/bootstrap/img/user.png" class="img-circle" alt="">
                                                                </div>
                                                                <div class="time">
                                                               '.$type_user.'
                                                                </div>
                                                            </div>
                                                            <div class="chat-message-body'.$msn_dir.'">
                                                                <span class="arrow"></span>
                                                                <div class="sender"><i class="eicon eicon-user"></i><a href="#">'.$name.'</a> - <i class="eicon eicon-clock"></i>'.$q->Fecha.'</div>
                                                                <div class="text">
                                                                    '.$q->Comentario.'
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>';
                            }
                            $r = array('bool' => true, 'code'=> $code);
                            return $r;

                        }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
                    }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
                }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
            }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
        }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
    }
    public function create_anotacion()
    {
        try
        {
            $Id_Alumno=$this->input->post('Id_Alumno');
            if(isset($Id_Alumno))
            {
                if(is_numeric($Id_Alumno) && 0 < $Id_Alumno)
                {
                    $Alumno_Id=$this->alumno_model->get_Alumno_Id($Id_Alumno);
                    if($Alumno_Id['bool'])
                    {
                        $Id_Alumno=$Alumno_Id['Id'];
                        $User_Id=$this->ion_auth->get_user_id();
                        $comentario=$this->input->post('comentario');
                        $tipo=$this->input->post('tipo');
                        $save_array=array(  'Descripcion'=>$comentario,
                                            'Tipo'=>$tipo,
                                            'Alumno_Id'=>$Id_Alumno,
                                            'Profesor_Id'=>$User_Id);
                        $save=$this->profesor_model->save_anotaciones($save_array);
                        if($save['bool'])
                        {
                            echo json_encode(array("bool"=>true));
                        }
                        else
                        {
                            echo json_encode(array("bool"=>false,"msg"=>"Error de sistema: No se ha guardado correctamente"));
                        }
                    }
                    else
                    {
                        echo json_encode(array("bool"=>false,"msg"=>"Error de sistema: No se ha logrado identificar el alumno"));                        
                    }
                }
                else
                {
                    echo json_encode(array("bool"=>false,"msg"=>"Error de sistema: No se ha logrado identificar el alumno"));
                }
            }
            else
            {
                echo json_encode(array("bool"=>false,"msg"=>"Error de sistema: No se ha logrado identificar el alumno"));
               
            }
        }
        catch(Exception $e)
        {
            echo json_encode(array("bool"=>false, "msg"=>"Error interno de sistema"));
            
        }
    } 
    public function save_setting_eva()
    {
        try
        {
            if($this->input->post('can_eva') && $this->input->post('Id_Cha'))
            {
                $can_eva=$this->input->post('can_eva'); 
                $Id_Cha=$this->input->post('Id_Cha'); 
                if(is_numeric($can_eva) && is_numeric($Id_Cha))
                {
                    if($this->input->post('fecha_array') && $this->input->post('tipo_array'))
                    {
                        $tipo_a=$this->input->post('tipo_array'); 
                        $fecha_a=$this->input->post('fecha_array'); 
                        if((count($tipo_a) == count($fecha_a)) &&(count($fecha_a) == $can_eva))
                        { 
                            $tipos="";
                            $fechas="";
                            $tabla_fin=array();
                            for($i=0;$i<count($tipo_a);$i++)
                            {
                                $tipo_texto=ucwords(strtolower($tipo_a[$i]));
                                $explode = explode('/',$fecha_a[$i]);
                                $dia = $explode[0];
                                $mes = $explode[1];
                                $año = $explode[2];
                                $fecha = $mes.'/'.$dia.'/'.$año;                    
                                $fecha = date('Y/m/d',strtotime($fecha));
                                $tabla = array('Curso_has_asignatura_Id'=>$Id_Cha,
                                             'Tipo' =>$tipo_texto,
                                             'Fecha'=>$fecha,
                                             'hide' =>false);
                                $tabla_fin[]=$tabla;
                            }   
                            $save=$this->profesor_model->save_setting_nota($tabla_fin);                            
                            if($save['bool'])
                            { 
                                $cantidad=$this->profesor_model->update_Notas_Curso_has_Asignatura($Id_Cha,$can_eva);
                                if($cantidad['bool'])
                                {
                                    echo json_encode(array("bool"=>true));
                                }
                                else
                                {
                                    echo json_encode(array("bool"=>true,"msg"=>" Error al guardar cantidad de evaluaciones"));
                                }
                            }  
                            else
                            { 
                                $delete=$this->profesor_model->delete_setting_nota($Id_Cha);
                                echo json_encode(array("bool"=>false,"msg"=>" Error Interno de Apple's Teacher... Intente nuevamente")); 
                            }    

                        }
                        else{echo json_encode(array("bool"=>false,"msg"=>"La cantidad de Fechas y Evaluaciones son diferentes"));}                       
                    }
                    else{echo json_encode(array("bool"=>false,"msg"=>"Error Arrays"));}
                }
                else{echo json_encode(array("bool"=>false,"msg"=>"Error al encontrar valores numericos en ID")); }            
            }
            else{echo json_encode(array("bool"=>false,"msg"=>"No hay Id"));}
        }
        catch(Exception $e){echo json_encode(array("bool"=>false,"msg"=>"Error Sistema"));}
    }
    public function save_edit_setting_eva()
    {
        try
        {
            if($this->input->post('can_eva') && $this->input->post('Id_Cha'))
            {
                $can_eva=$this->input->post('can_eva'); 
                $Id_Cha=$this->input->post('Id_Cha'); 
                if(is_numeric($can_eva) && is_numeric($Id_Cha))
                {
                    if($this->input->post('fecha_array') && $this->input->post('tipo_array') && $this->input->post('id_array')) 
                    {
                        $tipo_a=$this->input->post('tipo_array'); 
                        $fecha_a=$this->input->post('fecha_array'); 
                        $id_a=$this->input->post('id_array');
                        if((count($tipo_a) == count($fecha_a)) && (count($fecha_a) == $can_eva) && (count($id_a) == count($tipo_a)))
                        { 
                            $tipos="";
                            $fechas="";
                            $fila=array();
                            for($i=0;$i<count($tipo_a);$i++)
                            {
                                $tipo_texto=ucwords(mb_strtolower($tipo_a[$i]));  
                                $explode = explode('/',$fecha_a[$i]);
                                $dia = $explode[0];
                                $mes = $explode[1];
                                $año = $explode[2];
                                $fecha = $mes.'/'.$dia.'/'.$año;                    
                                $fecha = date('Y/m/d',strtotime($fecha));
                                $fila = array('Tipo' =>$tipo_texto,'Fecha'=>$fecha);
                                $update=$this->profesor_model->edit_setting_nota($id_a[$i],$fila);
                                if($update['bool'])
                                {
                                    $_update=true;
                                }
                                else
                                {
                                    $_update=false;
                                    break;
                                }
                            }                             
                            if($_update)
                            { 
                                echo json_encode(array("bool"=>true));
                            }  
                            else
                            { 
                                echo json_encode(array("bool"=>false,"msg"=>" Error Interno de Apple's Teacher... Intente nuevamente más tarde")); 
                            }    

                        }
                        else{echo json_encode(array("bool"=>false,"msg"=>"La cantidad de Fechas y Evaluaciones son diferentes"));}                       
                    }
                    else{echo json_encode(array("bool"=>false,"msg"=>"Error Arrays"));}
                }
                else{echo json_encode(array("bool"=>false,"msg"=>"Error al encontrar valores numericos en ID")); }            
            }
            else{echo json_encode(array("bool"=>false,"msg"=>"No hay Id"));}
        }
        catch(Exception $e){echo json_encode(array("bool"=>false,"msg"=>"Error Sistema"));}
    }
    public function mi_curso($Id)
    {

        if(isset($Id))
        {
            if($Id>0)
            {                
                $data="";
                $Id_Profesor=$this->ion_auth->get_profesor_id();             
                $verificar=$this->profesor_model->verificar_curso_Jefe($Id_Profesor,$Id);                     
                if($verificar['bool'])
                { 

                    $My_Curso=$this->profesor_model->get_curso_jefe($Id,$Id_Profesor);   
                    if($My_Curso['bool']){$data['My_Curso']=$My_Curso['query'];}

                    $Lista_Alumnos=$this->profesor_model->get_alumnos_curso($Id); 

                    $Lista_Apoderado=$this->profesor_model->get_apoderados_curso($Id);               

                    $Hoja_Vida=$this->profesor_model->get_Hoja_vida($Id);            

                    $reuniones=$this->profesor_model->get_reuniones($Id);

                    $asignatura=$this->profesor_model->get_asignaturas_jefe($Id);

                    $set_notas_cursos=$this->profesor_model->get_all_set_notas($Id);

                    $notas=$this->profesor_model->get_all_notas($Id);

                    $data['pan_1']=$this->micurso->Mi_Curso($My_Curso,$asignatura,$Lista_Alumnos,$Lista_Apoderado,$reuniones,$notas,$set_notas_cursos);
               
                    $data['Id_Profesor']=$Id_Profesor;

                    $this->layout->set_specific_js(array('js/loader.js','js/forms-elemets.js','lib/jquery.dataTables.min.js','js/ui-dialogs.js','js/tables-dynamic.js'));
                    
                    $this->layout->view('profesor/jefe/mi_curso',$data);
                }
                else
                {
                  redirect(base_url('profesor/'));
                }
            }
            else
            {
                redirect(base_url('profesor/'));
            }
        } 
        else
        {
            redirect('/');
        }
    }
    public function informe_notas($id_alumno)
    {
        try
        {
            if(!empty($id_alumno))
            {
                if(is_numeric($id_alumno) && $id_alumno > 0)
                {
                    $id_alumno=$this->profesor_model->get_Alumno_Id($id_alumno)['query']->Id;
                    $alumno = $this->administracion_model->all_Alumno($id_alumno);
                    $mis_notas = $this->alumno_model->get_Mis_Evaluaciones($id_alumno);
                    $datos['notas']=$this->apoderado_model->get_notas_alumno($id_alumno);
                    $datos['set_notas'] = $this->apoderado_model->get_set_notas_alumno($id_alumno);
                    $datos['asignaturas'] = $this->apoderado_model->get_asignaturas_alumno($id_alumno);
                    //var_dump($mis_notas2);
                    $curso = $this->alumno_model->get_Curso_Id($id_alumno);
                    $datos['mis_notas'] = $mis_notas['query'];
                    //var_dump($datos['mis_notas']);
                    $datos['salida'] = $alumno['query'];
                    $datos['curso'] = $curso['query'];
                    $curso_profesor = $datos['curso']->Profesor_Id;
                    $profesor = $this->administracion_model->all_Profesor2($curso_profesor);
                    $datos['profesor'] = $profesor['query'];
                    $apoderado = $this->administracion_model->all_Apoderado(0,$alumno['query']->Apoderado_Id);
                    $datos['apoderado'] = $apoderado['query'];
                    $this->layout->set_specific_js(array('js/print.js'));
                    $this->layout->view('profesor/cursos/informe_notas',$datos);
                    /*$html = $this->load->view('profesor/cursos/informe_notas',$datos,true);
                    $pdfFilePath = "Informe_notas_2015.pdf";
                    $this->load->library('m_pdf');
                    $pdf = $this->m_pdf->load();
                    $pdf->WriteHTML($html);
                    $pdf->Output($pdfFilePath, "D");*/
                }                
            }
            else
            {
                redirect('profesor/index');
            }
            
            
        }
        catch(Exception $ex){
            redirect('profesor/index');
        }
    }
    public function show_ficha_alumno()
    {
        if($this->input->post('id') && $this->input->post('Curso'))
        {
            if(is_numeric($this->input->post('id')) && is_numeric($this->input->post('Curso')) && $this->input->post('id') > 0 && $this->input->post('Curso') > 0)
            {
                $alumno=$this->profesor_model->show_alumno($this->input->post('id'),$this->input->post('Curso'),$this->ion_auth->get_profesor_id());
                if($alumno['bool'])
                {
                    $alumno=$alumno['query'];
                    $fecha=date("d/m/Y",strtotime($alumno->f_nacimiento));
                    if($alumno->sex===1)
                    {
                        $sexo="Masculino";
                    }
                    else
                    {
                        $sexo="Femenino";
                    }
                    $code='<div id="modal_alumno" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">x</button>
                                <h4 class="modal-title" id=""><i class="fa fa-eye"></i> Ficha de Alumno</h4>
                            </div>
                            <div class="modal-body">
                                <table width="100%">
                                    
                                    <tr style="background-color:withe">
                                        <td  style="background-color:white;">
                                            <h5>Run</h5>                                            
                                        </td>
                                        <td  style="background-color:white;">
                                            '.$alumno->username.'
                                        </td>
                                    </tr>
                                     <tr style="background-color:withe">
                                        <td  style="background-color:white;">
                                            <h5>Nombre</h5>                                            
                                        </td>
                                        <td  style="background-color:white;">
                                            '.$alumno->first_name.'
                                        </td>
                                    </tr>
                                     <tr style="background-color:withe">
                                        <td  style="background-color:white;">
                                            <h5>Apellido</h5>                                            
                                        </td>
                                        <td  style="background-color:white;">
                                            '.$alumno->last_name.'
                                        </td>
                                    </tr>
                                     <tr style="background-color:withe">
                                        <td  style="background-color:white;">
                                            <h5>Fecha Nacimiento</h5>                                            
                                        </td>
                                        <td  style="background-color:white;">
                                            '.$fecha.'
                                        </td>
                                    </tr>
                                     <tr style="background-color:withe">
                                        <td  style="background-color:white;">
                                            <h5>Sexo</h5>                                            
                                        </td>
                                        <td  style="background-color:white;">
                                            '.$sexo.'
                                        </td>
                                    </tr>
                                </table>
                                <hr style="height: 1px; background-color: rgb(85, 85, 85)"></hr>
                                <table width="100%">
                                    
                                    <tr style="background-color:withe">
                                        <td  style="background-color:white;">
                                            <h5>Tipo de Sangre</h5>                                            
                                        </td>
                                        <td  style="background-color:white;">
                                            '.$alumno->Tipo_Sangre.'
                                        </td>
                                    </tr>
                                     <tr style="background-color:withe">
                                        <td  style="background-color:white;">
                                            <h5>Alergias</h5>                                            
                                        </td>
                                        <td  style="background-color:white;">
                                            '.$alumno->Alergias.'
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>';
                    echo json_encode(array("bool"=>true,"tabla"=>$code));
                }
                else
                {
                    echo json_encode(array("bool"=>false,"tabla"=>'No se encontró el alumno'));
                }
            }
            else
            {
                echo json_encode(array("bool"=>false,"tabla"=>'Los valores ingresados son incorrectos'));
            }
        }
        else
        {
            echo json_encode(array("bool"=>false,"tabla"=>'Faltan valores por ingresar'));
        }  
    }
    public function save_reunion()
    {
        if($this->input->post('Fecha') && $this->input->post('Curso') && $Motivo=$this->input->post('Motivo') && $hora=$this->input->post('Hora'))
        {
            $fecha=$this->input->post('Fecha'); 
            $hora=$this->input->post('Hora'); 
            $Motivo=$this->input->post('Motivo'); 
            $Id_Curso=$this->input->post('Curso'); 
            if($Id_Curso>0)
            {
                
                $explode = explode('/',$fecha);
                $dia = $explode[0];
                $mes = $explode[1];
                $año = $explode[2];
                $fecha = $año.'-'.$mes.'-'.$dia;                   
                //$fecha = date('d-m-yyyy',strtotime($fecha));
                $save=$this->profesor_model->save_reunion($Id_Curso,$fecha,$hora,$Motivo);
                if($save['bool'])
                {
                    $Apoderados=$this->profesor_model->get_apoderados_curso($Id_Curso);
                    if($Apoderados['bool'])
                    {
                        $this->send_msg->twitter_Masivo($Apoderados['query'],'Se ha registrado una reunión: '.$Motivo.' para el '.$dia.'/'.$mes.'/'.$año.' a las '.$hora);
                    }
                    $reuniones=$this->profesor_model->get_reuniones($Id_Curso);
                    if($reuniones['bool'])
                    {
                        $reuniones=$reuniones['query'];
                        $horas="";
                        $i=1;
                        foreach ($reuniones as $q) 
                        {
                            $horas.='<tr><td>'.$i.'</td><td>'.$q->Motivo.'</td><td>'.$q->Fecha.'</td><td>'.$q->Hora.'</td></tr>';
                            $i++;
                        }
                        $tabla='<table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="color:#ffffff">#</th>
                                            <th style="color:#ffffff">Motivo</th>
                                            <th style="color:#ffffff">Fecha</th>
                                            <th style="color:#ffffff">Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>  
                                        '.$horas.'                                      
                                    </tbody>
                                </table>';
                        echo json_encode(array("bool"=>true,"tabla"=>$tabla));
                    }
                    else
                    {
                         echo json_encode(array("bool"=>false));
                    }     
                }
                else
                {
                     echo json_encode(array("bool"=>false,"msg"=>"Error de Sistema: No se logró guardar reunión"));
                }
            }
            else
            {
                echo json_encode(array("bool"=>false,"msg"=>"Error de Sistema: No se ha encontrado Curso para registrar"));
            }
        }
        else
        {
            echo json_encode(array("bool"=>false,"msg"=>"Error de Sistema: No se han encontrado valores para registrar"));
        }
    }
    public function edit_reunion()
    {
        if($this->input->post('Id') && $this->input->post('Fecha') && $this->input->post('Curso') && $Motivo=$this->input->post('Motivo') && $hora=$this->input->post('Hora'))
        {
            $id_reunion=$this->input->post('Id');
            $fecha=$this->input->post('Fecha'); 
            $hora=$this->input->post('Hora'); 
            $Motivo=$this->input->post('Motivo'); 
            $Id_Curso=$this->input->post('Curso'); 
            if($Id_Curso>0)
            {
                
                $explode = explode('/',$fecha);
                $dia = $explode[0];
                $mes = $explode[1];
                $año = $explode[2];
                $fecha = $año.'-'.$mes.'-'.$dia;       
               // $fecha = mktime(0,0,0,$mes,$dia,$año);
               // $fecha = date('Y-m-d',strtotime($fecha));
                
                $save=$this->profesor_model->edit_reunion($id_reunion,$fecha,$hora,$Motivo);
                if($save['bool'])
                {
                    //NOTIFICAR
                    $Apoderados=$this->profesor_model->get_apoderados_curso($Id_Curso);

                    if($Apoderados['bool'])
                    {
                       $this->send_msg->twitter_Masivo($Apoderados['query'],'Se ha registrado una reunión: '.$Motivo.' para el '.$dia.'/'.$mes.'/'.$año.' a las '.$hora);
                    }
                    $reuniones=$this->profesor_model->get_reuniones($Id_Curso);
                    if($reuniones['bool'])
                    {
                        $reuniones=$reuniones['query'];
                        $horas="";
                        $i=1;
                        foreach ($reuniones as $q) 
                        {
                            $horas.='<tr><td>'.$i.'</td><td>'.$q->Motivo.'</td><td>'.$q->Fecha.'</td><td>'.$q->Hora.'</td></tr>';
                            $i++;
                        }
                        $tabla='<table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="color:#ffffff">#</th>
                                            <th style="color:#ffffff">Motivo</th>
                                            <th style="color:#ffffff">Fecha</th>
                                            <th style="color:#ffffff">Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>  
                                        '.$horas.'                                      
                                    </tbody>
                                </table>';
                        echo json_encode(array("bool"=>true,"tabla"=>$tabla));
                    }
                    else
                    {
                       echo json_encode(array("bool"=>false,"msg"=>"Error de Sistema: No se encontraron reuniones")); 
                    }     
                }
                else
                {
                     echo json_encode(array("bool"=>false,"msg"=>"Error de Sistema: No se logró registrar la reunión")); 
                }
            }
            else
            {
                echo json_encode(array("bool"=>false,"msg"=>"Error de Sistema: No se ha encontrado Curso para registrar")); 
            }
        }
        else
        {
            echo json_encode(array("bool"=>false,"msg"=>"Error de Sistema: No se han encontrado valores para registrar"));        
        }
    }
    public function delete_reunion()
    {
        if($this->input->post('Id_Reunion') && $this->input->post('Id_Curso'))
        {
            $curso=$this->input->post('Id_Curso');
            $id=$this->input->post('Id_Reunion'); 
            if($id>0 && $curso>0)
            {
                $delete=$this->profesor_model->delete_reunion($id);
                if($delete['bool'])
                {
                    $Apoderados=$this->profesor_model->get_apoderados_curso($curso);
                    if($Apoderados['bool'])
                    {
                        $this->send_msg->twitter_Masivo($Apoderados['query'],'Se ha Eliminado la reunión');
                    }
                    $reuniones=$this->profesor_model->get_reuniones($curso);
                    if($reuniones['bool'])
                    {
                        $reuniones=$reuniones['query'];
                        $horas="";
                        $i=1;
                        foreach ($reuniones as $q) 
                        {
                            $horas.='<tr><td>'.$i.'</td><td>'.$q->Motivo.'</td><td>'.$q->Fecha.'</td><td>'.$q->Hora.'</td></tr>';
                            $i++;
                        }
                        $tabla='<table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="color:#ffffff">#</th>
                                            <th style="color:#ffffff">Motivo</th>
                                            <th style="color:#ffffff">Fecha</th>
                                            <th style="color:#ffffff">Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>  
                                        '.$horas.'                                      
                                    </tbody>
                                </table>';
                        echo json_encode(array("bool"=>true,"tabla"=>$tabla));
                    }
                    else
                    {
                         echo json_encode(array("bool"=>false));
                    }     
                }
                else
                {
                     echo json_encode(array("bool"=>false));
                }
            }
            else
            {
                echo json_encode(array("bool"=>false));
            }
        }
        else
        {
            echo json_encode(array("bool"=>false));
        }
    }
    public function save_edit_nota() //ARREGLAR
    {       
        if($this->input->post('Nota') && $this->input->post('Comentario') && $this->input->post('Id_alumno') && $this->input->post('Id_Set'))
        {  

            $Nota_nueva=$this->input->post('Nota');
            $Comentario=$this->input->post('Comentario'); 
            $Id_Alumno= $this->input->post('Id_alumno');
            $Id_Set=$this->input->post('Id_Set');
            $Id_Alumno=$this->profesor_model->get_Alumno_Id($Id_Alumno);
            if($Id_Alumno['bool'])
            {           
                $Id_Nota=$this->profesor_model->get_Id_Nota($Id_Set,$Id_Alumno['query']->Id);
                if($Id_Nota['bool'])
                {   
                    $nota_vieja=$Id_Nota['Nota'];
                    $motivo_cambio=$this->profesor_model->save_cambio_nota($Nota_nueva,$nota_vieja->Nota,$Comentario,$nota_vieja->Id);
                    if($motivo_cambio['bool'])
                    {                        
                        $cambio=$this->profesor_model->update_set_nota($Nota_nueva,$Id_Alumno['query']->Id,$Id_Set);
                        if($cambio['bool'])
                        {
                            echo json_encode(array("bool"=>true));
                        }
                        else
                        {
                            echo json_encode(array("bool"=>false));
                        }
                    }
                    else
                    {
                        echo json_encode(array("bool"=>false));
                    }
                }
                else
                {
                    echo json_encode(array("bool"=>false));
                }
            }
            else
            {
                echo json_encode(array("bool"=>false));
            }
        }
        else
        {
             echo json_encode(array("bool"=>false));
        }
    }
    public function asistencia($Id_Cha)
    {
        if(isset($Id_Cha))
        { 
            if($Id_Cha>0)
            {
                $Id_U=$this->ion_auth->get_user_id();
                $Id_Profesor=$this->profesor_model->get_Profesor_Id($Id_U);                
                if($Id_Profesor['bool']){$Id_Profesor=$Id_Profesor['Id'];}

                $curso=$this->profesor_model->get_Id_curso($Id_Cha);
                if($curso['bool']){$Id_C=$curso['Id'];}

                $asi=$this->profesor_model->get_asignatura_asistencia($Id_Cha);
                if($asi['bool']){$datos['asi']=$asi['query'];}
                $curso=$this->profesor_model->get_curso_asistencia($Id_C);
                if($curso['bool']){$datos['curso']=$curso['query'];}

                $alumnos=$this->profesor_model->get_alumnos_curso($Id_C);
                if($alumnos['bool']){$datos['alumnos']=$alumnos['query'];}
                             
                $datos['Id_Cha']=$Id_Cha;
                $datos['Id_C']=$Id_C;
                $this->layout->view('profesor/cursos/save_asistencia',$datos);
            }
            else
            {
                redirect('profesor/index');
            }
        }
        else
        {
            redirect('profesor/index');
        }


    }
    public function save_asistencia()
    {
        if($this->input->post('presente') && $this->input->post('Id_Cha'))
        {  
             $Presente=$this->input->post('presente');
             $Id_Cha=$this->input->post('Id_Cha');
             if($Id_Cha>0)
             {
                $Id_U=$this->ion_auth->get_user_id();
                $Id_Profesor=$this->profesor_model->get_Profesor_Id($Id_U);                
                if($Id_Profesor['bool']){$Id_Profesor=$Id_Profesor['Id'];}

                $asi=$this->profesor_model->get_asignatura_asistencia($Id_Cha);
                if($asi['bool']){$datos['asi']=$asi['query'];}

                $curso=$this->profesor_model->get_curso_asistencia($Id_C);
                if($curso['bool']){$curso=$curso['query'];}

                $alumnos=$this->profesor_model->get_alumnos_curso($Id_C);
                if($alumnos['bool']){$alumnos=$alumnos['query'];}

                    $i=0;
                    $rows=array();
                    foreach ($alumnos as $q) 
                    {
                        $rows[]=array('Fecha_asistencia'=>date('Y-m-d'),
                                        'Alumno_Id'=>$q->Id_Alumno,
                                        'Curso_has_Asignatura_Id'=>$Id_Cha,
                                        'presente'=>$Presente[$i],
                                        'hide'=>false);   
                        $i++;                     
                    }
                    $save=$this->profesor_model->guarda_asistencia($rows);
                    if($save['bool'])
                    {
                        echo json_encode(array("bool"=>true));
                    }
                    else
                    {
                        echo json_encode(array("bool"=>false));
                    }
             }
             else
            {
                 echo json_encode(array("bool"=>false));
            }
        }
        else
        {
             echo json_encode(array("bool"=>false));
        }
    }
    public function anotaciones_alumno()
    {
        $tbody='No se encontraron anotaciones';
        if($this->input->post('id'))
        {
            if(is_numeric($this->input->post('id')) && $this->input->post('id')>0)
            {
                $Alumno_Id=$this->alumno_model->get_Alumno_Id($this->input->post('id'));
                if($Alumno_Id['bool'])
                {
                    $anotaciones = $this->apoderado_model->get_hojaVida_alumno($Alumno_Id['Id']);
                    if($anotaciones['bool'])
                    {
                        $tbody='';
                        foreach ($anotaciones['query'] as $q) 
                        {
                            if($q->Tipo){ $Tipo='<span class="label label-success">Positiva</span>'; }else{ $Tipo='<span class="label label-danger">Negativa</span>'; }
                            $tbody.='<tr><td>'.$Tipo.'</td><td>'.$q->Descripcion.'</td></tr>';                   
                        }               
                    
                        $code='<div id="modal_alumno" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">x</button>
                                            <h4 class="modal-title" id=""><i class="fa fa-eye"></i> Anotaciones de Alumno</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Tipo</th>
                                                        <th>Descripcion</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                '.$tbody.' 
                                                </tbody>                                           
                                            </table>                           
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                            echo json_encode(array("bool"=>true,'tabla'=>$code));
                    }
                    else
                    {
                        echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: No se han encontrado anotaciones'));
                    }
                }
                else
                {
                    echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: No se ha encontrado Alumno'));
                }
            }
            else
            {
                echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: Error en el identificador del Alumno'));
            }
            
            
        }
        else
        {
            echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: No se encontró el Identificador de Alumno'));
        }
        
    }
    public function editar_foro()
    {
        if($this->input->post('Id') && $this->input->post('Comentario') && $this->input->post('Titulo'))
        {
            $id=$this->input->post('Id');
            if($id>0 && is_numeric($id))
            {
                if(strlen($this->input->post('Comentario'))>0 && strlen($this->input->post('Titulo'))>0)
                {
                    $Id_U=$this->ion_auth->get_user_id();
                    $respuesta=$this->profesor_model->verificar_profesor_foro($Id_U,$id);
                    if($respuesta['bool'])
                    {
                        $fila = array('Comentario' =>$this->input->post('Comentario'),'Titulo'=>$this->input->post('Titulo'));
                        $editado=$this->profesor_model->edit_foro($id,$fila);
                        if($editado['bool'])
                        {
                            echo json_encode(array("bool"=>true));
                        }
                        else
                        {
                            echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: No se editó correctamente, intente más tarde'));
                        }
                    }
                    else
                    {
                        echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: No tiene permisos para editar foro'));
                    }
                }
                else
                {
                    echo json_encode(array("bool"=>false,'msg'=>'No se encontró titulo o comentario de foro'));
                }
                
            }
            else
            {
                echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: Identificador de Foro incorrecto'));
            }
        }
        else
        {
            echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: No se han encontrado valores para registrar'));
        }
    }
    public function eliminar_foro()
    {
        if($this->input->post('Id'))
        {
            $id=$this->input->post('Id');
            if($id>0 && is_numeric($id))
            {
                $fila = array('hide' =>true);
                $Id_U=$this->ion_auth->get_user_id();
                $respuesta=$this->profesor_model->verificar_profesor_foro($Id_U,$id);
                if($respuesta['bool'])
                {
                    $editado=$this->profesor_model->edit_foro($id,$fila);
                    if($editado['bool'])
                    {
                        echo json_encode(array("bool"=>true));
                    }
                    else
                    {
                        echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: No se editó correctamente, intente más tarde'));
                    }
                }
                else
                {
                    echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: No tiene permisos para eliminar foro'));
                }
                
            }
            else
            {
                echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: Identificador de Foro incorrecto'));
            }
        }
        else
        {
            echo json_encode(array("bool"=>false,'msg'=>'Error de sistema: No se han encontrado valores para registrar'));
        }
    }
    



}
?>
