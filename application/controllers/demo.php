<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Demo extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
        
		$this->load->library('twilio');
		$this->load->library('session');
    	$this->load->library('ion_auth');
		$this->load->library('tw_msg');
		$this->load->library('Mandrill');
		$this->load->library('whatsapp');
		$this->load->library('security_lib');
		$this->load->helper('app');
		$this->load->model('app_model');
		$this->load->model('ion_auth_model');
		$this->CI =& get_instance();
		//$this->load->config('mandrill');

	}

	function index()
	{
		//$this->verificar();
        $Apoderados=$this->profesor_model->get_apoderados_curso(5);
        $Id_bueno=array();
        $Mensaje="HOLI";
       	$this->CI->config->load('twitter');
			$this->CI->load->library('twitteroauth');
			$consumer=$this->CI->config->item('twitter_consumer_token');
			$consumer_secret=$this->CI->config->item('twitter_consumer_secret');
			$token=$this->CI->config->item('twitter_access_token');
			$token_secret=$this->CI->config->item('twitter_access_secret');
			$connection = $this->CI->twitteroauth->create(	$consumer, $consumer_secret,$token,$token_secret);
			foreach ($Apoderados['query'] as $t) 
			{
				

				if($t->Id_twitter!=NULL)
				{
					if($t->Id_twitter>0)
					{
						echo $t->Id_twitter;
						$Id_bueno[]=$t->Id_twitter;
					}
				}
			}
			var_dump($Id_bueno);
			if(count($Id_bueno)>0)
			{
				for($i=0;$i<count($Id_bueno);$i++)
				{

					$options = array("user_id" => $Id_bueno[$i], "text" => $Mensaje);
					$result = $connection->post('direct_messages/new', $options);
					if(isset($result->errors)){ var_dump($result);}
				}
				echo "SI";
			}
			else
			{ echo "NO";}		
	}
	function sms()
	{		

		$var = array('type' =>'password' , 'pass'=>'aE3c#s', 'to'=>'133456789');
		$r=$this->tw_msg->send($var);
		if($r['bool'])
		{
			echo $r['msg'];
		}
		else
		{
			echo $r['msg'];
		}
	}
	function group()
	{
		$v=$this->ion_auth->get_profesor_id();
		var_dump($v);
	}
	function whatsapp()
	{
		try
		{
			$intentos=1; //Intentos simultaneos => 10|20|50|100/
			$num=array('+56972363570');
			
				$datos['numero']=$num[0];
				for($i=1; $i<=$intentos;$i++)
				{
					$datos['mensaje']='Holi '.$i.'/'.$intentos;
					$r=$this->whatsapp->send_wsp($datos);
					if($r)
					{
						echo "Mensaje enviado a ".$datos['numero'].' | '.$i.'/'.$intentos.'<br>';
					}
					else
					{
						echo 'Error al enviar mensaje | '.$i.'/'.$intentos.'<br>';
					}
					$datos['mensaje']=null;
				}
			
		}
		catch(Exception $e)
		{
			echo $e;
		}
	}
	function mandrill()
	{
		$mandrill_ready = NULL;

		try 
		{
		    $this->mandrill->init($this->CI->config->item('mandrill_key'));;
		    $mandrill_ready = TRUE;

		} 
		catch(Mandrill_Exception $e) 
		{

		    $mandrill_ready = FALSE;
		    echo "ERROR".$e;

		}
		if( $mandrill_ready ) 
		{
			$var=array('type'=>'Welcome','mensaje'=>'Prueba');
		    $body=$this->mandrill->msg_create($var);		   
		    $email = array(
		        'html' => $body,// 'text' => '<h1>Ejemplo</h1>',//$body,
		        'subject' => 'Mensaje Prueba 2', 'from_email' => 'applesteacher2015@gmail.com',
		        'from_name' => 'Ejemplo','to' => array(array('email' => 'maxsotov@gmail.com')));
		    $result = $this->mandrill->messages_send($email);
		    var_dump($result);
		    echo "Enviado a ".$email['to'][0]['email'];
		}
	}
	function test_lib()
	{
		$this->load->library('Send_msg');
		$this->load->model('profesor_model');
		//2866775745
		$objeto=$this->profesor_model->get_apoderados_curso(4);
		if($objeto['bool'])
		{
		if($this->send_msg->twitter_Masivo($objeto['query'],'Hola Mundssoasdasd'))
			{
				echo "SI";
			}
			else
			{
				echo "NO";
			}
		}
		else
			{
			echo"NO23";
		}

		/*$objeto=$this->profesor_model->get_apoderados_curso(4);
		if($objeto['bool']){
			$users=$objeto['query'];
			if($this->send_msg->Mandril_Masivo($users,'Hola Mundo','Ejemplo','Sujeto23'))
			{
				echo "SI";
			}
			else
			{
				echo "NO";
			}
		}
		else
		{
			echo"NO23";
		}*/
	}
	function inbox($error=NULL)
	{
		$datos=NULL;
		$id=$this->ion_auth->get_user_id();		
		$this->layout->set_specific_js(array('js/inbox.js','lib/wysihtml5/advanced.js','js/ui-dialogs.js'));
		$query=$this->app_model->inbox_load($id);
		if($query['bool'])
		{
			$datos['inbox_load']=$query['query'];
			$datos['n_msg']=$query['n_new'];
			$datos['n_all']=$query['n_all'];
			if($error!==NULL)
			{
				$datos['msg']=$error;
			}
			
		}
		$datos['Id_Rol']=1;
		$this->layout->view('app/inbox',$datos);
	}	
	function create_message()
	{
		if(!$this->input->post('title'))
		{
			$this->layout->set_specific_js(array('js/forms-article.js'));
			$id=$this->ion_auth->get_user_id();	
			$var=array('Id_User'=>$id);
			$cursos=$this->app_model->load_curso($var);
			if($cursos['bool'])
			{
				$cursos=$cursos['query'];
				if(isset($cursos['bool_admin_cursos'])){if($cursos['bool_admin_cursos']){$datos['todos']['cursos']=$cursos['query_admin_cursos'];}}
				if(isset($cursos['bool_admin_profes'])){if($cursos['bool_admin_profes']){$datos['todos']['profes']=$cursos['query_admin_profes'];}}
				if(isset($cursos['bool_profe'])){if($cursos['bool_profe']){$datos['alumnos']=$cursos['query_profe'];}}
				if(isset($cursos['bool_jefe'])){if($cursos['bool_jefe']){$datos['apoderados']=$cursos['query_jefe'];}}
			}
			$Id_rol=$this->security_model->find_rol($id);
			if($Id_rol['bool'])
			{
				$rol=$Id_rol['query'];
				foreach ($rol as $r) 
				{
					$id=$r->group_id;
					if($id==1)
					{
						$datos['rango']='admin';
						break;
					}
					if($id==2)
					{
						$Id_rol=2;
					}
					if($id==3)
					{
						$Id_rol=3;
					}
					if($id==4)
					{
						$datos['rango']=NULL;
						$Id_rol=4;
					}

					if($id==5)
					{
						$datos['rango']='jefe';
						break;
					}
				}
				if($Id_rol==2||$Id_rol==3)
				{
					redirect('demo/inbox');
				}
			}
			else
			{
				$datos['rango']=NULL;
			}
			$this->layout->view('app/create_message',$datos);
		}		
		else
		{
			$mensaje= array(	'Titulo' =>$this->input->post('title'), 
								'Contenido' =>$this->input->post('text'),
								'Fecha' =>date("Y-m-d"));
			$origen_destino=array(	'alumnos' =>$this->input->post('alumnos'),
									'apoderado' =>$this->input->post('apoderados'),
									'nivel' =>$this->input->post('status'));
			$datos = array('mensaje' =>$mensaje,'od'=>$origen_destino);
			//var_dump($datos);
			$this->enviar_message($datos);
		}	
	}
	function enviar_message($datos)
	{
		//Guardar mensajes

		$Id_mensaje=0;
		$lvl=1;
		$Id_origen=$this->ion_auth->get_user_id();	
		if(isset($datos['mensaje']))
		{
			$datos_mensaje=$datos['mensaje'];
			$save=$this->app_model->save_alert($datos_mensaje);
			if($save['bool'])
			{
				$Id_mensaje=$save['id'];
			}
			else
			{
				echo $save['msg'].'<br>'.$save['e'];
			}
		}
		if(isset($datos['od']))
		{
			
			$datos_od=$datos['od'];			
			if($Id_mensaje!==0)
			{
				$mensaje=$datos['mensaje']['Contenido'];
				$title=$datos['mensaje']['Titulo'];
				if(isset($datos_od['alumnos']))
				{					
					$alumnos=$datos_od['alumnos'];
					if(count($alumnos)>0 && $alumnos != false)
					{
						$lvl=1;
						foreach ($alumnos as $q) 
						{
							$save_OD=$save=$this->app_model->save_OD($q,$Id_origen,$Id_mensaje,'alumno',$lvl,$mensaje,$title);
						}
					}
				}				
				if(isset($datos_od['apoderado']))
				{
					if(isset($datos_od['nivel']) &&  $datos_od['nivel'] != NULL)
					{
						$lvl=$datos_od['nivel'];
						if($lvl!=1 && $lvl!=2 && $lvl!=3 && $lvl!=4)
						{
							$lvl=1;
						}
					}
					$apoderado=$datos_od['apoderado'];
					if(count($apoderado)>0 &&  $apoderado != false)
					{
						foreach ($apoderado as $q) 
						{
							$save_OD=$save=$this->app_model->save_OD($q,$Id_origen,$Id_mensaje,'apoderado',$lvl,$mensaje,$title);
						}
					}
				}
				$url=$this->session->userdata('soy');
				redirect($url.'/inbox');

			}
			else
			{
				echo "no funca";
			}
		}
		
		//Enviar mensajes
	}
	function facebook()
	{
		$this->load->library('facebook');
		$fb1=$this->facebook->logged_in();
		$fb2=$this->facebook->user();
		var_dump($fb1);
	}
	function demo2()
	{
		$this->load->model('apoderado_model');
		$id_apoderado=$this->ion_auth->get_apoderado_id();
		echo "ID APODERADO:".$id_apoderado."<br>";
		$alumnos=$this->apoderado_model->get_alumnos_and_cursos($id_apoderado);
		$cantidad=$alumnos['cantidad'];
        $alumnos=$alumnos['query'];
        $pupilos=NULL;
                
                foreach ($alumnos as $r) 
                {          
                                                      
                    $nombre=$r->first_name;
                    $nombre=preg_split("/[\s,]+/",$nombre);
                    $apellido=$r->last_name;
                    $apellido=preg_split("/[\s,]+/",$apellido);
                    $nombre=$nombre[0].' '.$apellido[0]; 
                    $curso=$r->Grado."°".$r->Letra;
                    $id=$r->Alumno_Id;
                    echo $id." - ".$nombre." - ".$curso."<br>";
                }
                echo "TOTAL=".$cantidad;
	}


}
?>