<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Security extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();    
        $this->load->helper('url'); 
        $this->load->library('layout');
        $this->load->library('security_lib');
        $this->load->library('session');
        $this->load->helper('elements');
        $this->load->helper('form');
        $this->load->model('security_model');
    }
    public function index()
    {
        $Session_user=$this->session->userdata('user');
        if($Session_user['Rol_Id']==NULL)
        {            
            $dato['url']=base_url().'assets/bootstrap/';
            $this->load->view("security/login",$dato);
        }
        else
        {      
            echo 'hola'.$Session_user['Rol_Id'];      
            $this->security_lib->selectiRol($Session_user['Rol_Id']);
        }
    } 
    //CAMBIAR LOS REDIRECT DE LOGIN
    public function login() 
    {
        $Session_user=$this->session->userdata('user');
        
            $dato['url']=base_url().'assets/bootstrap/';
            if($this->input->post('password')!=="" && $this->input->post('rut') !=="")
            {            
                $password=$this->input->post('password');
                $rut=trim($this->input->post('rut'));
                $consulta=$this->security_model->cargarUser($password,$rut);
                if($consulta!==NULL)
                {
                    $this->session->set_userdata('user',$consulta);
                    $this->security_lib->selectiRol($consulta['Rol_Id']);
                }
                else
                {
                  $this->load->view("security/login",$dato);  
                }            
            }
            else
            {
                $this->load->view("security/login",$dato);
            }
    }
    public function logout()
    {
        $Session_user=$this->session->userdata('user');
        if(isset($Session_user))
        {
            $this->session->unset_userdata('user');
            redirect('/');
        }
        else
        {
            $this->security_lib->selectiRol();
        }
    }
    public function first_login()
    {
        $dato['url']=base_url().'assets/bootstrap/';
        $dato['url_rol']=base_url()."index.php/administracion";
        $this->load->view("security/first_login",$dato);
    }
   

}?>