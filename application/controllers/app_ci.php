<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class App_ci extends CI_Controller {
 
 	private $siguiente=false;
 	private $color_input='style="background-color:rgba(51, 51, 51, 0.4) !important"';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->model('app_model');
		$this->load->library(array('security_lib','ion_auth'));
		$this->load->helper('app');
		$this->soy();
	}
	private function soy()
	{
		$ruta = uri_string().'';
        $sp = explode('/', $ruta);
        $lower = strtolower($sp[0]);
        if($lower==$this->session->userdata('soy'))
        {
		 	if($lower=='apoderado' || $lower == 'alumno')
		 	{
		 		$this->layout->set_template('white');
		 		$this->color_input='style="background-color:rgba(230, 230, 230, 0.4) !important"';
		 	}
		 	$this->siguiente=true;	 	
		}
		else
		{
			redirect('/');
		}
	}
	public function change_see()
	{
			$Id_Message=$this->input->post('id');
			$resp=false;
			if(is_numeric($Id_Message))
			{
				$Id_User=$this->ion_auth->get_user_id();
				$query=$this->app_model->check_see($Id_User,$Id_Message);					
					if($query['bool'])
					{
						$respuesta=$this->app_model->change_see($Id_User,$Id_Message);
						if($respuesta['bool'])
						{							
							$resp=true;
						}
						else
						{
							$resp=false;
						}
					}
					else
					{
						$resp=false;
					}
			}
			echo json_encode(array("bool"=>$resp));	
	}
	public function MyPerfil()
	{
		$ruta = uri_string().'';
        $sp = explode('/', $ruta);
        $lower = strtolower($sp[0]);
        $data['twitter_active']=false;
        $data['soy']=$this->session->userdata('soy');
        if($this->siguiente)
        {

	        $MyId=$this->ion_auth->get_user_id();     
	        $user=$this->ion_auth->user()->row();
	        $data['myuser']=$user;   
	        $data['input']=$this->color_input;
	        if($user->Id_twitter != null)
	        {
	        	$data['twitter_active']=true;
	            if($this->session->userdata('twitter_screen_name'))
	            {
	                $username_twitter=$this->session->userdata('twitter_screen_name');
	                $data['twitter_name']=$username_twitter;
	            }
	        }
	        $this->layout->view('app/perfil',$data);
        }
        else
        {
        	redirect('auth/login');
        }
	}

	public function inbox($id_mensaje=NULL)
	{
		$datos=NULL; 
		$id=$this->ion_auth->get_user_id();   
		if(is_numeric($id_mensaje))
		{
			$datos['id_msg']=$id_mensaje;
			$this->app_model->change_see($id,$id_mensaje);
		}
		$ruta = uri_string().'';
        $sp = explode('/', $ruta);
        $lower = strtolower($sp[0]);
        if($lower==$this->session->userdata('soy'))
        {
	        if($lower=='apoderado'){$this->layout->set_template('white');}  
	        if($lower=='profesor' || $lower=='administrador'){$datos['url']=$lower;}
	        $this->layout->set_specific_js(array('lib/wysihtml5/advanced.js','js/ui-dialogs.js','lib/jquery.dataTables.min.js','js/tables-dynamic.js'));
	        $query=$this->app_model->inbox_load($id);
	        if($query['bool'])
	        {
	            $datos['inbox_load']=$query['query'];
	            $datos['n_msg']=$query['n_new'];
	            $datos['n_all']=$query['n_all'];        
	        }
	        $datos['Id_Rol']=2;
	        $datos['soy']=$this->session->userdata('soy');
	        $this->layout->view('app/inbox',$datos);
	        
        }
        else
        {
        	redirect('auth/login');
        }
	}
	public function nuevo_mensaje()
    {
        if($this->input->post('title')==NULL || $this->input->post('title')=="")
        {
            $this->layout->set_specific_js(array('js/forms-article.js'));
            $id=$this->ion_auth->get_user_id(); 
            $var=array('Id_User'=>$id);
            $cursos=$this->app_model->load_curso($var);
            if($cursos['bool'])
            {
                $cursos=$cursos['query'];
                if(isset($cursos['bool_profe'])){if($cursos['bool_profe']){$datos['alumnos']=$cursos['query_profe'];}}
                if(isset($cursos['bool_jefe'])){if($cursos['bool_jefe']){$datos['apoderados']=$cursos['query_jefe'];}}
            }
            $datos['rango']=4;            
            $cursos_jefe=$this->profesor_model->load_cursos_jefe($id);
            $this->layout->view('app/create_message',$datos);

        }       
        else
        {
            $mensaje= array(    'Titulo' =>$this->input->post('title'), 
                                'Contenido' =>$this->input->post('text'),
                                'Fecha' =>date("Y-m-d"));
            $origen_destino=array(  'alumnos' =>$this->input->post('alumnos'),
                                    'apoderado' =>$this->input->post('apoderados'),
                                    'nivel' =>$this->input->post('status'));
            $datos = array('mensaje' =>$mensaje,'od'=>$origen_destino);
            var_dump($datos);
          //  $this->enviar_message($datos);
        }   
    }

}
?>	