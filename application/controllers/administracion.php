<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Administracion extends CI_Controller {
    private $color_input='style="background-color:rgba(51, 51, 51, 0.4) !important"';
    function __construct()
    {
        parent::__construct();
 
        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        
        $this->load->helper('url');
        $this->load->helper('checkbox');
        /* ------------------ */ 
        $this->load->model('app_model');
        $this->load->library('layout');
        $this->load->helper('app');
        $this->load->library(array('security_lib','ion_auth'));
        $this->load->helper('elements');
        $this->load->helper('form');
        $this->load->model(array('administracion_model','ion_auth_model','alumno_model','apoderado_model'));

        if(!$this->session->userdata('soy'))
        {
            $this->session->set_userdata('soy','administracion');
        }
        else
        {
            $this->session->unset_userdata('soy');
            $this->session->set_userdata('soy','administracion');
        }
    }
    private function vrut($caracteres){
        $array_vrut = explode("-",$caracteres);
        $count = 0;
        foreach($array_vrut as $k){
            $count++;
        }
        if($count>0){
            return true;
        }
        else{
            return false;
        }
        /*$first = $array_vrut[0];
        $codigo_verificador = $array_vrut[1];
        $codigo_verificador = strtoupper($codigo_verificador);
        $first_inv = strrev($first);
        $suma = 0;
        $aux = 2;
        for($i=0;$i<strlen($first_inv);$i++){
            if($aux <= 7){
                $suma = (intval($first_inv[$i])*intval($aux)) + intval($suma);
                $aux++;
            }else{
                $aux = 2;
                $suma = (intval($first_inv[$i])*intval($aux)) + intval($suma);
                $aux++;
            }
        }
        $resto = intval($suma)%11;
        $codigo_aux = 11-$resto;
        if($codigo_aux == 11){
            $codigo_aux = 0;
        }
        elseif ($codigo_aux == 10) {
            $codigo_aux = "K";
        }
        if($codigo_aux == $codigo_verificador){
            return true;
        }
        else{
            return false;
        }*/
    }

    private function verificar(){
        $email = $this->session->userdata('identity');
        $codigo = $this->ion_auth_model->get_activication_code($email);
        $valor = $codigo['query'];
        if($valor == NULL || $valor == ""){
            redirect('auth/change_password');
        }
        else{
            $group = 1;
            if (!$this->ion_auth->in_group($group,$this->ion_auth->get_user_id())){
                //$this->session->set_flashdata('message', 'You must be a part of the gangstas or group 2');
                redirect('auth/ver_nuevo'); //Mostrar la vista
            }
        }      
    }
    public function pdf_html($html){
        $pdfFilePath = "Informe_notas_2015.pdf";
        $this->load->library('m_pdf');
        $pdf = $this->m_pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output($pdfFilePath, "D");
    }
    public function informe_notas($id_alumno){
        try{
            if(!empty($id_alumno))
            {
                $alumno = $this->administracion_model->all_Alumno($id_alumno);
                $mis_notas = $this->alumno_model->get_Mis_Evaluaciones($id_alumno);
                $datos['notas']=$this->apoderado_model->get_notas_alumno($id_alumno);
                $datos['set_notas'] = $this->apoderado_model->get_set_notas_alumno($id_alumno);
                $datos['asignaturas'] = $this->apoderado_model->get_asignaturas_alumno($id_alumno);
                //var_dump($mis_notas2);
                $curso = $this->alumno_model->get_Curso_Id($id_alumno);
                $datos['mis_notas'] = $mis_notas['query'];
                //var_dump($datos['mis_notas']);
                $datos['salida'] = $alumno['query'];
                $datos['curso'] = $curso['query'];
                $curso_profesor = $datos['curso']->Profesor_Id;
                $profesor = $this->administracion_model->all_Profesor2($curso_profesor);
                $datos['profesor'] = $profesor['query'];
                $apoderado = $this->administracion_model->all_Apoderado(0,$alumno['query']->Apoderado_Id);
                $datos['apoderado'] = $apoderado['query'];
                $this->layout->set_specific_js(array('js/print.js'));
                $this->layout->view('profesor/cursos/informe_notas',$datos);
                /*$html = $this->load->view('profesor/cursos/informe_notas',$datos,true);
                $pdfFilePath = "Informe_notas_2015.pdf";
                $this->load->library('m_pdf');
                $pdf = $this->m_pdf->load();
                $pdf->WriteHTML($html);
                $pdf->Output($pdfFilePath, "D");*/
                
            }
            else
            {
                
            }
            
            
        }
        catch(Exception $ex){

        }
    }
    public function index()
    {
        $this->verificar();
        $this->layout->set_specific_js(array(   'lib/nvd3/lib/d3.v2.js',
                                                'lib/nvd3/nv.d3.custom.js',
                                                'lib/nvd3/src/models/scatter.js',
                                                'lib/nvd3/src/models/axis.js',
                                                'lib/nvd3/src/models/legend.js',
                                                'lib/nvd3/src/models/multiBar.js',
                                                'lib/nvd3/src/models/multiBarChart.js',
                                                'lib/nvd3/src/models/line.js',
                                                'lib/nvd3/src/models/lineChart.js',
                                                'lib/nvd3/stream_layers.js',
                                                'js/index.js','js/chat.js',
                                                'lib/icheck.js/jquery.icheck.js',
                                                'lib/sparkline/jquery.sparkline.js',
                                                'lib/jquery-ui-1.10.3.custom.js',
                                                'lib/jquery.slimscroll.js',
                                                'js/forms-elemets.js'));
        $datos['cant_profesores'] = $this->administracion_model->get_number('profesor');
        $datos['cant_alumnos'] = $this->administracion_model->get_number('alumno');
        $datos['cant_apoderados'] = $this->administracion_model->get_number('apoderado');
        $datos['cant_all'] = $datos['cant_profesores']+$datos['cant_alumnos']+$datos['cant_apoderados'];
        $apoderado_cinco = $this->administracion_model->last_cinco_apoderados();
        $alumno_cinco = $this->administracion_model->last_cinco_alumno();
        $profesor_cinco = $this->administracion_model->last_cinco_profesor();
        $respuesta_log = $this->administracion_model->get_log();         
        if($respuesta_log['bool'] == TRUE)
        {   
            $datos['log_log']=$respuesta_log['query'];
        }
        if($apoderado_cinco['bool'] == TRUE)
        {
            $datos['apoderado_cinco'] = $apoderado_cinco['query'];
        }
        if($alumno_cinco['bool'] == TRUE)
        {
            $datos['alumno_cinco'] = $alumno_cinco['query'];
        }
        if($profesor_cinco['bool'] == TRUE)
        {
            $datos['profesor_cinco'] = $profesor_cinco['query'];
        }
        $this->layout->view('main',$datos);
    }
    // *************    APODERADO  *******************
        public function apoderado($function=NULL,$Id=NULL)
        {
            
            //$Session_user=$this->session->userdata('user');
            //$this->security_lib->issetUser($Session_user['Id']);
            $this->verificar();
            //$this->layout->set_name($Session_user);
            if(!is_null($function))
            {            
               if($function=="Add")
               {                     
                    if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                    {
                        $datos['password']=$this->security_lib->generator();
                        $this->session->set_userdata('password',$datos['password']);
                        $datos['comunas']=$this->administracion_model->comunas();
                        $this->layout->set_specific_js(array('js/forms-elemets.js','js/validarut.js'));
                        $this->layout->view('administracion/apoderado/ad_apoderado_add',$datos);

                    }
                    else
                    {
                        if($this->vrut($this->input->post('rut')))
                        {
                            $datos_apoderado=array( 'nombre'=>$this->input->post('nombre'),
                                                'apellido'=>$this->input->post('apellido'),
                                                'rut'=>$this->input->post('rut'),
                                                'f_nacimiento'=>$this->input->post('fnacimiento'),
                                                't_fijo'=>$this->input->post('t_fijo'),
                                                't_celular'=>$this->input->post('t_celular'),
                                                'password'=>$this->session->userdata('password'),
                                                'correo'=>$this->input->post('correo'),
                                                'comuna'=>$this->input->post('comuna'),
                                                'sexo'=>$this->input->post('sexo'),
                                                'domicilio'=>$this->input->post('domicilio'),
                                                'wsp'=>$this->input->post('wsp1'),
                                                'sms'=>$this->input->post('sms1'));                     
                        $this->apoderado_add($datos_apoderado);
                        }
                        else
                        {
                            $datos['error_rut'] = 'El RUT no es correcto';
                            $datos['password']=$this->security_lib->generator();
                            $this->session->set_userdata('password',$datos['password']);
                            $datos['comunas']=$this->administracion_model->comunas();
                            $this->layout->set_specific_js(array('js/forms-elemets.js'));
                            $this->layout->view('administracion/apoderado/ad_apoderado_add',$datos);
                        }
                        
                    }
               } 
               if($function=="Ficha"){
                    if($this->input->post('nombre') ==NULL || $this->input->post('nombre') =="")
                    {
                        $query=$this->administracion_model->ver_Apoderado($Id);
                        $datos['q'] = $this->ion_auth->user($Id)->row();
                        $datos['comunas']=$this->administracion_model->comunas();
                        $datos['input']=$this->color_input;
                        if($datos['q']->username != '')
                        {
                            $datos['p']=$query['query'];
                            $datos['fecha'] = date('m/d/Y',strtotime($datos['q']->f_nacimiento));
                            $datos['Id']=$Id;
                            $this->session->set_userdata('apoderado',$query['query']);
                            $this->layout->set_specific_js(array('js/forms-elemets.js'));
                            $this->layout->view('administracion/apoderado/ad_apoderado_ficha',$datos);
                        }
                        else
                        {
                            redirect("administracion/apoderado");
                        }
                    }
               }
               if($function=="FichaPdf"){
                    if($this->input->post('nombre') ==NULL || $this->input->post('nombre') =="")
                    {
                        $query=$this->administracion_model->ver_Apoderado($Id);
                        $datos['q'] = $this->ion_auth->user($Id)->row();
                        $datos['comunas']=$this->administracion_model->comunas();
                        if($datos['q']->username != '')
                        {
                            $datos['p']=$query['query'];
                            $datos['fecha'] = date('m/d/Y',strtotime($datos['q']->f_nacimiento));
                            $datos['Id']=$Id;
                            $this->session->set_userdata('apoderado',$query['query']);
                            $this->layout->set_specific_js(array('js/forms-elemets.js'));
                            $this->layout->view('administracion/apoderado/ad_apoderado_fichapdf',$datos);
                        }
                        else
                        {
                            redirect("administracion/apoderado");
                        }
                    }
               }
               if($function=="Delete"){
                    try
                    {
                        $datos_log=array('Descripcion'=>'Eliminado el dato con Id '.$Id, 'Tabla'=>'apoderado','Usuario_id'=>$this->ion_auth->get_user_id().'');
                        $datos_apoderado=array('hide'=>1);
                        $this->administracion_model->agregar_tabla('log',$datos_log);
                        $this->administracion_model->editar_tabla($Id, 'apoderado', $datos_apoderado);
                        //editar el active de users
                        redirect("administracion/apoderado");
                    }
                    catch(Exception $ex){

                        redirect("administracion/apoderado?error");
                    }
               } 
               if($function=="Edit")
               {
                    if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                    {
                        //$datos['bd_comunas']=$this->administracion_model->comunas();   
                        $query=$this->administracion_model->ver_Apoderado($Id);
                        $datos['q'] = $this->ion_auth->user($Id)->row();
                        $datos['comunas']=$this->administracion_model->comunas();
                        $datos['fecha'] = date('m/d/Y',strtotime($datos['q']->f_nacimiento));
                        
                        if($datos['q']->username != '')
                        {
                            $datos['p']=$query['query'];
                            $datos['Id']=$Id;
                            $this->session->set_userdata('apoderado',$query['query']);
                            $this->layout->set_specific_js(array('js/forms-elemets.js','js/validarut.js'));
                            $this->layout->view('administracion/apoderado/ad_apoderado_edit',$datos);
                        }
                        else
                        {
                            redirect("administracion/apoderado");
                        }
                    }
                    else
                    {
                        $db_apoderado=$this->session->userdata('apoderado');
                        //echo $db_apoderado->Id;

                        $datos_apoderado=array( 'nombre'=>$this->input->post('nombre'),
                                                'apellido'=>$this->input->post('apellido'),
                                                'rut'=>$this->input->post('rut'),
                                                'f_nacimiento'=>$this->input->post('fnacimiento'),
                                                't_fijo'=>$this->input->post('t_fijo'),
                                                't_celular'=>$this->input->post('t_celular'),
                                                'password'=>$this->session->userdata('password'),
                                                'correo'=>$this->input->post('correo'),
                                                'comuna'=>$this->input->post('comuna'),
                                                'sexo'=>$this->input->post('sexo'),
                                                'domicilio'=>$this->input->post('domicilio'),
                                                'wsp'=>$this->input->post('wsp1'),
                                                'sms'=>$this->input->post('sms1'),
                                                'Id_Ap'=>$db_apoderado->Id,
                                                'Id_Us'=>$db_apoderado->Usuario_id);

                        $this->apoderado_edit($datos_apoderado);
                        
                    }
               } 

            }
            else
            {               
                $mysql=$this->administracion_model->all_apoderado();           
                if($mysql['bool'])
                {   
                    $datos['mysql']=$mysql['query'];
                    $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js','js/forms-elemets.js','js/print.js'));
                    $this->layout->view('administracion/apoderado/ad_apoderado',$datos);
                }
                else
                {
                    $datos['errormsg']=$mysql['msg'];
                    $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js','js/forms-elemets.js','js/print.js'));
                    $this->layout->view('administracion/apoderado/ad_apoderado',$datos);
                }
            }
        }
        public function apoderado_add($datos_apoderado)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->add_apoderado($datos_apoderado);
            $r=$respuesta['bool'];
            if($r)
            {
                $datos_log=array('Descripcion'=>'Agregado el apoderado con rut '.$datos['rut'], 'Tabla'=>'apoderado','Usuario_id'=>$this->ion_auth->get_user_id().'');
                $this->administracion_model->agregar_tabla('log',$datos_log);                
               redirect('administracion/apoderado');
            }
            else
            {
                $datos['errormsg']=$respuesta['msg'];
                $datos['comunas']=$this->administracion_model->comunas();
                $this->layout->view('administracion/apoderado/ad_apoderado_add',$datos);
            }
        }
        public function apoderado_edit($datos_apoderado)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->edit_apoderado($datos_apoderado);
            $r=$respuesta['bool'];
            if($r)
            {
                $datos_log=array('Descripcion'=>'Editado el apoderado con Id '.$datos_apoderado["Id_Ap"], 'Tabla'=>'apoderado','Usuario_id'=>$this->ion_auth->get_user_id().'');
                $this->administracion_model->agregar_tabla('log',$datos_log); 
               redirect('administracion/apoderado');
            }
            else
            {
                $dato['errormsg']=$respuesta['msg'];
                $dato['password']=$this->security_lib->generator();
                $dato['q']=$this->session->userdata('apoderado');
                $dato['Id']=$dato['q']->Id;
                $this->session->unset_userdata('apoderado');
                $this->layout->view('administracion/apoderado/ad_apoderado_edit',$dato);
            }
        }
        public function alumno_edit($datos_alumno)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->edit_alumno($datos_alumno);
            $r=$respuesta['bool'];
            if($r)
            {
                
               redirect('administracion/alumno');
            }
            else
            {
                $datos_log=array('Descripcion'=>'Editado el alumno con Id '.$datos_alumno["Id_Al"], 'Tabla'=>'alumno','Usuario_id'=>$this->ion_auth->get_user_id().'');
                $this->administracion_model->agregar_tabla('log',$datos_log); 
                $dato['errormsg']=$respuesta['msg'];
                $dato['password']=$this->security_lib->generator();
                $dato['q']=$this->session->userdata('alumno');
                $dato['Id']=$dato['q']->Id;
                $this->session->unset_userdata('alumno');
                $this->layout->view('administracion/alumno/ad_alumno_edit',$dato);
            }
        }
        /*public function apoderado_delete($datos_apoderado)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->add_apoderado($datos_apoderado);
            $r=$respuesta['bool'];
            if($r)
            {
               $datos_log=array('Descripcion'=>'Eliminado el apoderado con Id '.$datos_apoderado["Id_Al"], 'Tabla'=>'alumno','Usuario_id'=>$this->ion_auth->get_user_id().'');
                $this->administracion_model->agregar_tabla('log',$datos_log);  
               redirect('administracion/apoderado');
            }
            else
            {
                $dato['errormsg']=$respuesta['msg'];
                $datos['password']=$this->security_lib->generator();
                $this->layout->view('administracion/apoderado/ad_apoderado_add',$dato);
            }
        }*/
    // ************  FIN APODERADO *****************
    // *************    ALUMNO  *******************
        public function Alumno($function=NULL,$Id=NULL)
        {
            $this->verificar();
            // APODERADO EJEMPLO
            
            //$id_apoderado=7;
            if(!is_null($function))
            {            
               if($function=="Add")
               {                     
                    if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                    {
                        $datos['password']=$this->security_lib->generator();
                        $this->session->set_userdata('password',$datos['password']);
                        $apoderados_all = $this->administracion_model->all_Apoderado();
                        $datos['apoderados_all'] = $apoderados_all['query'];
                        $this->layout->set_specific_js(array('js/forms-elemets.js','js/validarut.js'));
                        $this->layout->view('administracion/alumno/ad_alumno_add',$datos);             
                    }
                    else
                    {  
                        $datos_alumno=array(    'nombre'=>$this->input->post('nombre'),
                                                'apellido'=>$this->input->post('apellido'),
                                                'rut'=>$this->input->post('rut'),
                                                'f_nacimiento'=>$this->input->post('f_nacimiento'),
                                                't_fijo'=>$this->input->post('t_fijo'),
                                                't_celular'=>$this->input->post('t_celular'),
                                                'password'=>$this->session->userdata('password'),
                                                'correo'=>$this->input->post('correo'),
                                                't_sangre'=>$this->input->post('tipo_sangre'), 
                                                'alergias'=>$this->input->post('alergias'),
                                                'sexo'=>$this->input->post('sexo'),     
                                                'Apoderado_Id'=>$this->input->post('apoderado'));
                        $this->alumno_add($datos_alumno);
                    }
               }
               if($function=="Ficha"){
                    if($this->input->post('nombre') ==NULL || $this->input->post('nombre') =="")
                    {
                        $apoderados_all = $this->administracion_model->all_Apoderado(false);
                        $query=$this->administracion_model->ver_Alumno($Id);
                        $datos['apoderados_all'] = $apoderados_all['query'];
                        $datos['input']=$this->color_input;
                        //var_dump($query['query']);
                        if($query['bool'] == TRUE)
                        {
                            $datos['q']=$query['query'];
                            $datos['fecha'] = date('m/d/Y',strtotime($datos['q']->f_nacimiento));
                            $datos['Id']=$query['query']->Id;
                            $this->session->set_userdata('alumno',$query['query']);
                            $this->layout->set_specific_js(array('js/forms-elemets.js'));
                            $this->layout->view('administracion/alumno/ad_alumno_ficha',$datos);
                        }
                        else
                        {
                            redirect("administracion/alumno");
                        }
                    }
               }
               if($function=="Delete"){
                    try
                    {
                        $datos_profesor=array('hide'=>1);
                        $this->administracion_model->editar_tabla($Id, 'alumno', $datos_profesor);
                        $datos_log=array('Descripcion'=>'Eliminado el alumno con Id '.$Id, 'Tabla'=>'alumno','Usuario_id'=>$this->ion_auth->get_user_id().'');
                        $this->administracion_model->agregar_tabla('log',$datos_log);
                        //editar el active de users
                        redirect("administracion/alumno");
                    }
                    catch(Exception $ex){

                        redirect("administracion/alumno?error");
                    }
               } 
               if($function=="Edit")
               {
                    if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                    {
                        $apoderados_all = $this->administracion_model->all_Apoderado(false);
                        $datos['password']=$this->security_lib->generator();
                        $query=$this->administracion_model->ver_Alumno($Id);
                        $datos['apoderados_all'] = $apoderados_all['query'];

                        if($query['bool'])
                        {
                            $datos['q']=$query['query'];
                            $datos['fecha'] = date('m/d/Y',strtotime($datos['q']->f_nacimiento));
                            $datos['Id']=$query['query']->Id;
                            $this->session->set_userdata('alumno',$query['query']);
                            $this->layout->set_specific_js(array('js/forms-elemets.js','js/validarut.js'));
                            $this->layout->view('administracion/alumno/ad_alumno_edit',$datos);
                        }
                        else
                        {
                            redirect("administracion/alumno");
                        }
                    }
                    else
                    {
                        $db_alumno=$this->session->userdata('alumno');
                        
                        $datos_alumno=array(    'nombre'=>$this->input->post('nombre'),
                                                'apellido'=>$this->input->post('apellido'),
                                                'rut'=>$this->input->post('rut'),
                                                'f_nacimiento'=>$this->input->post('f_nacimiento'),
                                                't_fijo'=>$this->input->post('t_fijo'),
                                                't_celular'=>$this->input->post('t_celular'),
                                                'password'=>$this->session->userdata('password'),
                                                'correo'=>$this->input->post('correo'),
                                                't_sangre'=>$this->input->post('tipo_sangre'), 
                                                'alergias'=>$this->input->post('alergias'),
                                                'sexo'=>$this->input->post('sexo'),     
                                                'Apoderado_Id'=>$this->input->post('apoderado'),
                                                'Id_Al'=>$db_alumno->Id,
                                                'Id_Us'=>$db_alumno->Usuario_id
                                                );
                        $this->Alumno_edit($datos_alumno);
                    }                    
               } 
            }
            else
            {               
                $mysql=$this->administracion_model->all_alumno();

                if($mysql['bool'])
                {   
                    $datos['mysql']=$mysql['query'];
                    $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js','js/forms-elemets.js','js/print.js'));
                    $this->layout->view('administracion/alumno/ad_alumno',$datos);
                    // Generar PDF
                    /*$pdfFilePath = "output_pdf_name.pdf";
                    $this->load->library('m_pdf');
                    $pdf = $this->m_pdf->load();
                    $pdf->WriteHTML($html);
                    $pdf->Output($pdfFilePath, "D");*/
                }
                else
                {
                    $datos['errormsg']=$mysql['msg'];
                    $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js','js/forms-elemets.js','js/print.js'));                   
                    $this->layout->view('administracion/alumno/ad_alumno',$datos);
                }
            }
        }
        public function Alumno_add($datos_alumno)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->add_alumno($datos_alumno);
            $r=$respuesta['bool'];
            if($r)
            {
                $datos_log=array('Descripcion'=>'Agrego al alumno con Rut '.$datos_alumno["rut"], 'Tabla'=>'alumno','Usuario_id'=>$this->ion_auth->get_user_id().'');
                $this->administracion_model->agregar_tabla('log',$datos_log);
               redirect('administracion/alumno');
            }
            else
            {
                $datos['errormsg']=$respuesta['msg'];
                $datos['password']=$this->security_lib->generator();
                $this->layout->view('administracion/alumno/ad_alumno_add',$datos);
            }
        }
        /*public function Alumno_delete($datos_alumno)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->add_alumno($datos_alumno);
            $r=$respuesta['bool'];
            if($r)
            {
                
               redirect('administracion/alumno');
            }
            else
            {
                $dato['errormsg']=$respuesta['msg'];
                $datos['password']=$this->security_lib->generator();
                $this->layout->view('administracion/alumno/ad_alumno_add',$dato);
            }
        }*/
    // ************  FIN ALUMNO *****************        
    // *************   PROFESOR  *******************
        public function createPDF(){
            // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/ 
            $pdfFilePath = FCPATH."/downloads/reports/$filename.pdf";
            $data['page_title'] = 'Hello world'; // pass data to the view

            if (file_exists($pdfFilePath) == FALSE)
            {
                ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $html = $this->load->view('pdf_report', $data, true); // render the view into HTML
                 
                $this->load->library('pdf');
                $pdf = $this->pdf->load();
                $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
                $pdf->WriteHTML($html); // write the HTML into the PDF
                $pdf->Output($pdfFilePath, 'F'); // save to file because we can
            }
            redirect("/downloads/reports/$filename.pdf");
        }
        public function profesor($function=NULL,$Id=NULL)
        {
             $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js','js/forms-elemets.js','js/print.js'));
             $this->verificar();       
            if(!is_null($function))
            {            
               if($function=="Add")
               {                     
                    if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                    {
                        $datos['password']=$this->security_lib->generator();
                        $this->session->set_userdata('password',$datos['password']);
                        $this->layout->set_specific_js(array('js/forms-elemets.js','js/validarut.js'));
                        $this->layout->view('administracion/profesor/ad_profesor_add',$datos);

                    }
                    else
                    {  
                        $datos_profesor=array( 
                                                'nombre'=>$this->input->post('nombre'),
                                                'apellido'=>$this->input->post('apellido'),
                                                'rut'=>$this->input->post('rut'),
                                                'f_nacimiento'=>$this->input->post('fnacimiento'),
                                                't_fijo'=>$this->input->post('t_fijo'),
                                                't_celular'=>$this->input->post('t_celular'),
                                                'password'=>$this->session->userdata('password'),
                                                'correo'=>$this->input->post('correo'),
                                                'sexo'=>$this->input->post('sexo'),     
                                                'Apoderado_Id'=>$this->input->post('apoderado'),
                                                'profesion'=>$this->input->post('profesion'));
                        $this->profesor_add($datos_profesor);
                    }
               }
               if($function=="Ficha"){
                    if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                    {
                        $datos['password']=$this->security_lib->generator();
                        $query=$this->administracion_model->ver_profesor($Id);
                        $datos['input']=$this->color_input;
                        if($query['bool'])
                        {
                            $datos['q']=$query['query'];
                            $datos['fecha'] = date('m/d/Y',strtotime($datos['q']->f_nacimiento));
                            $datos['Id']=$query['query']->Id;
                            $this->session->set_userdata('profesor',$query['query']);
                            $this->layout->set_specific_js(array('js/forms-elemets.js'));
                            $this->layout->view('administracion/profesor/ad_profesor_ficha',$datos);
                        }
                        else
                        {
                            redirect("administracion/profesor");
                        }
                    }
               } 
               if($function=="Edit")
               {
                    if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                    {
                        $datos['password']=$this->security_lib->generator();
                        $query=$this->administracion_model->ver_profesor($Id);
                        if($query['bool'])
                        {
                            $datos['q']=$query['query'];
                            $datos['fecha'] = date('m/d/Y',strtotime($datos['q']->f_nacimiento));
                            $datos['Id']=$query['query']->Id;
                            $this->session->set_userdata('profesor',$query['query']);
                            $this->layout->set_specific_js(array('js/forms-elemets.js','js/validarut.js'));
                            $this->layout->view('administracion/profesor/ad_profesor_edit',$datos);
                        }
                        else
                        {
                            redirect("administracion/profesor");
                        }
                    }
                    else
                    {
                        $db_profesor=$this->session->userdata('profesor');                        
                        $datos_apoderado=array( 'nombre'=>$this->input->post('nombre'),
                                                'apellido'=>$this->input->post('apellido'),
                                                'rut'=>$this->input->post('rut'),
                                                'f_nacimiento'=>$this->input->post('fnacimiento'),
                                                't_fijo'=>$this->input->post('t_fijo'),
                                                't_celular'=>$this->input->post('t_celular'),
                                                'password'=>$this->session->userdata('password'),
                                                'correo'=>$this->input->post('correo'),
                                                'sexo'=>$this->input->post('sexo'),     
                                                'Apoderado_Id'=>$this->input->post('apoderado'),
                                                'profesion'=>$this->input->post('profesion'),
                                                'Id_Pr'=>$db_profesor->Id,
                                                'Id_Us'=>$db_profesor->Usuario_id);
                        $this->profesor_edit($datos_apoderado);
                    }
               }
               if($function=="Delete"){
                    try
                    {
                        $datos_profesor=array('hide'=>1);
                        $this->administracion_model->editar_tabla($Id, 'profesor', $datos_profesor);
                        $datos_log=array('Descripcion'=>'Elimino al profesor con Id '.$Id, 'Tabla'=>'profesor','Usuario_id'=>$this->ion_auth->get_user_id().'');
                        $this->administracion_model->agregar_tabla('log',$datos_log);
                        //editar el active de users
                        redirect("administracion/profesor");
                    }
                    catch(Exception $ex){

                        redirect("administracion/profesor?error");
                    }
               } 
            }
            else
            {               
                $mysql=$this->administracion_model->all_profesor();           
                if($mysql['bool'])
                {   
                    $datos['mysql']=$mysql['query'];
                    $this->layout->view('administracion/profesor/ad_profesor',$datos);
                }
                else
                {
                    $datos['errormsg']=$mysql['msg'];
                    $this->layout->view('administracion/profesor/ad_profesor',$datos);
                }
            }
        }
        public function profesor_add($datos_profesor)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->add_profesor($datos_profesor);
            $r=$respuesta['bool'];
            if($r)
            {
                $datos_log=array('Descripcion'=>'Agrego al profesor con Rut '.$datos_profesor['rut'], 'Tabla'=>'profesor','Usuario_id'=>$this->ion_auth->get_user_id().'');
                $this->administracion_model->agregar_tabla('log',$datos_log);
               redirect('administracion/profesor');
            }
            else
            {
                $dato['errormsg']=$respuesta['msg'];
                $datos['password']=$this->security_lib->generator();
                $this->layout->view('administracion/profesor/ad_profesor_add',$dato);
            }
        }
        public function profesor_edit($datos_profesor)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->edit_profesor($datos_profesor);
            $r=$respuesta['bool'];
            if($r)
            {
                $datos_log=array('Descripcion'=>'Agrego al profesor con Id '.$datos_profesor['Id_Pr'], 'Tabla'=>'profesor','Usuario_id'=>$this->ion_auth->get_user_id().'');
                $this->administracion_model->agregar_tabla('log',$datos_log);
               redirect('administracion/profesor');
            }
            else
            {
                $datos['errormsg']=$respuesta['msg'];
                $datos['password']=$this->security_lib->generator();
                $this->layout->view('administracion/profesor/ad_profesor_add',$datos);
            }
        }
        /*public function profesor_delete($datos_profesor)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->add_profesor($datos_profesor);
            $r=$respuesta['bool'];
            if($r)
            {
                
               redirect('administracion/profesor');
            }
            else
            {
                $dato['errormsg']=$respuesta['msg'];
                $datos['password']=$this->security_lib->generator();
                $this->layout->view('administracion/profesor/ad_profesor_add',$dato);
            }
        }*/
    // ************  FIN PROFESOR *****************

    public function bloque($function =NULL,$Id=NULL)
    {
        $this->verificar();
        $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js','js/forms-elemets.js','js/print.js'));
        if($function!==NULL)
        {
            if($function=="Add")
            {
                if($this->input->post('hi')==NULL || $this->input->post('hi')=="")
                {
                    $this->layout->set_specific_js(array('js/forms-elemets.js'));
                    $this->layout->view('administracion/bloque/ad_bloque_add');

                }
                else
                {  
                    $datos_bloque=array( 
                                            'hora_inicio'=>$this->input->post('hi'),
                                            'hora_fin'=>$this->input->post('hf'));
                    $this->bloque_add($datos_bloque);
                }
            }
            if($function=="Edit")
           {
                if($this->input->post('hi')==NULL || $this->input->post('hi')=="")
                {
                    $query=$this->administracion_model->all_Bloques($Id);
                    if($query['bool'])
                    {
                        $datos['z']=$query['query'];
                        $datos['Id']=$query['query']->Id;
                        $this->session->set_userdata('bloque',$query['query']);
                        $this->layout->set_specific_js(array('js/forms-elemets.js'));
                        $this->layout->view('administracion/bloque/ad_bloque_edit',$datos);
                    }
                    else
                    {
                        redirect("administracion/bloque");
                    }
                }
                else
                {
                    $db_bloque=$this->session->userdata('bloque');
                    $datos_bloque=array( 
                                        'hora_inicio'=>$this->input->post('hi'),
                                        'hora_fin'=>$this->input->post('hf'));
                    $this->administracion_model->editar_tabla($db_bloque->Id,'bloques', $datos_bloque);
                    $datos_log=array('Descripcion'=>'Edito el Bloque con Id '.$db_bloque->Id, 'Tabla'=>'bloque','Usuario_id'=>$this->ion_auth->get_user_id().'');
                    $this->administracion_model->agregar_tabla('log',$datos_log);
                    redirect('administracion/bloque');
                }
           }
           if($function=="Delete"){
                try
                {
                    $datos_blque=array('hide'=>1);
                    $this->administracion_model->editar_tabla($Id, 'bloques', $datos_blque);
                    //editar el active de users
                    $datos_log=array('Descripcion'=>'Elimino el Bloque con Id '.$Id, 'Tabla'=>'bloque','Usuario_id'=>$this->ion_auth->get_user_id().'');
                    $this->administracion_model->agregar_tabla('log',$datos_log);
                    redirect("administracion/bloque");
                }
                catch(Exception $ex){

                    redirect("administracion/bloque?error");
                }
           } 

        }
        else
        {
            $mysql=$this->administracion_model->all_Bloques();           
            if($mysql['bool'])
            {   
                $datos['mysql']=$mysql['query'];
                $this->layout->view('administracion/bloque/ad_bloque',$datos);
            }
            else
            {
                $datos['errormsg']=$mysql['msg'];
                $this->layout->view('administracion/bloque/ad_bloque',$datos);
            }
        }
    }

    public function bloque_add($datos_bloque)
    {
        $this->verificar();
        $respuesta=$this->administracion_model->add_bloque($datos_bloque);
        $r=$respuesta['bool'];
        if($r)
        {
            $datos_log=array('Descripcion'=>'Agrego el Bloque con hora inicio '.$datos_bloque['hora_inicio'].' y hora final'.$datos_bloque['hora_fin'], 'Tabla'=>'bloque','Usuario_id'=>$this->ion_auth->get_user_id().'');
            $this->administracion_model->agregar_tabla('log',$datos_log);
           redirect('administracion/bloque');
        }
        else
        {
            $dato['errormsg']=$respuesta['msg'];
            $this->layout->view('administracion/bloque/ad_bloque_add',$dato);
        }
    }
    public function curso($function =NULL,$Id=NULL)
    {
        $this->verificar();
        $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js'));
        if($function!==NULL)
        {
            if($function=="Add")
            {

                if($this->input->post('tipo')==NULL || $this->input->post('tipo')=="")
                {
                    $profesores_all = $this->administracion_model->all_Profesor();
                    $datos['all_profesor'] = $profesores_all['query'];
                    $this->layout->set_specific_js(array('js/forms-elemets.js'));
                    $this->layout->view('administracion/curso/ad_curso_add',$datos);

                }
                else
                {  
                    $datos_curso=array( 
                                            'tipo'=>$this->input->post('tipo'),
                                            'grado'=>$this->input->post('grado'),
                                            'letra'=>$this->input->post('letra'),
                                            'profesor_id'=>$this->input->post('id_profesor'));
                    $this->curso_add($datos_curso);
                }
                //$this->layout->view('administracion/curso/ad_curso_add');
            }
            if($function=="Ficha"){
                if($this->input->post('tipo')==NULL || $this->input->post('tipo')=="")
                {
                    $profesores_all = $this->administracion_model->all_Profesor();
                    $datos['all_profesor'] = $profesores_all['query'];
                    $query=$this->administracion_model->all_curso($Id);
                    $datos['input']=$this->color_input;
                    if($query['bool'])
                    {
                        $datos['q']=$query['query'];
                        $this->session->set_userdata('curso',$query['query']);
                        $this->layout->set_specific_js(array('js/forms-elemets.js'));
                        $this->layout->view('administracion/curso/ad_curso_ficha',$datos);
                    }
                    else
                    {
                        redirect("administracion/curso");
                    }
                }
           } 
           if($function=="Edit")
           {
                if($this->input->post('tipo')==NULL || $this->input->post('tipo')=="")
                {
                    $profesores_all = $this->administracion_model->all_Profesor();
                    $datos['all_profesor'] = $profesores_all['query'];
                    $query=$this->administracion_model->all_curso($Id);

                    if($query['bool'])
                    {
                        $datos['q']=$query['query'];
                        $this->session->set_userdata('curso',$query['query']);
                        $this->layout->set_specific_js(array('js/forms-elemets.js'));
                        $this->layout->view('administracion/curso/ad_curso_edit',$datos);
                    }
                    else
                    {
                        redirect("administracion/curso");
                    }
                }
                else
                {
                    $db_profesor=$this->session->userdata('curso');   
                    $letra = strtoupper($this->input->post('letra'));
                    $datos_curso=array(     'Tipo'=>$this->input->post('tipo'),
                                            'Grado'=>$this->input->post('grado'),
                                            'Letra'=>$letra,
                                            'Profesor_Id'=>$this->input->post('id_profesor'));                      
                    
                    $this->administracion_model->editar_tabla($db_profesor->Id,'curso', $datos_curso);
                    $datos_log=array('Descripcion'=>'Edito curso con id '.$db_profesor->Id, 'Tabla'=>'curso','Usuario_id'=>$this->ion_auth->get_user_id().'');
                    $this->administracion_model->agregar_tabla('log',$datos_log);

                    //editar el active de users
                    redirect("administracion/curso");
                }
           }
           if($function=="Delete"){
                try
                {
                    $datos_curso=array('hide'=>1);
                    $this->administracion_model->editar_tabla($Id, 'curso', $datos_curso);
                    $datos_log=array('Descripcion'=>'Elimino curso con id '.$Id, 'Tabla'=>'curso','Usuario_id'=>$this->ion_auth->get_user_id().'');
                    $this->administracion_model->agregar_tabla('log',$datos_log);
                    //editar el active de users
                    redirect("administracion/curso");
                }
                catch(Exception $ex){

                    redirect("administracion/curso?error");
                }
           }

        }
        else
        {
            $mysql=$this->administracion_model->all_Curso();          
            if($mysql['bool'])
            {   
                $profesores_all = $this->administracion_model->all_Profesor();
                $datos['all_profesor'] = $profesores_all['query'];
                $datos['mysql']=$mysql['query'];
                $this->layout->view('administracion/curso/ad_curso',$datos);
            }
            else
            {
                $datos['errormsg']=$mysql['msg'];
                $this->layout->view('administracion/curso/ad_curso',$datos);
            }
            /*$this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js','js/forms-elemets.js'));
            $this->layout->view('administracion/curso/ad_curso');*/
        }
    }
    public function curso_add($datos_curso)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->add_curso($datos_curso);
            $r=$respuesta['bool'];
            if($r)
            {
                $datos_log=array('Descripcion'=>'Agrego el Curso '.$datos_curso['Grado'].' '.$datos_curso['Letra'], 'Tabla'=>'curso','Usuario_id'=>$this->ion_auth->get_user_id().'');
                $this->administracion_model->agregar_tabla('log',$datos_log);
               redirect('administracion/curso');
            }
            else
            {
                $dato['errormsg']=$respuesta['msg'];
                //$datos['password']=$this->security_lib->generator();
                $this->layout->view('administracion/curso/ad_curso_add',$dato);
            }
        }
    public function asignatura($function = NULL,$Id=NULL)
    {
        $this->verificar();
        $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js'));
        if($function!==NULL)
        {
            if($function=="Add")
            {
                if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                {
                    $this->layout->set_specific_js(array('js/forms-elemets.js'));
                    $this->layout->view('administracion/asignatura/ad_asignatura_add');

                }
                else
                {
                    /* 1    Id  Nombre  Descripcion hora_total */  
                    $datos_asignatura=array( 
                        'Nombre'=>$this->input->post('nombre'),
                        'Descripcion'=>$this->input->post('descripcion'),
                        'Hora_Total'=>$this->input->post('hora_total'));
                    $this->add_asignatura($datos_asignatura);
                }

            }
            if($function=="Ficha"){
                if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                {
                    $query=$this->administracion_model->all_Asignatura($Id);
                    if($query['bool'])
                    {
                        $datos['q']=$query['query'];
                        $this->session->set_userdata('asignatura',$query['query']);
                        $datos['input']=$this->color_input;
                        $this->layout->set_specific_js(array('js/forms-elemets.js'));
                        $this->layout->view('administracion/asignatura/ad_asignatura_ficha',$datos);
                    }
                    else
                    {
                        redirect("administracion/asignatura");
                    }
                }
           } 
           if($function=="Edit")
           {
                if($this->input->post('nombre')==NULL || $this->input->post('nombre')=="")
                {
                    $query=$this->administracion_model->all_Asignatura($Id);
                    if($query['bool'])
                    {
                        $datos['q']=$query['query'];
                        $this->session->set_userdata('asignatura',$query['query']);
                        $this->layout->set_specific_js(array('js/forms-elemets.js'));
                        $this->layout->view('administracion/asignatura/ad_asignatura_edit',$datos);
                    }
                    else
                    {
                        redirect("administracion/asignatura");
                    }
                }
                else
                {
                    $db_asignatura=$this->session->userdata('asignatura');   
                    $datos_asignatura=array( 
                        'Nombre'=>$this->input->post('nombre'),
                        'Descripcion'=>$this->input->post('descripcion'),
                        'Hora_Total'=>$this->input->post('hora_total'));                      
                    
                    $this->administracion_model->editar_tabla($db_asignatura->Id,'asignatura', $datos_asignatura);
                    $datos_log=array('Descripcion'=>'Edito la Asignatura con id '.$db_asignatura->Id, 'Tabla'=>'asignatura','Usuario_id'=>$this->ion_auth->get_user_id().'');
                    $this->administracion_model->agregar_tabla('log',$datos_log);
                    //editar el active de users
                    redirect("administracion/asignatura");
                }
           }
           if($function=="Delete"){
                try
                {
                    $datos_asignatura=array('hide'=>1);
                    $this->administracion_model->editar_tabla($Id, 'asignatura', $datos_asignatura);
                    $datos_log=array('Descripcion'=>'Elimino la Asignatura con id '.$Id, 'Tabla'=>'asignatura','Usuario_id'=>$this->ion_auth->get_user_id().'');
                    $this->administracion_model->agregar_tabla('log',$datos_log);
                    //editar el active de users
                    redirect("administracion/asignatura");
                }
                catch(Exception $ex){

                    redirect("administracion/asignatura?error");
                }
           }
        }
        else
        {
            $mysql=$this->administracion_model->all_Asignatura();           
            if($mysql['bool'])
            {   
                if($mysql >0){
                    $datos['mysql']=$mysql['query'];
                    $this->layout->view('administracion/asignatura/ad_asignatura',$datos);
                }
                else{
                    $this->layout->view('administracion/asignatura/ad_asignatura');
                }
            }
            else
            {
                $datos['errormsg']=$mysql['msg'];
                $datos['mysql']=NULL;
                $this->layout->view('administracion/asignatura/ad_asignatura',$datos);
            }
            
        }
    }
    public function add_asignatura($datos){
        $this->verificar();
        $respuesta=$this->administracion_model->add_asignatura($datos);
        $r=$respuesta['bool'];
        if($r)
        {
            $datos_log=array('Descripcion'=>'Agrego la asignatura '.$datos['Nombre'], 'Tabla'=>'asignatura','Usuario_id'=>$this->ion_auth->get_user_id().'');
                $this->administracion_model->agregar_tabla('log',$datos_log);
           redirect('administracion/asignatura');
        }
        else
        {
            $dato['errormsg']=$respuesta['msg'];
            //$datos['password']=$this->security_lib->generator();
            $this->layout->view('administracion/asignatura/ad_asignatura_add',$dato);
        }
    }
    //AQUI VOY
    ////
    ////
    ///
    ////
    ///
    ////
    ///
    public function horario($function = NULL,$Id=NULL){
        $this->verificar();
        $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js'));
        if($function!==NULL)
        {
            if($function=="Add")
            {
                if($this->input->post('temporada')==NULL || $this->input->post('temporada')=="")
                {
                    $_bloque = $this->administracion_model->all_Bloques();
                    $_curso_asignatura = $this->administracion_model->all_Curso_Asignatura();
                    $datos['all_bloque'] = $_bloque['query'];
                    $datos['all_curso_asignatura'] = $_curso_asignatura['query'];
                    $this->layout->set_specific_js(array('js/forms-elemets.js'));
                    $this->layout->view('administracion/horario/ad_horario_add',$datos);

                }
                else
                {  
                    $datos_curso=array( 
                        'Temporada' => $this->input->post('temporada'),
                        'Año' => $this->input->post('agno'),
                        'Dia' => $this->input->post('dia'),
                        'Bloque_Id'=>$this->input->post('id_bloque'),
                        'Curso_Asignatura_Id'=>$this->input->post('id_curso_asignatura'));
                    $this->horario_add($datos_curso);
                }

            }
            if($function=="Edit")
           {
                if($this->input->post('temporada')==NULL || $this->input->post('temporada')=="")
                {
                    $_bloque = $this->administracion_model->all_Bloques();
                    $_curso_asignatura = $this->administracion_model->all_Curso_Asignatura();
                    $datos['all_bloque'] = $_bloque['query'];
                    $datos['all_curso_asignatura'] = $_curso_asignatura['query'];
                    $query=$this->administracion_model->all_horario($Id);
                    if($query['bool'])
                    {
                        $datos['q']=$query['query'];
                        $this->session->set_userdata('horario',$query['query']);
                        $this->layout->set_specific_js(array('js/forms-elemets.js'));
                        $this->layout->view('administracion/horario/ad_horario_edit',$datos);
                    }
                    else
                    {
                        redirect("administracion/horario");
                    }
                }
                else
                {
                    $db_asignatura=$this->session->userdata('horario');   
                    $datos_curso=array( 
                        'Temporada' => $this->input->post('temporada'),
                        'Año' => $this->input->post('agno'),
                        'Dia' => $this->input->post('dia'),
                        'Bloques_Id'=>$this->input->post('id_bloque'),
                        'Curso_has_Asignatura_Id'=>$this->input->post('id_curso_asignatura'));                     
                    
                    $this->administracion_model->editar_tabla($db_asignatura->Id,'horario', $datos_curso);
                    $datos_log=array('Descripcion'=>'Edito el Horario con Id '.$db_asignatura->Id, 'Tabla'=>'horario','Usuario_id'=>$this->ion_auth->get_user_id().'');
                    $this->administracion_model->agregar_tabla('log',$datos_log);
                    //editar el active de users
                    redirect("administracion/horario");
                }
           }
           if($function=="Delete"){
                try
                {
                    $datos_asignatura=array('hide'=>1);
                    $this->administracion_model->editar_tabla($Id, 'horario', $datos_asignatura);
                    //editar el active de users
                    $datos_log=array('Descripcion'=>'Elimino el Horario con Id '.$Id, 'Tabla'=>'horario','Usuario_id'=>$this->ion_auth->get_user_id().'');
                    $this->administracion_model->agregar_tabla('log',$datos_log);
                    redirect("administracion/horario");
                }
                catch(Exception $ex){

                    redirect("administracion/horario?error");
                }
           }
        }
        else
        {
            $mysql=$this->administracion_model->all_horario();           
            if($mysql['bool'])
            {   
                $datos['mysql']=$mysql['query'];
                $this->layout->view('administracion/horario/ad_horario',$datos);
            }
            else
            {
                $datos['errormsg']=$mysql['msg'];
                $this->layout->view('administracion/horario/ad_horario',$datos);
            }
        }
    }
    public function horario_add($datos){
        $this->verificar();
        $respuesta=$this->administracion_model->horario_add($datos);
        $r=$respuesta['bool'];
        if($r)
        {            

            /*
            'Temporada' => $this->input->post('temporada'),
                        'Año' => $this->input->post('agno'),
                        'Dia' => $this->input->post('dia'),
                        'Bloque_Id'=>$this->input->post('id_bloque'),
                        'Curso_Asignatura_Id'=>$this->input->post('id_curso_asignatura'));
            */
            $datos_log=array('Descripcion'=>'Agrego el Horario con año '.$datos['Año'].', para el día '.$datos['Dia'].', en el curso/asignatura con id '.$datos['Curso_Asignatura_Id'], 'Tabla'=>'horario','Usuario_id'=>$this->ion_auth->get_user_id().'');
                    $this->administracion_model->agregar_tabla('log',$datos_log);
           redirect('administracion/horario');
        }
        else
        {
            $dato['errormsg']=$respuesta['msg'];
            //$datos['password']=$this->security_lib->generator();
            $this->layout->view('administracion/horario/ad_horario_add',$dato);
        }
    }
    public function curso_asignatura($function =NULL,$Id=NULL)
    {
        $this->verificar();
        $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js'));
        if($function!==NULL)
        {
            if($function=="Add")
            {
                if($this->input->post('id_curso')==NULL || $this->input->post('id_curso')=="")
                {
                    $_curso = $this->administracion_model->all_Curso();
                    $_asignatura = $this->administracion_model->all_Asignatura();
                    $datos['all_curso'] = $_curso['query'];
                    $datos['all_asignatura'] = $_asignatura['query'];
                    $this->layout->set_specific_js(array('js/forms-elemets.js'));
                    $this->layout->view('administracion/curso_asignatura/ad_profasig_add',$datos);

                }
                else
                {  
                    $datos_curso=array( 
                        'Curso_Id'=>$this->input->post('id_curso'),
                        'Asignatura_Id'=>$this->input->post('id_asignatura'));
                    $this->curso_asignatura_add($datos_curso);
                }

            }
            if($function=="Edit")
           {
                if($this->input->post('id_curso')==NULL || $this->input->post('id_curso')=="")
                {
                    $_curso = $this->administracion_model->all_Curso();
                    $_asignatura = $this->administracion_model->all_Asignatura();
                    $query=$this->administracion_model->all_Curso_Asignatura($Id);
                    if($query['bool'])
                    {
                        $datos['q']=$query['query'];
                        $datos['all_curso'] = $_curso['query'];
                        $datos['all_asignatura'] = $_asignatura['query'];
                        $this->session->set_userdata('curso_asignatura',$query['query']);
                        $this->layout->set_specific_js(array('js/forms-elemets.js'));
                        $this->layout->view('administracion/curso_asignatura/ad_profasig_edit',$datos);
                    }
                    else
                    {
                        redirect("administracion/curso_asignatura");
                    }
                }
                else
                {
                    $db_asignatura=$this->session->userdata('curso_asignatura');   
                    $datos_curso=array( 
                        'Curso_Id'=>$this->input->post('id_curso'),
                        'Asignatura_Id'=>$this->input->post('id_asignatura'));                     
                    
                    $this->administracion_model->editar_tabla($db_asignatura->Id,'curso_has_asignatura', $datos_curso);
                    //editar el active de users
                    redirect("administracion/curso_asignatura");
                }
           }
           if($function=="Delete"){
                try
                {
                    $datos_asignatura=array('hide'=>1);
                    $this->administracion_model->editar_tabla($Id, 'curso_has_asignatura', $datos_asignatura);
                    //editar el active de users
                    redirect("administracion/curso_asignatura");
                }
                catch(Exception $ex){

                    redirect("administracion/curso_asignatura?error");
                }
           }
        }
        else
        {
            $mysql=$this->administracion_model->all_Curso_Asignatura();           
            if($mysql['bool'])
            {   
                $datos['mysql']=$mysql['query'];
                $this->layout->view('administracion/curso_asignatura/ad_profasig',$datos);
            }
            else
            {
                $datos['errormsg']=$mysql['msg'];
                $this->layout->view('administracion/curso_asignatura/ad_profasig',$datos);
            }
        }
        
    }
    public function curso_asignatura_add($datos_curso_asignatura)
        {
            $this->verificar();
            $respuesta=$this->administracion_model->curso_asignatura_add($datos_curso_asignatura);
            $r=$respuesta['bool'];
            if($r)
            {
                
               redirect('administracion/curso_asignatura');
            }
            else
            {
                $dato['errormsg']=$respuesta['msg'];
                //$datos['password']=$this->security_lib->generator();
                $this->layout->view('administracion/curso_asignatura/ad_profasig_add',$dato);
            }
        }
    public function profesor_asignatura($function =NULL,$Id=NULL)
    {
        $this->verificar();
        $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js'));
        if($function!==NULL)
        {
            if($function=="Add")
            {
                if($this->input->post('id_profesor')==NULL || $this->input->post('id_profesor')=="")
                {
                    //$_profesor_asignatura = $this->administracion_model->all_Profesor_Asignatura();
                    $_profesor = $this->administracion_model->all_Profesor();
                    $_asignatura = $this->administracion_model->all_Curso_Asignatura();
                    $datos['all_profesor'] = $_profesor['query'];
                    $datos['all_asignatura'] = $_asignatura['query'];
                    $this->layout->set_specific_js(array('js/forms-elemets.js'));
                    $this->layout->view('administracion/profesor_asignatura/ad_profasig_add',$datos);

                }
                else
                {  
                    $datos_profesor_asignatura=array( 
                        'Profesor_Id'=>$this->input->post('id_profesor'),
                        'Curso_has_Asignatura_Id'=>$this->input->post('id_asignatura'));
                    $this->profesor_asignatura_add($datos_profesor_asignatura);
                }
                //$this->layout->view('administracion/profesor_asignatura/ad_profasig_add');
            }
            if($function=="Edit")
           {
                if($this->input->post('id_profesor')==NULL || $this->input->post('id_profesor')=="")
                {
                    $_profesor = $this->administracion_model->all_Profesor();
                    $_asignatura = $this->administracion_model->all_Curso_Asignatura();
                    $datos['all_profesor'] = $_profesor['query'];
                    $datos['all_asignatura'] = $_asignatura['query'];
                    $query=$this->administracion_model->all_Profesor_Asignatura($Id);  
                    if($query['bool'])
                    {
                        $datos['q']=$query['query'];
                        $this->session->set_userdata('profesor_asignatura',$query['query']);
                        $this->layout->set_specific_js(array('js/forms-elemets.js'));
                        $this->layout->view('administracion/profesor_asignatura/ad_profasig_edit',$datos);
                    }
                    else
                    {
                        redirect("administracion/profesor_asignatura");
                    }
                }
                else
                {
                    $db_asignatura=$this->session->userdata('profesor_asignatura');   
                    $datos_profesor_asignatura=array( 
                        'Profesor_Id'=>$this->input->post('id_profesor'),
                        'Curso_has_Asignatura_Id'=>$this->input->post('id_asignatura'));                   
                    
                    $this->administracion_model->editar_tabla($db_asignatura->Id,'profesor_has_asignatura', $datos_profesor_asignatura);
                    //editar el active de users
                    redirect("administracion/profesor_asignatura");
                }
           }
           if($function=="Delete"){
                try
                {
                    $datos_asignatura=array('hide'=>1);
                    $this->administracion_model->editar_tabla($Id, 'profesor_has_asignatura', $datos_asignatura);
                    //editar el active de users
                    redirect("administracion/profesor_asignatura");
                }
                catch(Exception $ex){

                    redirect("administracion/profesor_asignatura?error");
                }
           }

        }
        else
        {
            $this->verificar();
            $mysql=$this->administracion_model->all_Profesor_Asignatura();           
            if($mysql['bool'])
            {   
                $datos['mysql']=$mysql['query'];
                $this->layout->view('administracion/profesor_asignatura/ad_profasig',$datos);
            }
            else
            {
                $datos['errormsg']=$mysql['msg'];
                $this->layout->view('administracion/profesor_asignatura/ad_profasig',$datos);
            }
            //$this->layout->view('administracion/profesor_asignatura/ad_profasig');
        }
    }
    public function profesor_asignatura_add($datos){
        $this->verificar();
        $respuesta=$this->administracion_model->profesor_asignatura_add($datos);
        $r=$respuesta['bool'];
        if($r)
        {
            
           redirect('administracion/profesor_asignatura');
        }
        else
        {
            $dato['errormsg']=$respuesta['msg'];
            //$datos['password']=$this->security_lib->generator();
            $this->layout->view('administracion/profesor_asignatura/ad_profasig_add',$dato);
        }
    }
    public function curso_alumno($function =NULL,$Id=NULL)
    {
        $this->verificar();
        $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js'));
        if($function!==NULL)
        {
            if($function=="Add")
            {
                if($this->input->post('id_curso')==NULL || $this->input->post('id_curso')=="")
                {
                    //$_profesor_asignatura = $this->administracion_model->all_Profesor_Asignatura();
                    $_curso = $this->administracion_model->all_Curso();
                    $_alumno = $this->administracion_model->all_Alumno();
                    $datos['all_curso'] = $_curso['query'];
                    $datos['all_alumno'] = $_alumno['query'];
                    //$datos['all_profesor_asignatura'] = $_profesor_asignatura['query'];
                    //var_dump($datos['all_profesor_asignatura']);
                    $this->layout->set_specific_js(array('js/forms-elemets.js'));
                    $this->layout->view('administracion/curso_alumno/ad_curalum_add',$datos);

                }
                else
                {  
                    $datos_=array( 
                        'Curso_Id'=>$this->input->post('id_curso'),
                        'Alumno_Id'=>$this->input->post('id_alumno'));
                    $this->curso_alumno_add($datos_);
                }
                //$this->layout->view('administracion/profesor_asignatura/ad_profasig_add');
            }
            if($function=="Edit")
           {
                if($this->input->post('id_curso')==NULL || $this->input->post('id_curso')=="")
                {
                    $_curso = $this->administracion_model->all_Curso();
                    $_alumno = $this->administracion_model->all_Alumno();
                    $datos['all_curso'] = $_curso['query'];
                    $datos['all_alumno'] = $_alumno['query'];
                    $query=$this->administracion_model->all_Curso_Alumno($Id);  
                    if($query['bool'])
                    {
                        $datos['q']=$query['query'];
                        $this->session->set_userdata('curso_alumno',$query['query']);
                        $this->layout->set_specific_js(array('js/forms-elemets.js'));
                        $this->layout->view('administracion/curso_alumno/ad_curalum_edit',$datos);
                    }
                    else
                    {
                        redirect("administracion/curso_alumno");
                    }
                }
                else
                {
                    $db_asignatura=$this->session->userdata('curso_alumno');   
                    $datos_=array( 
                        'Curso_Id'=>$this->input->post('id_curso'),
                        'Alumno_Id'=>$this->input->post('id_alumno'));                  
                    
                    $this->administracion_model->editar_tabla($db_asignatura->Id,'curso_has_alumno', $datos_);
                    //editar el active de users
                    redirect("administracion/curso_alumno");
                }
           }
           if($function=="Delete"){
                try
                {
                    $datos_asignatura=array('hide'=>1);
                    $this->administracion_model->editar_tabla($Id, 'curso_has_alumno', $datos_asignatura);
                    //editar el active de users
                    redirect("administracion/curso_alumno");
                }
                catch(Exception $ex){

                    redirect("administracion/curso_alumno?error");
                }
           }

        }
        else
        {
            $mysql=$this->administracion_model->all_Curso_Alumno();           
            if($mysql['bool'])
            {   
                $datos['mysql']=$mysql['query'];
                $this->layout->view('administracion/curso_alumno/ad_curalum',$datos);
            }
            else
            {
                $datos['errormsg']=$mysql['msg'];
                $this->layout->view('administracion/curso_alumno/ad_curalum',$datos);
            }
            //$this->layout->view('administracion/profesor_asignatura/ad_profasig');
        }
    }
    public function curso_alumno_add($datos){
        $this->verificar();
        $respuesta=$this->administracion_model->curso_alumno_add($datos);
        $r=$respuesta['bool'];
        if($r)
        {
            
           redirect('administracion/curso_alumno');
        }
        else
        {
            $dato['errormsg']=$respuesta['msg'];
            //$datos['password']=$this->security_lib->generator();
            $this->layout->view('administracion/curso_alumno/ad_curalum_add',$dato);
        }
    }
    public function reunion($function =NULL,$Id=NULL)
    {
        $this->verificar();
        if($function!==NULL)
        {
            if($function=="Add")
            {                
                $id_user = $this->ion_auth->get_user_id();
                
                $id_profesor = $this->administracion_model->all_Profesor($id_user);
                
                $id_curso = $this->administracion_model->get_id_Curso($id_profesor['query']->Id);
                //Fecha, Curso_Id, hide, Motivo
                $fecha_get = $this->input->post('fecha_reunion');
                $explode = explode('/',$fecha_get);
                $dia = $explode[0];
                $mes = $explode[1];
                $año = $explode[2];
                $fecha = $mes.'/'.$dia.'/'.$año;
                //echo $fecha;                 
                $newformat = date('Y/m/d',strtotime($fecha));
                //echo $newformat;
                try{
                    $datos_=array( 
                        'Fecha'=>$newformat,
                        'Hora'=>$this->input->post('hora'),
                        'hide'=>0,
                        'Curso_Id'=>$id_curso['query']->Id,
                        'Motivo'=>$this->input->post('asunto'));
                    $this->administracion_model->agregar_tabla('reuniones',$datos_);
                    redirect("administracion/reunion");
                }
                catch(Exception $ex){

                    redirect("administracion/reunion?error");
                }                
            }
            if($function=="Edit"){
                try{
                    $id_user = $this->ion_auth->get_user_id();                
                    $id_profesor = $this->administracion_model->all_Profesor($id_user);
                    $id_pro = $id_profesor['query']->Id;
                    $reuniones = $this->administracion_model->all_Reuniones($id_pro);
                    $sigue = false;
                    foreach($reuniones['query'] as $k){
                        if($Id == $k->Id){
                            $sigue = true;
                            break;
                        }
                    }
                    if($sigue){
                        $fecha_get = $this->input->post('fecha_reunion');
                        $explode = explode('/',$fecha_get);
                        $dia = $explode[0];
                        $mes = $explode[1];
                        $año = $explode[2];
                        $fecha = $mes.'/'.$dia.'/'.$año;
                        //echo $fecha;                 
                        $newformat = date('Y/m/d',strtotime($fecha));
                        $datos_=array( 
                            'Fecha'=>$newformat,
                            'Hora'=>$this->input->post('hora'),
                            'Motivo'=>$this->input->post('asunto'));
                        $this->administracion_model->editar_tabla($Id, 'reuniones', $datos_);
                        //editar el active de users
                        $datos['errormsg'] = "Se eliminaron los datos";
                        //echo $datos['errormsg'];
                        redirect("administracion/reunion",$datos);
                    }
                    else{
                        $datos['errormsg'] = "No puede eliminar un dato que no le pertenece";
                        //echo $datos['errormsg'];
                        redirect("administracion/reunion?errormsg=".$datos['errormsg']);
                    }
                }
                catch(Exception $ex){

                }
            }
            if($function=="Delete"){
                try
                {
                    $id_user = $this->ion_auth->get_user_id();                
                    $id_profesor = $this->administracion_model->all_Profesor($id_user);
                    $id_pro = $id_profesor['query']->Id;
                    $reuniones = $this->administracion_model->all_Reuniones($id_pro);
                    $sigue = false;
                    foreach($reuniones['query'] as $k){
                        if($Id == $k->Id){
                            $sigue = true;
                            break;
                        }
                    }
                    if($sigue){
                        $datos_asignatura=array('hide'=>1);
                        $this->administracion_model->editar_tabla($Id, 'reuniones', $datos_asignatura);
                        //editar el active de users
                        $datos['errormsg'] = "Se eliminaron los datos";
                        //echo $datos['errormsg'];
                        redirect("administracion/reunion",$datos);
                    }
                    else{
                        $datos['errormsg'] = "No puede eliminar un dato que no le pertenece";
                        //echo $datos['errormsg'];
                        redirect("administracion/reunion?errormsg=".$datos['errormsg']);
                    }
                }
                catch(Exception $ex){

                    redirect("administracion/reunion?error");
                }
           }

        }
        else
        {
            $id_user = $this->ion_auth->get_user_id();                
            $id_profesor = $this->administracion_model->all_Profesor($id_user);
            $id_pro = $id_profesor['query']->Id;
            $reuniones = $this->administracion_model->all_Reuniones($id_pro);

            $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js','js/forms-elemets.js'));
            if($reuniones['bool'])
            {   
                $datos['reuniones_all_profesor'] =  $reuniones['query'];
                /*$fecha_get = $reuniones->Fecha;
                $explode = explode('-',$fecha_get);
                $año = $explode[0];
                $mes = $explode[1];
                $dia = $explode[2];
                $datos['fecha_format'] = $dia.'/'.$mes.'/'.$año;*/
                $this->layout->view('administracion/reunion/ad_reunion',$datos);
            }
            else
            {
                $datos['errormsg']=$reuniones['msg'];
                $this->layout->view('administracion/reunion/ad_reunion',$datos);

            }
        }
    }
    function inbox($error=NULL)
    {
        $this->verificar();
        $datos=NULL;
        $id=$this->ion_auth->get_user_id();     
        $this->layout->set_specific_js(array('lib/jquery.dataTables.min.js','js/tables-dynamic.js','js/forms-elemets.js'));
        $query=$this->app_model->inbox_load($id);
        if($query['bool'])
        {
            $datos['inbox_load']=$query['query'];
            $datos['n_msg']=$query['n_new'];
            $datos['n_all']=$query['n_all'];
            if($error!==NULL)
            {
                $datos['msg']=$error;
            }
            
        }
        $datos['url']='administracion';
        $this->layout->view('app/inbox',$datos);
    }
    function create_message()
    {
        $this->verificar();
        if($this->input->post('title')==NULL || $this->input->post('title')=="")
        {
            $this->layout->set_specific_js(array('js/forms-article.js'));
            $id=$this->ion_auth->get_user_id(); 
            $var=array('Id_User'=>$id);
            $cursos=$this->app_model->load_curso($var);
            if($cursos['bool'])
            {
                $cursos=$cursos['query'];
                if(isset($cursos['bool_admin_cursos'])){if($cursos['bool_admin_cursos']){$datos['todos']['cursos']=$cursos['query_admin_cursos'];}}
                if(isset($cursos['bool_admin_profes'])){if($cursos['bool_admin_profes']){$datos['todos']['profes']=$cursos['query_admin_profes'];}}   
            }
            $datos['rango']='admin';
            $this->layout->view('app/create_message',$datos);
        }       
        else
        {
            $mensaje= array(    'Titulo' =>$this->input->post('title'), 
                                'Contenido' =>$this->input->post('text'),
                                'Fecha' =>date("Y-m-d"));
            $origen_destino=array(  'alumnos' =>$this->input->post('alumnos'),
                                    'apoderado' =>$this->input->post('apoderados'),
                                    'nivel' =>$this->input->post('status'));
            $datos = array('mensaje' =>$mensaje,'od'=>$origen_destino);
            //var_dump($datos);
            $this->enviar_message($datos);
        }   
    }
    function enviar_message($datos)
    {
        $this->verificar();
        //Guardar mensajes

        $Id_mensaje=0;
        $lvl=1;
        $Id_origen=$this->ion_auth->get_user_id();  
        if(isset($datos['mensaje']))
        {
            $datos_mensaje=$datos['mensaje'];
            $save=$this->app_model->save_alert($datos_mensaje);
            if($save['bool'])
            {
                $Id_mensaje=$save['id'];
            }
            else
            {
                echo $save['msg'].'<br>'.$save['e'];
            }
        }
        if(isset($datos['od']))
        {
            
            $datos_od=$datos['od'];         
            if($Id_mensaje!==0)
            {
                $mensaje=$datos['mensaje']['Contenido'];
                $title=$datos['mensaje']['Titulo'];
                if(isset($datos_od['alumnos']))
                {                   
                    $alumnos=$datos_od['alumnos'];
                    if(count($alumnos)>0 && $alumnos != false)
                    {
                        $lvl=1;
                        foreach ($alumnos as $q) 
                        {
                            $save_OD=$save=$this->app_model->save_OD($q,$Id_origen,$Id_mensaje,'alumno',$lvl,$mensaje,$title);
                        }
                    }
                }               
                if(isset($datos_od['apoderado']))
                {
                    if(isset($datos_od['nivel']) &&  $datos_od['nivel'] != NULL)
                    {
                        $lvl=$datos_od['nivel'];
                        if($lvl!=1 && $lvl!=2 && $lvl!=3 && $lvl!=4)
                        {
                            $lvl=1;
                        }
                    }
                    $apoderado=$datos_od['apoderado'];
                    if(count($apoderado)>0 &&  $apoderado != false)
                    {
                        foreach ($apoderado as $q) 
                        {
                            $save_OD=$save=$this->app_model->save_OD($q,$Id_origen,$Id_mensaje,'apoderado',$lvl,$mensaje,$title);
                        }
                    }
                }
                redirect('administracion/inbox');

            }
            else
            {
                redirect('administracion/inbox');
            }
        }
        
        //Enviar mensajes
    }   

    function miperfil()
    {
        $this->verificar();
        $MyId=$this->ion_auth->get_user_id();     
        $user=$this->ion_auth->user()->row();
        $data['myuser']=$user;
        $nombre=$user->first_name;
        $nombre=preg_split("/[\s,]+/",$nombre);
        $apellido=$user->last_name;
        $apellido=preg_split("/[\s,]+/",$apellido);
        $data['nombre']=$nombre[0].' '.$apellido[0]; 
        if($user->Id_twitter != null)
        {
            if($this->session->userdata('twitter_screen_name'))
            {
                $data['twitter_name']=$this->session->userdata('twitter_screen_name');
            }
            else
            {
                $data['twitter_name']="Inicio no Twitter";
            }
        }
        else{
            $data['twitter_active']=true;
        }
        $this->layout->view('app/perfil',$data);
    }


}
?>