<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Alumno extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
 
        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->model('app_model');
        $this->load->helper('url');
        $this->load->helper('alumno');
        $this->load->library(array('security_lib','ion_auth'));
        $this->load->model(array('administracion_model','ion_auth_model'));
        $this->load->model('alumno_model');
        $this->load->model('apoderado_model');
        $this->load->model('profesor_model'); 
        $this->soy();
        $this->mi_alumno();
        $this->load->library('layout');
        $this->load->library('dashboard');
        $this->load->library('academia');
        $this->layout->set_template('white');
    }
    private function soy()
    {
        if(!$this->session->userdata('soy'))
        {
            $this->session->set_userdata('soy','alumno');
        }
        else
        {
            $this->session->unset_userdata('soy');
            $this->session->set_userdata('soy','alumno');
        }
    }
    public function mi_alumno()
    {
        if(!$this->session->userdata('alumno_view'))
        {
            $alumno=$this->alumno_model->get_alumno($this->ion_auth->get_alumno_id());
            if($alumno['bool'])
            { 
                $this->session->set_userdata('alumno_view',$alumno['one']);
            }
        }
    }
    private function verificar(){
        $email = $this->session->userdata('identity');
        $codigo = $this->ion_auth_model->get_activication_code($email);
        $valor = $codigo['query'];
        if($valor == NULL || $valor == ""){
            redirect('auth/change_password');
        }
        else{
        $group = 3;
        if (!$this->ion_auth->in_group($group,$this->ion_auth->get_user_id())){
            //$this->session->set_flashdata('message', 'You must be a part of the gangstas or group 2');
            redirect('auth/ver_nuevo'); //Mostrar la vista
        }}
    }
    public function index()
    {
        $this->verificar();
        $this->layout->set_specific_js(array(   'js/loader.js',
                                                'lib/icheck.js/jquery.icheck.js',
                                                'lib/sparkline/jquery.sparkline.js',
                                                'lib/jquery-ui-1.10.3.custom.js',
                                                'lib/jquery.slimscroll.js',
                                                'lib/nvd3/lib/d3.v2.js',
                                                'lib/nvd3/nv.d3.custom.js',
                                                'lib/nvd3/src/models/scatter.js',
                                                'lib/nvd3/src/models/axis.js',
                                                'lib/nvd3/src/models/legend.js',
                                                'lib/nvd3/src/models/multiBar.js',
                                                'lib/nvd3/src/models/multiBarChart.js',
                                                'lib/nvd3/src/models/line.js',
                                                'lib/nvd3/src/models/lineChart.js',
                                                'lib/nvd3/stream_layers.js',
                                                'lib/backbone/backbone.localStorage-min.js',
                                                'js/index.js',
                                                'lib/fullcalendar/fullcalendar.js',
                                                'lib/jquery-ui-1.10.3.custom.js',
                                                'js/forms-elemets.js'));
        $data['dashboard']=$this->dashboard_generator();
        $data['soy']=$this->session->userdata('soy');
        $this->layout->view('apoderado/main',$data);
    }
    public function Academia($function=NULL)
    {
        $this->verificar();   
        $this->layout->set_specific_js(array('js/loader.js','lib/jquery.slimscroll.js','lib/fullcalendar/fullcalendar.js'));//'js/calendar.js
        $data['content']=$this->academia_generator($function);
        $data['soy']=$this->session->userdata('soy');
        $this->layout->view('apoderado/Academica/academia',$data);   
    }
    private function dashboard_generator()
    {
        $id=$this->ion_auth->get_alumno_id();
        $anotaciones = $this->apoderado_model->get_hojaVida_alumno($id);
        if($anotaciones['bool']){$anotaciones=$anotaciones['query'];}else{$anotaciones=null;}        
        $_anotaciones=$this->dashboard->anotaciones($anotaciones);

        //REUNIONES
        $reuniones = $this->apoderado_model->get_reuniones_alumno($id);
        if($reuniones['bool']){$reuniones=$reuniones['query'];}else{$reuniones=null;}        
        $_reuniones=$this->dashboard->reuniones($reuniones);

        //ULTIMAS NOTAS
        $notas = $this->apoderado_model->get_notas_alumno($id);        
        if($notas['bool']){$notas=$notas['query'];}else{$notas=null;}      
        $_notas=$this->dashboard->notas($notas); 

        //PRUEBAS
        $pruebas = $this->apoderado_model->get_pruebas_news($id); 
        if($pruebas['bool']){$pruebas=$pruebas['query'];}else{$pruebas=null;}        
        $_pruebas=$this->dashboard->prox_pruebas($pruebas);

        //NOTIFICACIONES
        //$notificaciones= $this->app->get_notificaciones($this->ion_auth->get_user_id());
        $_notificacion=$this->dashboard->notificaciones();
        $_stat=$this->dashboard->stat_apoderado($reuniones,$notas,$pruebas);
        $code=' <div class="row">
                    <div class="col-md-12">
                        <h2 class="page-title">Alumno <small>Plataforma educativa</small></h2>
                    </div>
                </div>   
                <div class="row">
                    <div class="col-md-8">
                        '.$_stat.'
                        <div class="col-md-6">
                            '.$_reuniones.'
                            '.$_notas.'
                    </div>  
                    <div class="col-md-6">
                        '.$_anotaciones.'
                        '.$_pruebas.'
                    </div>    
                    </div>
                    <div class="col-md-4">       
                        '.$_notificacion.'
                    </div>              
                </div>  ';
        return $code;
    }
    private function academia_generator($function)
    {
        $id=$this->app->get_alumno_id();     
        $notas=$this->apoderado_model->get_notas_alumno($id);
        $asignaturas=$this->apoderado_model->get_asignaturas_alumno($id);
        $set_notas=$this->apoderado_model->get_set_notas_alumno($id);
        $promedio=$this->apoderado_model->get_promedio_nota_alumno($id);
        $hojaVida=$this->apoderado_model->get_hojaVida_alumno($id);
        $horario=$this->apoderado_model->get_horario_alumno($id);
        $bloques=$this->apoderado_model->get_bloques_horario();
        $profe_jefe=$this->apoderado_model->get_porfe_jefe_alumno($id);
        $reuniones=$this->apoderado_model->get_reuniones_alumno($id);
        $profesores=$this->apoderado_model->get_profesores_alumno($id);
        $_content=$this->academia->content_pr($profesores,$profe_jefe,$reuniones,$function);
        $content=$this->academia->content_tab($notas,$asignaturas,$set_notas,$promedio,$hojaVida,$horario,$bloques,$function);
        $content=$this->academia->container($content,$_content);
        return $content;
    }
    public function foro($Id_foro)
    {
        $this->verificar();
        $datos=NULL;
        $this->layout->set_specific_js(array('js/forms-elemets.js','js/forms-article.js'));
        if(isset($Id_foro))
        {
            if(is_numeric($Id_foro) && $Id_foro > 0)
            {
                $Id_U=$this->ion_auth->get_user_id();
                $respuesta=$this->alumno_model->verificar_alumno_foro($Id_U,$Id_foro);
                if($respuesta['bool'])
                {
                    $foro=$this->profesor_model->get_foro($Id_foro);   
                    if($foro['bool'])                 
                    {
                        $datos['profesor'] = false;              
                        $datos['first_foro'] = $foro['query'];
                        $respuestas=$this->profesor_model->get_respuesta_foro($Id_foro);
                        if($respuestas['bool'])
                        {
                            $datos['respuestas_foro'] = $respuestas['query'];
                        }
                        $datos['url']="alumno";
                        $this->layout->view('app/foro',$datos); 
                    }                    
                    else
                    {
                        redirect('alumno/mis_cursos');
                    }                                                      
                }                
                else
                {   
                    redirect('alumno/mis_cursos');
                }
            }
            else
            {
                redirect('alumno/mis_cursos');
            }
        }
        else
        {
                redirect('alumno/mis_cursos');
        }
    } 
    public function mis_cursos()
    {
        $this->verificar();
        $Id_U=$this->ion_auth->get_user_id();
        $Alumno_Id=$this->alumno_model->get_Alumno_Id($Id_U);       
        if($Alumno_Id['bool'])
        {
            $Alumno_Id=$Alumno_Id['Id'];
            $Curso_Id=$this->alumno_model->get_Curso_Id($Alumno_Id);   
            if($Curso_Id['bool'])
            {
                $Id_C=$Curso_Id['Id'];
                $foros=$this->alumno_model->get_foros_curso($Alumno_Id,$Id_C);
                if($foros['bool']){$data['query_foros']=$foros['query'];}
                //var_dump($foros);
            }
            $asignaturas=$this->alumno_model->get_Asignaturas_Curso($Alumno_Id);
            if($asignaturas['bool']){$data['query_asg']=$asignaturas['query'];}
            $docentes=$this->alumno_model->get_Docentes_Asignatura($Alumno_Id);
            if($docentes['bool']){$data['query_docentes']=$docentes['query'];}
            

            $this->layout->view('alumno/Curso/mis_cursos',$data);
        }
        else
        {
            //$data['msg_error']=$Alumno_Id['msg'];
            redirect('alumno');
        }
    }
    public function save_comentario()
    {
        $this->verificar();
        try
        {
            $Id_foro = $this->input->post('id_foro');
            if(isset($Id_foro))
            {
                if(is_numeric($Id_foro))
                {

                    $User_Id=$this->ion_auth->get_user_id();
                    $comentario = (string)$this->input->post('comentario');
                    $save=$this->profesor_model->save_comentario_foro($Id_foro,$User_Id,$comentario);    
                                    
                    if($save['bool'])
                    {
                        $list=$this->update_comentarios($Id_foro);
                        if($list['bool'])
                        {
                            echo json_encode(array("bool"=>true,"respuestas"=>$list['code']));
                        }
                        else
                        {
                            echo json_encode(array("bool"=>false));
                        }
                    }
                    else
                    {
                        echo json_encode(array("bool"=>false));
                    }
                }
                else
                {
                    echo json_encode(array("bool"=>false));
                }
            }
            else
            {
                 echo json_encode(array("bool"=>false));
            }
        }
        catch(Exception $e)
        {
             echo json_encode(array("bool"=>false));
        }
    }
    private function update_comentarios($Id_foro)
    {
        if(isset($Id_foro))
        {
            if(is_numeric($Id_foro))
            {
                $foro=$this->profesor_model->get_foro($Id_foro);   
                if($foro['bool'])                 
                {
                    $Id_U=$this->ion_auth->get_user_id();
                    $respuesta=$this->alumno_model->verificar_alumno_foro($Id_U,$Id_foro);
                    if($respuesta['bool'])
                    {
                        $respuestas=$this->profesor_model->get_respuesta_foro($Id_foro);
                        if($respuestas['bool'])
                        {
                            $dt="left";
                            $code=null;
                            foreach ($foro['query'] as $f) 
                            {
                                $Titulo_foro=$f->Titulo;                                
                            }
                            foreach ($respuestas['query'] as $q) 
                            {
                                    $nombre=$q->first_name;
                                    $nombre=preg_split("/[\s,]+/",$nombre);
                                    $apellido=$q->last_name;
                                    $apellido=preg_split("/[\s,]+/",$apellido);
                                    $name=$nombre[0].' '.$apellido[0]; 
                                    if($q->Id_User===$Id_U){$type_user="Profesor";}else{$type_user="Alumno";}
                                    $msn_dir="";
                                    $img_dir=" pull-left";                                    
                                    $code.=' <section class="search-result">                            
                                                <header><h4>Re:'.$Titulo_foro.'</h4></header>
                                                <div id="chat" class="chat">
                                                    <div id="chat-messages" class="chat-messages">
                                                        <div class="chat-message">
                                                            <div class="sender'.$img_dir.'">
                                                                <div class="icon">
                                                                    <img src="'.base_url().'assets/bootstrap/img/user.png" class="img-circle" alt="">
                                                                </div>
                                                                <div class="time">
                                                               '.$type_user.'
                                                                </div>
                                                            </div>
                                                            <div class="chat-message-body'.$msn_dir.'">
                                                                <span class="arrow"></span>
                                                                <div class="sender"><i class="eicon eicon-user"></i><a href="#">'.$name.'</a> - <i class="eicon eicon-clock"></i>'.$q->Fecha.'</div>
                                                                <div class="text">
                                                                    '.$q->Comentario.'
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>';
                            }
                            $r = array('bool' => true, 'code'=> $code);
                            return $r;

                        }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
                    }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
                }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
            }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
        }else{$r = array('bool' => false, 'msg'=> $error);return $r;}
    }
     
}
