<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Apoderado extends CI_Controller {
 
    function __construct()
    {
        parent::__construct(); 
        $this->load->database();
        $this->load->model('app_model');
        $this->load->helper('url');
        $this->load->model('apoderado_model');
        $this->load->model('alumno_model');
        $this->load->library('layout');
        $this->load->library('dashboard');
        $this->load->library('academia');
        $this->soy();
        $this->mi_alumno();
        $this->layout->set_template('white');
    }
    private function soy()
    {
        if(!$this->session->userdata('soy'))
        {
            $this->session->set_userdata('soy','apoderado');
        }
        else
        {
            $this->session->unset_userdata('soy');
            $this->session->set_userdata('soy','apoderado');
        }
    }
    public function mi_alumno($id=null)
    {
        if(!$this->session->userdata('alumno_view'))
        {
            $alumno=$this->apoderado_model->get_alumnos($this->ion_auth->get_apoderado_id());
            if($alumno['bool'])
            { 
                $this->session->set_userdata('alumno_view',$alumno['one']);
                $this->session->set_userdata('mis_alumnos',$alumno['query']);
            }

        }
        if(!is_null($id))
        {
            if(is_numeric($id))
            {
                $alumnos=$this->session->userdata('mis_alumnos');
                $esta=false;
                foreach ($alumnos as $q) 
                {
                    if($q->Alumno_Id === $id)
                    {   
                        $esta=true;
                        $this->session->unset_userdata('alumno_view');
                        $this->session->set_userdata('alumno_view',$q);
                        break;
                    }
                }
                if($esta)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                         
            }   
            else
            {
                return false;
            }        
        }
        else
        {
            return false;
        }         
    }
    private function verificar()
    {
        $email = $this->session->userdata('identity');
        $codigo = $this->ion_auth_model->get_activication_code($email);
        $valor = $codigo['query'];
        if($valor == NULL || $valor == ""){
            redirect('auth/change_password');
        }
        else{
        $group = 2;
        if (!$this->ion_auth->in_group($group,$this->ion_auth->get_user_id())){
            //$this->session->set_flashdata('message', 'You must be a part of the gangstas or group 2');
            redirect('auth/ver_nuevo','refresh'); //Mostrar la vista
        }}
    }
    public function index()
    {
        $this->verificar();
        $this->layout->set_specific_js(array(   'js/loader.js',
                                                'lib/icheck.js/jquery.icheck.js',
                                                'lib/sparkline/jquery.sparkline.js',
                                                'lib/jquery-ui-1.10.3.custom.js',
                                                'lib/jquery.slimscroll.js',
                                                'lib/nvd3/lib/d3.v2.js',
                                                'lib/nvd3/nv.d3.custom.js',
                                                'lib/nvd3/src/models/scatter.js',
                                                'lib/nvd3/src/models/axis.js',
                                                'lib/nvd3/src/models/legend.js',
                                                'lib/nvd3/src/models/multiBar.js',
                                                'lib/nvd3/src/models/multiBarChart.js',
                                                'lib/nvd3/src/models/line.js',
                                                'lib/nvd3/src/models/lineChart.js',
                                                'lib/nvd3/stream_layers.js',
                                                'lib/backbone/backbone.localStorage-min.js',
                                                'js/index.js',
                                                'lib/fullcalendar/fullcalendar.js',
                                                'lib/jquery-ui-1.10.3.custom.js',
                                                'js/forms-elemets.js'));
        $data['dashboard']=$this->dashboard_generator();
        $data['soy']=$this->session->userdata('soy');
        $this->layout->view('apoderado/main',$data);
    }
    public function Academia($function = NULL)
    {
        $this->verificar();   
        $this->layout->set_specific_js(array('js/loader.js','lib/jquery.slimscroll.js','lib/fullcalendar/fullcalendar.js'));//'js/calendar.js
        $data['content']=$this->academia_generator($function);
        $data['soy']=$this->session->userdata('soy');
        $this->layout->view('apoderado/Academica/academia',$data);   
    }
    private function academia_generator($function)
    {
        $id=$this->app->get_alumno_id();     
        $notas=$this->apoderado_model->get_notas_alumno($id);
        $asignaturas=$this->apoderado_model->get_asignaturas_alumno($id);
        $set_notas=$this->apoderado_model->get_set_notas_alumno($id);
        $promedio=$this->apoderado_model->get_promedio_nota_alumno($id);
        $hojaVida=$this->apoderado_model->get_hojaVida_alumno($id);
        $horario=$this->apoderado_model->get_horario_alumno($id);
        $bloques=$this->apoderado_model->get_bloques_horario();
        $profe_jefe=$this->apoderado_model->get_porfe_jefe_alumno($id);
        $reuniones=$this->apoderado_model->get_reuniones_alumno($id);
        $profesores=$this->apoderado_model->get_profesores_alumno($id);
        $_content=$this->academia->content_pr($profesores,$profe_jefe,$reuniones,$function);
        $content=$this->academia->content_tab($notas,$asignaturas,$set_notas,$promedio,$hojaVida,$horario,$bloques,$function);
        $content=$this->academia->container($content,$_content);
        return $content;
    }
    public function academia_change()
    {
        $this->verificar();
        if($this->input->post('id'))
        {
            $id=$this->input->post('id');
            $function=$this->input->post('opcion');
            if(is_numeric($id))
            {
                if($this->mi_alumno($id))
                {                    
                    echo json_encode(array("bool"=>true,'dashboard'=>$this->academia_generator($function)));
                }
                else{ echo json_encode(array("bool"=>false,"msg"=>'No tiene acceso a la información del Alumno')); }
            }
            else{ echo json_encode(array("bool"=>false,"msg"=>'Error al seleccionar Alumno')); }
        }
        else{ echo json_encode(array("bool"=>false,"msg"=>'Error al seleccionar Alumno')); }     
    }
    private function dashboard_generator()
    {
        $anotaciones = $this->apoderado_model->get_hojaVida_alumno($this->app->get_alumno_id());
        if($anotaciones['bool']){$anotaciones=$anotaciones['query'];}else{$anotaciones=null;}        
        $_anotaciones=$this->dashboard->anotaciones($anotaciones);

        //REUNIONES
        $reuniones = $this->apoderado_model->get_reuniones_alumno($this->app->get_alumno_id());
        if($reuniones['bool']){$reuniones=$reuniones['query'];}else{$reuniones=null;}        
        $_reuniones=$this->dashboard->reuniones($reuniones);

        //ULTIMAS NOTAS
        $notas = $this->apoderado_model->get_notas_alumno($this->app->get_alumno_id());        
        if($notas['bool']){$notas=$notas['query'];}else{$notas=null;}      
        $_notas=$this->dashboard->notas($notas);

        //PRUEBAS
        $pruebas = $this->apoderado_model->get_pruebas_news($this->app->get_alumno_id()); 
        if($pruebas['bool']){$pruebas=$pruebas['query'];}else{$pruebas=null;}        
        $_pruebas=$this->dashboard->prox_pruebas($pruebas);

        //NOTIFICACIONES
        //$notificaciones= $this->app->get_notificaciones($this->ion_auth->get_user_id());
        $_notificacion=$this->dashboard->notificaciones();
        $_stat=$this->dashboard->stat_apoderado($reuniones,$notas,$pruebas);
        $code=' <div class="row">
                    <div class="col-md-12">
                        <h2 class="page-title">Apoderado <small>Plataforma educativa</small></h2>
                    </div>
                </div>   
                <div class="row">
                    <div id="change_alumno">
                        <div class="col-md-8">
                            '.$_stat.'
                            <div class="col-md-6">
                                '.$_reuniones.'
                                '.$_notas.'
                            </div>  
                            <div class="col-md-6">
                                '.$_anotaciones.'
                                '.$_pruebas.'
                            </div>    
                        </div>
                    </div>
                    <div class="col-md-4">       
                        '.$_notificacion.'
                    </div>              
                </div>  ';
        return $code;
    }
    private function dashboard_generator_change()
    {
        $anotaciones = $this->apoderado_model->get_hojaVida_alumno($this->app->get_alumno_id());
        if($anotaciones['bool']){$anotaciones=$anotaciones['query'];}else{$anotaciones=null;}        
        $_anotaciones=$this->dashboard->anotaciones($anotaciones);

        //REUNIONES
        $reuniones = $this->apoderado_model->get_reuniones_alumno($this->app->get_alumno_id());
        if($reuniones['bool']){$reuniones=$reuniones['query'];}else{$reuniones=null;}        
        $_reuniones=$this->dashboard->reuniones($reuniones);

        //ULTIMAS NOTAS
        $notas = $this->apoderado_model->get_notas_alumno($this->app->get_alumno_id());        
        if($notas['bool']){$notas=$notas['query'];}else{$notas=null;}      
        $_notas=$this->dashboard->notas($notas);

        //PRUEBAS
        $pruebas = $this->apoderado_model->get_pruebas_news($this->app->get_alumno_id()); 
        if($pruebas['bool']){$pruebas=$pruebas['query'];}else{$pruebas=null;}        
        $_pruebas=$this->dashboard->prox_pruebas($pruebas);

        //NOTIFICACIONES
        //$notificaciones= $this->app->get_notificaciones($this->ion_auth->get_user_id());
        $_notificacion=$this->dashboard->notificaciones();
        $_stat=$this->dashboard->stat_apoderado($reuniones,$notas,$pruebas);
        $code=' <div class="col-md-8">
                    '.$_stat.'
                    <div class="col-md-6">
                        '.$_reuniones.'
                        '.$_notas.'
                    </div>  
                    <div class="col-md-6">
                        '.$_anotaciones.'
                        '.$_pruebas.'
                    </div>    
                </div>';
        return $code;
    }
    public function dashboard_change()
    {
        $this->verificar();
        if($this->input->post('id'))
        {
            $id=$this->input->post('id');
            if(is_numeric($id) && $id>0)
            {
                $old_id=$this->app->get_alumno_id();
                if($this->mi_alumno($id))
                {                    
                    echo json_encode(array("bool"=>true,"dashboard"=>$this->dashboard_generator_change()));
                }
                else
                {
                    echo json_encode(array("bool"=>false,"msg"=>'No tiene acceso a la información del Alumno'));
                }
            }
            else
            {
                echo json_encode(array("bool"=>false,"msg"=>'Error al seleccionar Alumno'));
            }
        }
        else
        {
            echo json_encode(array("bool"=>false,"msg"=>'Error al seleccionar Alumno'));
        }     
    }
     
}
