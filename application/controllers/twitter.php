<?php
/**
 * Twitter OAuth library.
 * Sample controller.
 * Requirements: enabled Session library, enabled URL helper
 * Please note that this sample controller is just an example of how you can use the library.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Twitter extends CI_Controller
{
	private $connection;
	function __construct()
	{
		parent::__construct();
		$this->load->library('twitteroauth');
		$this->config->load('twitter');		
		$this->load->library(array('security_lib','ion_auth'));
		$this->load->model('twitter_model');
		$this->load->model(array('security_model','ion_auth_model'));
		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('access_token'),  $this->session->userdata('access_token_secret'));
		}
		elseif($this->session->userdata('request_token') && $this->session->userdata('request_token_secret'))
		{
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('request_token'), $this->session->userdata('request_token_secret'));
		}
		else
		{
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
		}
	}
	private function index()
	{
		$this->config->load('twitter');
		$consumer=$this->config->item('twitter_consumer_token');
		$consumer_secret=$this->config->item('twitter_consumer_secret');
		$token=$this->config->item('twitter_access_token');
		$token_secret=$this->config->item('twitter_access_secret');
		$connection = $this->twitteroauth->create(	$consumer, $consumer_secret,$token,$token_secret);
		$message="HOLA MUNDO - Apple's Teacher (Testing)".date('d-m-Y H:i:s');
		$name=$this->session->userdata('twitter_screen_name');
		$options = array("screen_name" => $name, "text" => $message);
		$result = $connection->post('direct_messages/new', $options);
		if(!isset($result->errors))
		{
			echo "***Enviado a ".$name.": \"".$message."\"***<br>";
		}
		else
		{
			echo "**No Enviado***<br>";
		}
	}
	/**
	 * Here comes authentication process begin.
	 * @access	public
	 * @return	void
	 */
	public function auth()
	{
		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			redirect(base_url('/'));
		}
		else
		{
			$request_token = $this->connection->getRequestToken(base_url('/twitter/callback'));
			$this->session->set_userdata('request_token', $request_token['oauth_token']);
			$this->session->set_userdata('request_token_secret', $request_token['oauth_token_secret']);			
			if($this->connection->http_code == 200)
			{
				$url = $this->connection->getAuthorizeURL($request_token);
				redirect($url);
			}
			else
			{
				redirect(base_url('/'));				
			}
		}
	}
	
	/**
	 * Callback function, landing page for twitter.
	 * @access	public
	 * @return	void
	 */
	public function callback()
	{
		if($this->input->get('oauth_token') && $this->session->userdata('request_token') !== $this->input->get('oauth_token'))
		{
			$this->reset_session2();
			redirect(base_url('/twitter/auth'));
		}
		else
		{
			$access_token = $this->connection->getAccessToken($this->input->get('oauth_verifier'));
		
			if ($this->connection->http_code == 200)
			{
				$this->session->set_userdata('access_token', $access_token['oauth_token']);
				$this->session->set_userdata('access_token_secret', $access_token['oauth_token_secret']);
				$this->session->set_userdata('twitter_user_id', $access_token['user_id']);
				$this->session->set_userdata('twitter_screen_name', $access_token['screen_name']);
				$this->session->unset_userdata('request_token');
				$this->session->unset_userdata('request_token_secret');
				if($this->ion_auth->get_user_id()!=null)
				{
					$Id=$this->ion_auth->get_user_id();
					$vereficar=$this->twitter_model->verificar_twitter($Id);
					if($vereficar['bool'])
					{
						$save=$this->twitter_model->save_twitter_Id($this->session->userdata('twitter_user_id'),$Id);
						$soy=$this->session->userdata('soy');
						if($save['bool'])
						{
							redirect(base_url('/'.$soy.'/perfil'));
						}
						else
						{
							//YA SE ENCUENTRA EN USO ERROR
							$this->reset_session2();
							redirect(base_url('/'.$soy.'/perfil'));
						}
					}
					else
					{
						//YA TIENE UN TWITTER ASIGNADO
						$this->reset_session2();
						redirect(base_url('/'));						
					}
				}
				else
				{
					$login_twitter=$this->twitter_model->login_twitter($this->session->userdata('twitter_user_id'));
					if($login_twitter['bool'])
					{
						$consumer=$this->config->item('twitter_consumer_token');
						$consumer_secret=$this->config->item('twitter_consumer_secret');
						$token=$this->session->userdata('access_token_secret');				
						$connection4 = $this->twitteroauth->create($consumer, $consumer_secret,$access_token['oauth_token'],$access_token['oauth_token_secret']);
						$resultado=$connection4->post('friendships/create', array('screen_name' => 'applesteacher','follow'=>'true'));
						if(!isset($resultado->errors))
						{					
							redirect(base_url('/'));				
						}
						else
						{
							redirect(base_url('/'));					
						}					
					}
					else
					{
						$this->reset_session();
						redirect(base_url('/'));						
					}
				}				
			}
			else
			{
				redirect(base_url('/'));				
			}
		}
	}
	
	public function post($in_reply_to=NULL)
	{
		$message = "HOLA MUNDO - Apple's Teacher (Testing)";//$this->input->post('message');
		if(!$message || mb_strlen($message) > 140 || mb_strlen($message) < 1)
		{
			// Restrictions error. Notification here.
			redirect(base_url('/'));
		}
		else
		{
			if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
			{
				$content = $this->connection->get('account/verify_credentials');
				if(isset($content->errors))
				{
					// Most probably, authentication problems. Begin authentication process again.
					$this->reset_session();
					redirect(base_url('/twitter/auth'));
				}
				else
				{
					$data = array(
						'status' => $message,
						'in_reply_to_status_id' => $in_reply_to
					);
					//$result = $this->connection->post('statuses/update', $data);


					$options = array("screen_name" => "rixarxx", "text" => "HOLA MUNDO - Apple's Teacher (Testing)");
					$result = $this->connection->post('direct_messages/new', $options);

					if(!isset($result->errors))
					{
						// Everything is OK
						echo "Enviado: \"".$message."\"";
						//redirect(base_url('/'));
					}
					else
					{
						echo "CHAO";
						// Error, message hasn't been published
						//redirect(base_url('/'));
					}
				}
			}
			else
			{
				// User is not authenticated.
				redirect(base_url('/twitter/auth'));
			}
		}
	}
	public function get_name()
	{
		//$save=$this->twitter_model->save_twitter_Id($this->session->userdata('twitter_user_id'),$Id);
		$consumer=$this->config->item('twitter_consumer_token');
		$consumer_secret=$this->config->item('twitter_consumer_secret');
		$token=$this->config->item('twitter_access_token');
		$token_secret=$this->config->item('twitter_access_secret');
		$connection = $this->twitteroauth->create(	$consumer, $consumer_secret,$token,$token_secret);
		$resultado=$connection->get('users/show', array('user_id' => '2866775745','follow'=>'true'));
		if(!isset($resultado->errors))
		{									
			echo $resultado->screen_name;
			//redirect(base_url('/'));
		}
		else
		{
			//MENSAJE DE ERROR
			//redirect(base_url('/'));	
		}
		//var_dump($resultado);

	}	
	public function reset_session()
	{
		$this->session->unset_userdata('access_token');
		$this->session->unset_userdata('access_token_secret');
		$this->session->unset_userdata('request_token');
		$this->session->unset_userdata('request_token_secret');
		$this->session->unset_userdata('twitter_user_id');
		$this->session->unset_userdata('twitter_screen_name');
		redirect('auth/logout');
	}
	public function reset_session2()
	{
		$this->session->unset_userdata('access_token');
		$this->session->unset_userdata('access_token_secret');
		$this->session->unset_userdata('request_token');
		$this->session->unset_userdata('request_token_secret');
		$this->session->unset_userdata('twitter_user_id');
		$this->session->unset_userdata('twitter_screen_name');
	}
	public function delete()
    {
        if($this->ion_auth->get_user_id())
        {
            $MyId=$this->ion_auth->get_user_id(); 
            $delete=$this->twitter_model->delete_twitter_Id($MyId);
            if($delete['bool'])
            {
            	$btn='<p align="center">¿Desea asociar su cuenta twitter a su cuenta Apple\'s Teacher para recibir notificaciones?, precione "Registrar Twitter"</p>
              
            			<div class="col-ms-8">
                        	<a href="/tesis/twitter/auth">
                            	<button style="width:100%" type="button" class="btn btn-info btn-lg" data-placement="top" data-original-title=".btn .btn-info">
                                	&nbsp;<span><i class="fa fa-twitter fa-lg"></i> Registrar Twitter</span>&nbsp;
                            	</button>
                         	</a>
                      	</div>';
                $this->reset_session2();
            	echo json_encode(array("bool"=>true,"btn"=>$btn));
            }
            else
            {
            	echo json_encode(array("bool"=>false,"msg"=>"No se logró eliminar Twitter"));
            }
        }
        else
        {
        	echo json_encode(array("bool"=>false,"msg"=>"Error Sistema"));
        }
    }
}

