<!DOCTYPE html>

<?php 
    if(!isset($msg)){$msg='';}
    if(!isset($identity)){$identity="";}

?>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <title>Apple's Teacher</title>
    <link href="<?=$url?>css/application.min.css" rel="stylesheet">
    <link rel="icon" href="<?=$url?>img/favicon.png" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script src="<?=$url?>lib/jquery/jquery-2.0.3.min.js"> </script>
    <script src="<?=$url?>lib/jquery-pjax/jquery.pjax.js"></script>
    <script src="<?=$url?>lib/backbone/underscore-min.js"></script>
    <script src="<?=$url?>js/settings.js"> </script>
</head>
<body>
<div class="single-widget-container">
    <section class="widget login-widget">
        <header class="text-align-center">
            <h4>Apple's teacher</h4>
        </header>
        <div class="body">
            <form class="no-margin" action="<?=base_url()?>auth/login" method="post">
                <fieldset>
                    <div class="form-group no-margin">
                        <label for="email" >Correo Electrónico</label>

                        <div class="input-group input-group-lg">
                                <span class="input-group-addon">
                                    <i class="eicon-user"></i>
                                </span>
                            <input id="rut" type="text" name="identity"class="form-control input-lg" placeholder="Ingrese su Correo Electrónico" value="<?=$identity?>">
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="password" >Contraseña</label>

                        <div class="input-group input-group-lg">
                                <span class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </span>
                            <input id="password" type="password" name="password"class="form-control input-lg" placeholder="Tu contraseña">
                        </div>

                    </div>
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-block btn-lg btn-danger">
                        <span class="small-circle"><i class="fa fa-caret-right"></i></span>
                        <small>Iniciar Sesión</small>
                    </button>
                    <input type="hidden" name="submit" value="yes"> 
                    <?=$msg?>
                    <div class="forgot"></div>
                   
                </div>
               
            </form>
            <footer>
            <div class="facebook-login"  style="background:#55acee !important">
                <a href="/tesis/twitter/auth"><span><i class="fa fa-twitter fa-lg"></i> Iniciar con Twitter</span></a>

            </div>
            <div class="facebook-login">
            <a href="/tesis/auth/forgot_password"><span>¿Has olvidado la contraseña? </span></a>
            </div>
        </footer>
        </div>        
    </section>
</div>
<script src="/tesis/assets/bootstrap/lib/jquery/jquery-2.0.3.min.js"> </script>
<script src="/tesis/assets/bootstrap/lib/jquery-pjax/jquery.pjax.js"></script>
<script src="/tesis/assets/bootstrap/lib/bootstrap/alert.js"></script>
</body>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
</html>