
    <div class="row">
        <div class="col-md-12">
            <h2 class="page-title">Form Elements <small>Inputs, controls and more</small></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <section class="widget">
                <header>
                    <h4>
                        <i class="fa fa-align-left"></i>
                        Inputs
                    </h4>
                </header>
                <div class="body">
                    <form class="form-horizontal" method="post">
                        <fieldset>
                            <legend class="section">Horizontal form</legend>
                        <div class="control-group">
                            <label class="control-label" for="normal-field">Normal field</label>
                            <div class="controls form-group">
                                <div class="col-sm-8">
                                    <input type="text" id="normal-field" class="form-control" placeholder="May have placeholder">
                                </div>
                            </div>
                        </div>
                            <div class="control-group">
                                <label class="control-label" for="normal-field">Transparent input</label>
                                <div class="controls form-group">
                                    <div class="col-sm-8">
                                        <input type="text" id="transparent-field" class="form-control input-transparent" placeholder="Stylish, huh??">
                                    </div>
                                </div>
                            </div>
                        <div class="control-group">
                            <label class="control-label" for="hint-field">
                                Label hint
                                <span class="help-block">Some help text</span>
                            </label>
                            <div class="controls form-group">
                                <div class="col-sm-8"><input type="text" id="hint-field" class="form-control"></div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label " for="tooltip-enabled">Tooltip enabled</label>
                            <div class="controls form-group">
                                <div class="col-sm-8"><input type="text" id="tooltip-enabled" class="form-control" data-placement="top" data-original-title="Some explanation text here" placeholder="Hover over me.."></div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label " for="disabled-input">Disabled input</label>
                            <div class="controls form-group">
                                <div class="col-sm-8"><input type="text" id="disabled-input" class="form-control" disabled="disabled" value="Default value"></div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label " for="max-length">Max length</label>
                            <div class="controls form-group">
                                <div class="col-sm-8"><input type="text" id="max-length" maxlength="3" class="form-control" placeholder="Max length 3 characters" data-placement="top" data-original-title="You cannot write more than 3 characters."></div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="prepended-input">Prepended input</label>
                            <div class="controls form-group">
                                <div class="input-group col-sm-8">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input id="prepended-input" class="form-control" size="16" type="text" placeholder="Username">
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="password-field">Password</label>
                            <div class="controls form-group">
                                <div class="input-group col-sm-8">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" class="form-control" id="password-field" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="appended-input">Appended input</label>
                            <div class="controls form-group">
                                <div class="input-group col-sm-8">
                                    <input id="appended-input" class="form-control" size="16" type="text">
                                    <span class="input-group-addon">.00</span> </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="combined-input">Combined</label>
                            <div class="controls form-group">
                                <div class="input-group col-sm-8">
                                    <span class="input-group-addon">$</span>
                                    <input id="combined-input" class="form-control" size="16" type="text">
                                    <span class="input-group-addon">.00</span> </div>
                            </div>
                        </div>
                        </fieldset>
                        <div class="form-actions">
                            <div>
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                                <button type="button" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        <div class="col-md-6">
            <section class="widget">
                <header>
                    <h4>
                        <i class="fa fa-align-right"></i>
                        Prepended and appended inputs
                    </h4>
                </header>
                <div class="body">
                    <form method="post">
                        <fieldset>
                            <legend class="section">Default form</legend>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">

                                        <label for="search-input">Search type input</label>

                                        <div class="input-group">
                                            <input type="search" class="form-control" id="search-input">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-transparent">Search</button>
                                    </span>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="bar">Whole bar appended</label>

                                        <div class="input-group">
                                            <input type="text" class="form-control" id="bar">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-danger"><i class="fa fa-pencil"></i></button>
                                                <button type="button" class="btn btn-warning"><i class="fa fa-plus"></i></button>
                                                <button type="button" class="btn btn-success"><i class="fa fa-refresh"></i></button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="dropdown-appended">Actions dropdown</label>

                                        <div class="input-group">
                                            <input type="text" class="form-control" id="dropdown-appended">
                                            <div class="input-group-btn">
                                                <button class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                                    Action
                                                    <i class="fa fa-caret-down"></i>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="type-dropdown-appended">Types dropdown</label>

                                        <div class="input-group">
                                            <input type="text" class="form-control" id="type-dropdown-appended">
                                            <div class="input-group-btn">
                                                <select id="phone-type" class="selectpicker" data-style="btn-primary" style="display: none;">
                                                <option>Type one</option>
                                                <option>Some another type</option>
                                                <option>Even more types</option>
                                                </select><div class="btn-group bootstrap-select"><button class="btn dropdown-toggle clearfix btn-primary" data-toggle="dropdown" id="phone-type"><span class="filter-option">Type one</span>&nbsp;<i class="fa fa-caret-down"></i></button><ul class="dropdown-menu" role="menu" style="max-height: 77px; overflow-y: auto; min-height: 60px;"><li rel="0"><a tabindex="-1" href="#" class="">Type one</a></li><li rel="1"><a tabindex="-1" href="#" class="">Some another type</a></li><li rel="2"><a tabindex="-1" href="#" class="">Even more types</a></li></ul></div>
                                            </div>
                                        </div>
                                        <p class="help-block">You can select some type of a field just right in the place.</p>

                                    </div>
                                    <div class="form-group">
                                        <label for="segmented-dropdown">Segmented dropdown</label>

                                        <div class="input-group">
                                            <input id="segmented-dropdown" class="form-control" type="text">
                                            <div class="input-group-btn">
                                                <button class="btn btn-warning" tabindex="-1">Action</button>
                                                <button class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-caret-down"></i>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <span class="help-block">Everything can be appended to the right</span>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="form-actions ">
                            <div class=" ">
                                <button type="submit" class="btn btn-default">Save Changes</button>
                                <button type="button" class="btn btn-inverse">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <section class="widget">
                <header>
                    <h4>
                        <i class="fa fa-font"></i>
                        Textareas
                    </h4>
                </header>
                <div class="body">
                    <form class="form-horizontal form-condensed" action="#">
                        <fieldset>
                            <legend class="section">Condensed form</legend>
                            <div class="control-group">
                                <label class="control-label" for="default-textarea">Default textarea</label>
                                <div class="controls form-group">
                                    <textarea rows="4" class="form-control" id="default-textarea"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="elastic-textarea">
                                    Auto-growing textarea
                                </label>
                                <div class="controls form-group">
                                    <textarea rows="3" class="autogrow form-control" id="elastic-textarea" placeholder="Try to add few new lines.." style="height: 70px;"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="wysiwyg">
                                    Wysiwyg
                                    <span class="help-block">
                                        With bottom toolbar appended
                                    </span>
                                </label>
                                <div class="controls form-group">
                                    <ul class="wysihtml5-toolbar"><li class="dropdown"><a class="btn btn-sm btn-inverse dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-font"></i>&nbsp;<span class="current-font">Normal text</span>&nbsp;&nbsp;<i class="fa fa-caret-down"></i></a><ul class="dropdown-menu"><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="div" href="javascript:;" unselectable="on">Normal text</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="p" href="javascript:;" unselectable="on">Normal text</a></li><li><a data-wysihtml5-command="formatInline" data-wysihtml5-command-value="span" href="javascript:;" unselectable="on">Normal text</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1" href="javascript:;" unselectable="on">Heading 1</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2" href="javascript:;" unselectable="on">Heading 2</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h3" href="javascript:;" unselectable="on">Heading 3</a></li></ul></li><li><div class="btn-group"><a class="btn btn-sm btn-inverse " data-wysihtml5-command="bold" title="CTRL+B" href="javascript:;" unselectable="on"><i class="fa fa-bold"></i></a><a class="btn btn-sm btn-inverse " data-wysihtml5-command="italic" title="CTRL+I" href="javascript:;" unselectable="on"><i class="fa fa-italic"></i></a></div></li><li><div class="btn-group"><a class="btn btn-sm btn-inverse " data-wysihtml5-command="insertUnorderedList" title="Unordered list" href="javascript:;" unselectable="on"><i class="fa fa-list"></i></a><a class="btn btn-sm btn-inverse " data-wysihtml5-command="insertOrderedList" title="Ordered list" href="javascript:;" unselectable="on"><i class="fa fa-th-list"></i></a><a class="btn btn-sm btn-inverse " data-wysihtml5-command="Outdent" title="Outdent" href="javascript:;" unselectable="on"><i class="fa fa-outdent"></i></a><a class="btn btn-sm btn-inverse " data-wysihtml5-command="Indent" title="Indent" href="javascript:;" unselectable="on"><i class="fa fa-indent"></i></a></div></li><li><div class="btn-group"><a class="btn btn-sm btn-inverse " data-wysihtml5-action="change_view" title="Edit HTML" href="javascript:;" unselectable="on"><i class="fa fa-pencil"></i></a></div></li><li><div class="bootstrap-wysihtml5-insert-link-modal modal hide fade"><div class="modal-header"><a class="close" data-dismiss="modal">×</a><h3>Insert link</h3></div><div class="modal-body"><input value="http://" class="bootstrap-wysihtml5-insert-link-url input-xlarge"></div><div class="modal-footer"><a href="#" class="btn" data-dismiss="modal">Cancel</a><a href="#" class="btn btn-primary" data-dismiss="modal">Insert link</a></div></div><a class="btn btn-sm btn-inverse " data-wysihtml5-command="createLink" title="Insert link" href="javascript:;" unselectable="on"><i class="fa fa-share"></i></a></li><li><div class="bootstrap-wysihtml5-insert-image-modal modal hide fade"><div class="modal-header"><a class="close" data-dismiss="modal">×</a><h3>Insert image</h3></div><div class="modal-body"><input value="http://" class="bootstrap-wysihtml5-insert-image-url input-xlarge"></div><div class="modal-footer"><a href="#" class="btn" data-dismiss="modal">Cancel</a><a href="#" class="btn btn-primary" data-dismiss="modal">Insert image</a></div></div><a class="btn btn-sm btn-inverse " data-wysihtml5-command="insertImage" title="Insert image" href="javascript:;" unselectable="on"><i class="fa fa-picture-o"></i></a></li></ul><textarea rows="4" class="form-control" id="wysiwyg" style="display: none;"></textarea><input type="hidden" name="_wysihtml5_mode" value="1"><iframe class="wysihtml5-sandbox" security="restricted" allowtransparency="true" frameborder="0" width="0" height="0" marginwidth="0" marginheight="0" style="border-collapse: separate; border: 1px solid rgb(136, 136, 136); clear: none; display: block; float: none; margin: 0px; outline: rgb(102, 102, 102) none 0px; outline-offset: 0px; padding: 4px 12px; position: static; top: auto; left: auto; right: auto; bottom: auto; z-index: auto; vertical-align: baseline; text-align: start; box-sizing: border-box; -webkit-box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset; box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset; border-radius: 1px; width: 393.15625px; height: 90px; background-color: rgb(255, 255, 255);"></iframe>
                                    <div class="btn-toolbar pull-right">
                                        <button type="button" class="btn btn-sm btn-danger">Save</button>
                                        <button type="button" class="btn btn-sm btn-default">Clear</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </section>
        </div>
        <div class="col-md-6">
            <section class="widget">
                <header>
                    <h4>
                        <i class="fa fa-list-alt"></i>
                        Selects
                    </h4>
                </header>
                <div class="body">
                    <form class="form-horizontal label-left" action="#">
                        <fieldset>
                            <legend class="section">Default form with labels on left</legend>
                            <div class="control-group">
                                <label class="control-label" for="default-select">Default select</label>
                                <div class="controls form-group">
                                    <div class="select2-container chzn-select select-block-level" id="s2id_default-select"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Which galaxy is closest to Milky Way?</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select data-placeholder="Which galaxy is closest to Milky Way?" data-width="off" data-minimum-results-for-search="10" tabindex="-1" class="chzn-select select-block-level select2-offscreen" id="default-select">
                                        <option value=""></option>
                                        <option value="Magellanic">Large Magellanic Cloud</option>
                                        <option value="Andromeda">Andromeda Galaxy</option>
                                        <option value="Sextans">Sextans A</option>
                                    </select>
                                    <span class="help-block">
                                        <code>.select-block-level</code> it to make 100% width
                                    </span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="country" class="control-label">
                                    Select with search
                                </label>
                                <div class="controls form-group">
                                    <div class="select2-container select-block-level chzn-select" id="s2id_country"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Select country</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="country" required="required" data-placeholder="Select country" class="select-block-level chzn-select select2-offscreen" tabindex="-1" name="country">
                                        <option value=""></option>
                                        <option value="United States">United States</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                        <option value="Antarctica">Antarctica</option>
                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Azerbaijan">Azerbaijan</option>
                                        <option value="Bahamas">Bahamas</option>
                                        <option value="Bahrain">Bahrain</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option>
                                        <option value="Belarus">Belarus</option>
                                        <option value="Belgium">Belgium</option>
                                        <option value="Belize">Belize</option>
                                        <option value="Benin">Benin</option>
                                        <option value="Bermuda">Bermuda</option>
                                        <option value="Bhutan">Bhutan</option>
                                        <option value="Bolivia">Bolivia</option>
                                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                        <option value="Botswana">Botswana</option>
                                        <option value="Bouvet Island">Bouvet Island</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                                        <option value="Bulgaria">Bulgaria</option>
                                        <option value="Burkina Faso">Burkina Faso</option>
                                        <option value="Burundi">Burundi</option>
                                        <option value="Cambodia">Cambodia</option>
                                        <option value="Cameroon">Cameroon</option>
                                        <option value="Canada">Canada</option>
                                        <option value="Cape Verde">Cape Verde</option>
                                        <option value="Cayman Islands">Cayman Islands</option>
                                        <option value="Central African Republic">Central African Republic</option>
                                        <option value="Chad">Chad</option>
                                        <option value="Chile">Chile</option>
                                        <option value="China">China</option>
                                        <option value="Christmas Island">Christmas Island</option>
                                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="Comoros">Comoros</option>
                                        <option value="Congo">Congo</option>
                                        <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                        <option value="Cook Islands">Cook Islands</option>
                                        <option value="Costa Rica">Costa Rica</option>
                                        <option value="Cote D'ivoire">Cote D'ivoire</option>
                                        <option value="Croatia">Croatia</option>
                                        <option value="Cuba">Cuba</option>
                                        <option value="Cyprus">Cyprus</option>
                                        <option value="Czech Republic">Czech Republic</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Djibouti">Djibouti</option>
                                        <option value="Dominica">Dominica</option>
                                        <option value="Dominican Republic">Dominican Republic</option>
                                        <option value="Ecuador">Ecuador</option>
                                        <option value="Egypt">Egypt</option>
                                        <option value="El Salvador">El Salvador</option>
                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                        <option value="Eritrea">Eritrea</option>
                                        <option value="Estonia">Estonia</option>
                                        <option value="Ethiopia">Ethiopia</option>
                                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                        <option value="Faroe Islands">Faroe Islands</option>
                                        <option value="Fiji">Fiji</option>
                                        <option value="Finland">Finland</option>
                                        <option value="France">France</option>
                                        <option value="French Guiana">French Guiana</option>
                                        <option value="French Polynesia">French Polynesia</option>
                                        <option value="French Southern Territories">French Southern Territories</option>
                                        <option value="Gabon">Gabon</option>
                                        <option value="Gambia">Gambia</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="Ghana">Ghana</option>
                                        <option value="Gibraltar">Gibraltar</option>
                                        <option value="Greece">Greece</option>
                                        <option value="Greenland">Greenland</option>
                                        <option value="Grenada">Grenada</option>
                                        <option value="Guadeloupe">Guadeloupe</option>
                                        <option value="Guam">Guam</option>
                                        <option value="Guatemala">Guatemala</option>
                                        <option value="Guinea">Guinea</option>
                                        <option value="Guinea-bissau">Guinea-bissau</option>
                                        <option value="Guyana">Guyana</option>
                                        <option value="Haiti">Haiti</option>
                                        <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                        <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                        <option value="Honduras">Honduras</option>
                                        <option value="Hong Kong">Hong Kong</option>
                                        <option value="Hungary">Hungary</option>
                                        <option value="Iceland">Iceland</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                        <option value="Iraq">Iraq</option>
                                        <option value="Ireland">Ireland</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Jamaica">Jamaica</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Jordan">Jordan</option>
                                        <option value="Kazakhstan">Kazakhstan</option>
                                        <option value="Kenya">Kenya</option>
                                        <option value="Kiribati">Kiribati</option>
                                        <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                        <option value="Korea, Republic of">Korea, Republic of</option>
                                        <option value="Kuwait">Kuwait</option>
                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                        <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                        <option value="Latvia">Latvia</option>
                                        <option value="Lebanon">Lebanon</option>
                                        <option value="Lesotho">Lesotho</option>
                                        <option value="Liberia">Liberia</option>
                                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                        <option value="Liechtenstein">Liechtenstein</option>
                                        <option value="Lithuania">Lithuania</option>
                                        <option value="Luxembourg">Luxembourg</option>
                                        <option value="Macao">Macao</option>
                                        <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                        <option value="Madagascar">Madagascar</option>
                                        <option value="Malawi">Malawi</option>
                                        <option value="Malaysia">Malaysia</option>
                                        <option value="Maldives">Maldives</option>
                                        <option value="Mali">Mali</option>
                                        <option value="Malta">Malta</option>
                                        <option value="Marshall Islands">Marshall Islands</option>
                                        <option value="Martinique">Martinique</option>
                                        <option value="Mauritania">Mauritania</option>
                                        <option value="Mauritius">Mauritius</option>
                                        <option value="Mayotte">Mayotte</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                        <option value="Moldova, Republic of">Moldova, Republic of</option>
                                        <option value="Monaco">Monaco</option>
                                        <option value="Mongolia">Mongolia</option>
                                        <option value="Montserrat">Montserrat</option>
                                        <option value="Morocco">Morocco</option>
                                        <option value="Mozambique">Mozambique</option>
                                        <option value="Myanmar">Myanmar</option>
                                        <option value="Namibia">Namibia</option>
                                        <option value="Nauru">Nauru</option>
                                        <option value="Nepal">Nepal</option>
                                        <option value="Netherlands">Netherlands</option>
                                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                                        <option value="New Caledonia">New Caledonia</option>
                                        <option value="New Zealand">New Zealand</option>
                                        <option value="Nicaragua">Nicaragua</option>
                                        <option value="Niger">Niger</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Niue">Niue</option>
                                        <option value="Norfolk Island">Norfolk Island</option>
                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Oman">Oman</option>
                                        <option value="Pakistan">Pakistan</option>
                                        <option value="Palau">Palau</option>
                                        <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                        <option value="Panama">Panama</option>
                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                        <option value="Paraguay">Paraguay</option>
                                        <option value="Peru">Peru</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Pitcairn">Pitcairn</option>
                                        <option value="Poland">Poland</option>
                                        <option value="Portugal">Portugal</option>
                                        <option value="Puerto Rico">Puerto Rico</option>
                                        <option value="Qatar">Qatar</option>
                                        <option value="Reunion">Reunion</option>
                                        <option value="Romania">Romania</option>
                                        <option value="Russian Federation">Russian Federation</option>
                                        <option value="Rwanda">Rwanda</option>
                                        <option value="Saint Helena">Saint Helena</option>
                                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                        <option value="Saint Lucia">Saint Lucia</option>
                                        <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                        <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                        <option value="Samoa">Samoa</option>
                                        <option value="San Marino">San Marino</option>
                                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                        <option value="Senegal">Senegal</option>
                                        <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                                        <option value="Seychelles">Seychelles</option>
                                        <option value="Sierra Leone">Sierra Leone</option>
                                        <option value="Singapore">Singapore</option>
                                        <option value="Slovakia">Slovakia</option>
                                        <option value="Slovenia">Slovenia</option>
                                        <option value="Solomon Islands">Solomon Islands</option>
                                        <option value="Somalia">Somalia</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                        <option value="Spain">Spain</option>
                                        <option value="Sri Lanka">Sri Lanka</option>
                                        <option value="Sudan">Sudan</option>
                                        <option value="Suriname">Suriname</option>
                                        <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                        <option value="Swaziland">Swaziland</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Switzerland">Switzerland</option>
                                        <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                        <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                        <option value="Tajikistan">Tajikistan</option>
                                        <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Timor-leste">Timor-leste</option>
                                        <option value="Togo">Togo</option>
                                        <option value="Tokelau">Tokelau</option>
                                        <option value="Tonga">Tonga</option>
                                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                        <option value="Tunisia">Tunisia</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Turkmenistan">Turkmenistan</option>
                                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                        <option value="Tuvalu">Tuvalu</option>
                                        <option value="Uganda">Uganda</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                        <option value="Uruguay">Uruguay</option>
                                        <option value="Uzbekistan">Uzbekistan</option>
                                        <option value="Vanuatu">Vanuatu</option>
                                        <option value="Venezuela">Venezuela</option>
                                        <option value="Viet Nam">Viet Nam</option>
                                        <option value="Virgin Islands, British">Virgin Islands, British</option>
                                        <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                        <option value="Wallis and Futuna">Wallis and Futuna</option>
                                        <option value="Western Sahara">Western Sahara</option>
                                        <option value="Yemen">Yemen</option>
                                        <option value="Zambia">Zambia</option>
                                        <option value="Zimbabwe">Zimbabwe</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="multiple">
                                    Multiple select
                                    <span class="help-block">With search too</span>
                                </label>
                                <div class="controls form-group">
                                    <div class="select2-container select2-container-multi chzn-select select-block-level" id="s2id_multiple">    <ul class="select2-choices">  <li class="select2-search-choice">    <div>Belarus</div>    <a href="#" onclick="return false;" class="select2-search-choice-close" tabindex="-1"></a></li><li class="select2-search-field">    <input type="text" autocomplete="off" class="select2-input" tabindex="-1" style="width: 20px;">  </li></ul><div class="select2-drop select2-drop-multi" style="display:none;">   <ul class="select2-results">   </ul></div></div><select data-placeholder="Multiple countries this time" class="chzn-select select-block-level select2-offscreen" tabindex="-1" multiple="multiple" id="multiple">
                                        <option value="United States">United States</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="Afghanistan">Afghanistan</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                        <option value="Antarctica">Antarctica</option>
                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Azerbaijan">Azerbaijan</option>
                                        <option value="Bahamas">Bahamas</option>
                                        <option value="Bahrain">Bahrain</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option>
                                        <option selected="selected" value="Belarus">Belarus</option>
                                        <option value="Belgium">Belgium</option>
                                        <option value="Belize">Belize</option>
                                        <option value="Benin">Benin</option>
                                        <option value="Bermuda">Bermuda</option>
                                        <option value="Bhutan">Bhutan</option>
                                        <option value="Bolivia">Bolivia</option>
                                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                        <option value="Botswana">Botswana</option>
                                        <option value="Bouvet Island">Bouvet Island</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                                        <option value="Bulgaria">Bulgaria</option>
                                        <option value="Burkina Faso">Burkina Faso</option>
                                        <option value="Burundi">Burundi</option>
                                        <option value="Cambodia">Cambodia</option>
                                        <option value="Cameroon">Cameroon</option>
                                        <option value="Canada">Canada</option>
                                        <option value="Cape Verde">Cape Verde</option>
                                        <option value="Cayman Islands">Cayman Islands</option>
                                        <option value="Central African Republic">Central African Republic</option>
                                        <option value="Chad">Chad</option>
                                        <option value="Chile">Chile</option>
                                        <option value="China">China</option>
                                        <option value="Christmas Island">Christmas Island</option>
                                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="Comoros">Comoros</option>
                                        <option value="Congo">Congo</option>
                                        <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                        <option value="Cook Islands">Cook Islands</option>
                                        <option value="Costa Rica">Costa Rica</option>
                                        <option value="Cote D'ivoire">Cote D'ivoire</option>
                                        <option value="Croatia">Croatia</option>
                                        <option value="Cuba">Cuba</option>
                                        <option value="Cyprus">Cyprus</option>
                                        <option value="Czech Republic">Czech Republic</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Djibouti">Djibouti</option>
                                        <option value="Dominica">Dominica</option>
                                        <option value="Dominican Republic">Dominican Republic</option>
                                        <option value="Ecuador">Ecuador</option>
                                        <option value="Egypt">Egypt</option>
                                        <option value="El Salvador">El Salvador</option>
                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                        <option value="Eritrea">Eritrea</option>
                                        <option value="Estonia">Estonia</option>
                                        <option value="Ethiopia">Ethiopia</option>
                                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                        <option value="Faroe Islands">Faroe Islands</option>
                                        <option value="Fiji">Fiji</option>
                                        <option value="Finland">Finland</option>
                                        <option value="France">France</option>
                                        <option value="French Guiana">French Guiana</option>
                                        <option value="French Polynesia">French Polynesia</option>
                                        <option value="French Southern Territories">French Southern Territories</option>
                                        <option value="Gabon">Gabon</option>
                                        <option value="Gambia">Gambia</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="Ghana">Ghana</option>
                                        <option value="Gibraltar">Gibraltar</option>
                                        <option value="Greece">Greece</option>
                                        <option value="Greenland">Greenland</option>
                                        <option value="Grenada">Grenada</option>
                                        <option value="Guadeloupe">Guadeloupe</option>
                                        <option value="Guam">Guam</option>
                                        <option value="Guatemala">Guatemala</option>
                                        <option value="Guinea">Guinea</option>
                                        <option value="Guinea-bissau">Guinea-bissau</option>
                                        <option value="Guyana">Guyana</option>
                                        <option value="Haiti">Haiti</option>
                                        <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                        <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                        <option value="Honduras">Honduras</option>
                                        <option value="Hong Kong">Hong Kong</option>
                                        <option value="Hungary">Hungary</option>
                                        <option value="Iceland">Iceland</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                        <option value="Iraq">Iraq</option>
                                        <option value="Ireland">Ireland</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italy">Italy</option>
                                        <option value="Jamaica">Jamaica</option>
                                        <option value="Japan">Japan</option>
                                        <option value="Jordan">Jordan</option>
                                        <option value="Kazakhstan">Kazakhstan</option>
                                        <option value="Kenya">Kenya</option>
                                        <option value="Kiribati">Kiribati</option>
                                        <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                        <option value="Korea, Republic of">Korea, Republic of</option>
                                        <option value="Kuwait">Kuwait</option>
                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                        <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                        <option value="Latvia">Latvia</option>
                                        <option value="Lebanon">Lebanon</option>
                                        <option value="Lesotho">Lesotho</option>
                                        <option value="Liberia">Liberia</option>
                                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                        <option value="Liechtenstein">Liechtenstein</option>
                                        <option value="Lithuania">Lithuania</option>
                                        <option value="Luxembourg">Luxembourg</option>
                                        <option value="Macao">Macao</option>
                                        <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                        <option value="Madagascar">Madagascar</option>
                                        <option value="Malawi">Malawi</option>
                                        <option value="Malaysia">Malaysia</option>
                                        <option value="Maldives">Maldives</option>
                                        <option value="Mali">Mali</option>
                                        <option value="Malta">Malta</option>
                                        <option value="Marshall Islands">Marshall Islands</option>
                                        <option value="Martinique">Martinique</option>
                                        <option value="Mauritania">Mauritania</option>
                                        <option value="Mauritius">Mauritius</option>
                                        <option value="Mayotte">Mayotte</option>
                                        <option value="Mexico">Mexico</option>
                                        <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                        <option value="Moldova, Republic of">Moldova, Republic of</option>
                                        <option value="Monaco">Monaco</option>
                                        <option value="Mongolia">Mongolia</option>
                                        <option value="Montenegro">Montenegro</option>
                                        <option value="Montserrat">Montserrat</option>
                                        <option value="Morocco">Morocco</option>
                                        <option value="Mozambique">Mozambique</option>
                                        <option value="Myanmar">Myanmar</option>
                                        <option value="Namibia">Namibia</option>
                                        <option value="Nauru">Nauru</option>
                                        <option value="Nepal">Nepal</option>
                                        <option value="Netherlands">Netherlands</option>
                                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                                        <option value="New Caledonia">New Caledonia</option>
                                        <option value="New Zealand">New Zealand</option>
                                        <option value="Nicaragua">Nicaragua</option>
                                        <option value="Niger">Niger</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Niue">Niue</option>
                                        <option value="Norfolk Island">Norfolk Island</option>
                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Oman">Oman</option>
                                        <option value="Pakistan">Pakistan</option>
                                        <option value="Palau">Palau</option>
                                        <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                        <option value="Panama">Panama</option>
                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                        <option value="Paraguay">Paraguay</option>
                                        <option value="Peru">Peru</option>
                                        <option value="Philippines">Philippines</option>
                                        <option value="Pitcairn">Pitcairn</option>
                                        <option value="Poland">Poland</option>
                                        <option value="Portugal">Portugal</option>
                                        <option value="Puerto Rico">Puerto Rico</option>
                                        <option value="Qatar">Qatar</option>
                                        <option value="Reunion">Reunion</option>
                                        <option value="Romania">Romania</option>
                                        <option value="Russian Federation">Russian Federation</option>
                                        <option value="Rwanda">Rwanda</option>
                                        <option value="Saint Helena">Saint Helena</option>
                                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                        <option value="Saint Lucia">Saint Lucia</option>
                                        <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                        <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                        <option value="Samoa">Samoa</option>
                                        <option value="San Marino">San Marino</option>
                                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                        <option value="Senegal">Senegal</option>
                                        <option value="Serbia">Serbia</option>
                                        <option value="Seychelles">Seychelles</option>
                                        <option value="Sierra Leone">Sierra Leone</option>
                                        <option value="Singapore">Singapore</option>
                                        <option value="Slovakia">Slovakia</option>
                                        <option value="Slovenia">Slovenia</option>
                                        <option value="Solomon Islands">Solomon Islands</option>
                                        <option value="Somalia">Somalia</option>
                                        <option value="South Africa">South Africa</option>
                                        <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                        <option value="South Sudan">South Sudan</option>
                                        <option value="Spain">Spain</option>
                                        <option value="Sri Lanka">Sri Lanka</option>
                                        <option value="Sudan">Sudan</option>
                                        <option value="Suriname">Suriname</option>
                                        <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                        <option value="Swaziland">Swaziland</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Switzerland">Switzerland</option>
                                        <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                        <option value="Taiwan, Republic of China">Taiwan, Republic of China</option>
                                        <option value="Tajikistan">Tajikistan</option>
                                        <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Timor-leste">Timor-leste</option>
                                        <option value="Togo">Togo</option>
                                        <option value="Tokelau">Tokelau</option>
                                        <option value="Tonga">Tonga</option>
                                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                        <option value="Tunisia">Tunisia</option>
                                        <option value="Turkey">Turkey</option>
                                        <option value="Turkmenistan">Turkmenistan</option>
                                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                        <option value="Tuvalu">Tuvalu</option>
                                        <option value="Uganda">Uganda</option>
                                        <option value="Ukraine">Ukraine</option>
                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option>
                                        <option value="United States">United States</option>
                                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                        <option value="Uruguay">Uruguay</option>
                                        <option value="Uzbekistan">Uzbekistan</option>
                                        <option value="Vanuatu">Vanuatu</option>
                                        <option value="Venezuela">Venezuela</option>
                                        <option value="Viet Nam">Viet Nam</option>
                                        <option value="Virgin Islands, British">Virgin Islands, British</option>
                                        <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                        <option value="Wallis and Futuna">Wallis and Futuna</option>
                                        <option value="Western Sahara">Western Sahara</option>
                                        <option value="Yemen">Yemen</option>
                                        <option value="Zambia">Zambia</option>
                                        <option value="Zimbabwe">Zimbabwe</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="grouped">Select with groups</label>
                                <div class="controls form-group">
                                    <div class="select2-container select2-container-multi chzn-select select-block-level" id="s2id_grouped">    <ul class="select2-choices">  <li class="select2-search-field">    <input type="text" autocomplete="off" class="select2-input select2-default" tabindex="-1" style="width: 330px;">  </li></ul><div class="select2-drop select2-drop-multi" style="display:none;">   <ul class="select2-results">   </ul></div></div><select data-placeholder="Your Favorite Football Team" class="chzn-select select-block-level select2-offscreen" multiple="multiple" tabindex="-1" id="grouped">
                                        <option value=""></option>
                                        <optgroup label="NFC EAST">
                                            <option>Dallas Cowboys</option>
                                            <option>New York Giants</option>
                                            <option>Philadelphia Eagles</option>
                                            <option>Washington Redskins</option>
                                        </optgroup>
                                        <optgroup label="NFC NORTH">
                                        <option>Chicago Bears</option>
                                        <option>Detroit Lions</option>
                                        <option>Green Bay Packers</option>
                                        <option>Minnesota Vikings</option>
                                    </optgroup>
                                        <optgroup label="NFC SOUTH">
                                            <option>Atlanta Falcons</option>
                                            <option>Carolina Panthers</option>
                                            <option>New Orleans Saints</option>
                                            <option>Tampa Bay Buccaneers</option>
                                        </optgroup>
                                        <optgroup label="NFC WEST">
                                            <option>Arizona Cardinals</option>
                                            <option>St. Louis Rams</option>
                                            <option>San Francisco 49ers</option>
                                            <option>Seattle Seahawks</option>
                                        </optgroup>
                                        <optgroup label="AFC EAST">
                                            <option>Buffalo Bills</option>
                                            <option>Miami Dolphins</option>
                                            <option>New England Patriots</option>
                                            <option>New York Jets</option>
                                        </optgroup>
                                        <optgroup label="AFC NORTH">
                                            <option>Baltimore Ravens</option>
                                            <option>Cincinnati Bengals</option>
                                            <option>Cleveland Browns</option>
                                            <option>Pittsburgh Steelers</option>
                                        </optgroup>
                                        <optgroup label="AFC SOUTH">
                                            <option>Houston Texans</option>
                                            <option>Indianapolis Colts</option>
                                            <option>Jacksonville Jaguars</option>
                                            <option>Tennessee Titans</option>
                                        </optgroup>
                                        <optgroup label="AFC WEST">
                                            <option>Denver Broncos</option>
                                            <option>Kansas City Chiefs</option>
                                            <option>Oakland Raiders</option>
                                            <option>San Diego Chargers</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend class="section">Dropdown based colored selects</legend>
                            <div class="control-group">
                                <label class="control-label" for="simple-colored">Simple select</label>
                                <div class="controls form-group">
                                    <select class="selectpicker" data-style="btn-default" tabindex="-1" id="simple-colored" style="display: none;">
                                        <option value="0">Option One</option>
                                        <option value="1">Option Two</option>
                                        <option value="2">Option Three</option>
                                    </select><div class="btn-group bootstrap-select"><button class="btn dropdown-toggle clearfix btn-default" data-toggle="dropdown" id="simple-colored" tabindex="-1"><span class="filter-option">Option One</span>&nbsp;<i class="fa fa-caret-down"></i></button><ul class="dropdown-menu" role="menu" style="overflow-y: auto; min-height: 60px;"><li rel="0"><a tabindex="-1" href="#" class="">Option One</a></li><li rel="1"><a tabindex="-1" href="#" class="">Option Two</a></li><li rel="2"><a tabindex="-1" href="#" class="">Option Three</a></li></ul></div>
                                    <span class="help-block">Auto size</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="simple-red">
                                    Colored ones
                                    <span class="help-block">
                                        A bit of Japanese
                                    </span>
                                </label>
                                <div class="controls form-group">
                                    <select class="selectpicker" data-style="btn-danger" tabindex="-1" id="simple-red" style="display: none;">
                                        <option value="0">Ichi</option>
                                        <option value="1">Ni</option>
                                        <option value="2">San</option>
                                    </select><div class="btn-group bootstrap-select"><button class="btn dropdown-toggle clearfix btn-danger" data-toggle="dropdown" id="simple-red" tabindex="-1"><span class="filter-option">Ichi</span>&nbsp;<i class="fa fa-caret-down"></i></button><ul class="dropdown-menu" role="menu" style="overflow-y: auto; min-height: 60px;"><li rel="0"><a tabindex="-1" href="#" class="">Ichi</a></li><li rel="1"><a tabindex="-1" href="#" class="">Ni</a></li><li rel="2"><a tabindex="-1" href="#" class="">San</a></li></ul></div>
                                    <select class="selectpicker" data-style="btn-warning" tabindex="-1" id="simple-orange" style="display: none;">
                                        <option value="0">Shi</option>
                                        <option value="1">Go</option>
                                        <option value="2">Roku</option>
                                    </select><div class="btn-group bootstrap-select"><button class="btn dropdown-toggle clearfix btn-warning" data-toggle="dropdown" id="simple-orange" tabindex="-1"><span class="filter-option">Shi</span>&nbsp;<i class="fa fa-caret-down"></i></button><ul class="dropdown-menu" role="menu" style="overflow-y: auto; min-height: 60px;"><li rel="0"><a tabindex="-1" href="#" class="">Shi</a></li><li rel="1"><a tabindex="-1" href="#" class="">Go</a></li><li rel="2"><a tabindex="-1" href="#" class="">Roku</a></li></ul></div>
                                    <select class="selectpicker" data-style="btn-success" tabindex="-1" id="simple-green" style="display: none;">
                                        <option value="0">Hichi</option>
                                        <option value="1">Hachi</option>
                                        <option value="2">Ku</option>
                                        <option value="3">Ju</option>
                                    </select><div class="btn-group bootstrap-select"><button class="btn dropdown-toggle clearfix btn-success" data-toggle="dropdown" id="simple-green" tabindex="-1"><span class="filter-option">Hichi</span>&nbsp;<i class="fa fa-caret-down"></i></button><ul class="dropdown-menu" role="menu" style="overflow-y: auto; min-height: 60px;"><li rel="0"><a tabindex="-1" href="#" class="">Hichi</a></li><li rel="1"><a tabindex="-1" href="#" class="">Hachi</a></li><li rel="2"><a tabindex="-1" href="#" class="">Ku</a></li><li rel="3"><a tabindex="-1" href="#" class="">Ju</a></li></ul></div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="simple-big">Big one</label>
                                <div class="controls form-group">
                                    <select class="selectpicker" data-style="btn-primary btn-lg" tabindex="-1" id="simple-big" style="display: none;">
                                        <option value="0">Fourth Item</option>
                                        <option value="1">Fifth Item</option>
                                        <option value="2">Sixth item</option>
                                    </select><div class="btn-group bootstrap-select"><button class="btn dropdown-toggle clearfix btn-primary btn-lg" data-toggle="dropdown" id="simple-big" tabindex="-1"><span class="filter-option">Fourth Item</span>&nbsp;<i class="fa fa-caret-down"></i></button><ul class="dropdown-menu" role="menu" style="overflow-y: auto; min-height: 60px;"><li rel="0"><a tabindex="-1" href="#" class="">Fourth Item</a></li><li rel="1"><a tabindex="-1" href="#" class="">Fifth Item</a></li><li rel="2"><a tabindex="-1" href="#" class="">Sixth item</a></li></ul></div>
                                    <span class="help-block">
                                        Size can be controlled via <code>.btn-*</code>
                                    </span>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <section class="widget">
                <header>
                    <h4>
                        <i class="fa fa-check"></i>
                        Checkboxes and Radio
                    </h4>
                </header>
                <div class="body">
                    <form class="form-horizontal" method="post">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label">Checkboxes</label>
                                <div class="controls form-group">
                                    <label class="checkbox">
                                        <div class="icheckbox_square-grey checked" style="position: relative;"><input type="checkbox" id="checkbox-1" checked="checked" class="iCheck" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        Not so much needs to be done </label>
                                    <label class="checkbox">
                                        <div class="icheckbox_square-grey" style="position: relative;"><input type="checkbox" id="checkbox-2" class="iCheck" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        It's really hard to generate random text </label>
                                    <label class="checkbox">
                                        <div class="icheckbox_square-grey" style="position: relative;"><input type="checkbox" id="checkbox-3" class="iCheck" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        We're trying our best! </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Inline ones</label>
                                <div class="controls form-group">
                                    <label class="checkbox inline">
                                        <div class="icheckbox_square-grey checked" style="position: relative;"><input type="checkbox" id="checkbox-4" class="iCheck" value="true" checked="checked" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        True </label>
                                    <label class="checkbox inline">
                                        <div class="icheckbox_square-grey" style="position: relative;"><input type="checkbox" value="false" class="iCheck" id="checkbox-5" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        False </label>
                                    <label class="checkbox inline">
                                        <div class="icheckbox_square-grey checked disabled" style="position: relative;"><input type="checkbox" value="disabled" class="iCheck" id="checkbox-6" checked="checked" disabled="disabled" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        Disabled </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">As button groups</label>
                                <div class="controls form-group">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-inverse btn-sm" data-toggle-class="btn-default" data-toggle-passive-class="btn-inverse">
                                            <input type="checkbox"> One
                                        </label>
                                        <label class="btn btn-inverse btn-sm" data-toggle-class="btn-default" data-toggle-passive-class="btn-inverse">
                                            <input type="checkbox"> Two
                                        </label>
                                    </div>
                                </div>
                                <div class="controls form-group">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-primary btn-sm active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-inverse">
                                            <input type="checkbox"> Left
                                        </label>
                                        <label class="btn btn-inverse btn-sm" data-toggle-class="btn-primary" data-toggle-passive-class="btn-inverse">
                                            <input type="checkbox"> Straight
                                        </label>
                                        <label class="btn btn-inverse btn-sm" data-toggle-class="btn-primary" data-toggle-passive-class="btn-inverse">
                                            <input type="checkbox"> Right
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Radio Buttons</label>
                                <div class="controls form-group">
                                    <label class="radio">
                                        <div class="iradio_square-grey checked" style="position: relative;"><input type="radio" id="radio-4" class="iCheck" name="can" checked="checked" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        We can sing</label>
                                    <label class="radio">
                                        <div class="iradio_square-grey" style="position: relative;"><input type="radio" name="can" class="iCheck" id="radio-5" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        We can dance </label>
                                    <label class="radio">
                                        <div class="iradio_square-grey" style="position: relative;"><input type="radio" name="can" class="iCheck" id="radio-6" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        We are trying our best! </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Same Inline</label>
                                <div class="controls form-group">
                                    <label class="radio inline">
                                        <div class="iradio_square-grey checked" style="position: relative;"><input type="radio" id="radio-1" class="iCheck" name="cani" checked="checked" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        Sing</label>
                                    <label class="radio inline">
                                        <div class="iradio_square-grey" style="position: relative;"><input type="radio" name="cani" class="iCheck" id="radio-2" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        Dance </label>
                                    <label class="radio inline">
                                        <div class="iradio_square-grey" style="position: relative;"><input type="radio" name="cani" class="iCheck" id="radio-3" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                        Best </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">As button groups</label>
                                <div class="controls form-group">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-transparent btn-sm">
                                            <input type="radio"> One
                                        </label>
                                        <label class="btn btn-transparent btn-sm">
                                            <input type="radio"> Two
                                        </label>
                                    </div>
                                </div>
                                <div class="controls form-group">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-primary btn-sm active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-inverse">
                                            <input type="radio"> Left
                                        </label>
                                        <label class="btn btn-inverse btn-sm" data-toggle-class="btn-primary" data-toggle-passive-class="btn-inverse">
                                            <input type="radio"> Straight
                                        </label>
                                        <label class="btn btn-inverse btn-sm" data-toggle-class="btn-primary" data-toggle-passive-class="btn-inverse">
                                            <input type="radio"> Right
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="simple-switch" class="control-label">Switches</label>
                                <div class="controls form-group">
                                    <label class="checkbox inline">
                                        <span class="switch switch-small has-switch" data-on="primary" data-off="danger">
                                            <div class="switch-animate switch-on"><input id="simple-switch" type="checkbox" checked="checked"><span class="switch-left switch-small switch-primary">ON</span><label class="switch-small" for="simple-switch">&nbsp;</label><span class="switch-right switch-small switch-danger">OFF</span></div>
                                        </span>
                                    </label>
                                    <label class="checkbox inline">
                                        <span class="switch switch-small has-switch" data-on="success" data-off="warning">
                                            <div class="switch-animate switch-off"><input id="simple-switch-2" type="checkbox"><span class="switch-left switch-small switch-success">ON</span><label class="switch-small" for="simple-switch-2">&nbsp;</label><span class="switch-right switch-small switch-warning">OFF</span></div>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </section>
        </div>
        <div class="col-md-5">
            <section class="widget">
                <header>
                    <h4>
                        <i class="fa fa-magic"></i>
                        Pickers
                    </h4>
                </header>
                <div class="body">
                    <form method="post">
                        <fieldset>
                            <legend class="section">Dates</legend>
                            <div class="row"><div class="col-sm-8">
                                <div class="control-group">
                                    <label for="in-place-date" class="control-label">In place</label>

                                    <input id="in-place-date" class="form-control date-picker" type="text" name="in-place-date" value="">

                                </div>
                                <div class="control-group">
                                    <label for="btn-enabled-date" class="control-label">Button-enabled</label>

                                    <div class="input-group">
                                        <input id="btn-enabled-date" class="form-control" type="text" name="btn-enabled-date" value="">

                                    <span class="input-group-btn"><a href="#" id="btn-select-calendar" class="btn btn-danger" data-date-format="yyyy/mm/dd" data-date="2013/02/25">
                                        <i class="fa fa-calendar"></i>
                                    </a></span>
                                    </div>

                                </div>
                            </div></div>
                        </fieldset>
                        <fieldset>
                            <legend class="section">Colors</legend>
                            <div class="row"><div class="col-sm-8">
                                <div class="control-group">
                                    <label for="color" class="control-label">Simple Select</label>
                                    <div class="input-group">
                                        <input id="color" class="form-control colorpicker" type="text" name="color" value="#eac85e">
                                        <span id="color-holder" class="input-group-addon" style="background-color: #eac85e;">&nbsp;&nbsp;&nbsp;</span>
                                    </div>

                                </div>
                            </div></div>
                        </fieldset>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <section class="widget">
                <header>
                    <h4>
                        <i class="fa fa-align-left"></i>
                        Input Sizing
                        <small>From <code>.col-*-1</code> to <code>.col-*-12</code></small>
                    </h4>
                </header>
                <div class="body">
                    <form method="post">
                        <fieldset>
                            <div class="row"><div class="col-sm-6">
                                <div class="form-group">
                                    <label><code>.col-sm-6</code></label>

                                    <input type="text" class="form-control">

                                </div>
                            </div></div>
                            <div class="row"><div class="col-sm-4">
                                <div class="form-group">
                                    <label><code>.col-sm-4</code></label>

                                    <input type="text" class="form-control">

                                </div>
                            </div></div>
                            <div class="row"><div class="col-sm-2">
                                <div class="form-group">
                                    <label><code>.col-sm-2</code></label>

                                    <input type="text" class="form-control">

                                </div>
                            </div></div>

                            <div class="row"><div class="col-sm-12">
                                <div class="form-group">
                                    <label><code>.col-sm-12</code></label>

                                    <input type="text" class="form-control">

                                </div>
                            </div></div>
                            <div class="row"><div class="col-sm-11">
                                <div class="form-group">
                                    <label><code>.col-sm-11</code></label>

                                    <input type="text" class="form-control">

                                </div>
                            </div></div>

                            <div class="row"><div class="col-sm-9">
                                <div class="form-group">
                                    <label><code>.col-sm-9</code></label>

                                    <input type="text" class="form-control">

                                </div>
                            </div></div>
                            <div class="row"><div class="col-sm-7">
                                <div class="form-group">
                                    <label><code>.col-sm-7</code></label>

                                    <input type="text" class="form-control">

                                </div>
                            </div></div>
                        </fieldset>
                    </form>
                </div>
            </section>
        </div>
        <div class="col-md-6">
            <section class="widget">
                <header>
                    <h4>
                        <i class="fa fa-pencil"></i>
                        Masked Inputs
                    </h4>
                </header>
                <div class="body">
                    <form method="post" class="form-horizontal label-left">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="mask-phone">
                                    Phone
                                    <span class="help-block">(123) 456-7890</span>
                                </label>
                                <div class="controls form-group">
                                    <input id="mask-phone" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="mask-int-phone">
                                    International Phone
                                    <span class="help-block">+375 123 456 789</span>
                                </label>
                                <div class="controls form-group">
                                    <input id="mask-int-phone" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="mask-date">
                                    Date Format
                                    <span class="help-block">07-03-2013</span>
                                </label>
                                <div class="controls form-group">
                                    <input id="mask-date" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="mask-time">
                                    Time
                                    <span class="help-block">13:43</span>
                                </label>
                                <div class="controls form-group">
                                    <input id="mask-time" type="text" class="form-control">
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </section>
        </div>
    </div>
