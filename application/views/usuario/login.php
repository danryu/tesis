<!DOCTYPE html>
<html>

<!-- Mirrored from demo.okendoken.com/2.0/login.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 25 Mar 2014 15:50:43 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <title>Light Blue - Admin Template</title>
    <link href="<?=$url?>assets/bootstrap/css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="<?=$url?>assets/bootstrap/img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script src="<?=$url?>assets/bootstrap/lib/jquery/jquery-2.0.3.min.js"> </script>
    <script src="<?=$url?>assets/bootstrap/lib/jquery-pjax/jquery.pjax.js"></script>
    <script src="<?=$url?>assets/bootstrap/lib/backbone/underscore-min.js"></script>
    <script src="<?=$url?>assets/bootstrap/js/settings.js"> </script>
</head>
<body>
<div style=" height:200px; width:200px; margin: 0 auto;"> </div>
<div class="single-widget-container">

    <section class="widget login-widget">
        <header class="text-align-center">
            <h4>Ingresa a tu cuenta</h4>
        </header>
        <div class="body">
            <form class="no-margin" action="http://localhost/tesis/" method="post">
                <fieldset>
                    <div class="form-group no-margin">
                        <label for="email" >RUN</label>

                        <div class="input-group input-group-lg">
                                <span class="input-group-addon">
                                    <i class="eicon-user"></i>
                                </span>
                            <input id="email" type="email" class="form-control input-lg"
                                   placeholder="Tu run...">
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="password" >Contraseña</label>

                        <div class="input-group input-group-lg">
                                <span class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </span>
                            <input id="password" type="password" class="form-control input-lg"
                                   placeholder="Tu contraseña...">
                        </div>

                    </div>
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-block btn-lg btn-danger">
                        <span class="small-circle"><i class="fa fa-caret-right"></i></span>
                        <small>Iniciar</small>
                    </button>
                    <div class="forgot"><a class="forgot" href="#">Olvidaste tu contraseña?</a></div>
                </div>
            </form>
        </div>
        <footer>
            
        </footer>
    </section>
</div>
</body>

<!-- Mirrored from demo.okendoken.com/2.0/login.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 25 Mar 2014 15:50:43 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
</html>