<!DOCTYPE html>
<html>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <title><?=$title_for_layout?></title>
        <?=$headdefault_for_layout?>
        <?=$head_for_layout?>
    </head>
    <body class="background-dark">
        <div class="logo hidden-print">
            <h4><a href="<?=base_url()?>">Apple's <strong>Teacher</strong></a></h4>
        </div>
        <nav id="sidebar" class="sidebar nav-collapse collapse">
            <?=$menu_for_layout?>
        </nav>
        <div class="wrap">
            <header class="page-header">
                <?=$header_for_layout?>
            </header>
            <div class="content container">
            <?= $content_for_layout ?>
            </div>
            <div class="loader-wrap hiding hide">
                <i class="fa fa-spinner fa-spin"></i>
            </div>
        </div>
        <?=$finalCode_for_layout?>
    </body>
</html>