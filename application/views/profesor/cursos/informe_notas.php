
<!-- hidden-print //CSS para ocultar cosas
    class="hidden-xs" -->
<div class="content container">
    <div class="row">
        <div class="col-md-12">
            <section class="widget">
                <div class="body no-margin">
                    <div class="row">
                        <div class="col-sm-6 col-print-6">
                          <h2>Informe de notas</h2>
                        </div>
                        <div class="col-sm-6 col-print-6">
                            <div class="invoice-number text-align-right">
                                <h5>Fecha: <?php echo date("d/m/Y"); ?></h5>
                            </div>
                            
                        </div>
                    </div>
                    <hr>
                    <section class="invoice-info well">
                        <div class="row">
                            <div class="col-sm-6 col-print-6">
                                <h4 class="details-title">Información de Alumno</h4>
                                <h4 class="company-name">
                                   <?php 
                                   echo $salida->first_name.' '.$salida->last_name; ?>
                                </h3>
                                <address>
                                    RUN: <?=$salida->username?><br>
                                    Curso: <?=$curso->Grado.'° '.$curso->Letra.' '.$curso->Tipo?><br>
                                    Apoderado: <?=$apoderado->first_name.' '.$apoderado->last_name?><br>
                                    Profesor Jefe: <?=$profesor->first_name.' '.$profesor->last_name?>                                   
                                </address>
                            </div>
                            <div class="col-sm-6 col-print-6 client-details">
                                <h4 class="details-title">Apple's Teacher</h4>
                                <h3 class="client-name">
                                    Colegio Jampsur
                                </h3>
                                <address>
                                    Dirección: Los Tilos #123<br>
                                    Teléfono: 12345678
                                </address>
                            </div>
                        </div>
                    </section>
                    <table class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th class="text-center" rowspan="2">#</th>
                            <th class="text-center" rowspan="2">Asignatura</th>
                            <th class="text-center" colspan="10">Notas</th>
                                
                            <th class="text-center" colspan="2">Promedio</th>
                        </tr>
                        <tr>
                            <th class="text-center">1</th>
                            <th class="text-center">2</th>
                            <th class="text-center">3</th>
                            <th class="text-center">4</th>
                            <th class="text-center">5</th>
                            <th class="text-center">6</th>
                            <th class="text-center">7</th>
                            <th class="text-center">8</th>
                            <th class="text-center">9</th>
                            <th class="text-center">10</th>
                            <th class="text-center">Desempeño</th>
                            <th class="text-center">Nota</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                          if($asignaturas['bool']) 
                          {
                            //$acordion='';
                            $count_fila = 1;
                            foreach ($asignaturas['query'] as $q) 
                            {
                                $count=0;                                
                                $fila='';
                                $suma=0;
                                $notas_total=0;
                                $restante ='';
                                $desempeño = '';
                                $nombre_asignatura=$q->Nombre;
                                if($notas['bool'])
                                {

                                    foreach ($set_notas['query'] as $s) 
                                    {
                                        if($s->Asignatura_Id===$q->Asignatura_Id)
                                        {
                                            if($s->Registrada)
                                            {                               
                                                foreach ($notas['query'] as $n) 
                                                {  
                                                    if($n->set_nota_Id==$s->set_nota_Id)
                                                    {                          
                                                                                               
                                                        if($n->Nota<4)
                                                        {
                                                            $Nota='<label style="color:red">'.$n->Nota.'</label>';
                                                        }
                                                        else
                                                        {
                                                            $Nota=$n->Nota;
                                                        }
                                                        $fila.='<td>'.$Nota.'</td>';
                                                        $suma+=$n->Nota;
                                                        $notas_total++;
                                                        $count++;
                                                    }
                                                }  
                                            }
                                            else
                                            {
                                                $fila.='<td> - </td>';
                                                $count++;
                                            }
                                        }
                                    }
                                    for($i=$count;$i<10;$i++){
                                        $restante.='<td> - </td>';
                                    }
                                    if($notas_total!=0)
                                    {
                                        $suma=$suma/$notas_total;
                                        if($suma<4)
                                        {
                                            $desempeño = '<label style="color:red">Reprobado</label>';
                                        }
                                        else{
                                            $desempeño = 'Aprobado';
                                        }

                                        $promedio_fin=$suma;
                                    }
                                    else{
                                        $desempeño = ' - ';
                                        $promedio_fin = ' - ';
                                    }
                                    //$promedio = $suma/$notas_total;
                                    //echo $promedio.' ';
                                    echo '<tr><td>'.$count_fila.'</td><td>'.$nombre_asignatura.'</td>'.$fila.$restante.'<td>'.$desempeño.'</td><td>'.$promedio_fin.'</td>';
                                    $restante = "";  
                                    $promedio_fin = 0;
                                    $count_fila++;                                
                                }
                                else
                                {
                                    $fila='<tr><td colspan="10" style="text-align:center"> No hay registro de evaluaciones</td></tr>';
                                    $promedio_fin='No hay evaluaciones registradas';
                                }
                            }
                        }                
                    
                         
                        ?>
                       
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6 col-print-6">
                            <blockquote>
                                <strong>Observación:</strong><br>________________________________________________________________________________________________________________________________________________________________________________<br>________________________________________________________________________________________________________________________________________________________________________________<br>________________________________________________________________________________________________________________________________________________________________________________<br>________________________________________________________________________________________________________________________________________________________________________________
                            </blockquote>
                        </div>
                        
                    </div>
                    <footer>
                    <div class="row">
                        <div class="col-sm-6 col-print-6">
                        <center>
                        __________________________________<br>
                        Firma Profesor Jefe
                        </center>
                        </div>
                        <div class="col-sm-6 col-print-6">
                        <center>
                        __________________________________<br>
                        Firma Director
                        </center>
                        </div>
                    </div>
                    <div class="invoice-actions text-align-right hidden-print">
                        <button id="print" class="btn btn-inverse">
                            <i class="fa fa-print"></i>
                            &nbsp;&nbsp;
                            Imprimir
                        </button>
                    </div>
                    </footer>
                </div>
            </section>
        </div>
    </div>
</div>
