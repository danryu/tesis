<div class="row">
        <div class="col-md-12">
            <h2 class="page-title">Form Wizard <small>Form validation</small></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <section class="widget">
            <header>
                <h4>
                    <i class="glyphicon glyphicon-remove"></i>
                    Wizard
                    <small>Tunable widget</small>
                </h4>
            </header>
            <div class="body">
                <div id="wizard" class="form-wizard">
                    <ul class="wizard-navigation nav-justified">
                        <li><a href="#tab1" data-toggle="tab"><small>1.</small> <strong>Prueba</strong></a></li>
                        <li><a href="#tab2" data-toggle="tab"><small>2.</small> <strong>Preguntas</strong></a></li>
                        <li><a href="#tab3" data-toggle="tab"><small>3.</small> <strong>Finalizar</strong></a></li>
                    </ul>
                    <div id="bar" class="progress progress-small">
                        <div class="progress-bar progress-bar-inverse"></div>
                    </div>
                    <div class="tab-content">                        
                        <div class="tab-pane" id="tab1">
                            <div class="col-md-12">
                                <h2>Opciones de Prueba</h2>
                                <form class="form-horizontal" action='#' method="POST">
                                    <div class="col-md-9">
                                        <div class="control-group">
                                            <label class="control-label"  for="name">Nombre de la Prueba</label>
                                            <div class="controls form-group">
                                                <div class="col-md-10"><input type="text" id="nombre_prueba" name="nombre_prueba" placeholder="" class="form-control"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-row control-group">
                                            <label for="credit-card-type" class="control-label">Cursos</label>
                                            <div class="controls form-group">
                                                    <select id="cursos" name="cursos" data-placeholder="Seleccione un curso" class="chzn-select select-block-level">
                                                        <option value=""></option>
                                                        <option value="2323">NO EXISTE</option>
                                                    <?php   if(isset($asg_query))
                                                            {
                                                                
                                                                foreach ($asg_query as $q) 
                                                                {
                                                                    echo '<option value="'.$q->Id.'">'.$q->Grado.'°'.$q->Letra.'</option>';
                                                                }

                                                            } 
                                                    ?>                                     
                                                    </select>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-row control-group">
                                            <label for="credit-card-type" class="control-label">Asignaturas</label>
                                            <div class="controls form-group">
                                                <div class="col-md-4"><select id="asignatura" name="asignatura" data-placeholder="Seleccione una Asignatura" class="chzn-select select-block-level">
                                                    <option value=""></option>                                                    
                                                </select></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-row control-group">
                                                <label for="expiration-date" class="control-label">Fecha</label>
                                                <div class="controls form-group">
                                                    <input type="text" id="expiration-date" name="fecha" class="form-control">
                                                </div></form>
                                        </div>
                                    </div>
                            </div>  
                        </div>
                        <div class="tab-pane" id="tab2">
                            <h2>Preguntas de la prueba</h2>       
                                <div class="col-md-12">
                                    <div class="control-group">
                                        <label for="text" class="control-label">Pregunta</label>
                                        <div class="controls form-group">
                                            <textarea id="text" rows="3" name="text" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <?=input(array( 'name'=>'r_[]','label'=>'Respuesta 1'))?>
                                    <?=input(array( 'name'=>'r_[]','label'=>'Respuesta 2'))?>
                                    <?=input(array( 'name'=>'r_[]','label'=>'Respuesta 3'))?>
                                    <?=input(array( 'name'=>'r_[]','label'=>'Respuesta 4'))?>
                                    <?=input(array( 'name'=>'r_[]','label'=>'Respuesta 5'))?>

                                </div>

                                <div class="container_12">                         
                                        <div id="nuevo" class="grid_12">
                                            <input type="text" value="" id="nombre" name="nombre[]" />
                                            <select name="puntos[]" id="puntos">    
                                                    <option value="">Escoge una puntuación</option>
                                                    <option value="1">1 punto</option>
                                                    <option value="2">2 puntos</option>
                                                    <option value="3">3 puntos</option>
                                                    <option value="4">4 puntos</option>
                                            </select>
                                            <span class="grid_2" id="add_selector">Añadir etiqueta</span>   
                                        </div><br /><br />        
                                </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <h2>Le querí guardar la wa????</h2> 
                            <p>Si desea guardar la prueba, seleccione guardar, recuerde que no podrá editar la prueba luego de ser almacenada</p>
                        </div>                        
                        <div class="description">
                            <ul class="pager wizard">
                                <li class="previous">
                                    <button class="btn btn-primary pull-left"><i class="fa fa-caret-left"></i> Previous</button>
                                </li>
                                <li class="next">
                                    <button class="btn btn-primary pull-right" >Siguiente <i class="fa fa-caret-right"></i></button>
                                </li>
                                <li class="finish" style="display: none">
                                    <button class="btn btn-success pull-right" >Finalizar <i class="fa fa-check"></i></button>
                                </li>
                            </ul>
                        </div>
                    </div>                   
                </div>
            </div>
            </section>
        </div>
    </div>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript">
        //Rellenado de asignaturas
        $(document).ready(function() {
            $("#cursos").change(function() {
                $("#cursos option:selected").each(function() {
                    cursos = $('#cursos').val();
                    $.post("http://localhost/tesis/profesor/loadAsg", {cursos: cursos}, function(data) {
                        $("#asignatura").html(data);
                    });
                });
            })
        });
    </script>
    <script type="text/javascript">
            $(document).ready(function(){ 
                $("#nuevo").find("#add_selector:last").live('click',function(){ 
                    if($(this).prevAll("#nombre").val() == '')  
                    {
                        alert('No puedes dejar un campo vacío');
                    }
                    else if($(this).prevAll("#puntos:last").val() == '')
                    {
                        alert('Debes dar una puntuacion');
                    }else{

                        $("#nuevo").append('<div style="margin:10px 0px 0px -10px; border:0"'+
                        '<div id="nuevo" class="grid_12">'+
                        '<input type="text" id="nombre" name="nombre[]" />&nbsp;'+
                        '<select name="puntos[]" id="puntos">'+ 
                        '<option value="">Escoge una puntuación</option><option value="1">1 punto</option>'+
                        '<option value="2">2 puntos</option><option value="3">3 puntos</option>'+
                        '<option value="4">4 puntos</option></select>'+
                        '<span class="grid_2" id="add_selector">Añadir etiqueta</span></div><div class="grid_2"'+
                        'id="eliminar">Eliminar</div>');
                    }               
                }) 
                $('#nuevo').find("#eliminar").live('click',function(){ 
                    $(this).prevAll().eq(0).css('background','yellow'); 
                    valor_input = $(this).prevAll().eq(0).find('input').val(); 
                    valor_select = $(this).prevAll().eq(0).find('select').val();
                    if(confirm('¿Quieres eliminar la etiqueta con nombre '+valor_input+' y puntuación '+valor_select+'?' ))
                    {
                        $(this).prev("#nuevo").remove();
                        $(this).remove();
                    }else{
                        alert('No se hicieron cambios');
                        $(this).prevAll().eq(0).css('background','#2792bd');
                    }               
                })
            });
        </script> 