

<div class="col-md-12">
    <section class="widget">
    	<header>
        	<h4>Asistencia dia:<?=date('d-m-Y')?></h4>
            <h5>Clase: °<?=$curso->Grado?><?=$curso->Letra?> </h5>
            <h5>Asignatura: <?=$asi->Nombre?></h5>
        </header>
        <div class="body">
        <?php
            if(isset($alumnos))
        	{
        		echo list_alumnos_asistencia($alumnos,$Id_Cha);
        	}
        ?>
        <div class="form-actions">
                <button type="submit" class="btn btn-danger" id="guardar">Guardar</button>
                <button type="button" class="btn btn-default" id="limpiar">Limpiar</button>
        	</div>
        </div>
    </section>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		
		$('#limpiar').click(function()
		{
			$('input[name^="presente"]').each(function(index)
			{
				$(this).val('');
			});
		});
		$('#guardar').click(function()
		{
			var presente= new Array();
			var bool_vacio=true;
			var bool_numero=true;
			$('input:checkbox[name^="presente"]').each(function(index)
			{
				var val = $(this).is(':checked');
                presente.push(val); 
			});
				$.post( "/tesis/profesor/demo12",{presente:presente,Id_Cha:<?=$Id_Cha?>}, function( data ) 
	            { 
	            	var bool = data.bool;
	            	if(bool)
	            	{
	            		var URL="/tesis/profesor/curso/<?=$Id_C?>";
	            		window.location = URL;
	            	}
	            	else
	            	{
	            		alert("CHAO");
	            	}
	            },"json");
		});
		$('.input_nota').on('input',function()
		{
			var total_count=0;
			var suma=0;
			var bool_vacio=true; 
			var bool_numero=true;
			var PromedioCurso=0;
			var NotasIn=0;	
			var NotasSu=0;
			var TotalNotas=0;
			$('input[name^="notas"]').each(function(index)
			{
               var val = $(this).val(); 
                $(this).css("border-color","");
                if(val.length > 0)
                {
                	val = val.replace(",",".");
                	val = parseFloat(val);
                	if(val>0 && val<=7)
                	{
                		TotalNotas++;
                		if(val>=4)
                		{	
                			suma+=val;
                    		NotasSu++;
                		}
                		else
                		{
                			suma+=val;
                			NotasIn++;
                		}                		
                	}
                	else
                	{
                		$(this).css("border-color","#e5603b");
                		bool_numero = false;

                	}                    
                }  
                else
                {                           
                    $(this).css("border-color","#e5603b");
                	bool_vacio = false;
                }  
            });         
            	if(!bool_numero)
            	{
            		PromedioCurso="NaN";
					NotasIn="NaN";
					NotasSu="NaN";
					TotalNotas="NaN";
            	}
                if(TotalNotas!=0){PromedioCurso=suma/TotalNotas;}
             	var 	texto='<p>Promedio Curso: '+PromedioCurso+'</p>';
             			texto+='<p>Notas Insuficientes: '+NotasIn+'</p>';
             			texto+='<p>Notas Suficientes: '+NotasSu+'</p>';
             			texto+='<p>Total Notas: '+TotalNotas+'</p>';

             	$('#resumen_Notas').html(texto);   
           
		});
	});
	

</script>