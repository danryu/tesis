<?php 
    foreach ($set_notas_cursos as $q) 
    {
        if($q->Id == $Id_set)
        {
            $Tipo=$q->Tipo;
        }
    }
?>

<div class="col-md-8">
    <section class="widget">
    	<header>
        	<h4>Registrar Notas <small><?=$Tipo?></small></h4>
        </header>
        <div class="body">

        <?php
        if($se_puede_registrar)
        {
            if(isset($alumnos))
        	{
        		echo list_alumnos($alumnos,$Id_set);
        	}
            ?>
            <div class="form-actions">
                    <button class="btn btn-danger" onClick="location.href='<?=$url_regresar?>'">Regresar</button>
                    <button type="submit" class="btn btn-danger" id="guardar">Guardar</button>
                    <button type="button" class="btn btn-default" id="limpiar">Limpiar</button>
            	</div>
            </div>
            <?php 
        }
        else 
        {
        ?>
            <div class="alert alert-danger">
                <h4><i class="fa fa-ban"></i> <strong>Lo sentimos, no podrá registrar notas!</strong></h4>
                <p>Debe registrar los resultado de evaluaciones anteriores para continuar.</p>
                <p>
                    <a class="btn btn-inverse btn-sm" onClick="location.href='<?=$url_regresar?>'">Regresar</a>
                </p>
            </div>
        <?php
                            }
        ?>
    </section>
</div>
<div class="col-md-4">
    <section class="widget">
    	<header>
        	<h4><i class="fa fa-check-square-o"></i> Avance de Resultado</h4>
        </header>
        <div id="resumen_Notas" class="body">
        <p>Promedio Curso:</p>
        <p>Notas insuficientes:</p>
        <p>Notas Suficientes:</p>
        <p>Total Notas:</p>
        </div>
    </section>
</div>
<?php ?>
<div class="col-md-4">
    <section class="widget">
        <header>
            <h4><i class="fa fa-check-square-o"></i> Notas por registrar</h4>
        </header>
        <div class="body">
        <?php 
            if(isset($set_notas_cursos))
            {
                echo list_set_notas_vertical($set_notas_cursos,$Id_set,$fecha_actual);
            }
        ?>
            
        </div>
    </section>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		
		$('#limpiar').click(function()
		{
			$('input[name^="notas"]').each(function(index)
			{
				$(this).val('');
			});
		});
		$('#guardar').click(function()
		{
			var notas= new Array();
			var Id=<?=$Id_set?>;
			var bool_vacio=true;
			var bool_numero=true;
			$('input[name^="notas"]').each(function(index)
			{
				var val = $(this).val(); 
                $(this).css("border-color","");
                if(val.length > 0)
                {
                	val = val.replace(",",".");                	
                	val = parseFloat(val);
                	if(val>=1 && val<=7)
                	{
                		notas.push(val);       		
                	}
                	else
                	{
                		$(this).css("border-color","#e5603b");
                		bool_numero = false;
                	}                    
                }  
                else
                {                           
                    $(this).css("border-color","#e5603b");
                	bool_vacio = false;
                }  
			});
			if(bool_vacio && bool_numero)
			{
				$.post( "/tesis/profesor/save_notas",{Id_Set:Id,Notas:notas}, function( data ) 
	            { 
	            	var bool = data.bool;
	            	if(bool)
	            	{
	            		var URL="/tesis/profesor/curso/<?=$Id_C?>";
                        sweetAlert("Registro Exitoso!", "Se han registrado y notificado las evaluaciones correctamente", "success");
	            		window.location = URL;
	            	}
	            	else
	            	{
	            		sweetAlert("Ups! Encontramos un error", data.msg, "error");
	            	}
	            },"json");
			}
			else
			{
                if(bool_vacio==false && bool_numero==false)
                {
                   sweetAlert("Ups! Encontramos un error","Debe completar todos los campos con notas superiores a 1 e inferiores a 7", "error"); 
                }
                else
                {
                    if(bool_vacio==true && bool_numero==false)
                    {
                       sweetAlert("Ups! Encontramos un error","Las notas ingresadas deben ser superior a 1 e inferior o igual 7", "error"); 
                    }
                    else
                    {
                        sweetAlert("Ups! Encontramos un error","Debe completar el formulario sin dejar campos vacios", "error"); 
                    }
                }
				
			}

		});
		$('.input_nota').on('input',function()
		{
			var total_count=0;
			var suma=0;
			var bool_vacio=true; 
			var bool_numero=true;
			var PromedioCurso=0;
			var NotasIn=0;	
			var NotasSu=0;
			var TotalNotas=0;
			$('input[name^="notas"]').each(function(index)
			{
               var val = $(this).val(); 
                $(this).css("border-color","");
                if(val.length > 0)
                {
                	val = val.replace(",",".");
                	val = parseFloat(val);
                	if(val>0 && val<=7)
                	{
                		TotalNotas++;
                		if(val>=4)
                		{	
                			suma+=val;
                    		NotasSu++;
                		}
                		else
                		{
                			suma+=val;
                			NotasIn++;
                		}                		
                	}
                	else
                	{
                		$(this).css("border-color","#e5603b");
                		bool_numero = false;

                	}                    
                }  
                else
                {                           
                    $(this).css("border-color","#e5603b");
                	bool_vacio = false;
                }  
            });         
            	if(!bool_numero)
            	{
            		PromedioCurso="NaN";
					NotasIn="NaN";
					NotasSu="NaN";
					TotalNotas="NaN";
            	}
                if(TotalNotas!=0){PromedioCurso=suma/TotalNotas;}
             	var 	texto='<p>Promedio Curso: '+PromedioCurso+'</p>';
             			texto+='<p>Notas Insuficientes: '+NotasIn+'</p>';
             			texto+='<p>Notas Suficientes: '+NotasSu+'</p>';
             			texto+='<p>Total Notas: '+TotalNotas+'</p>';

             	$('#resumen_Notas').html(texto);   
           
		});
	});
	

</script>