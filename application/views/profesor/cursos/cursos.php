
            <div class="col-md-9">
                <section class="widget widget-tabs">
                    <?php if(isset($query_asg)){echo menu_materias($query_asg,$Id_C);} ?>
                    <div class="body tab-content">
                        <?php  
                                if(!isset($query_foros)){$query_foros=NULL;}
                                if(!isset($query_notas)){$query_notas=NULL;}
                                if(!isset($reglamento_notas)){$reglamento_notas=NULL;}
                                if(!isset($tipos_evaluaciones)){$tipos_evaluaciones=NULL;}
                                if(!isset($horas_asig)){$horas_asig=NULL;}
                                if(!isset($query_alu)){$query_alu=NULL;}
                                if(!isset($set_notas_cursos)){$set_notas_cursos=NULL;}
                                if(!isset($tab_select)){$tab_select=NULL;}
                                if(isset($query_asg))
                                {
                                    echo tab_materia(   $query_asg,  
                                                        $query_foros,
                                                        $query_notas,
                                                        $reglamento_notas,
                                                        $tipos_evaluaciones,
                                                        $query_alu,
                                                        $set_notas_cursos,
                                                        $tab_select);
                                } 
                             

                        ?>                   
                    </div>
                </section>
            </div>
            <div class="col-md-3">
                <section class="widget widget-tabs">
                    <header>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#alumnos_tab" data-toggle="tab">Alumnos</a>
                            </li>
                            <li>
                                <a href="#apoderados_tab" data-toggle="tab">Apoderados</a>
                            </li>
                        </ul>
                    </header>
                    <div class="body tab-content">
                        <div id="alumnos_tab" class="tab-pane active clearfix">
                           <?php if(isset($query_alu)){echo list_usuario_curso_vertical($query_alu,'Alumno');}?>
                        </div>
                        <div id="apoderados_tab" class="tab-pane">
                            <?php if(isset($query_apo)){echo list_usuario_curso_vertical($query_apo,'Apoderado');}?>
                        </div>
                    </div>
                </section>
            </div>
            <div id="Seccion_Anotaciones">
                <div id="modal_anotaciones" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel2">Anotaciones Hoja de Vida</h4>
                                    </div>
                                    <div class="modal-body"> 
                                        <div style="height:120px">
                                            <h4>Comentario</h4>
                                            <textarea rows="4" class="form-control" id="wysiwyg" name="comentario_anotacion" required></textarea>
                                        </div>
                                        <div style="height:40px">
                                            <h4>Tipo</h4>
                                            <select  name="tipo_anotacion" class="selectpicker" data-style="btn-default" tabindex="-1" id="simple-colored">
                                                                <option value="0">Negativa</option>
                                                                <option value="1">Postivia</option>
                                            </select>
                                        </div>
                                        <input name="Id_Alumno" type="hidden" />

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cierrar</button>
                                        <button type="button" name="Save_alumno" class="btn btn-primary save_anotacion">Guardar</button>
                                    </div>

                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                </div>
            </div> 
            <div id="Edit_Nota_Alumno">
                <div id="modal_edit_nota" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel2">Editar Evaluación</h4>
                                    </div>
                                    <div class="modal-body"> 
                                        <div style="height:120px">
                                            <h4>Comentario</h4>
                                            <textarea rows="4" class="form-control" name="comentario_anotacion" required></textarea>
                                        </div>
                                        <input name="Id_Alumno_nota" type="hidden" />
                                        <input name="Id_Nota_Set_nota" type="hidden" />

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cierrar</button>
                                        <button type="button" name="Save_alumno" class="btn btn-primary save_anotacion">Guardar</button>
                                    </div>

                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                </div>
            </div>
            <input name="Set_cha_Id" type="hidden" />
           
        <script type="text/javascript">           
            $(document).ready(function(){  
                $(".modal-footer").on("click", ".save", function()
                { 
                    jsShowWindowLoad("Estamos buscando");
                    $(this).attr('disabled', true);
                    var id=$(this).attr('id');   
                    id=id.split('save'); 
                    id=id[1];   
                    var btn=this;
                    var titulo=$('input[name="titulo'+id+'"]').val().trim();
                    var comentario=$('textarea[name="comentario'+id+'"]').val().trim();
                    if(titulo === '' || comentario === '')
                    {
                        jsRemoveWindowLoad();  
                        if(titulo === '' &&  comentario === ''){sweetAlert("Ups! Encontramos un error", "Por favor completar los campos de titulo y comentario", "error");}else{if(titulo === ''){sweetAlert("Ups! Encontramos un error", "No encontramos un titulo para el foro", "error");}else{sweetAlert("Ups! Encontramos un error", "No encontramos comentario para el nuevo foro", "error");}} $(this).attr('disabled', false);
                    }
                    else
                    {                    
                        $.post( "/tesis/profesor/create_foro",{cha_Id : id, titulo : titulo, comentario : comentario}, function( data ) 
                        {                  
                            if(data.bool)
                            {  
                                $('#myModal'+id+'foro').modal('toggle'); 
                                $('#lista_foro_'+id).html(data.list);
                                $('input[name="titulo'+id+'"]').val('');
                                $('textarea[name="comentario'+id+'"]').val('')
                                $(btn).attr('disabled', false);
                                jsRemoveWindowLoad();  
                                
                            }
                            else
                            {
                                jsRemoveWindowLoad();  
                                sweetAlert("Ups! Encontramos un error", data.msg, "error");
                                $(btn).attr('disabled', false);

                            }
                        },"json");
                    }          
                });
                $(document).on('focus', ".date-picker", function () {$(this).datepicker( "destroy" ).datepicker(); });
                $(".modal-body").on("click", ".cantidad_eva", function()
                {
                    var inputs_list="";
                    var Id_Cha=$('input:hidden[name=Set_cha_Id]').val();                    
                    var cantidad=$('#Cantidad_Eva_'+Id_Cha+' option:selected').val();  
                    var tableId = "#notas_setting_"+Id_Cha;
                    var Notas_min = parseInt($('input:hidden[name=Notas_Min'+Id_Cha+']').val());
                    var num_rows=$(tableId+" tr").length-1;                    
                    var Notas_max = parseInt($('input:hidden[name=Notas_Max'+Id_Cha+']').val());  
                    //alert("ID:"+Id_Cha+" - Cantidad:"+cantidad+" - NumeroR:"+num_rows+" - NMin:"+Notas_min+" - MMax:"+Notas_max);                   
                    if(Notas_min <= cantidad)
                    {
                        if(num_rows>=cantidad)
                        {
                            var diferencia = num_rows - cantidad;                                                        
                            for(tn=0;tn<diferencia;tn++)
                            {
                                $(tableId+" tr:last").remove();
                            }  
                        }
                        else
                        {
                            var diferencia = cantidad - num_rows;                                                     
                            if(Notas_min<num_rows){Notas_min=num_rows;}
                            if(!Notas_max>=cantidad){cantidad=Notas_max;}
                            for(i=Notas_min;i<cantidad;i++)
                            {
                                inputs_list+='<tr><td>'+(i+1)+'</td><td><input type="text" id="normal-field" name="tipo_notas'+Id_Cha+'[]" class="form-control" placeholder="Ingrese Evaluación "></td>';
                                inputs_list+='<td><input id="in-place-date" class="form-control date-picker" type="text" name="fecha_notas'+Id_Cha+'[]" placeholder="Ingrese Fecha Evaluación " value=""></td></tr>';
                            }
                            $(".date-picker").datepicker();
                            $(tableId+" tr:last").after(inputs_list);
                        }      
                    }
                    else
                    {
                        alert("CHAO");
                    }                      
                });
                $("#alumnos_tab").on("click", ".btn", function(){var id=$(this).attr('id');$('#modal_anotaciones').modal('show');$('input:hidden[name=Id_Alumno]').val(id);});   
                $(".btn_nota_editar").on("click", function()
                {              
                    var id=$(this).attr('id');
                    var partes=id.split("btn_editar");
                    $('#modal_edit_nota').modal('show'); 
                    $('input:hidden[name=Id_Alumno_nota]').val(partes[1]);
                    $('input:hidden[name=Id_Nota_Set_nota]').val(partes[0]);
                });              
                $(".modal-footer").on("click", ".save_anotacion", function()
                {
                    jsShowWindowLoad("Estamos trabajando");
                    var Id_Alumno = $('input:hidden[name=Id_Alumno]').val();
                    var comentario = $('textarea[name=comentario_anotacion]').val();
                    var tipo = $('select[name=tipo_anotacion] option:selected').val();                    
                    if(comentario.length>0)
                    {
                        $.post( "/tesis/profesor/create_anotacion",{Id_Alumno : Id_Alumno, comentario : comentario, tipo:tipo}, function( data ) 
                        { 
                            var bool =data.bool;
                            if(bool)
                            {
                                jsRemoveWindowLoad();   
                                $('#modal_anotaciones').modal('hide'); 
                                $('input:hidden[name=Id_Alumno]').val('');                               
                                $('textarea[name=comentario_anotacion]').val('');
                                $('select[name=tipo_anotacion] option:selected').val('');
                                sweetAlert("Registro exitoso!", "Anotación guardada correctamente", "success");
                            }
                            else
                            {
                                jsRemoveWindowLoad(); 
                                sweetAlert("Ups! Encontramos un error", data.msg, "error"); 
                            }
                        },"json");
                    }
                    else
                    {
                        jsRemoveWindowLoad(); 
                        sweetAlert("Ups! Encontramos un error", "Comentario vacío", "error"); 
                    }
                });

                $(".modal-footer").on("click", ".save_setting_notas", function()
                {
                    var Id=$('input:hidden[name=Set_cha_Id]').val();
                    var cantidad_evaluaciones=$("#notas_setting_"+Id+" tr").length-1; 
                    var type_element= new Array();
                    var date_element= new Array();
                    var count=0;
                    var bool_fecha=true;
                    var bool_vacio=true;
                    var today = new Date();
                    var nextyear =new Date().getFullYear()+1;
                    $('input[name^="tipo_notas'+Id+'"]').each(function(index){
                        var val = $(this).val(); 
                        $(this).css("border-color","");
                        if(val.length > 0)
                        {
                            type_element.push(val); 
                        }  
                        else
                        {                           
                            $(this).css("border-color","#e5603b");
                            bool_vacio = false;
                        }                              
                    });                    
                    $('input[name^="fecha_notas'+Id+'"]').each(function(index){
                        var val = $(this).val();
                        $(this).css("border-color","");
                        if(val.length > 0)
                        {
                            var valSplit=val.split("/");                            
                            var dd =parseInt(valSplit['0']);
                            var mm = parseInt(valSplit['1'])-1; 
                            var yyyy = parseInt(valSplit['2']);
                            if(dd<10){dd='0'+dd;} 
                            if(mm<10){mm='0'+mm;} 
                            var InputDate= new Date(yyyy,mm,dd);
                            if(InputDate > today && today.getFullYear() < nextyear)
                            {
                                date_element.push(val);
                            }
                            else
                            {   
                                $(this).css("border-color","#e5603b");                         
                                bool_fecha= false;
                            }                      
                        }  
                        else
                        {
                           $(this).css("border-color","#e5603b");
                            bool_vacio = false;
                        }         
                    });
                    if(bool_vacio && bool_fecha)
                    {
                        var btn = this;
                        $(btn).attr('disabled', true);
                        $('#alerta_'+Id).html("");
                        $('#alerta_'+Id).html(alert_info('Se esta guardando su configuración'));
                        $.post( "/tesis/profesor/save_setting_eva",{Id_Cha:Id,can_eva:cantidad_evaluaciones,fecha_array:date_element,tipo_array:type_element}, function( data ) 
                        { 
                            var bool =data.bool;
                            if(bool)
                            {                                
                                $(btn).attr('disabled', false);
                                $('#alerta_'+Id).html(alert_success('Configuración guardada exitosamente!'))
                                $('input[name^="tipo_notas'+Id+'"]').each(function(index){$(this).val('');});
                                $('input[name^="fecha_notas'+Id+'"]').each(function(index){$(this).val('');});  
                                setTimeout(function(){$('#mymodal'+Id+'notas').modal('hide'); $('#alerta_'+Id).html(""); },3000);
                                location.reload();  
                                
                            }
                            else
                            {
                                var msg = data.msg;
                                $('#alerta_'+Id).html(alert_error('No se ha guardado correctamente, Error='+msg))
                                $(btn).attr('disabled', false);
                            }
                        },"json");
                    }
                    else
                    {
                        $('#alerta_'+Id).html("");
                        var fecha_actual=today.getDate()+'/'+(today.getMonth()+1)+'/'+today.getFullYear();
                        if(!bool_vacio && !bool_fecha)
                        {
                            var texto='Revise los campos marcados, puede tener estos errores: <ul>';
                            texto+='<li> 1.- El campo está vacio</li>';
                            texto+='<li> 2.- La fecha ingresada debe ser posterior a la fecha actual '+fecha_actual+' e inferior a '+nextyear+'</li><ul>';
                            var alerta=alert_error(texto);
                            $('#alerta_'+Id).html(alerta);
                        }
                        if(!bool_vacio && bool_fecha)
                        {
                            var alerta=alert_error('Debe completar todos los campos para continuar');
                            $('#alerta_'+Id).html(alerta);
                        }
                        if(bool_vacio && !bool_fecha)
                        {
                           
                            var alerta=alert_error('Las fechas ingresadas deben ser posteriores a la actual: '+fecha_actual+' e inferior a '+nextyear);
                            $('#alerta_'+Id).html(alerta);
                        }
                    }
                });

                $(".modal-footer").on("click", ".edit_nota", function()
                {
                    var id=$(this).attr('id');
                    jsShowWindowLoad("Estamos trabajando");
                    var Comentario=$('textarea[name=textarea_'+id+']').val();
                    var Nota=$('input:text[name=actual_'+id+']').val();
                    var Id_Set=$('select[name=vieja_'+id+'] option:selected').val();
                    var bool_nota=true;
                    if(Comentario.length>0 && Nota.length>0 && Id_Set.length>0){Nota = Nota.replace(",",".");Nota = parseFloat(Nota);if(Nota>0 && Nota<=7){bool_nota=true;}else{bool_nota=false;}}else{bool_nota=false;}
                    if(bool_nota)
                    {
                        var btn = this;
                        
                        $(btn).attr('disabled', true);
                        $('#alerta_edita_'+id).html("");
                        $('#alerta_edita'+id).html(alert_info('Se esta guardando su configuración'));
                        $.post( "/tesis/profesor/save_edit_nota",{Id_alumno:id,Comentario:Comentario,Nota:Nota,Id_Set:Id_Set}, function( data ) 
                        { 
                            var bool =data.bool;
                            if(bool)
                            {                                
                                $(btn).attr('disabled', false);
                                $('#alerta_edita_'+id).html(alert_success('Configuración guardada exitosamente!'));
                                location.reload();                                
                            }
                            else
                            {
                                var msg = data.msg;
                                $('#alerta_edita_'+id).html(alert_error('No se ha guardado correctamente, Error='+msg));
                                $(btn).attr('disabled', false);
                            }
                            jsRemoveWindowLoad();      
                        },"json");
                    }
                    else
                    {
                        jsRemoveWindowLoad();  
                        $('#alerta_edita_'+id).html(alert_error('Revise los los valores ingresados antes de continuar'));
                    }  
                });
                
                $(".modal-body").on("click", ".edit_set_nota", function()
                {
                    var Id=$('input:hidden[name=Set_cha_Id]').val();

                    var cantidad_evaluaciones=$("#notas_setting_"+Id+" tr").length-2;
                    var type_element= new Array();
                    var date_element= new Array();
                    var id_element= new Array();
                    var count=0;
                    var bool_fecha=true;
                    var bool_vacio=true;
                    var today = new Date();
                    $('input[name^="id_notas_editar'+Id+'"]').each(function(index){
                        var val = $(this).val(); 
                        if(val.length > 0)
                        {
                            id_element.push(val); 
                        }                              
                    });  
                    $('input[name^="tipo_notas_editar'+Id+'"]').each(function(index){
                        var val = $(this).val(); 
                        $(this).css("border-color","");
                        if(val.length > 0)
                        {
                            type_element.push(val); 
                        }  
                        else
                        {                           
                            $(this).css("border-color","#e5603b");
                            bool_vacio = false;
                        }                              
                    });                    
                    $('input[name^="fecha_notas_editar'+Id+'"]').each(function(index){
                        var val = $(this).val();
                        $(this).css("border-color","");
                        if(val.length > 0)
                        {
                            var valSplit=val.split("/");                            
                            var dd =parseInt(valSplit['0']);
                            var mm = parseInt(valSplit['1'])-1; 
                            var yyyy = parseInt(valSplit['2']);
                            if(dd<10){dd='0'+dd;} 
                            if(mm<10){mm='0'+mm;} 
                            var InputDate= new Date(yyyy,mm,dd);
                            if(InputDate > today)
                            {
                                date_element.push(val);
                            }
                            else
                            {   
                                $(this).css("border-color","#e5603b");                         
                                bool_fecha= false;
                            }                      
                        }  
                        else
                        {
                           $(this).css("border-color","#e5603b");
                            bool_vacio = false;
                        }         
                    });
                    if(bool_vacio && bool_fecha)
                    {
                         var btn = this;
                         $(btn).attr('disabled', true);
                        $('#alerta_'+Id).html("");
                        $('#alerta_'+Id).html(alert_info('Se esta guardando su configuración'));
                        $.post( "/tesis/profesor/save_edit_setting_eva",{Id_Cha:Id,can_eva:cantidad_evaluaciones,fecha_array:date_element,tipo_array:type_element,id_array:id_element}, function( data ) 
                        { 
                            var bool =data.bool;
                            if(bool)
                            {                                
                                $(btn).attr('disabled', false);
                                $('#alerta_'+Id).html(alert_success('Configuración guardada exitosamente!'))
                                $('input[name^="tipo_notas'+Id+'"]').each(function(index){$(this).val('');});
                                $('input[name^="fecha_notas'+Id+'"]').each(function(index){$(this).val('');});  
                                setTimeout(function(){$('#mymodal'+Id+'notas').modal('hide'); $('#alerta_'+Id).html(""); },3000);
                                location.reload();  
                                
                            }
                            else
                            {
                                var msg = data.msg;
                                $('#alerta_'+Id).html(alert_error('No se ha guardado correctamente, Error='+msg))
                                $(btn).attr('disabled', false);
                            }
                        },"json");
                    }
                    else
                    {
                        $('#alerta_'+Id).html("");
                        var fecha_actual=today.getDate()+'/'+(today.getMonth()+1)+'/'+today.getFullYear();
                        if(!bool_vacio && !bool_fecha)
                        {
                            var texto='Revise los campos marcados, puede tener estos errores: <ul>';
                            texto+='<li> 1.- El campo está vacio</li>';
                            texto+='<li> 2.- La fecha ingresada debe ser posterior a la fecha actual '+fecha_actual+'</li><ul>';
                            var alerta=alert_error(texto);
                            $('#alerta_'+Id).html(alerta);
                        }
                        if(!bool_vacio && bool_fecha)
                        {
                            var alerta=alert_error('Debe completar todos los campos para continuar');
                            $('#alerta_'+Id).html(alerta);
                        }
                        if(bool_vacio && !bool_fecha)
                        {
                           
                            var alerta=alert_error('Las fechas ingresadas deben ser posteriores a la actual: '+fecha_actual);
                            $('#alerta_'+Id).html(alerta);
                        }
                    }  
                });
                function alert_error(mensaje)
                {
                    var alerta="";
                    alerta+='<div class="alert alert-danger" style="margin-bottom: -15px;">';
                    alerta+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    alerta+='<h4 style="color: white;"><i class="fa fa-ban"></i> <strong>Ups! Encontramos un error</strong></h4>';
                    alerta+='<p>'+mensaje+'</p>';
                    alerta+='</div>';
                    return alerta;
                }
                function alert_info(mensaje)
                {
                    var alerta="";
                    alerta+='<div class="alert alert-info" style="margin-bottom: -15px;">';
                    alerta+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    alerta+='<h4 style="color: white;"><i class="fa fa-info-circle"></i> <strong>Espere un momento</strong></h4>';
                    alerta+='<p>'+mensaje+'</p>';
                    alerta+='</div>';
                    return alerta;
                }
                function alert_success(mensaje)
                {
                    var alerta="";
                    alerta+='<div class="alert alert-success" style="margin-bottom: -15px;">';
                    alerta+='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    alerta+='<h4 style="color: white;"><i class="fa fa-check"></i> <strong>Todo un Exito!</strong></h4>';
                    alerta+='<p>'+mensaje+'</p>';
                    alerta+='</div>';
                    return alerta;
                }
            });
        </script>