<?=$pan_1?> 
<div id="modals"></div>
<div id="alertas"></div>
<script type="text/javascript">
    function arreglo()
    {
        $('select').css("color","black");
    } 
        $(document).ready(function()
        { 
            $('#tabla_alumnos_lista').dataTable();
            $('#tabla_apoderado_listado').dataTable();
            $('#tabla_reuniones_lista').dataTable();
            arreglo();
            $('#btn_new_reunion').click(function(){
                $(this).attr('disabled', true);
                var btn=this;
                $('#new_reunion').css("border-color",""); 
                $('#mask-time').css("border-color","");
                $('#new_reunion_motivo').css("border-color","");
                var reunion=$('#new_reunion').val();
                var hora=$('#mask-time').val();
                var motivo =$('#new_reunion_motivo').val();
                var today = new Date();
                if(reunion.length>0 && hora.length>0 && motivo.length>0)
                {
                    var valSplit=reunion.split("/");                            
                    var dd =parseInt(valSplit['0']);
                    var mm = parseInt(valSplit['1'])-1; 
                    var yyyy = parseInt(valSplit['2']);
                    var InputDate= new Date(yyyy,mm,dd);
                    if(InputDate > today)
                    {
                        $.post( "/tesis/profesor/save_reunion",{Fecha:reunion,Hora:hora,Motivo:motivo, Curso:<?=$My_Curso->Id?>}, function( data ) 
                        {
                            var bool= data.bool;
                            if(bool)
                            {
                                var tabla= data.tabla;
                                $('#tabla_reuniones').html(tabla);
                                $('#tabla_reuniones2').html(tabla);
                                $('#new_reunion').val('');
                                $('#mask-time').val('');
                                $('#new_reunion_motivo').val('');
                                $(btn).attr('disabled', false);
                                location.reload();
                            }
                            else
                            {
                                sweetAlert("Ups! Encontramos un error", data.msg, "error");
                                $('#new_reunion').css("border-color","#e5603b"); 
                                $('#mask-time').css("border-color","#e5603b");
                                $('#new_reunion_motivo').css("border-color","#e5603b"); 
                                $(btn).attr('disabled', false);
                            }
                        },"json");
                    }
                    else
                    {   
                        $('#new_reunion').css("border-color","#e5603b"); 
                        $('#mask-time').css("border-color","#e5603b");
                        $('#new_reunion_motivo').css("border-color","#e5603b"); 
                        $(btn).attr('disabled', false); 
                    }                   
                }
                else
                {
                    $('#new_reunion').css("border-color","#e5603b"); 
                    $('#mask-time').css("border-color","#e5603b");
                    $('#new_reunion_motivo').css("border-color","#e5603b");
                    $(btn).attr('disabled', false);
                }
                
            });
            $(".modal-footer").on("click", ".save_anotacion", function()
            {
                var id=$(this).attr('id');                             
                var comentario = $('textarea[name=comentario_anotacion'+id+']').val();
                if(comentario.length>0)
                {
                    var tipo = $('select[name=tipo_anotacion'+id+'] option:selected').val();  
                    $.post( "/tesis/profesor/create_anotacion",{Id_Alumno : id, comentario : comentario, tipo:tipo}, function( data ) 
                    { 
                        var bool =data.bool;
                        if(bool)
                        {
                            $.unblockUI;
                            $('#modal_anotaciones').modal('hide'); 
                            $('input:hidden[name=Id_Alumno]').val('');
                            $('textarea[name=comentario_anotacion]').val('');
                            $('select[name=tipo_anotacion] option:selected').val('');
                            location.reload();
                        }
                        else
                        {
                            $.unblockUI;
                            sweetAlert("Ups! Encontramos un error", data.msg, "error");
                        }
                     },"json");
                }
            });
            $(".del_re").on("click", function()
            {
                $.post( "/tesis/profesor/delete_reunion",{Id_Reunion:$(this).attr('id'),Id_Curso:<?=$My_Curso->Id?>}, function( data ) 
                {
                    var bool= data.bool;
                    if(bool)
                    {
                        var tabla= data.tabla;
                        $('#tabla_reuniones').html(tabla);
                        $('#tabla_reuniones2').html(tabla);
                         location.reload();
                    }
                    else
                    {
                        sweetAlert("Ups! Encontramos un error", data.msg, "error");
                    }
                },"json");
                 
            });
            $(".edit_re").on("click", function()
            {
                var id=$(this).attr('id');
                var reunion=$('#fecha_reunion_'+id).val();
                var hora=$('#hora_reunion_'+id).val();
                var motivo =$('#motivo_reunion_'+id).val();
                var today = new Date();
                if(reunion.length>0 && hora.length>0 && motivo.length>0)
                {
                    var valSplit=reunion.split("/");                            
                    var dd =parseInt(valSplit['0']);
                    var mm = parseInt(valSplit['1'])-1; 
                    var yyyy = parseInt(valSplit['2']);
                    var InputDate= new Date(yyyy,mm,dd);
                    if(today < InputDate)
                    {
                        $.post( "/tesis/profesor/edit_reunion",{Id:id,Fecha:reunion,Hora:hora,Motivo:motivo, Curso:<?=$My_Curso->Id?>}, function( data ) 
                        {
                            var bool= data.bool;
                            if(bool)
                            {
                                var tabla= data.tabla;
                                $('#tabla_reuniones').html(tabla);
                                $('#fecha_reunion_'+id).val(''); 
                                $('#hora_reunion_'+id).val(''); 
                                $('#motivo_reunion_'+id).val('');  
                                location.reload();
                            }
                            else
                            {
                                sweetAlert("Ups! Encontramos un error", data.msg, "error");
                                $('#fecha_reunion_'+id).css("border-color","#e5603b"); 
                                $('#hora_reunion_'+id).css("border-color","#e5603b");
                                $('#motivo_reunion_'+id).css("border-color","#e5603b"); 
                            }
                        },"json");
                    }
                    else
                    {    
                        $('#fecha_reunion_'+id).css("border-color","#e5603b"); 
                        $('#hora_reunion_'+id).css("border-color","#e5603b");
                        $('#motivo_reunion_'+id).css("border-color","#e5603b"); 
                    }                   
                }
                else
                {

                    $('#fecha_reunion_'+id).css("border-color","#e5603b"); 
                                $('#hora_reunion_'+id).css("border-color","#e5603b");
                                $('#motivo_reunion_'+id).css("border-color","#e5603b"); 
                } 
            });
            $(".opciones-alumno").on("click", ".ver-ficha", function()
            {
                jsShowWindowLoad("Estamos buscando");
                var id=$(this).attr('id'); 
                 $.post( "/tesis/profesor/show_ficha_alumno",{id:id, Curso:<?=$My_Curso->Id?>}, function( data ) 
                {
                            var bool= data.bool;
                            if(bool)
                            {
                                var tabla= data.tabla;
                                $('#modals').html(tabla);
                                $('#modal_alumno').modal('show'); 
                                jsRemoveWindowLoad();     
                            }
                            else
                            {
                                var tabla= data.tabla;
                                jsRemoveWindowLoad();     
                                alert(tabla);
                            }
                        },"json");
                 jsRemoveWindowLoad();     
                
            });
            $(".opciones-alumno").on("click", ".anotaciones", function()
            {
                var id=$(this).attr('id'); //anotaciones_alumno
                $.post( "/tesis/profesor/anotaciones_alumno",{id:id}, function( data ) 
                {
                            var bool= data.bool;
                            if(bool)
                            {
                                var tabla= data.tabla;
                                $('#modals').html(tabla);
                                $('#modal_alumno').modal('show'); 
                                jsRemoveWindowLoad();     
                            }
                            else
                            {
                                sweetAlert("Ups! Encontramos un error", data.msg, "error");                                
                                jsRemoveWindowLoad();     
                            }
                        },"json");
                 jsRemoveWindowLoad();  
            });
        });
  
</script>