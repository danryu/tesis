<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <title>Apple's Teacher</title>
    <link href="<?=$url?>css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="<?=$url?>img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script src="<?=$url?>lib/jquery/jquery-2.0.3.min.js"> </script>
    <script src="<?=$url?>lib/jquery-pjax/jquery.pjax.js"></script>
    <script src="<?=$url?>lib/backbone/underscore-min.js"></script>
    <script src="<?=$url?>js/settings.js"> </script>    
</head>
<body>
<div class="single-widget-container">
    <section class="widget login-widget">
        <header class="text-align-center">
            <h4>Bienvenido a Apple's Teacher</h4>
        </header>
        <center><h2>¿Has olvidado la contraseña?</h2></center>
        <form action="verificar_reset" method="POST">
        	Correo Electronico<br><input type="email" minlength="3" class="form-control" name="email" /><br>
        	Ingrese los último 4 numeros de su Telefono Celular<br><input class="form-control" minlength="4" maxlength="4" type="text" name="celular" /><br>
       		<input type="submit" class="btn btn-primary" style="margin-bottom:25px" value="Siguiente" />
        </form>


</div>        
    </section>
</div>
</body><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<!-- jquery and friends -->
<script src="<?=$url?>lib/jquery/jquery-2.0.3.min.js"> </script>
<script src="<?=$url?>lib/jquery-pjax/jquery.pjax.js"></script>


<!-- jquery plugins -->
<script src="<?=$url?>lib/jquery-maskedinput/jquery.maskedinput.js"></script>
<script src="<?=$url?>lib/parsley/parsley.js"> </script>
<script src="<?=$url?>lib/icheck.js/jquery.icheck.js"></script>
<script src="<?=$url?>lib/select2.js"></script>


<!--backbone and friends -->
<script src="<?=$url?>lib/backbone/underscore-min.js"></script>

<!-- bootstrap default plugins -->
<script src="<?=$url?>lib/bootstrap/transition.js"></script>
<script src="<?=$url?>lib/bootstrap/collapse.js"></script>
<script src="<?=$url?>lib/bootstrap/alert.js"></script>
<script src="<?=$url?>lib/bootstrap/tooltip.js"></script>
<script src="<?=$url?>lib/bootstrap/popover.js"></script>
<script src="<?=$url?>lib/bootstrap/button.js"></script>
<script src="<?=$url?>lib/bootstrap/dropdown.js"></script>
<script src="<?=$url?>lib/bootstrap/modal.js"></script>

<!-- bootstrap custom plugins -->
<script src="<?=$url?>lib/bootstrap-datepicker.js"></script>
<script src="<?=$url?>lib/bootstrap-select/bootstrap-select.js"></script>
<script src="<?=$url?>lib/wysihtml5/wysihtml5-0.3.0_rc2.js"></script>
<script src="<?=$url?>lib/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- basic application js-->
<script src="<?=$url?>js/app.js"></script>
<script src="<?=$url?>js/settings.js"></script>

<script src="<?=$url?>js/forms-validation.js"></script>
<script src="<?=$url?>js/forms-validation.js"></script>
<script>
        var texto =["Al ser su primera vez en el sistema debes completar el siguiente formulario, no te tomara mas de 5 minutos.",
       "Tu información se encuentra segura, solo tu tienes acceso a ella. ",
       "Los numeros telefonicos e imail que registres, solo son para avisarte de las novedades de tu colegio. ",
       "Bienvenido a Apples Teacher el sistema que te ayudará estar organizado y al tanto de las novedades"]

    
        for(j=0;j<4;j++)
        {
            msj_aut(texto[j],j+5);
        }
     function msj_aut(txt , n)
    {
        var _n=n*1000;
        setTimeout('document.getElementById("intro").innerHTML = "'+txt+'";', _n );
    }

</script>
</html>
