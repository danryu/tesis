        <?php if(isset($errormsg)){?>
                <div class="alert alert-danger">
                    <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                </div>
        <?php } ?>
        <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-graduation-cap"></i> Academica</a></li>
                            <li class="active"><i class="fa fa-file-text-o"></i> Asistencia</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                    <section class="widget" style="height:60px;">
                        <header>
                            <label for="country" class="control-label"> Selección de periodo Academico</label>
                        </header>
                        <div class="col-sm-5">
                            <?php
                                echo "<select required name='periodo' data-placeholder='Periodo'
                                    data-width='off'
                                    data-minimum-results-for-search='10'
                                    tabindex='-1'
                                    class='chzn-select select-block-level' id='default-select'>";
                                    /** Nombre de la variable de curso = $marca **/
                                /*foreach ($marca->result() as $key){
                                    echo "<option value='".$key->id."'>".$key->nombre."</option>";
                                }*/
                                echo "</select>";  
                            ?> 
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                    <section class="widget">
                        <header>
                            <h4>
                                <i class="fa fa-list-alt"></i>
                                Asistencia
                            </h4>
                        </header>
                        <div class="body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Asignatura</th>
                                    <th>Porcentaje Asignatura</th>
                                    <th>Porcentaje Alumno</th>
                                    <th>Estado</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Matemica</td>
                                    <td>15%</td>
                                    <td>10%</td>
                                    <td><span class="badge badge-warning">No va al día</span></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Lenguaje</td>
                                    <td>15%</td>
                                    <td>15%</td>
                                    <td><span class="badge badge-success">Al día</span></td>
                                </tr>
                                
                                </tbody>
                            </table>
                            
                        </div>
                    </section>
                </div>
            </div>
   