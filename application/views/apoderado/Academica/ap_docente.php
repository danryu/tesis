        <?php if(isset($errormsg)){?>
                <div class="alert alert-danger">
                    <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                </div>
        <?php } ?>
        <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-graduation-cap"></i> Academica</a></li>
                            <li class="active"><i class="fa fa-user"></i> Docentes</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                    <section class="widget" style="height:60px;">
                        <header>
                            <label for="country" class="control-label"> Selección de periodo Academico</label>
                        </header>
                        <div class="col-sm-5">
                            <?php
                                echo "<select required name='periodo' data-placeholder='Periodo'
                                    data-width='off'
                                    data-minimum-results-for-search='10'
                                    tabindex='-1'
                                    class='chzn-select select-block-level' id='default-select'>";
                                    /** Nombre de la variable de curso = $marca **/
                                /*foreach ($marca->result() as $key){
                                    echo "<option value='".$key->id."'>".$key->nombre."</option>";
                                }*/
                                echo "</select>";  
                            ?> 
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="widget pnlopc">
                        <header>
                            <h4 style="color:#FFFF;">
                                <i class="fa fa-cog"></i> Panel de Opciones
                            </h4>
                        </header>
                        <div class="body">
                            <div>                                
                                <button type="button" class="btn btn-warning hidden-phone-landscape">
                                    <i class="eicon-print"></i>
                                    Imprimir
                                </button>
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-file-pdf-o"></i>
                                    Exportar PDF
                                </button>
                                <button class="btn btn-inverse" onClick="location.reload();">
                                    <i class="fa fa-refresh"></i>
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                    <section class="widget">
                        <header>
                            <h4>
                                <i class="fa fa-list-alt"></i>
                                Docentes
                            </h4>
                        </header>
                        <div class="body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Correo Electronico</th>
                                    <th>Asignatura</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($mysql)){
                                        $num = 1;
                                        foreach($mysql as $k){
                                            echo "<tr>";
                                            echo "<td>".$num."</td>";
                                            echo "<td>".$k->first_name.' '.$k->last_name."</td>";
                                            echo "<td>".$k->email."</td>";
                                            echo "<td>".$k->materia."</td>";
                                            echo "</tr>";
                                        }
                                    } ?>
                                
                                
                                </tbody>
                            </table>
                            
                        </div>
                    </section>
                </div>
            </div>
   