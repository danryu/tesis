        <?=$content?>
        <script type="text/javascript">
        $(document).ready(function()
        {
            $(".idalumno").click(function()
            {     
                jsShowWindowLoad("Estamos buscando");
                $.post("<?=base_url()?>apoderado/academia_change",{id : this.id}, function(data) 
                {                
                    if(data.bool)
                    {  
                        $(".container").html(data.dashboard);    
                         jsRemoveWindowLoad();                       
                    }
                    else
                    {
                        sweetAlert("Ups! Encontramos un error", data.msg, "error");
                        setTimeout(function(){window.location.reload(1);}, 3000);
                        jsRemoveWindowLoad(); 
                    }
                },"json");               
            });            
            $('#calendar').fullCalendar(
            {
                  header: {
                   left: 'prev,next today',
                   center: 'title',
                   right: 'month,agendaWeek,agendaDay'
                  },
                  droppable: true
                });
        });
        </script>