        <?php if(isset($errormsg)){?>
                <div class="alert alert-danger">
                    <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                </div>
        <?php } ?>
        <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-graduation-cap"></i> Academica</a></li>
                            <li class="active"><i class="fa fa-bookmark-o"></i> Notas</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>
            
            <div class="row">
               <div class="col-md-12">
                    <section class="widget" style="height:60px;">
                        <header>
                            <label for="country" class="control-label"> Selección de periodo Academico</label>
                        </header>
                        <div class="col-sm-5">
                            <?php
                                echo "<select required name='periodo' data-placeholder='Periodo'
                                    data-width='off'
                                    data-minimum-results-for-search='10'
                                    tabindex='-1'
                                    class='chzn-select select-block-level' id='default-select'>";
                                    /** Nombre de la variable de curso = $marca **/
                                /*foreach ($marca->result() as $key){
                                    echo "<option value='".$key->id."'>".$key->nombre."</option>";
                                }*/
                                echo "</select>";  
                            ?> 
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="widget pnlopc">
                        <header>
                            <h4 style="color:#FFFF;">
                                <i class="fa fa-cog"></i> Panel de Opciones
                            </h4>
                        </header>
                        <div class="body">
                            <div>                                
                                <button type="button" class="btn btn-warning hidden-phone-landscape">
                                    <i class="eicon-print"></i>
                                    Imprimir
                                </button>
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-file-pdf-o"></i>
                                    Exportar PDF
                                </button>
                                <button class="btn btn-inverse" onClick="location.reload();">
                                    <i class="fa fa-refresh"></i>
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <header>
                            <h5>
                                <i class="fa fa-bookmark-o"></i>
                                Notas
                            </h5>
                        </header>
                        <div class="body">
                            <div class="panel-group" id="accordion2">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                            Matematica
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse in collapse" style="height: auto;">
                                        <div class="panel-body">
                                            <div class="body">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Evaluación</th>
                                                        <th>Ponderación</th>
                                                        <th>Fecha</th>
                                                        <th>Nota</th>
                                                        <th>Promedio Nota Curso</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Prueba Parcial 1</td>
                                                        <td>60%</td>
                                                        <td>20/05/2015</td>
                                                        <td>5.8</td>
                                                        <td>4.7</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Prueba Parcial 2</td>
                                                        <td>60%</td>
                                                        <td>20/05/2015</td>
                                                        <td>5.8</td>
                                                        <td>4.7</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Prueba Parcial 3</td>
                                                        <td>60%</td>
                                                        <td>20/05/2015</td>
                                                        <td>5.8</td>
                                                        <td>4.7</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Prueba Parcial 4</td>
                                                        <td>60%</td>
                                                        <td>20/05/2015</td>
                                                        <td>5.8</td>
                                                        <td>4.7</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="panel-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                            Lenguaje
                                        </a>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="panel-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                            Biologia
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>            
            </div>
   