<?php $this->load->helper('app'); $size=12;?>
    <div class="row">
        <div class="col-md-12">
            <section class="widget">
                <header>  
                    <h2 id='folder-title' class="folder-title"><i class="fa fa-inbox"></i> Bandeja de Entrada 
                                    <small>
                                        <?php   
                                            if(isset($n_msg))
                                            {
                                                if($n_msg===1 || $n_msg===0)
                                                {
                                                    if($n_msg===1) { echo 'Tienes 1 mensaje nuevo';}                                                                                                        
                                                }
                                                else
                                                {
                                                    echo 'Tienes '.$n_msg.' mensajes nuevos';
                                                }
                                            }
                                        ?>
                                    </small>
                                </h2>  
                </header>
                <div class="body">
                    <div id="mailbox-app" class="mailbox">
                        <div class="row">  
                            <?php 
                            if(isset($url))
                            {
                                $size='10';
                            ?>
                            <div class="col-sm-2">
                                <ul id="folders-list" class="mailbox-folders">
                              
                                    <?=btn_nuevo($url,base_url())?> 
                                  
                                </ul>
                            </div>  <?php }?>
                            <div class="col-sm-<?=$size?>">
                                                    
                                <div id="mailbox-content" class="mailbox-content">
                                <div class="body">
                                    <table id="datatable-table" class="table table-striped">
                                        <thead>
                                            <tr role="row">
                                                <th></th>
                                                <th>Asunto</th>
                                                <th>Mensaje</th>
                                                <th>Autor</th>                                             
                                                <th>Fecha</th>                                  
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($inbox_load)) {?>
                                            <?=inbox_list($inbox_load)?>
                                        <?php }?>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script type="text/javascript">
        $( document ).ready(function() {
            $("#datatable-table").on("click", ".click", function(){              
                var id=$(this).attr('id');
                $.post( "<?=base_url()?><?=$soy?>/change_see",{id : id}, function( data ) 
                {
                    var bool=data.bool;
                    if(bool){$('#'+id+' td:first').html('<i class="fa fa-envelope"></i>');}

                },"json");
            });
            <?php
                if(isset($id_msg))
                {
                    echo "$('#myModal".$id_msg."').modal('show');";
                }
            ?>

         });
    </script>