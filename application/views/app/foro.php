<div class="row">
        <div class="col-md-12">
            <h2 class="page-title">Foro <small></small></h2>
        </div>
    </div>
<div class="row">
    <?php
        $Id_foro=NULL;
        $Id_User_profesor=NULL;
        $Titulo_foro=NULL;
        if($profesor)
        {
            $m="8";
            $code='';
        }
        else{ $m="12";}
    ?>
    <div class="col-md-12">
            <section class="widget">
                <div class="body no-margin">
                    <?php                     
                    if(isset($first_foro))
                    {    
                        $state=$first_foro->Estado;                              
                        $nombre=preg_split("/[\s,]+/",$first_foro->first_name);
                        $apellido=preg_split("/[\s,]+/",$first_foro->last_name);
                        $name=$nombre[0].' '.$apellido[0]; 
                        if($first_foro->Estado){$estado=true;}else{$estado=false;}
                        $Id_foro=$first_foro->Id;
                        $Titulo_foro=$first_foro->Titulo;
                        $Id_User_profesor=$first_foro->Id_User;
                        ?>
                        <section class="search-result">                            
                            <header><h4><?=$Titulo_foro?></h4></header>
                            <div id="chat" class="chat">
                                <div id="chat-messages" class="chat-messages">
                                    <div class="chat-message">
                                        <div class="sender pull-left">
                                            <div class="icon">
                                                <img src="<?=base_url().'assets/bootstrap/'?>img/user.png" class="img-circle" alt="">
                                            </div>
                                            <div class="time">
                                            </div>
                                        </div>
                                        <div class="chat-message-body">
                                            <span class="arrow"></span>
                                            <div class="sender "><i class="eicon eicon-user"></i><a href="#"><?=$name?></a> - <i class="eicon eicon-clock"></i><?=$first_foro->Fecha?></div>
                                            <div class="text">
                                                <?=$first_foro->Comentario?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                            <?php
                        
                    }
                        
               ?>
                <div id="div_respuesta">
                    <?php 
                        
                                     
                    if(isset($respuestas_foro))
                    {
                        $dt="left";
                        foreach ($respuestas_foro as $q) 
                        {                                                                
                            $nombre=$q->first_name;
                            $nombre=preg_split("/[\s,]+/",$nombre);
                            $apellido=$q->last_name;
                            $apellido=preg_split("/[\s,]+/",$apellido);
                            $name=$nombre[0].' '.$apellido[0]; 
                            if($q->Id_User===$Id_User_profesor){$type_user="Profesor";}else{$type_user="Alumno";}
                            
                                $msn_dir="";
                                $img_dir=" pull-left";
                                $dt="left";
                                                    
                    ?>
                        <section class="search-result">                            
                            <header><h4>Re:<?=$Titulo_foro?></h4></header>
                            <div id="chat" class="chat">
                                <div id="chat-messages" class="chat-messages">
                                    <div class="chat-message">
                                        <div class="sender<?=$img_dir?>">
                                            <div class="icon">
                                                <img src="<?=base_url().'assets/bootstrap/'?>img/user.png" class="img-circle" alt="">
                                            </div>
                                            <div class="time">
                                           <?=$type_user?>
                                            </div>
                                        </div>
                                        <div class="chat-message-body<?=$msn_dir?>">
                                            <span class="arrow"></span>
                                            <div class="sender"><i class="eicon eicon-user"></i><a href="#"><?=$name?></a> - <i class="eicon eicon-clock"></i><?=$q->Fecha?></div>
                                            <div class="text">
                                                <?=$q->Comentario?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php 
                        }
                    }?>
                    </div>
                </div>
            </section>
    </div>  
<?php
    if(!$state)
    { 
        if($profesor)
        {

?>  
            <div class="col-md-<?=$m?>">        
                <section class="widget">
                    <header>
                        <h4><i class="fa fa-file-alt"></i> Responder: <small><?=$Titulo_foro?></small></h4>
                    </header>
                    <div class="body">                       
                                <fieldset>
                                    <div class="control-group">
                                        
                                        <div class="controls form-group">
                                            <div class="col-md-11">
                                                <textarea id="text" rows="5" name="text<?=$Id_foro?>" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success" id="responder<?=$Id_foro?>">Responder</button>
                                    <button type="reset" class="btn btn-default">Limpiar</button>
                                </div>                        
                    </div>
                </section>
            </div>
<?php   }
   
    }
    else
    {

?>  
        <div class="col-md-<?=$m?>">        
            <section class="widget">
                <header>
                    <h4><i class="fa fa-file-alt"></i> Responder: <small><?=$Titulo_foro?></small></h4>
                </header>
                <div class="body">                       
                            <fieldset>
                                <div class="control-group">
                                    
                                    <div class="controls form-group">
                                        <div class="col-md-11">
                                            <textarea id="text" rows="5" name="text<?=$Id_foro?>" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success" id="responder<?=$Id_foro?>">Responder</button>
                                <button type="reset" class="btn btn-default">Limpiar</button>
                            </div>                        
                </div>
            </section>
        </div>
<?php

    }
    if($profesor)
    {
        if($state)
        {
            $type_btn="danger";
            $msg_btn="Cerrar Foro";
            $_estado="Abierto";
        }
        else
        {
            $type_btn="success";
            $msg_btn="Abrir Foro";
            $_estado="Cerrado";
        }



        ?>
    <div class="col-md-4">
                <section class="widget">
                    <header>
                        <h4><i class="fa fa-cog"></i> Opciones <small>Estado actual:<?=$_estado?></small></h4>
                    </header>
                    <div class="body">
                       <div id="btn_state" class="col-ms-8">
                            
                                <button style="width:100%" id="as" type="button" class="btn btn-<?=$type_btn?> btn-lg change_state" data-placement="top" data-original-title=".btn .btn-info">
                                    &nbsp;<span><i class="fa fa-comments fa-lg"></i> <?=$msg_btn?></span>&nbsp;
                                </button>
                            </div>
                    </div>
                    <?php
                    if(isset($first_foro))
                    {    
                        $state=$first_foro->Estado;                              
                        $nombre=preg_split("/[\s,]+/",$first_foro->first_name);
                        $apellido=preg_split("/[\s,]+/",$first_foro->last_name);
                        $name=$nombre[0].' '.$apellido[0]; 
                        if($first_foro->Estado){$estado=true;}else{$estado=false;}
                        $Id_foro=$first_foro->Id;
                        $Titulo_foro=$first_foro->Titulo;
                        $Id_User_profesor=$first_foro->Id_User;
                    ?>
                    <div class="body">
                        <div id="btn_state" class="col-ms-6">
                            <button type="button" class="btn btn-primary btn-lg btn-block" 
                                data-toggle="modal" data-target="#modal_edit_foro" 
                                data-backdrop="false"> <i class="fa fa-edit"></i> Editar Foro
                            </button>                            
                            <div id="modal_edit_foro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">x</button>
                                            <h4 class="modal-title" id=""><i class="fa fa-edit"></i> Editar Foro</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h4>Titulo</h4>
                                            <div class="control-group">                                         
                                                <div class="controls form-group">
                                                    <div class="col-sm-8">
                                                        <input type="text" id="normal-field" name="foro_titulo_editar" class="form-control" placeholder="Escriba un titulo..." required value="<?=$Titulo_foro?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                             <br>
                                              <br>
                                            <h4>Comentario</h4>
                                            <div class="control-group">
                                                <div class="controls form-group">
                                                    <textarea rows="4" class="form-control" id="wysiwyg" name="foro_comentario_editar" required><?=$first_foro->Comentario?></textarea>
                                                </div>
                                            </div>                        
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="<?=$Id_foro?>" class="btn btn-success edit_foro_js" data-dismiss="modal"><i class="fa fa-edit"></i> Guardar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div id="btn_state" class="col-ms-6">
                            <button type="button" class="btn btn-danger btn-lg btn-block" 
                                data-toggle="modal" data-target="#modal_del_foro" 
                                data-backdrop="false"><i class="glyphicon glyphicon-trash"></i> Eliminar Foro
                            </button>                            
                            <div id="modal_del_foro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">x</button>
                                            <h4 class="modal-title" id=""><i class="glyphicon glyphicon-trash"></i> Eliminar Foro</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="control-group"> 
                                                <p style="padding-bottom:10px; border-bottom: 1px dotted #575757"><strong>
                                                <i class="glyphicon glyphicon-trash"></i> Eliminar foro:</strong></br>
                                                    Para eliminar el foro, haga click en el boton Eliminar de la parte inferior de esta ventana. 
                                                    Recuerde que eliminará de manera permanente los datos guardados.</p>
                                                <div class="alert alert-danger">
                                                    <p><strong><i class="fa fa-warning"></i> ¡Cuidado!</strong></br>
                                                        Está apunto de Eliminar de manera permanente información, la cual podría ser de importancia para el colegio, 
                                                        asegúrese de verificar los datos antes eliminar un registro. Al ejecutar está acción está tomando la total responsabilidad del acto.  
                                                    </div>
                                            </div>                    
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="<?=$Id_foro?>" class="btn btn-danger del_foro_js" data-dismiss="modal"><i class="fa fa-times-circle"></i> Eliminar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
                   <?php }?>
                </section>
    </div>

<?php
    }
?>
</div>
<script type="text/javascript">
            $(document).ready(function(){
                $('#div_respuesta').slimScroll({height: '250px'});
                $("#responder<?=$Id_foro?>").click(function()
                { 
                    jsShowWindowLoad("Estamos buscando");
                    var comentario=$("textarea[name=text<?=$Id_foro?>]").val().trim();
                    if(comentario === '')
                    {
                        jsRemoveWindowLoad(); 
                        sweetAlert("Ups! Encontramos un error", "La respuesta esta vacía", "error");
                    }
                    else
                    {     
                        jsRemoveWindowLoad();                 
                        var url="<?=$url?>";
                        $.post( "/tesis/"+url+"/save_comentario",{id_foro : <?=$Id_foro?>, comentario : comentario}, function( data ) 
                        {               
                            var bool=data.bool;
                            var respuestas=data.respuestas;            
                            if(bool)
                            {  
                                $("#div_respuesta").html(respuestas);
                                $("#text").data("wysihtml5").editor.clear(); 
                                sweetAlert("Registro Exitoso!","Respuesta guardada correctamente", "success");                            
                            }
                            else
                            {
                               sweetAlert("Ups! Encontramos un error",data.msg, "error"); 
                            }
                        },"json");
                    }     
                });
        <?php 
            if($profesor)
            {
        ?>      $(".change_state").click(function()
                {
                    $(this).attr('disabled', true);
                    var btn = this;
                    var Id=<?=$Id_foro?>;                    
                    $.post( "/tesis/profesor/change_state_foro",{Id:Id}, function( data ) 
                    { 
                        var bool =data.bool;
                        if(bool)
                        {
                            var btn = data.btn;
                            $(btn).attr('disabled', false);                                                          
                            $('#btn_state').html(btn);
                            location.reload();                                     
                        }
                        else
                        {
                            $(this).attr('disabled', false);
                        }
                    },"json");
                });
                $(".edit_foro_js").click(function() 
                {
                    $(this).attr('disabled', true);
                    var btn = this;
                    var id=<?=$Id_foro?>;     
                    jsShowWindowLoad("Estamos buscando");
                    var comentario=$("textarea[name=foro_comentario_editar]").val().trim();
                    var titulo=$("input[name=foro_titulo_editar]").val().trim();
                    var bl_ti=true;
                    var bl_co=true;
                    if(titulo.length > 0 )
                    {
                        bl_ti=false;                       
                    }
                    if(comentario.length > 0 )
                    {
                        bl_co=false;                       
                    }
                    if(bl_co || bl_ti)
                    {
                        if(bl_co && bl_ti)
                        {
                            sweetAlert("Ups! Encontramos un error", "Falta titulo y comentario para continuar", "error");
                        }
                        else
                        {
                            if(bl_ti)
                            {
                                sweetAlert("Ups! Encontramos un error", "Falta titulo de foro para continuar", "error");
                            }
                            else
                            {
                                sweetAlert("Ups! Encontramos un error", "Falta comentario de foro para continuar", "error");
                            }
                        }
                        jsRemoveWindowLoad();
                        $(btn).attr('disabled', false);
                        
                    } 
                    else
                    {
                        $.post( "/tesis/profesor/editar_foro",{Id:id,Comentario:comentario,Titulo:titulo}, function( data ) 
                        { 
                            var bool =data.bool;
                            if(bool)
                            {
                                location.reload();                                     
                            }
                            else
                            {
                                sweetAlert("Ups! Encontramos un error", data.msg, "error");
                                $(btn).attr('disabled', false);
                            }
                        },"json");
                    }              jsRemoveWindowLoad();
                    
                });
                $(".del_foro_js").click(function() 
                {
                    $(this).attr('disabled', true);
                    var btn = this;
                    var id=<?=$Id_foro?>;     
                    jsShowWindowLoad("Estamos buscando");
                    if(id<=0)
                    {
                        sweetAlert("Ups! Encontramos un error","Error en Identificador del Foro", "error");
                        $(this).attr('disabled', false);
                    } 
                    else
                    {
                        $.post( "/tesis/profesor/eliminar_foro",{Id:id}, function( data ) 
                        { 
                            var bool =data.bool;
                            if(bool)
                            {
                                location.reload();                                     
                            }
                            else
                            {
                                sweetAlert("Ups! Encontramos un error", data.msg, "error");
                                $(btn).attr('disabled', false);
                            }
                        },"json");
                    }              jsRemoveWindowLoad();
                    
                });

        <?php 
            }
        ?>

            });
</script>