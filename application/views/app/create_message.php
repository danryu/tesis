<?php if(!isset($rango)){$rango=NULL;}
      $list=NULL;
      if(isset($todos))
      {
        if(isset($todos['cursos']))
        {
            $list.=list_cursos($todos['cursos'],'profe');
            $list.=list_cursos($todos['cursos'],'jefe');
        }
        if(isset($todos['profes']))
        {
            //$list.=list_profes($todos['cursos'],'profe');
            //$list.=list_profes($todos['cursos'],'jefe');
        }
      }
      if(isset($alumnos))
      {
        $list.=list_cursos($alumnos,'profe');
      } 
      if(isset($apoderados))
      {                
        $list.=list_cursos($apoderados,'jefe');     
      } 
?>

    
    <div class="row">
        <div class="col-md-8">
            <section class="widget">
                <header>
                    <h4><i class="fa fa-file-alt"></i> Crear Mensaje <small>Crear un nuevo mensaje, seleccione destino y nivel del mensaje</small></h4>
                </header>
                <div class="body">
                    <form class="form-horizontal" method="post" action="/tesis/demo/create_message" novalidate="novalidate" data-validate="parsley">
                    <fieldset>
                                <div class="control-group">
                                    <label for="title" class="control-label">Titulo <span class="required">*</span></label>
                                    <div class="controls form-group">
                                        <input type="text" id="title" name="title" class="form-control" required="required">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="text" class="control-label">Contenido</label>
                                    <div class="controls form-group">
                                        <textarea id="text" rows="10" name="text" class="form-control"></textarea>
                                    </div>
                                </div>
                    </fieldset>
                    <div class="form-actions">
                        <center>
                            <button type="submit" class="btn btn-success">Enviar</button>
                            <button type="button" class="btn btn-default">Cancelar</button>
                        </center>
                    </div>
                </div>  
            </section>
        </div>
        <div class="col-md-4">
            <section class="widget">
                <header>
                    <h4><i class="fa fa-cog"></i> Configuraciónes</h4>
                </header>
                <div class="body">                   
                        <fieldset>
                            <header>
                                <h4><i class="fa fa-users"></i> Destino</h4>
                            </header>                          
                            <?=$list?>                            
                        </fieldset>
                        <fieldset>
                            <header>
                                <h4><i class="fa fa-signal"></i> Nivel</h4>
                            </header>
                            <div id="des"></div> 
                            <br>                          
                            <?=status($rango)?>
                            </fieldset></form>

                </div>
            </section>
        </div>
    </div>
                <?=status_script($rango)?>