<?php 
        

        $nombre=$myuser->first_name;
        $nombre=preg_split("/[\s,]+/",$nombre);
        $apellido=$myuser->last_name;
        $apellido=preg_split("/[\s,]+/",$apellido);
        $nombre=$nombre[0].' '.$apellido[0];
        $url=base_url().'assets/bootstrap/';
        if($myuser->sex)
        {
            $sexo="Masculino";
        }
        else
        {
            $sexo="Femenino";
        }
        $fecha=explode("-",$myuser->f_nacimiento);
        $fecha=$fecha[2].' / '.$fecha[1].' / '.$fecha[0];
        
        if(isset($twitter_name)){$nameTW='@'.$twitter_name;}else{$nameTW="";}
?>
<div class="row">
        <div class="col-md-12">
            <h2 class="page-title">Mi Perfil <small></small></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <section class="widget">
                <header>
                    <h4><i class="fa fa-user"></i> Perfil <small>de usuarios</small></h4>
                </header>
                <div class="body">
                    <form id="user-form" class="form-horizontal label-left"
                          novalidate="novalidate"
                          method="post"
                          data-validate="parsley">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="text-align-center">
                                    <img class="img-circle" src="<?=$url?>img/user.png" alt="64x64" style="height: 112px;">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h3 class="no-margin"><?=$nombre?></h3>
                                <address>
                                    <abbr title="Work email">e-mail:</abbr> <a href="mailto:<?=$myuser->email?>"><?=$myuser->email?></a><br>
                                    <abbr title="Work Phone">Celular:</abbr> <?=$myuser->cellphone?>
                                </address>
                            </div>
                        </div>
                        <fieldset>
                            <legend class="section">Información Personal</legend>
                            <?=input(array( 'label'=>'RUN' ,"value"=>$myuser->username,'disable'=>true,'style'=>$input, 'transparent'=>true))?>                            
                            <?=input(array( 'label'=>'Nombres' ,"value"=>$myuser->first_name, 'disable'=>true,'style'=>$input,'transparent'=>true))?>
                            <?=input(array( 'label'=>'Apellidos' ,"value"=>$myuser->last_name,'disable'=>true, 'style'=>$input,'transparent'=>true))?>
                            <?=input(array( 'label'=>'Fecha Nacimiento' ,"value"=>$fecha, 'disable'=>true,'style'=>$input,'transparent'=>true))?>
                            <?=input(array( 'label'=>'Sexo','size'=>8 ,"value"=>$sexo, 'disable'=>true,'style'=>$input,'transparent'=>true))?>
                        </fieldset>
                        <fieldset>
                            <legend class="section">Información Contacto</legend>
                            <?=input(array( 'label'=>'Celular' ,"value"=>$myuser->cellphone, 'disable'=>true,'style'=>$input,'transparent'=>true))?>
                            <?=input(array( 'label'=>'Teléfono Fijo' ,"value"=>$myuser->phone, 'disable'=>true,'style'=>$input, 'transparent'=>true))?>
                            <?=input(array( 'label'=>'Email' ,"value"=>$myuser->email,'disable'=>true,'style'=>$input, 'transparent'=>true))?>                            
                        </fieldset>
                    </form>
                </div>
            </section>
        </div>
        <div class="col-md-5">
            <section class="widget">
                <header>
                    <h4><i class="fa fa-exclamation-triangle"></i> Información Personal</h4>                    
                </header>
                <div class="body">
                    <form method="post">
                        <fieldset>
                            <p>Para realizar cambios de su información personal, debe solicitar directamente en las oficinas del establecimiento. Debido a que ésta se debe comprobar con documentos legales (Cedula de Identidad)</p>
                        </fieldset>
                    </form>
                </div>
            </section>
        </div>
        <div class="col-md-5">
            <section class="widget">
                <header>
                    <h4 id="name_twitter"><i class="fa fa-cogs"></i> Configuración de Twitter <?=$nameTW?></h4>                    
                </header>
                <div class="body">
                    <form method="post">
                        <fieldset>
                        <div id="btn_twitter">
                        <?php 
                            if($twitter_active)
                            {
                        ?>

                                <p align="center"> Si desea desasociar su cuenta twitter de su cuenta Apple's Teacher, precione "Eliminar Twitter de Cuenta"</p>
                                <div class="col-ms-8">                                
                                    <button style="width:100%" id="btn_delete_twitter" type="button" class="btn btn-default btn-lg" data-placement="top" data-original-title=".btn .btn-info">
                                        &nbsp;<span><i class="fa fa-twitter fa-lg"></i> Eliminar Twitter de Cuenta</span>&nbsp;
                                    </button>
                                </div>
                        <?php    
                            }
                            else
                            {
                                ?>
                                <p align="center">¿Desea asociar su cuenta twitter a su cuenta Apple's Teacher para recibir notificaciones?, precione "Registrar Twitter"</p>
                               
                                <div class="col-ms-8">
                                <a href="/tesis/twitter/auth">
                                    <button style="width:100%" type="button" class="btn btn-info btn-lg" data-placement="top" data-original-title=".btn .btn-info">
                                        &nbsp;<span><i class="fa fa-twitter fa-lg"></i> Registrar Twitter</span>&nbsp;
                                    </button>
                                    </a>
                                </div>

                                <?php
                            }
                        ?>
                        </div>   
                        </fieldset>
                    </form>
                </div>
            </section>
        </div>
        <?php 
    
            if(isset($Apoderado))
            {
                if($SMS)
                {
                    $mensaje='Desactivar notificaciones via SMS, ahora no recivirá mensajes al menos que sea necesario';
                    $btn='  <div class="col-ms-8">
                                <button style="width:100%" type="button" class="btn btn-default btn-lg" data-placement="top" data-original-title=".btn .btn-info">
                                    &nbsp;<span><i class="fa fa-mobile-phone fa-lg"></i> Desactivar SMS</span>&nbsp;
                                </button>
                            </div>';
                }
                else
                {
                    $mensaje='¿Desea recibir notifiaciones via SMS?';
                    $btn='  <div class="col-ms-8">
                                <button style="width:100%" type="button" class="btn btn-success btn-lg" data-placement="top" data-original-title=".btn .btn-info">
                                    &nbsp;<span><i class="fa fa-mobile-phone fa-lg"></i> Activar SMS</span>&nbsp;
                                </button>
                            </div>';
                    
                }
        ?>
                <div class="col-md-5">
                    <section class="widget">
                        <header>
                            <h4><i class="fa fa-cogs"></i> Configuración de SMS</h4>                    
                        </header>
                        <div class="body">
                            <form method="post">
                                <fieldset>
                                    <p align="center"><?=$mensaje?></p>
                                    <?=$btn?>
                                </fieldset>
                            </form>
                        </div>
                    </section>
                </div>
      <?php } ?>
        
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        <?php 
            if($twitter_active)
            {
        ?>
                $("#btn_delete_twitter").click(function()
                {
                    $(this).attr('disabled', true);
                    $.post( "/tesis/twitter/delete", function( data ) 
                    { 
                        var bool =data.bool;
                        if(bool)
                        {
                            var btn=data.btn;
                            $(this).attr('disabled', false);
                            $('#btn_twitter').html(btn);
                            $('#name_twitter').html('<i class="fa fa-cogs"></i> Configuración de Twitter ');
                            $('#estado_twitter').remove();
                                     
                        }
                        else
                        {
                            var msg = data.msg;
                            $(this).attr('disabled', false); 
                        }
                    },"json");
                });
        <?php 
            }        
            if(isset($apoderado))
            {
        ?>
                $("#btn_delete_twitter").click(function()
                {
                    $(this).attr('disabled', true);
                    $.post( "/tesis/twitter/delete", function( data ) 
                    { 
                        var bool =data.bool;
                        if(bool)
                        {
                            var btn=data.btn;
                            $(this).attr('disabled', false);
                            $('#btn_twitter').html(btn);
                            $('#name_twitter').html('<i class="fa fa-cogs"></i> Configuración de Twitter ');
                                     
                        }
                        else
                        {
                            var msg = data.msg;
                            $(this).attr('disabled', false);
                            alert("ERROR: "+msg);
                        }
                    },"json");
                });
        <?php 
            }
        ?>

    });
    </script>