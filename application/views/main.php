
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Administración <small>Plataforma Educativa</small></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="box">
                <!-- style="font-size:200% !important;" -->
                    <div class="big-text">
                        <i class="fa fa-user"></i> <?php echo $cant_all;?>

                    </div>
                    <!-- style="margin-top:40px;" -->
                    <div class="description">
                         Usuarios Registrados
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="box">
                    <div class="big-text">
                        <i class="fa fa-user"></i> <?php echo $cant_profesores; ?>
                    </div>
                    <div class="description">
                        Profesores Registrados                        
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="box">
                    <div class="big-text">
                        <i class="fa fa-user"></i> <?php echo $cant_alumnos; ?>
                    </div>
                    <div class="description">
                        Alumnos Registrados
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="box">
                    <div class="big-text">
                        <i class="fa fa-user"></i> <?php echo $cant_apoderados; ?>
                    </div>
                    <div class="description">
                        Apoderados Registrados
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-list-alt"></i>
                            Últimos cambios realizados
                        </h4>
                    </header>
                    <div class="body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Descripción</th>
                                <th>CRUD</th>
                                <th>Cambio realizado por</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(isset($log_log))
                            {
                                $count = 1;
                                foreach($log_log as $r)
                                {
                                    echo "<tr>";
                                    echo "<td>".$count."</td>";
                                    echo "<td>".$r->Descripcion."</td>";
                                    echo "<td>".$r->Tabla."</td>";
                                    //echo "<td></td>";
                                    echo "<td>".$r->Nombre.' '.$r->Apellido."</td>";
                                    $count++;
                                    echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                
                <section class="widget widget-tabs">
                    <header>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#stats" data-toggle="tab">Profesores</a>
                            </li>
                            <li>
                                <a href="#report" data-toggle="tab">Alumnos</a>
                            </li>
                            <li>
                                <a href="#dropdown1" data-toggle="tab">Apoderados</a>
                            </li>
                        </ul>
                    </header>
                    <div class="body tab-content">
                        <div id="stats" class="tab-pane active clearfix">
                            <h5 class="tab-header"><i class="fa fa-group"></i> Últimos Profesores Registrados</h5>
                            <ul class="news-list">
                            
                                <?php if(isset($profesor_cinco))
                            {
                                foreach($profesor_cinco as $k)
                                {
                                    echo '<li>';
                                    echo    '<img src="../assets/bootstrap/img/user.png" alt="" class="pull-left img-circle"/>';
                                    echo    '<div class="news-item-info">';
                                    echo        '<div class="name"><a href="profesor/Ficha/'.$k->Id.'">'.$k->first_name.' '.$k->last_name.'</a></div>';
                                    echo        '<div class="comment">'.$k->username;
                                    echo        '</div>';
                                    echo    '</div>';
                                    echo '</li>';
                                }
                            }
                                ?>
                            </ul>
                        </div>
                        <div id="report" class="tab-pane">
                            <h5 class="tab-header"><i class="fa fa-group"></i> Últimos Alumnos Registrados</h5>
                            <ul class="news-list news-list-no-hover">
                                <?php if(isset($alumno_cinco))
                            {
                                foreach($alumno_cinco as $k)
                                {
                                    echo '<li>';
                                    echo    '<img src="../assets/bootstrap/img/user.png" alt="" class="pull-left img-circle"/>';
                                    echo    '<div class="news-item-info">';
                                    echo        '<div class="name"><a href="alumno/Ficha/'.$k->id.'">'.$k->first_name.' '.$k->last_name.'</a></div>';
                                    echo        '<div class="comment">'.$k->username;
                                    echo        '</div>';
                                    echo    '</div>';
                                    echo '</li>';
                                }
                            }
                                ?>
                            </ul>
                        </div>
                        <div id="dropdown1" class="tab-pane">
                            <h5 class="tab-header"><i class="fa fa-group"></i> Últimos Apoderados Registrados</h5>
                            <ul class="news-list">
                                <?php if(isset($apoderado_cinco))
                            {
                                foreach($apoderado_cinco as $k)
                                {
                                    echo '<li>';
                                    echo    '<img src="../assets/bootstrap/img/user.png" alt="" class="pull-left img-circle"/>';
                                    echo    '<div class="news-item-info">';
                                    echo        '<div class="name"><a href="apoderado/Ficha/'.$k->id.'">'.$k->first_name.' '.$k->last_name.'</a></div>';
                                    echo        '<div class="comment">'.$k->username;
                                    echo        '</div>';
                                    echo    '</div>';
                                    echo '</li>';
                                }
                            }
                                ?>
                                
                            </ul>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
    