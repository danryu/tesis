            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="http://127.0.0.1/tesis/alumno"><i class="fa fa-home"></i> Inicio</a></li>
                            <li class="active"><i class="fa fa-user"></i> Mi Curso</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>
             <div class="col-md-8" style="margin-left:-14px">
                <section class="widget widget-tabs">
                    <?php if(isset($query_asg)){echo menu_materias($query_asg);} ?>
                    <div class="body tab-content">
                        <?php if(!isset($query_foros)){$query_foros=NULL;}?>
                        <?php if(!isset($query_notas)){$query_notas=NULL;}?>
                        <?php if(!isset($reglamento_notas)){$reglamento_notas=NULL;}?>
                        <?php if(!isset($tipos_evaluaciones)){$tipos_evaluaciones=NULL;}?>
                        <?php if(isset($query_asg)){echo tab_materia($query_asg,$query_foros,$query_notas,$reglamento_notas,$tipos_evaluaciones);} ?>
                         
                        
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                 <section class="widget widget-tabs">
                    <header>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#Profesores_tab" data-toggle="tab">Profesores</a>
                            </li>
                        </ul>
                    </header>
                    <div class="body tab-content">
                        <div id="profesores_tab" class="tab-pane active clearfix">
                           <?php if(isset($query_docentes)){echo list_docentes($query_docentes);}?>
                        </div>
                    </div>
                </section>
            </div>
        <script type="text/javascript">
            $(document).ready(function(){   
                
                

            });
        </script>