        <?php if(isset($errormsg)){?>
                <div class="alert alert-danger">
                    <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                </div>
        <?php } ?>
        <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-graduation-cap"></i> Academica</a></li>
                            <li class="active"><i class="fa fa-user"></i> Docentes</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>
           
            <div class="row">
               <div class="col-md-12">
                    <section class="widget">
                        <header>
                            <h4>
                                <i class="fa fa-list-alt"></i>
                                Docentes
                            </h4>
                        </header>
                        <div class="body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Correo Electronico</th>
                                    <th>Asignatura</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $code=NULL;
                                if(isset($docentes))
                                {
                                    
                                    $i=0;
                                    foreach ($docentes as $q) 
                                    {
                                        $i++;

                                        $nombre=$q->first_name;
                                        $nombre=preg_split("/[\s,]+/",$nombre);
                                        $apellido=$q->last_name;
                                        $apellido=preg_split("/[\s,]+/",$apellido);
                                        $name=$nombre[0].' '.$apellido[0]; 
                                        $code.='   <tr> <td>'.$i.'</td>
                                                    <td>'.$name.'</td>
                                                    <td>'.$q->email.'</td>
                                                    <td>'.$q->Nombre.'</td> </tr>';
                                    }
                                }
                                else
                                {
                                    if(isset($error_msg))
                                    {
                                        echo '<tr><td colspan="4"  align="center">'.$error_msg.'</td></tr>';
                                    }
                                    else
                                    {
                                        echo '<tr><td colspan="4" align="center"> No hay docentes asignados</td></tr>';
                                    }
                                }
                                 echo $code;
                                ?>
                                
                                
                                </tbody>
                            </table>
                            
                        </div>
                    </section>
                </div>
            </div>
   