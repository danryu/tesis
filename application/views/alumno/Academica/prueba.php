<div class="col-md-12 col-md-offset-0">
    <section class="widget">
        <header>
            <h5>
                <i class="fa fa-file-o"></i>
                Prueba Intranet Apple's Teacher
            </h5>
        </header>
        <div class="body">
            <div class="modal" style="position: relative; top: auto; right: auto; left: auto; bottom: auto; z-index: 1; display: block; overflow: hidden;">
                <div class="modal-dialog" style="width: auto; padding: 0;">
                    <div class="modal-content">
                    <form action="prueba" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title">Nombre_Ramo</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <div class="control-group">
                                    <label class="control-label">Pregunta 1</label>
                                    <div class="controls form-group">
                                        <label class="radio">
                                            <input type="radio" id="radio-4" class="iCheck" name="respuesta1" checked="checked" value=1>
                                            Respuesta 1</label>
                                        <label class="radio">
                                            <input type="radio" name="respuesta1" value=2 class="iCheck" id="radio-5">
                                            Respuesta 2</label>
                                        <label class="radio">
                                            <input type="radio" name="respuesta1" value=3 class="iCheck" id="radio-6">
                                            Respuesta 3</label>
                                        <label class="radio">
                                            <input type="radio" name="respuesta1" value=4 class="iCheck" id="radio-6">
                                            Respuesta 4</label>
                                        <label class="radio">
                                            <input type="radio" name="respuesta1" value=5 class="iCheck" id="radio-6">
                                            Respuesta 5</label>
                                    </div>
                                </div>
                            </p>
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" class="btn btn-primary">Enviar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
        </div>
    </section>
</div>
