        <?php
            $ci = &get_instance();
            $ci->load->model("alumno_model");
        ?>
        <?php if(isset($errormsg)){?>
                <div class="alert alert-danger">
                    <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                </div>
        <?php } ?>
        <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-graduation-cap"></i> Academica</a></li>
                            <li class="active"><i class="fa fa-calendar"></i> Horario</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>
            
            <!--<div class="row">
               <div class="col-md-12">
                    <section class="widget" style="height:60px;">
                        <header>
                            <label for="country" class="control-label"> Selección de periodo Academico</label>
                        </header>
                        <div class="col-sm-5">
                            <?php
                                /*echo "<select required name='periodo' data-placeholder='Periodo'
                                    data-width='off'
                                    data-minimum-results-for-search='10'
                                    tabindex='-1'
                                    class='chzn-select select-block-level' id='default-select'>";
                                    /** Nombre de la variable de curso = $marca **/
                                /*foreach ($marca->result() as $key){
                                    echo "<option value='".$key->id."'>".$key->nombre."</option>";
                                }*/
                                /*echo "</select>";*/ 
                            ?> 
                        </div>
                    </section>
                </div>
            </div>-->
            <div class="row">
                <div class="col-md-12">
                    <section class="widget pnlopc">
                        <header>
                            <h4 style="color:#FFFF;">
                                <i class="fa fa-cog"></i> Panel de Opciones
                            </h4>
                        </header>
                        <div class="body">
                            <div>                                
                                <button type="button" onClick="window.print()" class="btn btn-warning hidden-phone-landscape">
                                    <i class="eicon-print"></i>
                                    Imprimir
                                </button>
                                <!--<button type="button" class="btn btn-success">
                                    <i class="fa fa-file-pdf-o"></i>
                                    Exportar PDF
                                </button>-->
                                <button class="btn btn-inverse" onClick="location.reload();">
                                    <i class="fa fa-refresh"></i>
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-calendar"></i>
                            Horario
                        </h4>
                        
                    </header>
                    <div class="body">
                        <table width="100%" border="1"> 
                          <tr> 
                            <td width="14%">hora</td> 
                            <td width="14%">Lunes</td> 
                            <td width="14%">Martes</td> 
                            <td width="14%">Miercoles</td> 
                            <td width="14%">Jueves</td> 
                            <td width="14%">Viernes</td> 
                            <td width="14%">Sabado</td> 
                          </tr> 
                          <tr> 
                            <td height="30" width="14%">08:30-09:15</td> 
                            <td colspan="6" rowspan="6"  height="35"> 
                            <?php 
                           $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
                           $horas = array('08:30','09:15','10:15','11:00','12:00','12:45');
                            echo "<table width='100%' border='1'>"; 
                                for ($i = 1; $i <= 6; $i++) { 
                                    echo "<tr width='100' height='30'>"; 
                                    for ($j = 1; $j <= 6; $j++) { 
                                        echo "<td width='14%'>";
                                        $hor = date('H:i',strtotime($horas[$i-1]));
                                        foreach($horario as $k){
                                            $fecha_i = $k->Hora_Inicio;
                                            $hor2 = date('H:i',strtotime($fecha_i));
                                             if($k->Dia == $dias[$j-1] && $hor2 == $hor){
                                                 echo $k->Nombre;   
                                             }                                                                                    
                                        } 
                                    echo "</td>"; 
                                    } 
                                    echo "</tr>"; 
                                } 
                            echo "</table>"; 
                            ?> 
                            </td> 
                          </tr> 
                          <?php 
                          foreach($bloques as $i){
                            $hi = date('H:i',strtotime($i->Hora_Inicio));
                            $hf = date('H:i',strtotime($i->Hora_Fin));
                            echo "<tr> 
                                    <td height='30'>".$hi."-".$hf."</td> 
                                  </tr>";
                              }
                          ?>                          
                        </table>
                    </div>
                </section>
            </div>
            
        </div>
