        <?php if(isset($errormsg)){?>
                <div class="alert alert-danger">
                    <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                </div>
        <?php } ?>
        <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-graduation-cap"></i> Academica</a></li>
                            <li class="active"><i class="fa fa-thumbs-up"></i> Hoja de Vida</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>
            <!--<div class="row">
               <div class="col-md-12">
                    <section class="widget" style="height:60px;">
                        <header>
                            <label for="country" class="control-label"> Selección de periodo Academico</label>
                        </header>
                        <div class="col-sm-5">
                            <?php
                                /*echo "<select required name='periodo' data-placeholder='Periodo'
                                    data-width='off'
                                    data-minimum-results-for-search='10'
                                    tabindex='-1'
                                    class='chzn-select select-block-level' id='default-select'>";
                                    /** Nombre de la variable de curso = $marca **/
                                /*foreach ($marca->result() as $key){
                                    echo "<option value='".$key->id."'>".$key->nombre."</option>";
                                }*/
                                //echo "</select>";  
                            ?> 
                        </div>
                    </section>
                </div>
            </div>-->
            <div class="row">
               <div class="col-md-12">
                    <section class="widget">
                        <header>
                            <h4>
                                <i class="fa fa-list-alt"></i>
                                Hoja de Vida
                            </h4>
                        </header>
                        <div class="body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Fecha</th>
                                    <th>Asignatura</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $code=null;
                                if(isset($Hoja_Vida))
                                {
                                    $i=0;
                                    foreach ($Hoja_Vida as $q) 
                                    {
                                        if($q->Tipo == 0 || $q->Tipo == 1)
                                        {
                                            if($q->Tipo)
                                            {
                                                $Tipo='<span class="label label-success">Positiva</span>';
                                            }
                                            else
                                            {
                                                $Tipo='<span class="label label-important">Negativa</span>';
                                            }
                                        }
                                        else
                                        {
                                            $Tipo='<span class="label label-important">Error Sistema</span>';
                                        }
                                        $i++;
                                        $code.=' <tr>
                                                    <td>'.$i.'</td>
                                                    <td>'.$q->Date.'</td>
                                                    <th>Matematicas</td>
                                                    <td>'.$Tipo.'</td>
                                                    <td>'.$q->Descripcion.'</td>
                                                </tr>';
                                    }
                                } ?>
                                
                                    <?=$code?>
                                </tbody>
                            </table>
                            
                        </div>
                    </section>
                </div>
            </div>
   