
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-title">Alumno <small>Plataforma educativa</small></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                 <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-bell"></i>
                            Alertas
                        </h4>
                        <div class="actions">
                            <button class="btn btn-transparent btn-xs">Mostrar Todos <i class="fa fa-arrow-down"></i></button>
                        </div>
                    </header>
                    <div class="body">
                        <table class="table table-striped no-margin sources-table">
                            <thead>
                            <tr>
                                <th class="source-col-header">Tipo</th>
                                <th>Mensaje</th>
                                <th>Fecha</th>
                            </tr>
                            </thead>
                            <tbody>
                                <td>Reunión de apoderados 4ta A</td>
                                <td>Comienza en pocos días</td>
                                <td>01/04/2015</td>
                                <!-- llenar con información de alertas -->
                            </tbody>
                        </table>
                    </div>
                </section>
                <section class="widget">
                    <div class="row">
            <div class="col-md-12">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-calendar"></i>
                            Horario
                        </h4>
                        <div class="actions">
                            <button id="today" type="button" class="btn btn-sm btn-warning">
                                Hoy
                            </button>
                            <div id="calendar-switcher" class="btn-group" data-toggle="buttons">
                                <label class="btn btn-sm btn-success active" data-toggle-class="btn-success" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="view" value="month"> Mes
                                </label>
                                <label class="btn btn-sm btn-default" data-toggle-class="btn-success" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="view" value="agendaWeek"> Semana
                                </label>
                                <label class="btn btn-sm btn-default" data-toggle-class="btn-success" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="view" value="agendaDay"> Día
                                </label>
                            </div>
                        </div>
                    </header>
                    <div class="body">
                        <div id="calendar" class="calendar"> </div>
                    </div>
                </section>
            </div>
            
        </div>
                </section>
                
            </div>
            <div class="col-md-4">
                <section class="widget large">
                    <header>
                        <h4>
                            <i class="eicon-share"></i>
                            Notificaciones
                        </h4>
                        <div class="actions">
                            <button class="btn btn-transparent btn-xs">Mostrar Todas <i class="fa fa-arrow-down"></i></button>
                        </div>
                    </header>
                    <div class="body">
                        <div id="feed" class="feed">
                            <div class="wrapper">
                                <div class="vertical-line"></div>
                                <section class="feed-item">
                                    <div class="icon pull-left">
                                        <i class="fa fa-comment"></i>
                                    </div>
                                    <div class="feed-item-body">
                                        <div class="text">
                                            <a href="#">John Doe</a> commented on <a href="#">What Makes Good Code Good</a>.
                                        </div>
                                        <div class="time pull-left">
                                            3 h
                                        </div>
                                    </div>
                                </section>
                                <section class="feed-item">
                                    <div class="icon pull-left">
                                        <i class="fa fa-check color-green"></i>
                                    </div>
                                    <div class="feed-item-body">
                                        <div class="text">
                                            <a href="#">Merge request #42</a> has been approved by <a href="#">Jessica Lori</a>.
                                        </div>
                                        <div class="time pull-left">
                                            10 h
                                        </div>
                                    </div>
                                </section>
                                <section class="feed-item">
                                    <div class="icon pull-left">
                                        <!--<img src="img/14.jpg" class="img-circle" alt="">-->
                                    </div>
                                    <div class="feed-item-body">
                                        <div class="text">
                                            New user <a href="#">Greg Wilson</a> registered.
                                        </div>
                                        <div class="time pull-left">
                                            Today
                                        </div>
                                    </div>
                                </section>
                                <section class="feed-item">
                                    <div class="icon pull-left">
                                        <i class="fa fa-bolt color-orange"></i>
                                    </div>
                                    <div class="feed-item-body">
                                        <div class="text">
                                            Server fail level raises above normal. <a href="#">See logs</a> for details.
                                        </div>
                                        <div class="time pull-left">
                                            Yesterday
                                        </div>
                                    </div>
                                </section>
                                <section class="feed-item">
                                    <div class="icon pull-left">
                                        <i class="eicon-database"></i>
                                    </div>
                                    <div class="feed-item-body">
                                        <div class="text">
                                            <a href="#">Database usage report</a> is ready.
                                        </div>
                                        <div class="time pull-left">
                                            Yesterday
                                        </div>
                                    </div>
                                </section>
                                <section class="feed-item">
                                    <div class="icon pull-left">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="feed-item-body">
                                        <div class="text">
                                            <a href="#">Order #233985</a> needs additional processing.
                                        </div>
                                        <div class="time pull-left">
                                            Wednesday
                                        </div>
                                    </div>
                                </section>
                                <section class="feed-item">
                                    <div class="icon pull-left">
                                        <i class="fa fa-arrow-down"></i>
                                    </div>
                                    <div class="feed-item-body">
                                        <div class="text">
                                            <a href="#">Load more...</a>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
                
                
            </div>
            <div class="col-md-4" >
                <section class="widget large" style="height:335px">
                    <header>
                        <h4>
                            <i class="fa fa-twitter"></i>
                            Twitter
                        </h4>
                        
                    </header>
                    <div class="body">
                        <a class="twitter-timeline" href="https://twitter.com/danryu2" data-widget-id="582331613499011072">Tweets por el @danryu2.</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

                    </div>
                </section>
                
                
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                var evento = new Object();
                evento.title='evento1_Jueves';
                evento.start='2015-06-05';
                evento.end  ='2015-06-07';
                var evento_add = new Array();
                evento_add[0]=evento;
               $('#calendar').fullCalendar("addEventSource",evento_add);
               $('#calendar').fullCalendar('rerenderEvents');
            });
        </script>
    