            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Asignación</a></li>
                            <li><a href="../curso_alumno"><i class="fa fa-user-plus"></i> Horario</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-12">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="../Edit" method="post">
                            <fieldset>
                                <legend class="section">Crear Asociación</legend>
                                <div class="col-sm-7">
                                    <?=input(array('value'=>$q->Temporada,'name'=>'temporada','label'=>'Temporada'))?>
                                    <?=input(array('value'=>$q->Año,'extra'=>'min="2015" max="2100"' ,'type'=>'number','name'=>'agno','label'=>'Año'))?>
                                    <div class="control-group">
                                        <label class="control-label" for="normal-field">Día</label>
                                        <div class="controls form-group">
                                            <div class="col-sm-8">
                                                <select required name='dia' data-placeholder='Ingrese un día...'
                                                data-width='off'
                                                data-minimum-results-for-search='10'
                                                tabindex='-1'
                                                class='chzn-select select-block-level' id='default-select' required>
                                                    <option value=""></option>
                                                    <?php
                                                    if($q->Dia == "Lunes"){
                                                        echo '<option selected value="Lunes">Lunes</option>';
                                                    }
                                                    else{
                                                        echo '<option value="Lunes">Lunes</option>';    
                                                    }
                                                    if($q->Dia == "Martes"){
                                                        echo '<option selected value="Martes">Martes</option>';
                                                    }
                                                    else{
                                                        echo '<option value="Martes">Martes</option>';    
                                                    }
                                                    if($q->Dia == "Miercoles"){
                                                        echo '<option selected value="Miercoles">Miercoles</option>';
                                                    }
                                                    else{
                                                        echo '<option value="Miercoles">Miercoles</option>';    
                                                    }
                                                    if($q->Dia == "Jueves"){
                                                        echo '<option selected value="Jueves">Jueves</option>';
                                                    }
                                                    else{
                                                        echo '<option value="Jueves">Jueves</option>';    
                                                    }
                                                    if($q->Dia == "Viernes"){
                                                        echo '<option selected value="Viernes">Viernes</option>';
                                                    }
                                                    else{
                                                        echo '<option value="Viernes">Viernes</option>';    
                                                    }
                                                    if($q->Dia == "Sabado"){
                                                        echo '<option selected value="Sabado">Sabado</option>';
                                                    }
                                                    else{
                                                        echo '<option value="Sabado">Sabado</option>';    
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>                                   
                                    <div class="control-group">
                                        <label class="control-label" for="normal-field">Bloque</label>
                                        <div class="controls form-group">
                                            <div class="col-sm-8">
                                                <select required name='id_bloque' data-placeholder='Bloque'
                                                data-width='off'
                                                data-minimum-results-for-search='10'
                                                tabindex='-1'
                                                class='chzn-select select-block-level' id='default-select' required>
                                                <option value=""></option>
                                                <?php /* Nombre de la variable de curso = $marca **/
                                                //  Textos completos    Id Hora_Inicio  Hora_Fin
                                            foreach ($all_bloque as $k){
                                                if($q->Bloques_Id == $k->Id){
                                                    echo "<option selected value='".$k->Id."'>".$k->Hora_Inicio.' - '.$k->Hora_Fin."</option>";
                                                }
                                                else{
                                                    echo "<option value='".$k->Id."'>".$k->Hora_Inicio.' - '.$k->Hora_Fin."</option>";    
                                                }
                                            }
                                            echo "</select>";  
                                        ?> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="normal-field">Curso / Asignatura</label>
                                        <div class="controls form-group">
                                            <div class="col-sm-8">
                                        <select required name='id_curso_asignatura' data-placeholder='Curso / Asignatura'
                                                data-width='off'
                                                data-minimum-results-for-search='10'
                                                tabindex='-1'
                                                class='chzn-select select-block-level' id='default-select' required>
                                                <option value=""></option>
                                                <?php /** Nombre de la variable de curso = $marca **/
                                            foreach ($all_curso_asignatura as $e){
                                                if($q->Curso_has_Asignatura_Id == $e->Id){
                                                    echo "<option selected value='".$e->Id."'>".$e->Grado.'° '.$e->Letra.' - '.$e->Nombre." (".$e->Tipo.")"."</option>";
                                                }
                                                else{
                                                    echo "<option value='".$e->Id."'>".$e->Grado.'° '.$e->Letra.' - '.$e->Nombre." (".$e->Tipo.")"."</option>";    
                                                }
                                            }
                                            echo "</select>";  
                                        ?> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                    </div>
                </section>
            </div>
            
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                        <?php if(isset($errormsg)){ ?>
                        <div class="alert alert-danger">
                            <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                        </div>
                        <?php } ?>
                    </center>
                                         </form>
                </div>

            </div>
