            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración</a></li>
                            <li><a href="../reunion"><i class="fa fa-calendar"></i> Reunión</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-12">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" method="post">
                            <fieldset>
                                <legend class="section">Datos de Reunión</legend>
                                <div class="col-sm-7">                              
                                    <?=input(array( 'name'=>'fecha_reunion','label'=>'Fecha de Reunión','datepicker'=> true,'id'=>'in-place-date'))?>

                                    <div class="control-group">
                                        <label class="control-label" for="normal-field">Curso</label>
                                        <div class="controls form-group">
                                            <div class="col-sm-8">
                                        <?php
                                            echo "<select required name='id_curso' data-placeholder='Curso'
                                                data-width='off'
                                                data-minimum-results-for-search='10'
                                                tabindex='-1'
                                                class='chzn-select select-block-level' id='default-select'>";
                                                /** Nombre de la variable de curso = $marca **/
                                            /*foreach ($marca->result() as $key){
                                                echo "<option value='".$key->id."'>".$key->nombre."</option>";
                                            }*/
                                            echo "</select>";  
                                        ?> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </section>
            </div>
            
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                    </center>
                                         </form>
                </div>
            </div>
