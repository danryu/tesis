        <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración</a></li>
                            <li class="active"><i class="fa fa-calendar"></i> Reunión</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>
           
            <div class="row" style="margin-top:30px;">
                <div class="col-md-12">
                    <section class="widget pnlopc">
                        <header>
                            <h4 style="color:#FFFF;">
                                <i class="fa fa-cog"></i> Panel de Opciones
                            </h4>
                        </header>
                        <div class="body">
                            <div>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" data-backdrop="false">
                                    <i class="fa fa-plus-square"></i>
                                    Agendar Reunión
                                </button>
                                <button class="btn btn-inverse" onClick="location.reload();">
                                    <i class="fa fa-refresh"></i>
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <div class="body">
                            <table id="datatable-table" class="table table-striped">
                                <?php 
                                    echo "<thead>
                                            <tr>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                            <th>Motivo</th>          
                                            <th class=\"no-sort hidden-phone-landscape sorting_disabled\"></th>
                                            </tr>
                                            </thead>";
                                    echo    "<tbody>
                                            ";
                                    if(isset($reuniones_all_profesor)){
                                        foreach($reuniones_all_profesor as $k){
                                            echo "<tr>";
                                            echo "<td>".$k->Fecha."</td>";
                                            echo "<td>".$k->Hora."</td>";
                                            echo "<td>".$k->Motivo."</td>"; ?>
                                            <td><?php echo "<div class=\"btn-group\">
                                            <button title=\"\" data-original-title=\"\" class=\"btn btn-success dropdown-toggle\" data-toggle=\"dropdown\">
                                                &nbsp; <i class=\"fa fa-gear\"></i> Acciones &nbsp;
                                                <i class=\"fa fa-caret-down\"></i>
                                            </button>
                                            <ul class=\"dropdown-menu\">
                                                <li><a href=\"#\" data-toggle=\"modal\" data-target=\"#myModalEdit".$k->Id."\" data-backdrop=\"false\"><i class=\"fa fa-pencil-square-o\"></i> Editar</a></li>
                                                <li><a href=\"#\" data-toggle=\"modal\" data-target=\"#myModax".$k->Id."\" data-backdrop=\"false\"><i class=\"glyphicon glyphicon-trash\"></i> Eliminar</a></li>
                                            </ul>
                                        </div></td>";
                                        ?>
                                        </div></td></tr> 
                                    <?php 
                                        $fecha_get = $k->Fecha;
                                        $explode = explode('-',$fecha_get);
                                        $año = $explode[0];
                                        $mes = $explode[1];
                                        $dia = $explode[2];
                                        $fecha_format = $dia.'/'.$mes.'/'.$año;
                                        echo '<div id="myModalEdit'.$k->Id.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">x</button>
                                                            <h4 class="modal-title" id="myModalLabel2"><i class="fa fa-plus-circle"></i> Editar Reunión</h4>
                                                        </div>

                                                        <form class="form-horizontal" method="post" action="reunion/Edit/'.$k->Id.'">
                                                        <div class="modal-body">
                                                            <p style="padding-bottom:10px; border-bottom: 1px dotted #575757"><strong><i class="eicon-window"></i> Formulario de Edición:</strong></br>
                                                            Cambie aquí información para modificar el registro. Recuerde completar todos los campos que se le 
                                                                solicitan para obtener una mejor experiencia de usuario con Apples Teacher.</p>

                                                        <div>
                                                            <div class="control-group">
                                                                <label class="control-label " for="tooltip-enabled">Fecha*
                                                                </label>
                                                                <div class="controls form-group">
                                                                    <input style="width:250px;" value="'.$fecha_format.'" width="250" required tabindex="2" name="fecha_reunion" type="text" class="form-control date-picker" placeholder="">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="control-group">
                                                                <label class="control-label " for="tooltip-enabled">Hora*
                                                                </label>
                                                                <div class="controls form-group">
                                                                    <input style="width:250px;" value="'.$k->Hora.'" width="250" required tabindex="2" name="hora" type="time" class="form-control" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="control-group">
                                                                <label class="control-label" for="normal-field">Asunto*
                                                                    </label>
                                                                <div class="controls form-group">
                                                                    <div class="col-sm-8">
                                                                        <input name="asunto" value="'.$k->Motivo.'" required maxlength="140" tabindex="3" type="text" id="normal-field" class="form-control" placeholder="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                    
                                                                  
                                                                    
                                                                </div>
                                                            </div>
                                                        
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Registrar</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancelar</button>
                                                            </div>
                                                        </form>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>"';
                                        echo '<div id="myModax'.$k->Id.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">x</button>
                                                            <h4 class="modal-title" id="myModalLabel3"><i class="glyphicon glyphicon-trash"></i> Eliminar Asociación de Horario</h4>
                                                        </div>

                                                        <form class="form-horizontal" method="post" action="reunion/Delete/'.$k->Id.'">
                                                            <div class="modal-body">
                                                                <p style="padding-bottom:10px; border-bottom: 1px dotted #575757"><strong><i class="glyphicon glyphicon-trash"></i> Eliminar un Registro:</strong></br>
                                                                Para eliminar el registro seleccionado, haga click en el boton Eliminar de la parte inferior de esta ventana. 
                                                                Recuerde que eliminará de manera permanente los datos guardados.</p>

                                                                <div class="alert alert-danger">
                                                                    <p><strong><i class="fa fa-warning"></i> ¡Cuidado!</strong></br>
                                                                        Está apunto de Eliminar de manera permanente información, la cual podría ser de importancia para el colegio, 
                                                                        asegúrese de verificar los datos antes eliminar un registro. Al ejecutar está acción está tomando la total responsabilidad del acto.  
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Eliminar</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancelar</button>
                                                            </div>
                                                        </form>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>';
                    
                                            echo "</tr>";
                                    }}
                                    echo    "</tbody>";
                                    ?>

                            </table>
                        </div>
                    </section>
                </div>
            </div>
            <!--
##############################
   MODAL REGISTRO COMUNICADO
##############################
-->
<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title" id="myModalLabel2"><i class="fa fa-plus-circle"></i> Registrar Reunión</h4>
            </div>

            <form class="form-horizontal" method="post" action="reunion/Add">
            <div class="modal-body">
                <p style="padding-bottom:10px; border-bottom: 1px dotted #575757"><strong><i class="eicon-window"></i> Formulario de Registro:</strong></br>
                Agregue aquí información para un nuevo registro. Recuerde completar todos los campos que se le 
                    solicitan para obtener una mejor experiencia de usuario con Apple's Teacher.</p>

            <div>
                <div class="control-group">
                    <label class="control-label " for="tooltip-enabled">Fecha*
                    </label>
                    <div class="controls form-group">
                        <input style="width:250px;" width="250" required tabindex="2" name="fecha_reunion" type="text" class="form-control date-picker" placeholder="">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label " for="tooltip-enabled">Hora*
                    </label>
                    <div class="controls form-group">
                        <input style="width:250px;" width="250" required tabindex="2" name="hora" type="time" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="normal-field">Asunto*
                        </label>
                    <div class="controls form-group">
                        <div class="col-sm-8">
                            <input name="asunto" required maxlength="140" tabindex="3" type="text" id="normal-field" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
                        
                      
                        
                    </div>
                </div>
            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Registrar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancelar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
