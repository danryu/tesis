            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Asignación</a></li>
                            <li><a href="../reunion"><i class="fa fa-user-plus"></i> Profesor con Asignatura</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-12">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="../profesor_asignatura/Add" method="post">
                            <fieldset>
                                <legend class="section">Crear Asociación</legend>
                                <div class="col-sm-7">                              
                                    <div class="control-group">
                                        <label class="control-label" for="normal-field">Profesor</label>
                                        <div class="controls form-group">
                                            <div class="col-sm-8">
                                        <?php
                                            echo "<select required name='id_profesor' data-placeholder='Profesor'
                                                data-width='off'
                                                data-minimum-results-for-search='10'
                                                tabindex='-1'
                                                class='chzn-select select-block-level' id='default-select'>";
                                                /** Nombre de la variable de curso = $marca **/
                                            foreach ($all_profesor as $key){
                                                echo "<option value='".$key->Id."'>".$key->first_name.' '.$key->last_name."</option>";
                                            }
                                            echo "</select>";  
                                        ?> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="normal-field">Asignatura</label>
                                        <div class="controls form-group">
                                            <div class="col-sm-8">
                                        <?php
                                            echo "<select required name='id_asignatura' data-placeholder='Asignatura'
                                                data-width='off'
                                                data-minimum-results-for-search='10'
                                                tabindex='-1'
                                                class='chzn-select select-block-level' id='default-select'>";
                                                /** Nombre de la variable de curso = $marca **/
                                            foreach ($all_asignatura as $e){
                                                echo "<option value='".$e->Id."'>".$e->Nombre.' ('.$e->Grado.'° '.$e->Letra.' ['.$e->Tipo."])</option>";
                                            }
                                            echo "</select>";  
                                        ?> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                    </div>
                </section>
            </div>
            
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                        <?php if(isset($errormsg)){ ?>
                        <div class="alert alert-danger">
                            <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                        </div>
                        <?php } ?>
                    </center>
                                         </form>
                </div>
            </div>
