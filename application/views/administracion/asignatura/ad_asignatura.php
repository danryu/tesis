        <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-graduation-cap"></i> Educacional</a></li>
                            <li class="active"><i class="fa fa-plus-square"></i> Asignatura </li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>
           
            <div class="row" style="margin-top:30px;">
                <div class="col-md-12">
                    <section class="widget pnlopc">
                        <header>
                            <h4 style="color:#FFFF;">
                                <i class="fa fa-cog"></i> Panel de Opciones
                            </h4>
                        </header>
                        <div class="body">
                            <div>
                                <button type="button" class="btn btn-primary" onClick="location.href='asignatura/Add'">
                                    <i class="fa fa-plus-square"></i>
                                    Crear Asignatura
                                </button>
                                <button onclick="window.print()" type="button" class="btn btn-warning hidden-phone-landscape">
                                    <i class="eicon-print"></i>
                                    Imprimir
                                </button>
                                <!--<button type="button" class="btn btn-success">
                                    <i class="fa fa-file-pdf-o"></i>
                                    Exportar PDF
                                </button>-->
                                <button class="btn btn-inverse" onClick="location.reload();">
                                    <i class="fa fa-refresh"></i>
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <div class="body">
                            <table id="datatable-table" class="table table-striped">
                                <?php 
                                    echo "<thead>
                                            <tr>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th>Horas Totales</th>   
                                            <th class=\"no-sort hidden-phone-landscape sorting_disabled\"></th>
                                            </tr>
                                            </thead>";
                                    echo    "<tbody>";
                                    if(isset($mysql)){
                                    foreach($mysql as $q){
                                        ?>        
                                    <td class="sorting_1"><?= $q->Nombre?></td> 
                                    <td><?= $q->Descripcion?></td> 
                                    <td><?= $q->hora_total?></td>
                                    <td><?php echo "<div class=\"btn-group\">
                                            <button title=\"\" data-original-title=\"\" class=\"btn btn-success dropdown-toggle\" data-toggle=\"dropdown\">
                                                &nbsp; <i class=\"fa fa-gear\"></i> Acciones &nbsp;
                                                <i class=\"fa fa-caret-down\"></i>
                                            </button>
                                            <ul class=\"dropdown-menu\">
                                                <li><a href='asignatura/Ficha/$q->Id'><i class='fa fa-eye'></i> Ver Ficha</a></li>
                                                <li><a href=\"asignatura/Edit/$q->Id\"><i class=\"fa fa-pencil-square-o\"></i> Editar</a></li>
                                                <li><a href=\"#\" data-toggle=\"modal\" data-target=\"#myModax".$q->Id."\" data-backdrop=\"false\"><i class=\"glyphicon glyphicon-trash\"></i> Eliminar</a></li>
                                                
                                                
                                            </ul>
                                        </div></td>";
                                        ?>
                                        </div></td></tr> 
                                    <?php 
                echo '<div id="myModax'.$q->Id.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">x</button>
                                                            <h4 class="modal-title" id="myModalLabel3"><i class="glyphicon glyphicon-trash"></i> Eliminar Asignatura</h4>
                                                        </div>

                                                        <form class="form-horizontal" method="post" action="asignatura/Delete/'.$q->Id.'">
                                                            <div class="modal-body">
                                                                <p style="padding-bottom:10px; border-bottom: 1px dotted #575757"><strong><i class="glyphicon glyphicon-trash"></i> Eliminar un Registro:</strong></br>
                                                                Para eliminar el registro seleccionado, haga click en el boton Eliminar de la parte inferior de esta ventana. 
                                                                Recuerde que eliminará de manera permanente los datos guardados.</p>

                                                                <div class="alert alert-danger">
                                                                    <p><strong><i class="fa fa-warning"></i> ¡Cuidado!</strong></br>
                                                                        Está apunto de Eliminar de manera permanente información, la cual podría ser de importancia para el colegio, 
                                                                        asegúrese de verificar los datos antes eliminar un registro. Al ejecutar está acción está tomando la total responsabilidad del acto.  
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Eliminar</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancelar</button>
                                                            </div>
                                                        </form>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div>';
                    ?>
                                    <?php }}   
                                    echo    "</tbody>";
                                    ?>

                            </table>
                        </div>
                    </section>
                    <?php if(isset($errormsg)){ ?>
                        <div class="alert alert-danger">
                            <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                        </div>
                        <?php } ?>
                </div>
            </div>
   