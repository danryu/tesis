            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-graduation-cap"></i> Educacional</a></li>
                            <li><a href="../curso"><i class="fa fa-plus-square"></i> Asignatura</a></li>
                            <li class="active">Ficha</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-12">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Ficha de Asignatura
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" method="post">
                            <fieldset>
                                <legend class="section">Crear Asigntura</legend>
                                <div class="col-sm-7">
                                    <?=input(array('disable'=>true,'style'=>$input,'transparent'=>true, 'name'=>'nombre','label'=>'Nombre','value'=>$q->Nombre))?>
                                    <?=input(array('disable'=>true,'style'=>$input,'transparent'=>true, 'name'=>'descripcion','label'=>'descripción','value'=>$q->Descripcion))?>
                                    <?=input(array( 'disable'=>true,'style'=>$input,'transparent'=>true,'name'=>'hora_total','label'=>'Horas Totales','value'=>$q->hora_total))?>                            
                                    
                                </div>
                            </fieldset>
                    </div>
                </section>
            </div>
            
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                        <?php if(isset($errormsg)){ ?>
                        <div class="alert alert-danger">
                            <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                        </div>
                        <?php } ?>
                    </center>
                </form>
                </div>
            </div>