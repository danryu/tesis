            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="../"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración de Usuarios</a></li>
                            <li><a href="../apoderado"><i class="glyphicon glyphicon-user"></i> Apoderados</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="<?=$Id?>" method="post" name="form1" onSubmit="javascript:return Rut(document.form1.rut.value)">
                            <fieldset>
                                <legend class="section">Datos Apoderado</legend>
                                <?=input(array( 'name'=>'nombre','label'=>'Nombres','help'=>'Ingrese ambos nombres','value'=>$q->Nombre))?>
                                <?=input(array( 'name'=>'apellido','label'=>'Apellidos','value'=>$q->Apellido))?>
                                <?=input(array( 'name'=>'rut','label'=>'RUN','value'=>$q->Rut))?>
                                <?=input(array( 'name'=>'fnacimiento','label'=>'Fecha de Nacimiento','datepicker'=> true,'id'=>'in-place-date','value'=>$q->Fnacimiento))?>
                                <?=input(array( 'name'=>'t_fijo','label'=>'Tel. Fijo','value'=>$q->Telefono_fijo))?>
                                <?=input(array( 'name'=>'t_celular','label'=>'Tel. Celular','value'=>$q->Telefono_celular))?>
                                <?=input(array( 'name'=>'correo','label'=>'E-mail','value'=>$q->Email))?>
                                
                            </fieldset>
                    </div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="widget">   
                     <header>
                        <h4>
                            <i class=""></i>&nbsp;
                        </h4>
                    </header>                
                    <div class="body">
                        <div class="form-horizontal">
                            <fieldset>                                
                                <legend class="section">Otros datos Apoderado</legend>
                                <?=input(array( 'name'=>'comuna','label'=>'Comuna','required'=>TRUE,'value'=>$q->Comuna))?>
                                <?=input(array( 'name'=>'domicilio','label'=>'Domicilio','value'=>$q->Domicilio))?>
                                <?php if($q->Wsp){$wsp_check=true;}else{$wsp_check=false;}?>
                                <?=switches(array( 'name'=>'wsp','label'=>'Whatsapp <i class="fa fa-whatsapp"></i>','checked'=>$wsp_check))?>
                                <?php if($q->Sms){$sms_check=true;}else{$sms_check=false;}?>
                                <?=switches(array( 'name'=>'sms','label'=>'SMS','checked'=>$sms_check,'value'=>$q->Sms))?>
                                <legend class="section">Seguridad</legend>
                                <div class="alert alert-info">
                                    <strong><i class="fa fa-info-circle"></i> Contraseña generada automaticamente</strong><br> Por favor cambiar al iniciar sesion por primera vez.
                                </div>
                                <?=helptext("Si desea cambiar contraseña por una automatica, cambie el switch")?>
                                <?=switches(array( 'name'=>'cambiar_pass','label'=>'Cambiar contraseña'))?>
                                <?=input(array( 'name'=>'password','label'=>'Contraseña','disable'=>true,'transparent'=>true, 'value'=>$password))?>
                            </fieldset>
                        </div>                       
                    </div>
                </section>
            </div>
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="submit"class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                        <?php if(isset($errormsg)){?>
                        <div class="alert alert-danger">
                            <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                        </div>
                        <?php } ?>

                    </center>

                    </form>
                </div>
            </div>