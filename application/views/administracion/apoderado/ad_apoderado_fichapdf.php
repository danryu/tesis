<?php
require_once(FCPATH."application/third_party/dompdf/dompdf_config.inc.php");
$html =       
'
<html>
<body>
<div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="../"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración de Usuarios</a></li>
                            <li><a href="../apoderado"><i class="glyphicon glyphicon-user"></i> Apoderados</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="../Edit" method="post">
                            <fieldset>
                                <legend class="section">Datos Apoderado</legend>'.
                                input(array("transparent"=>true, "disable"=>true, "name"=>"nombre","label"=>"Nombres","help"=>"Ingrese ambos nombres","value"=>$q->first_name)).
                                input(array("transparent"=>true, "disable"=>true, "name"=>"apellido","label"=>"Apellidos","value"=>$q->last_name)).
                                input(array("transparent"=>true, "disable"=>true, "name"=>"rut","label"=>"RUN","value"=>$q->username));
   
                                
                                    if($q->sex == 1){
                                        $html=$html.input(array("transparent"=>true, "disable"=>true,"value"=>"Femenino","label"=>"Sexo"));
                                    }
                                    if($q->sex == 2){ 
                                        $html=$html.input(array("transparent"=>true, "disable"=>true,"value"=>"Masculino","label"=>"Sexo"));
                                    }
                                    if($q->sex != 1 && $q->sex != 2){ 
                                        $html=$html.input(array("transparent"=>true, "disable"=>true,"value"=>"Sin especificar","label"=>"Sexo"));
                                    }                                            
                          

                                $html=$html.input(array("transparent"=>true, "disable"=>true, "name"=>"fnacimiento","label"=>"Fecha de Nacimiento","value"=>$fecha,"datepicker"=> true,"id"=>"in-place-date")).
                                input(array("transparent"=>true, "disable"=>true, "name"=>"t_fijo","label"=>"Tel. Fijo","value"=>$q->phone)).
                                input(array("transparent"=>true, "disable"=>true, "name"=>"t_celular","label"=>"Tel. Celular","help"=>"Solo para demostrar","value"=>$q->cellphone)).
                                input(array( "transparent"=>true, "disable"=>true,"name"=>"correo","label"=>"E-mail","value"=>$q->email)).'
                                
                            </fieldset>
                    </div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="widget">   
                     <header>
                        <h4>
                            <i class=""></i>&nbsp;
                        </h4>
                    </header>                
                    <div class="body">
                        <div class="form-horizontal">
                            <fieldset>                                
                                <legend class="section">Otros datos Apoderado</legend>                            
                                '.comunas(array("transparent"=>true, "disable"=>true,"comuna"=>$comunas,"id_selected"=>$p->Comuna_id)).
                                input(array("transparent"=>true, "disable"=>true, "name"=>"domicilio","label"=>"Domicilio", "required"=>TRUE, "value"=>$p->Domicilio)).'
                                <div class="control-group">
                                <label class="control-label">Servicios</label>
                                    <div class="controls form-group">';
	
                                            if($p->Sms == 0){
                                                $html=$html.'<label class="checkbox"><input type="checkbox" name="sms" id="checkbox1" class="iCheck"> SMS</label>';
                                            }
                                            if($p->Sms == 1){
                                                $html=$html.'<label class="checkbox"><input type="checkbox" name="sms" id="checkbox1" checked="checked" class="iCheck"> SMS</label>';
                                            }
                                            if($p->Wsp == 0){
                                                $html=$html.'<label class="checkbox"><input type="checkbox" name="wsp" id="checkbox2" class="iCheck"> Whatsapp </label>';
                                            }
                                            if($p->Wsp == 1){
                                                $html=$html.'<label class="checkbox"><input type="checkbox" name="wsp" id="checkbox2" checked="checked" class="iCheck"> Whatsapp </label>';    
                                            }
                                    $html=$html.'</div>                             
                                </div>                                
                            </fieldset>
                        </div>                       
                    </div>
                </section>
            </div>
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Volver</button>
                            </div>
                        </div>
                        <?php if(isset($errormsg)){?>
                        <div class="alert alert-danger">
                            <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                        </div>
                        <?php } ?>

                    </center>

                    </form>
                </div>
            </div>
            </body>
            </html>'; 
$dompdf = new DOMPDF();
$dompdf->load_html($html);
$dompdf->render();
$dompdf->stream("apoderado.pdf");
