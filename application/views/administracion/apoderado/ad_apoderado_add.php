            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="../"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración de Usuarios</a></li>
                            <li><a href="../apoderado"><i class="glyphicon glyphicon-user"></i> Apoderados</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="../apoderado/Add" method="post" name="form1" onSubmit="javascript:return Rut(document.form1.rut.value)">
                            <fieldset>
                                <legend class="section">Datos Apoderado</legend>
                                <?=input(array( 'name'=>'nombre','label'=>'Nombres','help'=>'Ingrese ambos nombres'))?>
                                <?=input(array( 'name'=>'apellido','label'=>'Apellidos'))?>
                                <?=input(array( 'name'=>'rut','label'=>'RUN'))?>
                                <?=sexo()?>
                                <?=input(array( 'name'=>'fnacimiento','label'=>'Fecha de Nacimiento','datepicker'=> true,'id'=>'in-place-date'))?>
                                <?=input(array( 'name'=>'t_fijo','label'=>'Tel. Fijo'))?>
                                <?=input(array( 'name'=>'t_celular','label'=>'Tel. Celular','maxlength'=>'8','extra'=>'minlength="8" '))?>
                                <?=input(array( 'type'=>'email','name'=>'correo','label'=>'E-mail'))?>
                            </fieldset>
                    </div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="widget">   
                     <header>
                        <h4>
                            <i class=""></i>&nbsp;
                        </h4>
                    </header>                
                    <div class="body">
                        <div class="form-horizontal">
                            <fieldset>                                
                                <legend class="section">Otros datos Apoderado</legend>                                
                                <?=comunas(array('comuna'=>$comunas))?>
                                <?=input(array( 'name'=>'domicilio','label'=>'Domicilio', 'required'=>TRUE))?>
                                <div class="control-group">
                                <label class="control-label">Servicios</label>
                                    <div class="controls form-group">
                                        <label class="checkbox"><input type="checkbox" name="sms" id="checkbox1" checked="checked" class="iCheck"> SMS</label>
                                        <label class="checkbox"><input type="checkbox" name="wsp" id="checkbox2" class="iCheck"> Whatsapp </label>
                                        <input type="hidden" name="sms1" id="sms1" />
                                        <input type="hidden" name="wsp1" id="wsp1" />
                                    </div>  
                                    <?php //echo form_checkbox(['name' => 'sms', 'value' => 1]); ?>                              
                                </div>                                
                            </fieldset>
                        </div>                       
                    </div>
                </section>
            </div>
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="submit"class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                        <?php if(isset($errormsg)){?>
                        <div class="alert alert-danger">
                            <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                        </div>
                        <?php } ?>

                    </center>

                    </form>
                </div>
            </div>

            <script>
                var sms = document.getElementById("checkbox1").checked;
                var wsp = document.getElementById("checkbox2").checked;
                if(sms){ 
                    document.getElementById("sms1").value = 1; 
                }
                else{ document.getElementById("sms1").value = 0; }
                if(wsp){ document.getElementById("wsp1").value = 1; }
                else{ document.getElementById("wsp1").value = 0; }
            </script>