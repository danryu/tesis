            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración de Usuarios</a></li>
                            <li><a href="#"><i class="glyphicon glyphicon-user"></i> Alumnos</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="../Edit" method="post">
                            <fieldset>
                                <legend class="section">Datos Alumno</legend>
                                <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'nombre','label'=>'Nombres','help'=>'Ingrese ambos nombres', 'value'=>$q->first_name))?>
                                <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'apellido','label'=>'Apellidos','value'=>$q->last_name))?>
                                <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'rut','label'=>'RUN','value'=>$q->username))?>
                                <?php
                                //MAX Agrega un SEXO modificable
                                    if($q->sex == 1){ ?> 
                                        <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Femenino','label'=>'Sexo'))?>
                                   <?php }
                                    if($q->sex == 2){ ?>
                                        <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Masculino','label'=>'Sexo'))?>
                                   <?php }
                                    if($q->sex != 1 && $q->sex != 2){ ?>
                                        <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Sin especificar','label'=>'Sexo'))?>
                                   <?php }                                            
                                ?>
                                       
                                <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'f_nacimiento','value'=>$fecha,'label'=>'Fecha de Nacimiento','datepicker'=> true,'id'=>'in-place-date'))?>
                                <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'t_fijo','label'=>'Tel. Fijo','value'=>$q->phone))?>
                                <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'t_celular','label'=>'Tel. Celular','help'=>'Solo para demostrar','value'=>$q->cellphone))?>
                                <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'correo','label'=>'E-mail','value'=>$q->email))?>
                                <legend class="section">Otros datos Alumno</legend>
                                <?php 
                                if($q->Tipo_Sangre == "Tipo A"){ ?>
                                    <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Tipo A','label'=>'Tipo de Sangre'))?>
                               <?php }
                                if($q->Tipo_Sangre == "Tipo B"){ ?>
                                    <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Tipo B','label'=>'Tipo de Sangre'))?>
                                <?php }
                                if($q->Tipo_Sangre == "Tipo AB"){ ?>
                                    <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Tipo AB','label'=>'Tipo de Sangre'))?>
                                <?php }
                                if($q->Tipo_Sangre == "Tipo O"){
                                    ?>
                                    <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Tipo O','label'=>'Tipo de Sangre'))?>
                                    <?php
                                }
                                if($q->Tipo_Sangre != "Tipo A" && $q->Tipo_Sangre != "Tipo B" && $q->Tipo_Sangre != "Tipo AB" && $q->Tipo_Sangre != "Tipo O"){
                                    ?>
                                    <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Sin especificar','label'=>'Tipo de Sangre'))?>
                                    <?php 
                                }
                                ?>      
                                    
                                <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'alergias','label'=>'Alergias','value'=>$q->Alergias))?>

                            </fieldset>
                       </div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="widget">   
                     <header>
                        <h4>
                            <i class=""></i>&nbsp;
                        </h4>
                    </header>                
                    <div class="body">
                        <div class="form-horizontal">
                            <fieldset>                                
                                <legend class="section">Datos Apoderado</legend>
                               
                                            <?php 
                                                foreach ($apoderados_all as $i) 
                                                {
                                                    if($i->Id == $q->Apoderado_Id){ ?>
                                                        <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'nombre','label'=>'Nombres','help'=>'Ingrese ambos nombres', 'value'=>$i->first_name))?>
                                                        <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'apellido','label'=>'Apellidos','value'=>$i->last_name))?>
                                                        <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'rut','label'=>'RUN','value'=>$i->username))?>
                                                        <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'t_fijo','label'=>'Tel. Fijo','value'=>$i->phone))?>
                                                        <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'t_celular','label'=>'Tel. Celular','help'=>'Solo para demostrar','value'=>$i->cellphone))?>
                                                        <?=input(array( 'transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'correo','label'=>'E-mail','value'=>$i->email))?>
                                
                                <?php                        
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                            </fieldset>
                        </div>                       
                    </div>
                </section>
            </div>
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Volver</button>
                            </div>
                        </div>
                        <?php if(isset($errormsg)){ ?>
                        <div class="alert alert-danger">
                            <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                        </div>
                        <?php } ?>
                    </center>
                    </form>
                </div>
            </div>