            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración de Usuarios</a></li>
                            <li><a href="#"><i class="glyphicon glyphicon-user"></i> Alumnos</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="../Edit" method="post" name="form1" onSubmit="javascript:return Rut(document.form1.rut.value)">
                            <fieldset>
                                <legend class="section">Datos Alumno</legend>
                                <?=input(array( 'name'=>'nombre','label'=>'Nombres','help'=>'Ingrese ambos nombres', 'value'=>$q->first_name))?>
                                <?=input(array( 'name'=>'apellido','label'=>'Apellidos','value'=>$q->last_name))?>
                                <?=input(array( 'name'=>'rut','label'=>'RUN','value'=>$q->username))?>
                                <div class="control-group">
                                <label class="control-label" for="simple-colored">Sexo</label>
                                    <div class="controls form-group">
                                        <select class="selectpicker" name="sexo" data-style="btn-default"  id="simple-colored">

                                        <?php
                                        //MAX Agrega un SEXO modificable
                                            if($q->sex == 1){ 
                                                echo   '<option value="0">Seleccione...</option>
                                                        <option value="1" selected>Femenino</option>
                                                        <option value="2">Masculino</option>';
                                            }
                                            if($q->sex == 2){
                                                echo '<option value="0">Seleccione...</option>
                                                    <option value="1">Femenino</option>
                                                    <option value="2" selected>Masculino</option>';
                                            }
                                            if($q->sex != 1  && $q->sex != 2){
                                                echo '<option value="0" selected>Seleccione...</option>
                                                    <option value="1">Femenino</option>
                                                    <option value="2">Masculino</option>';
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <?=input(array( 'name'=>'f_nacimiento','value'=>$fecha,'label'=>'Fecha de Nacimiento','datepicker'=> true,'id'=>'in-place-date'))?>
                                <?=input(array( 'name'=>'t_fijo','label'=>'Tel. Fijo','value'=>$q->phone))?>
                                <?=input(array( 'name'=>'t_celular','label'=>'Tel. Celular','maxlength'=>'8','extra'=>'minlength="8" ','help'=>'Solo para demostrar','value'=>$q->cellphone))?>
                                <?=input(array( 'type'=>'email','name'=>'correo','label'=>'E-mail','value'=>$q->email))?>
                                <legend class="section">Otros datos Alumno</legend>
                                <div class="control-group">
                                <label class="control-label" for="simple-colored">Tipo de Sangre</label>
                                <div class="controls form-group">
                                    <select class="selectpicker"  name="tipo_sangre" data-style="btn-default" tabindex="-1" id="simple-colored">
                                        <?php 
                                        if($q->Tipo_Sangre == "Tipo A"){ 
                                            echo '<option value="Tipo A selected">Tipo A</option>';
                                            echo '<option value="Tipo B">Tipo B</option>';
                                            echo '<option value="Tipo AB">Tipo AB</option>';
                                            echo '<option value="Tipo O">Tipo O</option>';
                                        }
                                        if($q->Tipo_Sangre == "Tipo B"){
                                            echo '<option value="Tipo A">Tipo A</option>';
                                            echo '<option value="Tipo B selected">Tipo B</option>';
                                            echo '<option value="Tipo AB">Tipo AB</option>';
                                            echo '<option value="Tipo O">Tipo O</option>';    
                                        }
                                        if($q->Tipo_Sangre == "Tipo AB"){
                                            echo '<option value="Tipo A">Tipo A</option>';
                                            echo '<option value="Tipo B">Tipo B</option>';
                                            echo '<option value="Tipo AB" selected>Tipo AB</option>';
                                            echo '<option value="Tipo O">Tipo O</option>';    
                                        }
                                        if($q->Tipo_Sangre == "Tipo O"){
                                            echo '<option value="Tipo A">Tipo A</option>';
                                            echo '<option value="Tipo B">Tipo B</option>';
                                            echo '<option value="Tipo AB">Tipo AB</option>';
                                            echo '<option value="Tipo O" selected>Tipo O</option>';    
                                        }
                                        if($q->Tipo_Sangre != "Tipo A" || $q->Tipo_Sangre != "Tipo B" || $q->Tipo_Sangre != "Tipo AB" || $q->Tipo_Sangre != "Tipo O"){
                                            echo '<option value="Tipo A">Tipo A</option>';
                                            echo '<option value="Tipo B">Tipo B</option>';
                                            echo '<option value="Tipo AB">Tipo AB</option>';
                                            echo '<option value="Tipo O">Tipo O</option>'; 
                                        }
                                        ?>      
                                    </select>
                                </div>
                            </div>
                                <?=input(array( 'name'=>'alergias','label'=>'Alergias','value'=>$q->Alergias))?>

                            </fieldset>
                       </div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="widget">   
                     <header>
                        <h4>
                            <i class=""></i>&nbsp;
                        </h4>
                    </header>                
                    <div class="body">
                        <div class="form-horizontal">
                            <fieldset>                                
                                <legend class="section">Datos Apoderado</legend>
                                <div class="control-group">
                                    <label class="control-label" for="simple-colored">Asociar Apoderado</label>
                                     <div class="controls form-group">
                                    <select name="apoderado" data-placeholder="Seleccione un apoderado"                                
                                            data-minimum-results-for-search="10"
                                            tabindex="-1"
                                            class="chzn-select select-block-level" id="default-select" required="required">
                                            <option value=""></option>
                                            <?php 
                                                foreach ($apoderados_all as $i) 
                                                {
                                                    if($i->Id == $q->Apoderado_Id){
                                                        echo '<option value="'.$i->Id.'" selected> '.$i->username.' | '.$i->first_name.' '.$i->last_name.'</option>';
                                                    }
                                                    else{
                                                        echo '<option value="'.$i->Id.'"> '.$i->username.' | '.$i->first_name.' '.$i->last_name.'</option>';          
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                            </fieldset>
                        </div>                       
                    </div>
                </section>
            </div>
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                        <?php if(isset($errormsg)){ ?>
                        <div class="alert alert-danger">
                            <strong><i class="fa fa-info-circle"></i>¡Error! </strong><?=$errormsg;?>
                        </div>
                        <?php } ?>
                    </center>
                    </form>
                </div>
            </div>