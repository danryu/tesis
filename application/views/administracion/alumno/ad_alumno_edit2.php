            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración de Usuarios</a></li>
                            <li><a href="#"><i class="glyphicon glyphicon-user"></i> Alumnos</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" method="post" name="form1" onSubmit="javascript:return Rut(document.form1.rut.value)">
                            <fieldset>
                                <legend class="section">Datos Alumno</legend>
                             
                                <?=input(array( 'name'=>'nombre','label'=>'Nombres','help'=>'Ingrese ambos nombres', 'value'=>$q->first_name))?>
                                <?=input(array( 'name'=>'apellido','label'=>'Apellidos','value'=>$q->last_name))?>
                                <?=input(array( 'name'=>'rut','label'=>'RUN','value'=>$q->username))?>
                                <?=input(array( 'name'=>'fnacimiento','label'=>'Fecha de Nacimiento','datepicker'=> true,'id'=>'in-place-date','value'=>$q->f_nacimiento))?>
                                <?=input(array( 'name'=>'tfijo','label'=>'Tel. Fijo','value'=>$q->phone))?>
                                <?=input(array( 'name'=>'tfijo','label'=>'Tel. Celular','value'=>$q->cellphone))?>
                                <?=input(array( 'name'=>'email','label'=>'E-mail','value'=>$q->email))?>
                                <legend class="section">Otros datos Alumno</legend>
                                <?=input(array( 'name'=>'tipo_sangre','label'=>'Tipo de Sangre','help'=>'Ingrese ambos nombres','value'=>$q->Tipo_Sangre))?>
                                <?=input(array( 'name'=>'alergias','label'=>'Alergias','value'=>$q->Alergias))?>
                            
                            </fieldset>
                        </form>
                    </div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="widget">   
                     <header>
                        <h4>
                            <i class=""></i>&nbsp;
                        </h4>
                    </header>                
                    <div class="body">
                        <div class="form-horizontal">
                            <fieldset>                                
                                <legend class="section">Datos Apoderado</legend>
                                <?=input(array( 'name'=>'ap_nombre','label'=>'Nombre Apoderado','disable'=>true,'transparent'=>true,'value'=>$q->Nombre))?>
                                <?=input(array( 'name'=>'ap_rut','label'=>'Rut Apoderado','disable'=>true,'transparent'=>true,'value'=>$q->Rut))?>
                                <legend class="section">Seguridad</legend>
                                <div class="alert alert-info">
                                    <strong><i class="fa fa-info-circle"></i> Contraseña generada automaticamente</strong>, por favor cambiar al iniciar sesion por primera vez.
                                </div>
                                <?=input(array( 'name'=>'password','label'=>'Contraseña','disable'=>true,'transparent'=>true, 'value'=>$password))?>
                            </fieldset>
                        </div>                       
                    </div>
                </section>
            </div>
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                    </center>
                                         </form>
                </div>
            </div>
