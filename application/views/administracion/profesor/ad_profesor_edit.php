            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="../"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración de Usuarios</a></li>
                            <li><a href="../profesor"><i class="glyphicon glyphicon-user"></i> Profesores</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="../Edit" method="post" name="form1" onSubmit="javascript:return Rut(document.form1.rut.value)">
                            <fieldset>
                                <legend class="section">Datos Profesor</legend>
                                <?=input(array( 'name'=>'nombre','label'=>'Nombres','value'=>$q->first_name,'help'=>'Ingrese ambos nombres'))?>
                                <?=input(array( 'name'=>'apellido','value'=>$q->last_name,'label'=>'Apellidos'))?>
                                <?=input(array( 'name'=>'rut','value'=>$q->username,'label'=>'RUN'))?>
                                <div class="control-group">
                                <label class="control-label" for="simple-colored">Sexo</label>
                                    <div class="controls form-group">
                                        <select class="selectpicker" name="sexo" data-style="btn-default"  id="simple-colored">

                                        <?php
                                        //MAX Agrega un SEXO modificable
                                            if($q->sex == 1){ 
                                                echo   '<option value="0">Seleccione...</option>
                                                        <option value="1" selected>Femenino</option>
                                                        <option value="2">Masculino</option>';
                                            }
                                            if($q->sex == 2){
                                                echo '<option value="0">Seleccione...</option>
                                                    <option value="1">Femenino</option>
                                                    <option value="2" selected>Masculino</option>';
                                            }
                                            if($q->sex != 1  && $q->sex != 2){
                                                echo '<option value="0" selected>Seleccione...</option>
                                                    <option value="1">Femenino</option>
                                                    <option value="2">Masculino</option>';
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <?=input(array( 'name'=>'fnacimiento','value'=>$fecha,'label'=>'Fecha de Nacimiento','datepicker'=> true,'id'=>'in-place-date'))?>
                                <?=input(array( 'name'=>'t_fijo','value'=>$q->phone,'label'=>'Tel. Fijo'))?>
                                <?=input(array( 'name'=>'t_celular','maxlength'=>'8','extra'=>'minlength="8" ','value'=>$q->cellphone,'label'=>'Tel. Celular','help'=>'Solo para demostrar','value'=>'56972363570'))?>
                                <?=input(array( 'type'=>'email','name'=>'correo','value'=>$q->email,'label'=>'E-mail'))?>
                                <?=input(array( 'name'=>'profesion','value'=>$q->Profesion,'label'=>'Profesión'))?>
                                
                            </fieldset>
                    </div>
                </section>
            </div>
            <!--<div class="col-md-6">
                <section class="widget">   
                     <header>
                        <h4>
                            <i class=""></i>&nbsp;
                        </h4>
                    </header>                
                    <div class="body">
                        <div class="form-horizontal">
                            <fieldset>                                
                                <legend class="section">Seguridad</legend>
                                <div class="alert alert-info">
                                    <strong><i class="fa fa-info-circle"></i> Contraseña generada automaticamente</strong>, por favor cambiar al iniciar sesion por primera vez.
                                </div> -->
                                <!--?=input(array( 'name'=>'password','label'=>'Contraseña','disable'=>true,'transparent'=>true, 'value'=>$password))?-->
                            <!--</fieldset>
                        </div>                       
                    </div>
                </section>
            </div>-->
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                    </center>
                                         </form>
                </div>
            </div>
