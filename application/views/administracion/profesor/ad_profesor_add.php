            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="../"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración de Usuarios</a></li>
                            <li><a href="../profesor"><i class="glyphicon glyphicon-user"></i> Profesores</a></li>
                            <li class="active">Agregar</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-6">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Formulario de registro
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="../profesor/Add" method="post" name="form1" onSubmit="javascript:return Rut(document.form1.rut.value)">
                            <fieldset>
                                <legend class="section">Datos Profesor</legend>
                                <?=input(array( 'name'=>'nombre','label'=>'Nombres','help'=>'Ingrese ambos nombres'))?>
                                <?=input(array( 'name'=>'apellido','label'=>'Apellidos'))?>
                                <?=input(array( 'name'=>'rut','label'=>'RUN'))?>
                                <?=sexo()?>
                                <?=input(array( 'name'=>'fnacimiento','label'=>'Fecha de Nacimiento','datepicker'=> true,'id'=>'in-place-date'))?>
                                <?=input(array( 'name'=>'t_fijo','label'=>'Tel. Fijo'))?>
                                <?=input(array( 'name'=>'t_celular','label'=>'Tel. Celular','maxlength'=>'8','extra'=>'minlength="8" ','help'=>'Solo para demostrar'))?>
                                <?=input(array( 'type'=>'email','name'=>'correo','label'=>'E-mail'))?>
                                <?=input(array( 'name'=>'profesion','label'=>'Profesión'))?>
                                
                            </fieldset>
                    </div>
                </section>
            </div>
            <!--<div class="col-md-6">
                <section class="widget">   
                     <header>
                        <h4>
                            <i class=""></i>&nbsp;
                        </h4>
                    </header>                
                    <div class="body">
                        <div class="form-horizontal">
                            <fieldset>                                
                                <legend class="section">Seguridad</legend>
                                <div class="alert alert-info">
                                    <strong><i class="fa fa-info-circle"></i> Contraseña generada automaticamente</strong>, por favor cambiar al iniciar sesion por primera vez.
                                </div> -->
                                <!--?=input(array( 'name'=>'password','label'=>'Contraseña','disable'=>true,'transparent'=>true, 'value'=>$password))?-->
                            <!--</fieldset>
                        </div>                       
                    </div>
                </section>
            </div>-->
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Cancelar</button>
                            </div>
                        </div>
                    </center>
                                         </form>
                </div>
            </div>
