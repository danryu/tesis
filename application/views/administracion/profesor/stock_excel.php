<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Tienda Loa")
							 ->setLastModifiedBy("Tienda Loa")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Creacion para Office 2007 XLSX, generado via web.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Resultado");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B2', 'Id')
            ->setCellValue('C2', 'Codigo')
            ->setCellValue('D2', 'Nombre')
            ->setCellValue('E2', 'Descripcion')
            ->setCellValue('F2', 'Categoria')
            ->setCellValue('G2', 'Marca')
            ->setCellValue('H2', 'Precio')
            ->setCellValue('I2', 'Estado');

// Miscellaneous glyphs, UTF-8
$number = 3;
foreach ($enlaces->result() as $row){
    $str_num = number_format( $row->precio_unidad, 2, ',', '.' );   
    
    foreach ($consulta2->result() as $key3){
        if($key3->id == $row->id_categoria){
            $categoria = $key3->nombre;
            
        }
        else{

        }
    }
    
    $sigue = true;
    foreach ($marca->result() as $key) {
        if($key->id == $row->id_marca){
            $marca_x = $key->nombre;
            $sigue = false;
        }
    }
    if($sigue==true){
        $marca_x = "";
    }
    if($row->id_estado==1){
        $estado = "Activo";
    }
    if($row->id_estado==2){
        $estado = "Inactivo";
    }
    $objPHPExcel->setActiveSheetIndex(0)
    	->setCellValue('B'.$number, $row->id)
        ->setCellValue('C'.$number, $row->codigo)
        ->setCellValue('D'.$number, $row->nombre)
        ->setCellValue('E'.$number, $row->descripcion_producto)
        ->setCellValue('F'.$number, $categoria)
        ->setCellValue('G'.$number, $marca_x)
        ->setCellValue('H'.$number, $row->precio_unidad)
        ->setCellValue('I'.$number, $estado);
    $number = $number + 1;
}
    

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Lista Productos');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="listaproductos.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
