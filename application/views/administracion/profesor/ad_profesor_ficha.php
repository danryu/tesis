            <div class="row">
                <div class="col-md-12">
                    <section class="widget">
                        <ol class="breadcrumb">
                            <li><a href="../"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li><a href="#"><i class="fa fa-briefcase"></i> Administración de Usuarios</a></li>
                            <li><a href="../profesor"><i class="glyphicon glyphicon-user"></i> Profesores</a></li>
                            <li class="active">Ver Ficha</li> 
                            <span class="label label-success" style="padding:1px 2px; background:#56bcb6;"></span>
                        </ol>
                    </section>
                </div>
            </div>           
            <div class="col-md-12">
                <section class="widget">
                    <header>
                        <h4>
                            <i class="fa fa-align-left"></i>
                            Ficha
                        </h4>
                    </header>
                    <div class="body">
                        <form class="form-horizontal" action="#" method="post">
                            <fieldset>
                            
                                <legend class="section">Datos Profesor</legend>
                                <div class="col-md-6">
                                <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true, 'name'=>'nombre','label'=>'Nombres','value'=>$q->first_name,'help'=>'Ingrese ambos nombres'))?>
                                <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'apellido','value'=>$q->last_name,'label'=>'Apellidos'))?>
                                <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'rut','value'=>$q->username,'label'=>'RUN'))?>
                                <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Masculino','label'=>'Sexo'))?>
                                <?php
                                        //MAX Agrega un SEXO modificable
                                            if($q->sex == 1){ 
                                                input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Femenino','label'=>'Sexo'));
                                            }
                                            if($q->sex == 2){
                                                input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Masculino','label'=>'Sexo'));
                                            }
                                            if($q->sex != 1 && $q->sex != 2){
                                                input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'value'=>'Sin especificar','label'=>'Sexo'));
                                            }
                                            
                                        ?>
                                <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'fnacimiento','value'=>$fecha,'label'=>'Fecha de Nacimiento','datepicker'=> true,'id'=>'in-place-date'))?>
                                <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'t_fijo','value'=>$q->phone,'label'=>'Tel. Fijo'))?>
                                <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'t_celular','value'=>$q->cellphone,'label'=>'Tel. Celular','help'=>'Solo para demostrar','value'=>'56972363570'))?>
                                <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'correo','value'=>$q->email,'label'=>'E-mail'))?>
                                <?=input(array('transparent'=>true,'style'=>$input, 'disable'=>true,'name'=>'profesion','value'=>$q->Profesion,'label'=>'Profesión'))?>
                                </div>
                            </fieldset>
                    </div>
                </section>
            </div>
            <div class="col-md-12">
                <div class="widget">
                    <center>
                        <div class="form-actions">                    
                            <div>
                                
                                <button type="reset" class="btn btn-default" onclick="history.go(-1);" >Volver</button>
                            </div>
                        </div>
                    </center>
                                         </form>
                </div>
            </div>
