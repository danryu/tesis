<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//*******Elementos con css*******
	if ( ! function_exists('test_method'))
	{
		function menu_materias($query)
		{
			if(isset($query))
			{			
					$first=true;
					$code=NULL;
					foreach ($query as $q) 
					{
						if($first)
						{
							$code.='	<li class="active">
					                        <a href="#asg'.$q->Id.'" data-toggle="tab">'.$q->Nombre.'</a>
					                	</li>';
					        $first=false;
						}
						else
						{
							$code.='	<li>
					                    	<a href="#asg'.$q->Id.'" data-toggle="tab">'.$q->Nombre.'</a>
					                	</li>';
						}
					}
					if($code!=NULL)
					{
						$codeFin='	<header>
					            		<ul class="nav nav-tabs">
					                    	'.$code.'
					                    	
					                	</ul>
					            	</header>';
					    return $codeFin;
					}
					else
					{
						return NULL;
					}
	        	
	    	}
		}
	}
	if ( ! function_exists('test_method'))
	{
		function menu_materia1($id)
		{
			$code='<ul id="myTab" class="nav nav-tabs">
	                    <li class="active"><a href="#des'.$id.'" data-toggle="tab">Descripción</a></li>
	                    <li><a href="#foro'.$id.'" data-toggle="tab">Foro</a></li>
	                    <li><a href="#tarea'.$id.'" data-toggle="tab">Tareas</a></li>
	                </ul>';
	        return $code;
		}	
	}
	if ( ! function_exists('test_method'))
	{
		function tab_materia($query,$query_foro=NULL,$query_notas=NULL,$reglamento_notas=NULL,$tipos_evaluaciones=NULL)
		{
			if(isset($query))
			{
				$first=true;
				$code=NULL;
				$lista_foro="";
				//var_dump($query_foro);
				foreach ($query as $q) 
				{
					if($first){$active='active clearfix';$first=false;}else{$active="";}
					if($query_foro!=NULL && isset($query_foro))
					{
						$lista_foro=foro_listar($q->cha_Id,$query_foro);
					}
					if($query_notas!=NULL && isset($query_notas))
					{
						$lista_notas=notas_listar($q->cha_Id,$query_notas);
					}
					else
					{
						$lista_notas='';
					}
					$menu = menu_materia1($q->Id);	
	                $code.='<div id="asg'.$q->Id.'" class="tab-pane '.$active.'">  
		                    	<section class="widget widget-tabs">
					        	'.$menu.'
					        		<div class="body tab-content">
					            		<div id="des'.$q->Id.'" class="tab-pane active clearfix">
					                        <header>
					                            <h2 style="color:#FFFF;">
					                                <i class="fa fa-book"></i> '.$q->Nombre.'
					                            </h2>
					                        </header>
				                            <h5 style="color:#FFFF;">
				                                <i class="fa fa-clock-o"></i> Duración: '.$q->hora_total.' horas.
					                        </h5>
					                        <h5 style="color:#FFFF;">
				                                <i class="fa fa-bookmark-o"></i> Descripción:<p>'.$q->Descripcion.'</p> 
					                        </h4>
		                                    <h4 style="color:#FFFF;">
				                                <i class="fa fa-cloud-upload"></i> Archivos:
				                            </h4>            
				                            <h3>No hay archivos en la Asignatura</h3>    
					                    </div>
					            	    <div class="tab-pane" id="notas'.$q->Id.'">
		                                    <header>
					                            <h2 style="color:#FFFF;">
					                                <i class="fa fa-book"></i> '.$q->Nombre.' 
					                            </h2>
					                            '.$lista_notas.'
					                        </header>                                   
		                                </div>
		                                <div class="tab-pane" id="foro'.$q->Id.'">
		                                    <header>
						                        <h2 style="color:#FFFF;">
						     
						    	                    <i class="fa fa-book"></i> '.$q->Nombre.' Foros
						                        </h2>
						                       <div id="lista_foro_'.$q->cha_Id.'">'.$lista_foro.'</div>
						                    </header>
		                                </div>
		                                <div class="tab-pane" id="tarea'.$q->Id.'">
		                                    <header>
						                        <h2 style="color:#FFFF;">						     
						    	                    <i class="fa fa-book"></i> '.$q->Nombre.' Tareas
						                        </h2> 
						                    </header>
		                                </div>
					                </div>
					            </section>
				            </div>';
					}
					if($code!=NULL)
					{
						$codeFin=$code;
					    return $codeFin;
					}
					else
					{
						return "Hola";
					}
			}		
		}
	}
	if ( ! function_exists('test_method'))
	{	
		function foro_listar($Id,$query)
		{
			$lista=NULL;
			$count_foro=0;
			$code=NULL;
            foreach ($query as $q) 
            {
                if($q->Id_cha==$Id)
                {
                	if($q->Estado)
                	{
                		$Estado='<span class="badge badge-success">Abierto</span>';
                    }
                    else
                    {
                    	$Estado='<span class="badge badge-danger">Cerrado</span>';
                    }
                    $lista.='<tr>
				                <td>'.($count_foro+1).'</td>
				                <td><a href="/tesis/alumno/foro/'.$q->Id.'">'.$q->Titulo.'</a><d>
				                <td class="hidden-xs-portrait">'.$q->Comentario.'</td>
				                <td>'.$q->Fecha.'</td>
				                <td class="hidden-xs-portrait">'.$Estado.'</td>
				            </tr>'; 
				    $count_foro++;
                }
            }            
            if($count_foro>0)
            {
				$code='	<table id="tabla_foro_'.$Id.'"" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Titulo</th>
                                <th class="hidden-xs-portrait">Comentario</th>
                                <th>Fecha</th>
                                <th class="hidden-xs-portrait">Estado</th>
                            </tr>
                            </thead>
                            <tbody>'.$lista.'</tbody>
                        </table>';
            }
			return $code;
		}
	}
	if ( ! function_exists('test_method'))
	{	
		function notas_listar($Id,$query)
		{
			$lista=NULL;
			$count_foro=0;
			$code=NULL;
            foreach ($query as $q) 
            {
                if($q->Id_cha==$Id)
                {
                	if($q->Estado)
                	{
                		$Estado='<span class="badge badge-success">Abierto</span>';
                    }
                    else
                    {
                    	$Estado='<span class="badge badge-danger">Cerrado</span>';
                    }
                    $lista.='<tr>
				                <td>'.($count_foro+1).'</td>
				                <td><a href="/tesis/profesor/foro/'.$q->Id.'">'.$q->Titulo.'</a><d>
				                <td class="hidden-xs-portrait">'.$q->Comentario.'</td>
				                <td>'.$q->Fecha.'</td>
				                <td class="hidden-xs-portrait">'.$Estado.'</td>
				            </tr>'; 
				    $count_foro++;
                }
            }            
            if($count_foro>0)
            {
				$code='	<table id="tabla_foro_'.$Id.'"" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Titulo</th>
                                <th class="hidden-xs-portrait">Comentario</th>
                                <th>Fecha</th>
                                <th class="hidden-xs-portrait">Estado</th>
                            </tr>
                            </thead>
                            <tbody>'.$lista.'</tbody>
                        </table>';
            }
			return $code;
		}
	}
	if ( ! function_exists('test_method'))
	{
		function documentos()
		{
		}
	}
	if ( ! function_exists('test_method'))
	{
		function upload($id)
		{
			$upfile='<div class="col-md-12">
	                    <div class="body">
	                        <form id="fileupload" action="#" method="POST" enctype="multipart/form-data">
	                            <div class="row">
	                                <div class="col-md-12">
	                                    <div id="dropzone"  class="dropzone">
	                                        Arrastre Aquí
	                                        <i class="fa fa-download-alt pull-right"></i>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-12 fileupload-progress fade">
	                                    <!-- The global progress bar -->
	                                    <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
	                                        <div class="bar" style="width:0%;"></div>
	                                    </div>
	                                    <!-- The extended global progress information -->
	                                    <div class="progress-extended">&nbsp;</div>
	                                </div>
	                            </div>
	                            <div class="form-actions fileupload-buttonbar no-margin">
	                                <span class="btn btn-sm btn-default fileinput-button">
	                                        <i class="fa fa-plus"></i>
	                                        <span>Agregar Archivos</span>
	                                        <input type="file" name="files[]" multiple="">
	                                    </span>
	                                <button type="submit" class="btn btn-primary btn-sm start">
	                                    <i class="fa fa-upload"></i>
	                                    <span>Subir</span>
	                                </button>
	                                <button type="reset" class="btn btn-inverse btn-sm cancel">
	                                    <i class="fa fa-ban"></i>
	                                    <span>Cancelar</span>
	                                </button>
	                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>  
	                            </div>
	                            <div class="fileupload-loading"><i class="fa fa-spin fa-spinner"></i></div>
	                            <!-- The table listing the files available for upload/download -->
	                            <table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
	                        </form>
	                    </div>
	            </div>';
	            $button='<button type="button" class="btn btn-default btn-sm col-md-1"
	                             data-toggle="modal" 
	                             data-target="#fileupload'.$id.'" 
	                             data-backdrop="false">Subir Archivo</button>
					    <div id="fileupload'.$id.'" 
					    		class="modal fade" tabindex="-1" 
					    		role="dialog" 
					    		aria-labelledby="myModalLabel" 
					    		aria-hidden="true" 
					    		style="display: none;">
					        <div class="modal-dialog">
					            <div class="modal-content">
		                                <button type="button" class="close" data-dismiss="modal" style="color:#FFFF !important;"><h3 style="color:#FFFF !important;">x</h3></button>	                                   
		                            '.$upfile.'    
					            </div><!-- /.modal-content -->
					        </div><!-- /.modal-dialog -->
					    </div>';
	            return $button;
		}
	}
	if ( ! function_exists('test_method')) 
	{
		function list_usuario_curso($query,$type)
		{
				if($type=='Apoderado')
				{
					$lista_=NULL;
					foreach ($query as $q) 
					{
						$lista_.='	<tr role="row" class="odd">                             
			                            <td class="sorting_1">'.$q->first_name.'</td> 
			                            <td>'.$q->last_name.'</td> 
			                            <td>'.$q->email.'</td>                            
			                            <td></td>
			                            <td>Funciones de Apoderado</td>
			                        </tr>';
			         }
				}
				if($type=='Alumno')
				{
					$lista_=NULL;
					foreach ($query as $q) 
					{
						$lista_.='	<tr role="row" class="odd">                             
			                            <td class="sorting_1">'.$q->first_name.'</td> 
			                            <td>'.$q->last_name.'</td> 
			                            <td>'.$q->email.'</td>                            
			                            <td></td>
			                            <td>Funciones de Alumno</td>
			                        </tr>';
			         }
				}
				$code='             <div class="body">
			                            <table id="datatable-table" class="table table-striped">                                
			                                <thead>
				                                <tr>			                                    
				                                    <th>Nombre</th>
				                                    <th>Apellido</th>                                            
				                                    <th>Email</th> 
				                                    <th class=\"no-sort hidden-phone-landscape sorting_disabled\"></th>
				                                </tr>
			                                </thead>
			                                <tbody>
			                                   '.$lista_.'         
											</tbody>
			                            </table>
			                        </div>';
	             return $code;
	    }           
	}
	if ( ! function_exists('test_method')) 
	{
		function list_docentes($query)
		{
				
			$url=base_url().'assets/bootstrap/';
			$lista_=NULL;
			$i=0;
			$Ids=NULL;
			$asignatura=NULL;
			$asig_list=NULL;
			foreach ($query as $q) 
			{				
						
						$first=true;
						foreach ($query as $w) 
						{
							
							if($q->Id==$w->Id)
							{
								if($first)
								{
									$asig_list=$w->Nombre;
									$first=false;
								}
								else
								{
									$asig_list.=', '.$w->Nombre;
								}
								
							}
						} 
						$nombre=$q->first_name;
                        $nombre=preg_split("/[\s,]+/",$nombre);
                        $apellido=$q->last_name;
                        $apellido=preg_split("/[\s,]+/",$apellido);
                        $name=$nombre[0].' '.$apellido[0]; 
                        if($i==0)
						{
							$Ids[$i]=$q->Id;
							$lista_.='	<li>
                                    	<img src="'.$url.'img/user.png" alt="" class="pull-left img-circle"/>
	                                    <div class="news-item-info">
	                                        <div class="name"><a href="#">'.$name.'</a></div>	                                        
                                        	<div class="position">'.$q->email.'</div>
                                        	<div class="position">'.$asig_list.'</div>
	                                    </div>
                                	</li>';
                            $i++;
						}
						else
						{
							for($j=0;$j<count($Ids);$j++)
							{
								if($Ids[$j]!=$q->Id)
								{
									//var_dump($q->Id);
									$Ids[$i]=$q->Id;
									$lista_.='	<li>
                                    	<img src="'.$url.'img/user.png" alt="" class="pull-left img-circle"/>
	                                    <div class="news-item-info">
	                                        <div class="name"><a href="#">'.$name.'</a></div>	                                        
                                        	<div class="position">'.$q->email.'</div>
                                        	<div class="position">'.$asig_list.'</div>
	                                    </div>
                                	</li>';
								}
							}
							$i++;							
						}
						$asig_list=NULL;
                       
		    }
		    $code='     <h5 class="tab-header"><i class="fa fa-group"></i> Lista de Profesores</h5>
                        <ul class="news-list news-list-no-hover">
                            '.$lista_.'                                
                        </ul>';
	        return $code;
	    }           
	}
	if ( ! function_exists('test_method'))
	{
		function select_cursos($query)
		{
			$option="";
			foreach ($query as $q) 
			{
				$option+='<option value="'.$q->Id.'">'.$q->Grado.'°'.$q->Letra.'</option>';
			}
			$codigo='	<select id="cursos" data-placeholder="Seleccione un curso" class="chzn-select select-block-level">
	                    	<option value=""></option>
	                    	'.$option.'
	                	</select>';
	        return $codigo;
		}
	}
?>