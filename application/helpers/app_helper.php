<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//*******Elementos con css*******
if ( ! function_exists('test_method'))
{
	function set_name($nombre,$apellido)
    {
        $nombre=preg_split("/[\s,]+/",$nombre);
        $apellido=preg_split("/[\s,]+/",$apellido);
       	return $nombre[0].' '.$apellido[0]; 
    }
	function inbox_list($var)
	{
		$fila=null;
		if(isset($var))
		{
			//var_dump($var);
			foreach ($var as $row) 
			{
				$hide=$row->hide;
				if(!$hide)
				{
					if($row->visto)
					{
						$icono='<i class="fa fa-envelope"></i>';
					}
					else
					{
						$icono='<span class="label label-important">Nuevo</span>';
					}						
					$short_msg=recortar_texto($row->Contenido);
					$name=set_name($row->first_name,$row->last_name);
					$fila.='	<tr data-target="#myModal'.$row->Id.'" data-toggle="modal" class="click hand" id="'.$row->Id.'">
		                            <td width="2%" class="tiny-column">'.$icono.'</td>
		                            <td width="20%" class="name hidden-xs" >'.$row->Titulo.'</td>
		                            <td width="40%" class="subject hidden-xs">'.$short_msg.'</td>
		                            <td width="15%" class="subject">'.$name.'</td>
		                            <td width="15%" class="date" style="width: inherit !important;text-align: right;">'.$row->Fecha.'</td>
	                        	</tr>';
	                $modal='<div id="myModal'.$row->Id.'"  class="modal fade" tabindex="-1" role="dialog" 
                                            aria-labelledby="myModalLabel" aria-hidden="true" 
                                            style="display: none;">
	                            <div class="modal-dialog">
	                                <div class="modal-content">
	                                    <div class="modal-header">
	                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                                        <h4 class="modal-title" id="myModalLabel2">Asunto: '.$row->Titulo.'</h4>
	                                    </div>
	                                    <div class="modal-body">
	                                        '.$row->Contenido.'
	                                    </div>
	                                    <div class="modal-footer">
	                                    	<label style="float: left;">Autor: '.$name.'</label>
	                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	                                    </div>

	                                </div>
	                            </div>
                        	</div>';
                    $fila.=$modal;
                }
			}
			if($fila!==NULL)
			{
				return $fila;
			}
			else
			{
				return "No hay mensajes";
			}
			
		}
		else
		{
			return "No hay mensajes";
		}
	}
}
if ( ! function_exists('test_method'))
{
	function inbox_cont($n)
	{
		$filas=20;
		if(is_numeric($n))
		{
			if($n<$filas)
			{
				$indice="1-".$n." de ".$n;
			}
			else
			{
				$indice;
			}
		}
	}
}
if ( ! function_exists('test_method'))
{
	function status($rango)
	{
		$_op="";		
		if(isset($rango))
		{
			if($rango=="jefe")
			{
				$_op='<option value="3">Nivel 3</option>';
			}
			if($rango=="admin")
			{				
				$_op='<option value="3">Nivel 3</option>
					  <option value="4">Nivel 4</option>';
			}
		}		
		$code='<div class="control-group">
                                <label class="control-label" for="default-select">Nivel de Mensaje</label>
                                <div class="controls form-group">
                                    <select data-placeholder="Seleccione un nivel"
                                            data-width="off"
                                            data-minimum-results-for-search="10"
                                            tabindex="-1"
                                            class="chzn-select select-block-level" id="default-select" 
                                            name="status">
                                        <option value=""></option>
                                        <option value="1">Nivel 1</option>
                                        <option value="2">Nivel 2</option>  
                                        '.$_op.'                                      
                                    </select>
                                </div>
                            </div>';
        return $code;
	}
}
if ( ! function_exists('test_method'))
{
	function status_script($rango)
	{
		$_op="";	
		$lvl_1='<b>Nivel 1:</b> Los mensajes serán enviados al correo';
		$lvl_2='<b>Nivel 2:</b> Los mensajes serán enviados al correo y Twitter si el usuario tiene activo el servicio';
		$lvl_3='<b>Nivel 3:</b> Los mensajes serán enviados al correo, Twitter y SMS si el usuario tiene activo los servicios';
		$lvl_4='<b>Nivel 4:[Urgente]</b> Los mensajes serán enviados al correo, Whatsapp y SMS sin importar la configuración del usuario';
		if(isset($rango))
		{
			if($rango=="jefe")
			{
				$_op='if($(this).val()=="3")
                          {
                          	$("#des").html(\''.$lvl_3.'\');
                          }';
			}
			if($rango=="admin")
			{
				$_op='if($(this).val()=="3")
                          {
                          	$("#des").html(\''.$lvl_3.'\');
                          }
					  if($(this).val()=="4")
                          {
                          	$("#des").html(\''.$lvl_4.'\');
                          }';
			}
		}		
		$code='<script>$( document ).ready(function() 
                {
                    $(\'#default-select\').on(\'change\', function() {
                          if($(this).val()=="1")
                          {
                          	$("#des").html(\''.$lvl_1.'\');
                          }
                          if($(this).val()=="2")
                          {
                          	$("#des").html(\''.$lvl_2.'\');
                          }
                          '.$_op.'
                        }); 
				});</script>';
        return $code;
	}
}
if ( ! function_exists('test_method'))
{
	function list_cursos($query,$rango)
	{		
		$code=NULL;
		$options=NULL;
		if($rango=='jefe')
		{
			$title="Apoderado";
			$name="apoderados[]";
			foreach ($query as $q) 
			{
				$curso=$q->Grado.'°'.$q->Letra;
				$options.='<option value="'.$q->Id.'">'.$curso.'</option> ';
			}
		}
		if($rango=='profe')
		{
			$title="Alumnos";
			$name="alumnos[]";
			foreach ($query as $q) 
			{
				$curso=$q->Grado.'°'.$q->Letra;
				$options.='<option value="'.$q->Id.'">'.$curso.'</option> ';
			}			
		}
		$code='<div class="control-group">
                    <label class="control-label" for="multiple">
                        '.$title.'
                    </label>
                    <div class="controls form-group">
                        <select data-placeholder="Seleccione los cursos" class="chzn-select select-block-level" tabindex="-1" multiple="multiple" id="multiple" name="'.$name.'">
                            '.$options.'                             
                        </select>
                    </div>
                </div>';
        return $code;

	}
}
if ( ! function_exists('test_method'))
{
	function btn_nuevo($url,$base)
	{
		if($url=="profesor" || $url=="administracion")
		{
			
			$btn='<button id="compose-btn" class="btn btn-danger btn-block" Onclick="window.location.href=\''.$base.$url.'/inbox/nuevo\'" >Nuevo</button>';
			return $btn;
		}
		else
		{
			return "";
		}
	}
}
function recortar_texto($texto, $limite=35)
{   
    $texto = trim($texto);
    $texto = strip_tags($texto);
    $tamano = strlen($texto);
    $resultado = '';
    if($tamano <= $limite)
    {
        return $texto;
    }
    else
    {
        $texto = substr($texto, 0, $limite);
        $palabras = explode(' ', $texto);
        $resultado = implode(' ', $palabras);
        $resultado .= '...';
    }   
    return $resultado;
}
?>