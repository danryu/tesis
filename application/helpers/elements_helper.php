<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//*******Elementos con css*******
if ( ! function_exists('test_method'))
{
	function section($text){
		$text='<legend class="section">'.$text.'</legend>';
		echo $text;
	}
}
if ( ! function_exists('test_method'))
{
	function helptext($text){
		$text='<span class="help-block">'.$text.'</span>';
		echo $text;
	}
}
if ( ! function_exists('test_method'))
{
	function input($var)
	{
		$size=8; $label="";
		$transparent=""; $type='type="text"';	$disable="";	$id="normal-field";	$placeholder="";
		$name="";	$value="";	$help=""; $maxlength=""; $icon=""; $_icon=""; $star=""; $end=""; $datepicker=""; $style=''; $extra="";
		$required="";
		if(isset($var["help"]))		{if(!is_null($var["help"])){$help='data-placement="top" data-original-title="'.$var["help"].'"'; $var["id"]="tooltip-enabled";}}
		if(isset($var["label"]))	{$label='<label class="control-label " for="normal-field">'.$var["label"].'</label>';}
		if(isset($var["id"]	))		{$id=$var["id"];}
		if(isset($var["name"]))		{$name='name="'.$var["name"].'"';}
		if(isset($var["maxlength"])){if(is_numeric($var["maxlength"])){$maxlength='maxlength="'.$var["maxlength"].'"';}}
		if(isset($var["value"])){$value='value="'.$var["value"].'"';}
		if(isset($var["placeholder"])){$placeholder='placeholder="'.$var["placeholder"].'"';}	
		if(isset($var["size"]))		{ if(is_numeric($var["size"])){$size=$var["size"];}}
		if(isset($var["transparent"])){ if(is_bool($var["transparent"])){if($var["transparent"]){$transparent='input-transparent';}}}//$style='style="background-color:rgba(51,51,51,0.4)!important"';
		if(isset($var["datepicker"])){ if(is_bool($var["datepicker"])){if($var["datepicker"]){$datepicker="date-picker";}}}
		if(isset($var["disable"]))	{ if(is_bool($var["disable"])){if($var["disable"]){$disable='disabled="disabled"';}}}	
		if(isset($var["icon"]))		{ if(!is_null($var["icon"])){$icon='<span class="input-group-addon"><i class="fa '.$var["icon"].'"></i></span>'; $_icon='input-group ';}}
		if(isset($var["star"]))		{ if(!is_null($var["star"])){$star='<span class="input-group-addon">'.$var["star"].'</span>'; $_icon='input-group ';}}
		if(isset($var["end"]))		{ if(!is_null($var["end"])){$end='<span class="input-group-addon">'.$var["end"].'</span>'; $_icon='input-group ';}}
		if(isset($var["endDate"]))	{ if(!is_null($var["endDate"])){$end='<span class="input-group-addon">'.$var["endDate"].'</span>'; $_icon='input-group ';}}
		if(isset($var["type"]))		{ if(!is_null($var["type"])){$type='type="'.$var["type"].'"';}}
		if(isset($var["extra"])) { if(!is_null($var["extra"])){$extra=$var["extra"];}}
		if(isset($var["style"])) { if(!is_null($var["style"])){$style=$var["style"];}}


		//$var['style']
		$text='<div class="control-group">
		            '.$label.'
		            <div class="controls form-group">
			            <div class="'.$_icon.'col-sm-'.$size.'">
			               '.$icon.$star.'
			               <input '.$type.' id="'.$id.'" '.$value.' '.$name.' '.$maxlength.' class="form-control '.$transparent.' '.$datepicker.'" '.$style.' '.$disable.' '.$help.' '.$placeholder.' '.$extra.' required>
			               '.$end.'
			            </div>
		            </div>
		        </div>';
		return $text;		
	}
}
if ( ! function_exists('test_method'))
{
	function btnForm($var)
	{
		$_btnSubmit="btn-primary"; $_btnCancel="btn-default";
		$textSubmit="Guardar"; $textCancel="Cancelar";
		if(isset($var["SubmitClass"])){ if(!is_null($var["SubmitClass"])){$_btnSubmit=$var["SubmitClass"];}}
		if(isset($var["CancelClass"])){ if(!is_null($var["CancelClass"])){$_btnCancel=$var["CancelClass"];}}
		if(isset($var["Submit"])){ if(!is_null($var["Submit"])){$_btnCancel=$var["Submit"];}}
		if(isset($var["Cancel"])){ if(!is_null($var["Cancel"])){$_btnCancel=$var["Cancel"];}}
		$div='	<div class="form-actions">
		            <div>
		                <button type="submit" class="btn '.$_btnSubmit.'">'.$textSubmit.'</button>
		                <button type="reset" class="btn '.$_btnCancel.'">'.$textCancel.'</button>
		            </div>
	            </div>';
	    echo $div;
	}
}
if ( ! function_exists('test_method'))
{
	function switches($var)
	{
		$checked=''; $name=''; $label="";
		$dataOn='success'; $dataOff='danger';
		if(isset($var["checked"]))	{ if(!is_null($var["checked"])){if(is_bool($var["checked"])){if($var["checked"]){$checked='checked="checked"';}}}}
		if(isset($var["data-on"]))	{ if(!is_null($var["data-on"])){$dataOn=$var["data-on"];}}
		if(isset($var["data-off"]))	{ if(!is_null($var["data-off"])){$dataOff=$var["data-off"];}}
		if(isset($var["label"]))	{ if(!is_null($var["label"])){$label='<label for="simple-switch" class="control-label">'.$var["label"].'</label>';}}
		if(isset($var["name"]))		{ if(!is_null($var["name"])){$name='name="'.$var["name"].'"';}}

		$div='	<div class="control-group">
	                '.$label.'
	                <div class="controls form-group">
		               	<label class="checkbox inline">
		                    <span class="switch switch-small" data-on="'.$dataOn.'" data-off="'.$dataOff.'">
		                    	<input id="simple-switch" '.$name.' type="checkbox" '.$checked.'>
		                    </span>
		                </label>                                    
	                </div>
	            </div>';

		echo $div;
	}
}
if ( ! function_exists('test_method'))
{
	function checkbox($var)
	{
		
	}
}
if ( ! function_exists('test_method'))
{
	function comunas($var)
	{
		$comunas=NULL;
		$id=0;
		$selected=NULL;
		if(isset($var["comuna"]))	{ if(!is_null($var["comuna"])){$comunas=$var["comuna"];}}
		if(isset($var["id_selected"]))	{ if(!is_null($var["id_selected"])){$id=$var["id_selected"];}}
		$options=NULL;
		foreach ($comunas as $row) 
		{
			if($row->codigoInterno==$id){$selected='selected';}else{$selected='';}
			$comuna=mb_convert_case($row->nombre, MB_CASE_LOWER, "UTF-8");
			$options.='<option '.$selected.' value="'.$row->codigoInterno.'">'.ucfirst($comuna).'</option>';
		}
		$code='<div class="control-group">
                    <label class="control-label" for="default-select">Comuna</label>
                    <div class="controls form-group">
                        <select name="comuna" data-placeholder="Seleccione una comuna"                                
                                data-minimum-results-for-search="10"
                                tabindex="-1"
                                class="chzn-select select-block-level" id="default-select">
                            <option '.$selected.' value=""></option>
                            '.$options.'
                        </select>
                    </div>
                </div>';
        return $code;
	}
}
if ( ! function_exists('test_method'))
{
	function comunas_selected($var)
	{

		$options=NULL;
		foreach ($var as $row) 
		{
			$comuna=mb_convert_case($row->nombre, MB_CASE_LOWER, "UTF-8");
			$options.='<option value="'.$row->codigoInterno.'">'.ucfirst($comuna).'</option>';
		}
		$code='<div class="control-group">
                    <label class="control-label" for="default-select">Comuna</label>
                    <div class="controls form-group">
                        <select name="comuna" data-placeholder="Seleccione una comuna"                                
                                data-minimum-results-for-search="10"
                                tabindex="-1"
                                class="chzn-select select-block-level" id="default-select">
                            <option '.$selected.' value=""></option>
                            '.$options.'
                        </select>
                    </div>
                </div>';
        return $code;
	}
}
if ( ! function_exists('test_method'))
{
	function sexo()
	{
		$selected = 'checked';
		$code='<div class="control-group">
                                <label class="control-label" for="simple-colored">Sexo</label>
                                    <div class="controls form-group">
                                        <select class="selectpicker" name="sexo" data-style="btn-default"  id="simple-colored">
                                            <option value="0">Seleccione...</option>
                                            <option value="1">Femenino</option>
                                            <option value="2">Masculino</option>
                                        </select>
                                    </div>
                                </div>';
        return $code;
	}
}




?>