<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//*******Elementos con css*******
	if ( ! function_exists('test_method'))
	{
		function name($nombre,$apellido)
		{
            $nombre=preg_split("/[\s,]+/",$nombre);
            $apellido=preg_split("/[\s,]+/",$apellido);
            return $nombre[0].' '.$apellido[0]; 
		}

		function menu_materias($query,$Id_C)
		{
			if(isset($query))
			{			
					$first=true;
					$code=NULL;
					foreach ($query as $q) 
					{
						if($first)
						{
							$code.='	<li class="active">
					                        <a href="#asg'.$q->Id.'" data-toggle="tab">'.$q->Nombre.'</a>
					                	</li>';
					        $first=false;
						}
						else
						{
							$code.='	<li>
					                    	<a href="#asg'.$q->Id.'" data-toggle="tab">'.$q->Nombre.'</a>
					                	</li>';
						}
					}
					if($code!=NULL)
					{
						$codeFin='	<header>
					            		<ul class="nav nav-tabs">
					                    	'.$code.'
					                    	
					                	</ul>
					            	</header>';
					    return $codeFin;
					}
					else
					{
						return NULL;
					}
	    	}
		}
	
		function menu_materia1($id)
		{
			$code='<ul id="myTab" class="nav nav-tabs">
	                    <li class="active"><a href="#des'.$id.'" data-toggle="tab">Descripción</a></li>
	                    <li><a href="#notas'.$id.'" data-toggle="tab">Notas</a></li>
	                    <li><a href="#foro'.$id.'" data-toggle="tab">Foro</a></li>
	                </ul>';
	        return $code;
		}	
	
		function tab_materia($query,$query_foro=NULL,$query_notas=NULL,$reglamento_notas=NULL,$tipos_evaluaciones=NULL,$lista_alumnos=NULL,$set_notas_curso=NULL,$tab_select=NULL)
		{
			if(!is_null($query))
			{
				if(!is_null($tab_select))
				{
					
				}
				$first=true	;
				$code=NULL;
				$lista_foro="";
				foreach ($query as $q) 
				{
					if($first){$active='active clearfix';$first=false;}else{$active="";}
					if($query_foro!=NULL && isset($query_foro))
					{
						$lista_foro=foro_listar($q->cha_Id,$query_foro);
					}
					if($q->Cantidad_Notas!=0)
					{

						if($query_notas!=NULL)
						{

							foreach ($query_notas as $qn) 
							{
								if($q->cha_Id==$qn->Id_Cha)
								{
									$lista_notas=notas_listar($q->cha_Id,$query_notas,$lista_alumnos,$set_notas_curso);
									$btn_notas=btn_config($q->cha_Id,$set_notas_curso);
									$btn_registrar_notas=registrar_nota_btn($q->cha_Id,$set_notas_curso);
								}
								else
								{
									$lista_notas=notas_listar_curso($q->cha_Id,$lista_alumnos,$set_notas_curso,$q->Cantidad_Notas);
									$btn_notas=btn_config($q->cha_Id,$set_notas_curso);
									$btn_registrar_notas=registrar_nota_btn($q->cha_Id,$set_notas_curso);									
								}								
							}							
						}
						else
						{
							$btn_notas=btn_config($q->cha_Id,$set_notas_curso);
							$btn_registrar_notas=registrar_nota_btn($q->cha_Id,$set_notas_curso);
							$lista_notas=notas_listar_curso($q->cha_Id,$lista_alumnos,$set_notas_curso,$q->Cantidad_Notas);
						}						
					}
					else
					{
						$btn_notas="";
						$btn_registrar_notas="";
						$lista_notas=notas_set_btn($q->cha_Id,$reglamento_notas,$q->cha_horas);	
					}
					$menu = menu_materia1($q->Id,$tab_select);	
	                $code.='<div id="asg'.$q->Id.'" class="tab-pane '.$active.'">  
		                    	<section class="widget widget-tabs">
					        	'.$menu.'
					        		<div class="body tab-content">
					            		<div id="des'.$q->Id.'" class="tab-pane active clearfix">
					                        <header>
					                            <h2 style="color:#FFFF;">
					                                <i class="fa fa-book"></i> '.$q->Nombre.'
					                            </h2>
					                        </header>
				                            <h5 style="color:#FFFF;">
				                                <i class="fa fa-clock-o"></i> Duración: '.$q->hora_total.' horas.
					                        </h5>
					                        <h5 style="color:#FFFF;">
				                                <i class="fa fa-bookmark-o"></i> Descripción:<p>'.$q->Descripcion.'</p> 
					                        </h4>
					                    </div>
					            	    <div class="tab-pane" id="notas'.$q->Id.'">
		                                    <header>
					                            <h2 style="color:#FFFF;">
					                                <i class="fa fa-book"></i> '.$q->Nombre.'
					                            </h2>  					                            
					                        </header> 
					                        '.$btn_registrar_notas.' 
					                        <div id="notas" class="tablas_notas">
					                           '.$lista_notas.'
					                        </div> 
					                        '.$btn_notas.'                                 
		                                </div>
		                                <div class="tab-pane" id="foro'.$q->Id.'">
		                                    <header>
						                        <h2 style="color:#FFFF;">						     
						    	                    <i class="fa fa-book"></i> '.$q->Nombre.'
						                        </h2>						                      
						                        	'.foro_create_btn($q->cha_Id).'						                        
						                       <div id="lista_foro_'.$q->cha_Id.'">'.$lista_foro.'</div>
						                    </header>
		                                </div>
					                </div>
					            </section>
				            </div>';
					}
					if($code!=NULL)
					{
						$codeFin=$code;
					    return $codeFin;
					}
					else
					{
						return "";
					}
			}		
		}
	
		function foro_create_btn($Id)
		{
			$fecha=date('d-m-Y H:i:s');
			$code='<button type="button" class="btn btn-default btn-lg btn-block" data-toggle="modal" data-target="#myModal'.$Id.'foro" data-backdrop="false">Nuevo Foro</button>
	                        <div id="myModal'.$Id.'foro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	                            <div class="modal-dialog">
	                                <div class="modal-content">

	                                    <div class="modal-header">
	                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                                        <h4 class="modal-title" id="myModalLabel'.$Id.'">Crear nuevo foro</h4>
	                                    </div>
	                                    <div class="modal-body">
	                                        <h4>Titulo</h4>
	                                        <div class="control-group">				                            
					                            <div class="controls form-group">
					                                <div class="col-sm-8">
					                                    <input type="text" id="normal-field" name="titulo'.$Id.'" class="form-control" placeholder="Escriba un titulo..." required>
					                                </div>
					                            </div>
					                        </div>
					                        <br>
					                         <br>
					                          <br>
					                        <h4>Comentario</h4>
	                                        <div class="control-group">
				                                <div class="controls form-group">
				                                    <textarea rows="4" class="form-control" id="wysiwyg" name="comentario'.$Id.'" required></textarea>
				                                </div>
				                            </div>
	                                    </div>
	                                    <div class="modal-footer">
	                                    	'.$fecha.'
	                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	                                        <button type="button" class="btn btn-primary save" id="save'.$Id.'">Guardar</button>
	                                    </div>

	                                </div>
	                            </div>
	                        </div>';
	        return $code;
		}
	
		function notas_set_btn($Id,$limites,$horas)
		{
			$fecha=date('d-m-Y H:i:s');
			
			$hora_ejemplo=$horas;//BORRAR
			$notas_lista=NULL;
			$tipos_lista=NULL;
			$listar_inputs=NULL;
			$listar_tabla=NULL;
			
			foreach ($limites as $q) 
			{
				if($q->Cantidad_Horas == $hora_ejemplo)
				{
					for($i=$q->Notas_minima; $i<=$q->Notas_maxima;$i++)
					{
						$notas_lista.='<option class="cantidad_eva" value="'.$i.'">'.$i.' Evaluaciones</option>';
						
					}
					$Nota_min=$q->Notas_minima;
					$Nota_max=$q->Notas_maxima;
					for($i=1; $i<=$q->Notas_minima;$i++)
					{
						$listar_tabla.='<tr>
											<td>'.$i.'</td>
											<td><input type="text" id="normal-field" name="tipo_notas'.$Id.'[]" class="form-control" placeholder="Ingrese Evaluación"></td>
											<td><input id="in-place-date" class="form-control date-picker" type="text" name="fecha_notas'.$Id.'[]" placeholder="Ingrese Fecha Evaluación "></td>
										</tr>';
					}


				}

			}

			$code='<button type="button" class="btn btn-default btn-lg btn-block" onclick="$(\'input:hidden[name=Set_cha_Id]\').val('.$Id.')" data-toggle="modal" data-target="#mymodal'.$Id.'notas" data-backdrop="false">Configuraciones de notas</button>
	                        <div id="mymodal'.$Id.'notas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	                            <div class="modal-dialog">
	                                <div class="modal-content">

	                                    <div class="modal-header">
	                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                                        <h4 class="modal-title" id="myModalLabel'.$Id.'">Configuraciones de Evaluaciones</h4>
	                                    </div>
	                                    <div class="modal-body">
	                                        <h4>Cantidad de Evaluaciones</h4>
	                                       	<div class="control-group">
				                                <div class="controls form-group">
				                                    <select class="selectpicker" data-style="btn-default" tabindex="-1" id="Cantidad_Eva_'.$Id.'">
				                                        '.$notas_lista.'
				                                    </select>
				                                </div>
				                            </div>					                           
				                            <div>
					                            <table id="notas_setting_'.$Id.'" class="table table-hover table-bordered">
						                            <thead>
							                            <tr>
							                                <th style="color:#000000">#</th>
							                                <th style="color:#000000">Tipo de Evaluación</th>
							                                <th style="color:#000000">Fecha de Evaluación</th>
							                            </tr>
						                            </thead>
						                            <tbody>
				                            			'.$listar_tabla.'
				                            		</tbody>
				                            	</table>
				                            </div>       
				                            <div id=alerta_'.$Id.'>
				                            </div>
				                            <input name="Notas_Min'.$Id.'" value="'.$Nota_min.'" type="hidden" />
				                            <input name="Notas_Max'.$Id.'" value="'.$Nota_max.'" type="hidden" />
 											
	                                    </div>
	                                    <div class="modal-footer">
	                                    	'.$fecha.'
	                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	                                        <button type="button" class="btn btn-primary save_setting_notas" id="save'.$Id.'">Guardar</button>
	                                    </div>

	                                </div>
	                            </div>
	                        </div>';
	        return $code;
		}
	
		function foro_listar($Id,$query)
		{
			$lista=NULL;
			$count_foro=0;
			$code=NULL;
            foreach ($query as $q) 
            {
                if($q->Id_cha==$Id)
                {
                	if($q->Estado)
                	{
                		$Estado='<span class="badge badge-success">Abierto</span>';
                    }
                    else
                    {
                    	$Estado='<span class="badge badge-danger">Cerrado</span>';
                    }
                    $lista.='<tr>
				                <td>'.($count_foro+1).'</td>
				                <td><a href="/tesis/profesor/foro/'.$q->Id.'">'.$q->Titulo.'</a><d>
				                <td class="hidden-xs-portrait">'.$q->Comentario.'</td>
				                <td>'.$q->Fecha.'</td>
				                <td class="hidden-xs-portrait">'.$Estado.'</td>
				            </tr>'; 
				    $count_foro++;
                }
            }            
            if($count_foro>0)
            {
				$code='	<table id="tabla_foro_'.$Id.'"" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Titulo</th>
                                <th class="hidden-xs-portrait">Comentario</th>
                                <th>Fecha</th>
                                <th class="hidden-xs-portrait">Estado</th>
                            </tr>
                            </thead>
                            <tbody>'.$lista.'</tbody>
                        </table>';
            }
			return $code;
		}
		
		function notas_listar_curso($Id,$alumnos,$set_notas,$cantidad_notas)
		{
			$lista=NULL;
			$count_foro=0;
			$code=NULL;
            foreach ($alumnos as $q) 
            {
                $td="";
                for($i=0;$i<$cantidad_notas;$i++)
                {
                	$td.='<td>&nbsp;</td>';
                }
				$lista.='	<tr>
				                <td>'.name($q->first_name,$q->last_name).'</a><td>
				                '.$td.'
				            </tr>'; 
				    $count_foro++;
                
            }            
            if($count_foro>0)
            {
            	$td="";
            	if($set_notas!=null)
            	{
            		foreach ($set_notas as $q) 
	            	{
	            		if($Id==$q->Id_Cha)
	            		$td.='<td id="'.$q->Id.'">'.$q->Tipo.'</td>';
	            	}
					$code='	<table id="lista_alumnos_notas_'.$Id.'" class="table table-hover ">
	                            <thead>
	                            <tr>
	                                <th>Nombre</th>
	                                '.$td.'
	                                <td>Opciones</td>
	                            </tr>
	                            </thead>
	                            <tbody>'.$lista.'</tbody>
	                        </table>';
            	}
            	else
            	{
            		$code="";
            	}
            	
            }
			return $code;
		}
		
		function btn_config($Id,$set_notas_curso)
		{
			$i=1;
			$listar_tabla="";
			$fecha=date('d-m-Y H:i:s');
			$hay=false;
			if($set_notas_curso!=null)
			{
				foreach ($set_notas_curso as $q) 
				{
					$lock='';
					if($q->Id_Cha == $Id)
					{	
						if(!$q->Registrada)
						{
							$_fecha=date("d/m/Y",strtotime($q->Fecha));
							$listar_tabla.='<tr>
												<td>'.$i.'<input type="hidden" name="id_notas_editar'.$Id.'[]" value="'.$q->Id.'"></td>

												<td><input type="text" id="normal-field" name="tipo_notas_editar'.$Id.'[]" class="form-control" placeholder="Ingrese Evaluación" value="'.$q->Tipo.'"></td>
												<td><input id="'.$q->Id.'" class="form-control date-picker" type="text" name="fecha_notas_editar'.$Id.'[]" placeholder="Ingrese Fecha Evaluación " value="'.$_fecha.'" '.$lock.'></td>
											</tr>';
							$i++;
							$hay=true;
						}
						
					}

				}
				if($hay)
				{
					$code='<button type="button" class="btn btn-default btn-lg btn-block" onclick="$(\'input:hidden[name=Set_cha_Id]\').val('.$Id.')" data-toggle="modal" data-target="#mymodal'.$Id.'notas" data-backdrop="false">Editar configuraciones de notas</button>
		                        <div id="mymodal'.$Id.'notas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		                            <div class="modal-dialog">
		                                <div class="modal-content">

		                                    <div class="modal-header">
		                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		                                        <h4 class="modal-title" id="myModalLabel'.$Id.'">Configuraciones de Evaluaciones</h4>
		                                    </div>
		                                    <div class="modal-body">	
		                                    	<p> Al cambiar los valores de las evaluaciones, podrá guardar los cambios, no así la cantidad de evaluaciones, ademas puede ingresar los resultados</p>                                       				                           
					                            <div>
						                            <table id="notas_setting_'.$Id.'" class="table table-hover">
							                            <thead>
								                            <tr>
								                                <th style="color:#000000">#</th>
								                                <th style="color:#000000">Tipo de Evaluación</th>
								                                <th style="color:#000000">Fecha de Evaluación</th>
								                            </tr>
							                            </thead>
							                            <tbody>
					                            			'.$listar_tabla.'
					                            			<tr><td colspan="4"><button type="button" style="width:100%" class="btn btn-success edit_set_nota">Guardar Cambios</button></td></tr>
					                            		</tbody>
					                            	</table>
					                            </div>       
					                            <div id=alerta_'.$Id.'>
					                            </div> 											
		                                    </div>
		                                    <div class="modal-footer">
		                                    	'.$fecha.'
		                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		                                    </div>

		                                </div>
		                            </div>
		                        </div>';
	     	
				}
				else
				{
					$code="";
				}
			}
			else
			{
				$code="";
			}
			return $code;
			
		}

		function registrar_nota_btn($Id,$set_notas_curso)
		{
			$i=0;
			$lista_insertar="";
			if($set_notas_curso!=null)
			{
				foreach ($set_notas_curso as $q) 
				{
					if($q->Id_Cha == $Id && !$q->Registrada)
					{	
							$lista_insertar.='<li><a href="nota/'.$q->Id.'">'.$q->Tipo.'</a></li>';
							$i++;
					}

				}
				if($i!=0)
				{
					$code='<div class="btn-group">
		                        <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
		                            &nbsp; Registrar Notas &nbsp;
		                            <i class="fa fa-caret-down"></i>
		                        </button>
		                        <ul class="dropdown-menu">
		                            '.$lista_insertar.'
		                        </ul>
		                    </div>';
	            }
	            else
	            {
	            	$code="";
	            }
	        }
	        else
	            {
	            	$code="";
	            }
            return $code;
		}
		
		function notas_listar($Id,$query_notas,$alumnos,$set_notas)
		{
			
			$lista=NULL;
			$count_foro=0;
			$code=NULL;
			$last_Id=NULL;
			$cantidad=NULL;			
            $td_=NULL;
            $notas=array();

            foreach ($alumnos as $al) 
            {
            	$first_row=true;
            	$td="";
            	$count_re=0;
            	$count_set_nota=0;
            	$options="";
            	foreach ($set_notas as $s) 
            	{
            		if($Id==$s->Id_Cha)
            		{
	            		$count_set_nota++;
	            		if($s->Registrada)
	            		{
	            			$count_re++;
	            		}            		
		            	foreach ($query_notas as $n) 
		            	{
		            		if($al->Id === $n->Id_U)
		            		{
		            			if($s->Id === $n->Id_set)
		            			{
		            				$td.='<td style="text-align:center">'.$n->Nota.'</td>';	    
		            				$options.='<option value="'.$s->Id.'">'.$s->Tipo.'</option>';     
		            			}			
		            		}       		
		            	}
	            	}
            	}

            	$td_respaldo="";
            	for($j=$count_re;$j<$count_set_nota;$j++)
            	{
            		$td_respaldo.='<td style="text-align:center">-</td>';
            	}
            	$editar=modal_editar($options,$al->Id);
	            $lista.='<tr>
            				<td>'.name($al->first_name,$al->last_name).'</td>
            				'.$td.$td_respaldo.'
            				<td>'.$editar.'
            				</td>
            			</tr>';         	
            	
            }     
            foreach ($set_notas as $s) 
           	{
           		if($Id==$s->Id_Cha)
           		{
           			$td_.='<td id="'.$s->Id.'" style="text-align:center">'.$s->Tipo.'</td>';
           		}     		
            }       	
			$code='	<table id="lista_alumnos_notas_'.$Id.'" class="table table-hover ">
                        <thead>
                            <tr>
                                <th style="width: 150px">Nombre</th>
                                '.$td_.'
                                <td style="width: 80px">Opciones</td>
                            </tr>
                        </thead>
                        <tbody>'.$lista.'</tbody>
                    </table>';
            
			return $code;
		}

		function modal_editar($options,$Id_Alumno)
		{
			$codigo='	<button type="button" class="btn btn-success btn-sm btn-block" data-toggle="modal" data-target="#editar_alumno'.$Id_Alumno.'" data-backdrop="false">
                            <i class="fa fa-edit"></i>Editar
                        </button>
                        <div id="editar_alumno'.$Id_Alumno.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel2">Editar Nota</h4>
                                    </div>
                                    <div class="modal-body">
                                    <p>Para editar el registro de una evaluación, debe especificar los motivos del cambio</p>
                                    <table  class="table" style="background-color:#fff">
						                <thead>
							                <tr>
							                    <th style="color:#000000">Evaluaciones registradas</th>
							                    <th style="color:#000000">Nuevo resultado</th>
							                </tr>
						                </thead>
						                <tbody>
						                    <tr>
						                        <td>
		                                    	<select class="selectpicker" data-style="btn-default" tabindex="-1" name="vieja_'.$Id_Alumno.'" id="simple-red">
		                                        '.$options.'
		                                    	</select>
		                                    	</td>
		                                    	<td>
		                                    		<input type="text" id="normal-field" name="actual_'.$Id_Alumno.'" class="form-control editarN_nuevo_input">
		                                    	</td>
	                                    	</tr>
	                                    	<tr><td>Motivos del cambio de resultados</td><td></td></tr>
					                        <tr>
					                        	<td colspan="2" >	
					                        		<textarea rows="4" name="textarea_'.$Id_Alumno.'"class="form-control" id="default-textarea"></textarea>
					                        	</td>
					                        </tr>
				                        </tbody>
				                    </table>    
				                    <div id="alerta_edita_'.$Id_Alumno.'"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        <button type="button" id="'.$Id_Alumno.'"class="btn btn-primary edit_nota">Guardar</button>
                                    </div>

                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>';
            return $codigo;
		}
	
		function documentos()
		{
		}
	
		function upload($id)
		{
			$upfile='<div class="col-md-12">
	                    <div class="body">
	                        <form id="fileupload" action="#" method="POST" enctype="multipart/form-data">
	                            <div class="row">
	                                <div class="col-md-12">
	                                    <div id="dropzone"  class="dropzone">
	                                        Arrastre Aquí
	                                        <i class="fa fa-download-alt pull-right"></i>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-12 fileupload-progress fade">
	                                    <!-- The global progress bar -->
	                                    <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
	                                        <div class="bar" style="width:0%;"></div>
	                                    </div>
	                                    <!-- The extended global progress information -->
	                                    <div class="progress-extended">&nbsp;</div>
	                                </div>
	                            </div>
	                            <div class="form-actions fileupload-buttonbar no-margin">
	                                <span class="btn btn-sm btn-default fileinput-button">
	                                        <i class="fa fa-plus"></i>
	                                        <span>Agregar Archivos</span>
	                                        <input type="file" name="files[]" multiple="">
	                                    </span>
	                                <button type="submit" class="btn btn-primary btn-sm start">
	                                    <i class="fa fa-upload"></i>
	                                    <span>Subir</span>
	                                </button>
	                                <button type="reset" class="btn btn-inverse btn-sm cancel">
	                                    <i class="fa fa-ban"></i>
	                                    <span>Cancelar</span>
	                                </button>
	                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>  
	                            </div>
	                            <div class="fileupload-loading"><i class="fa fa-spin fa-spinner"></i></div>
	                            <!-- The table listing the files available for upload/download -->
	                            <table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
	                        </form>
	                    </div>
	            </div>';
	            $button='<button type="button" class="btn btn-default btn-sm col-md-1"
	                             data-toggle="modal" 
	                             data-target="#fileupload'.$id.'" 
	                             data-backdrop="false">Subir Archivo</button>
					    <div id="fileupload'.$id.'" 
					    		class="modal fade" tabindex="-1" 
					    		role="dialog" 
					    		aria-labelledby="myModalLabel" 
					    		aria-hidden="true" 
					    		style="display: none;">
					        <div class="modal-dialog">
					            <div class="modal-content">
		                                <button type="button" class="close" data-dismiss="modal" style="color:#FFFF !important;"><h3 style="color:#FFFF !important;">x</h3></button>	                                   
		                            '.$upfile.'    
					            </div><!-- /.modal-content -->
					        </div><!-- /.modal-dialog -->
					    </div>';
	            return $button;
		}
	
		function list_usuario_curso($query,$type)
		{
				if($type=='Apoderado')
				{
					$lista_=NULL;
					foreach ($query as $q) 
					{
						$lista_.='	<tr role="row" class="odd">                             
			                            <td class="sorting_1">'.$q->first_name.'</td> 
			                            <td>'.$q->last_name.'</td> 
			                            <td>'.$q->email.'</td>                            
			                            <td></td>
			                            <td>Funciones de Apoderado</td>
			                        </tr>';
			         }
				}
				if($type=='Alumno')
				{
					$lista_=NULL;
					foreach ($query as $q) 
					{
						$lista_.='	<tr role="row" class="odd">                             
			                            <td class="sorting_1">'.$q->first_name.'</td> 
			                            <td>'.$q->last_name.'</td> 
			                            <td>'.$q->email.'</td>                            
			                            <td></td>
			                            <td>Funciones de Alumno</td>
			                        </tr>';
			         }
				}
				$code='             <div class="body">
			                            <table id="datatable-table" class="table table-striped">                                
			                                <thead>
				                                <tr>			                                    
				                                    <th>Nombre</th>
				                                    <th>Apellido</th>                                            
				                                    <th>Email</th> 
				                                    <th class=\"no-sort hidden-phone-landscape sorting_disabled\"></th>
				                                </tr>
			                                </thead>
			                                <tbody>
			                                   '.$lista_.'         
											</tbody>
			                            </table>
			                        </div>';
	             return $code;
	    }           
	
		function list_usuario_curso_vertical($query,$type)
		{
				$url=base_url().'assets/bootstrap/';
				if($type=='Apoderado')
				{
					$lista_=NULL;
					foreach ($query as $q) 
					{
						$lista_.='	<li>
                                    	<img src="'.$url.'img/user.png" alt="" class="pull-left img-circle"/>
	                                    <div class="news-item-info">
	                                        <div class="name"><a href="#">'.name($q->first_name,$q->last_name).'</a></div>	                                        
                                        	<div class="position">'.$q->email.'</div>
	                                    </div>
                                	</li>';
			         }
				}
				if($type=='Alumno')
				{
					$lista_=NULL;
					foreach ($query as $q) 
					{ 
						$lista_.='	 
                            <ul class="news-list">
                                <li style="height: 70px;">
                                    <img src="'.$url.'img/user.png" alt="" class="pull-left img-circle"/>
                                    <div class="news-item-info">
                                        <div class="name"><a href="#">'.name($q->first_name,$q->last_name).'</a></div>
                                        <div class="position">'.$q->email.'</div>
                                        <div class="options">
	                                            <button class="btn btn-xs btn-danger anotacion" id="'.$q->Id.'">
	                                                <i class="fa fa-exclamation-triangle"></i>
	                                                Anotaciones
	                                            </button>
	                                    </div>
                                    </div>
                                </li>
                            </ul>';
			         }
				}
				$code='     <h5 class="tab-header"><i class="fa fa-group"></i> Lista de '.$type.'s</h5>
                            <ul class="news-list news-list-no-hover">
                                '.$lista_.'                                
                            </ul>';
	             return $code;
	    }           
	
		function list_set_notas_vertical($query,$actual,$fecha_actual)
		{
				$url=base_url().'assets/bootstrap/';
					$lista_=NULL;
					foreach ($query as $q) 
					{		
						$completa='';			
						if(!$q->Registrada)	
						{	
							if($fecha_actual>$q->Fecha)
							{
								$completa='<span class="label label-danger">Debe registrar primero!</span>';
							}
							if(!($actual==$q->Id))
							{
								$lista_.='	<li>
												<div style="margin-left: 0px" class="news-item-info">
		                                        	<div class="position">Prueba:<a href="'.$q->Id.'">'.$q->Tipo.'</a> '.$completa.'</div>	                                        
	                                        		<div class="position">Fecha: '.date("d/m/Y",strtotime($q->Fecha)).'</div>
		                                    	</div>
		                                    </li>';
                            }
						}	
						
			         }				
				$code=' <ul class="news-list news-list-no-hover">'.$lista_.'</ul>';
	        	return $code;
	    }           
	
		function select_cursos($query)
		{
			$option="";
			foreach ($query as $q) 
			{
				if(!$q->Registrada)	
			  	{
					$option+='<option value="'.$q->Id.'">'.$q->Grado.'°'.$q->Letra.'</option>';
				}
			}
			$codigo='	<select id="cursos" data-placeholder="Seleccione un curso" class="chzn-select select-block-level">
	                    	<option value=""></option>
	                    	'.$option.'
	                	</select>';
	        return $codigo;
		}
	
		function list_alumnos($query,$Id)
		{
			$lista=NULL;
			$count_foro=0;
			$code=NULL;
            foreach ($query as $q) 
            {           	
               
                    $lista.='<tr>
				                <td>'.$q->username.'</td>
				                <td>'.$q->last_name.'<d>
				                <td class="hidden-xs-portrait">'.$q->first_name.'</td>
				                <td>
				                		<input type="text" size="5" name="notas[]" id="normal-field" class="form-control input_nota" placeholder="Nota">
				                	
				                </td>
				            </tr>'; 
				    $count_foro++;                
            }            
            if($count_foro>0)
            {
				$code='	<table id="tabla_Alumno_'.$Id.'"" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Apellidos</th>
                                <th class="hidden-xs-portrait">Nombres</th>
                                <th>Nota</th>
                            </tr>
                            </thead>
                            <tbody>
                            '.$lista.'                           
                            </tbody>
                        </table>';
            }
			return $code;
		}

		function list_alumnos_asistencia($query,$Id)
		{
			$lista=NULL;
			$count_foro=0;
			$code=NULL;
            foreach ($query as $q) 
            {           	
               
                    $lista.='<tr>
				                <td>'.$q->username.'</td>
				                <td>'.$q->last_name.'<d>
				                <td class="hidden-xs-portrait">'.$q->first_name.'</td>
				                <td>
				                		<input type="checkbox" name="presente[]">
				                </td>
				            </tr>'; 
				    $count_foro++;                
            }            
            if($count_foro>0)
            {
				$code='	<table id="tabla_Alumno_'.$Id.'"" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Apellidos</th>
                                <th class="hidden-xs-portrait">Nombres</th>
                                <th>Estado</th>
                            </tr>
                            </thead>
                            <tbody>
                            '.$lista.'                           
                            </tbody>
                        </table>';
            }
			return $code;
		}
	
		function AAS($query,$Id)
		{
			$lista=NULL;
			$count_foro=0;
			$code=NULL;
            foreach ($query as $q) 
            {           	
               
                    $lista.='<tr>
				                <td>'.$q->username.'</td>
				                <td>'.$q->last_name.'<d>
				                <td class="hidden-xs-portrait">'.$q->first_name.'</td>
				                <td>
				                		<input type="text" size="5" name="notas[]" id="normal-field" class="form-control input_nota" placeholder="Nota">
				                	
				                </td>
				            </tr>'; 
				    $count_foro++;                
            }            
            if($count_foro>0)
            {
				$code='	<table id="tabla_Alumno_'.$Id.'"" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Apellidos</th>
                                <th class="hidden-xs-portrait">Nombres</th>
                                <th>Nota</th>
                            </tr>
                            </thead>
                            <tbody>
                            '.$lista.'                           
                            </tbody>
                        </table>';
            }
			return $code;
		}

		function opciones_alumnos($Notas=NULL,$Hoja_Vida=NULL,$asignatura=NULL,$set_notas_cursos,$Id_Alumno,$nombre,$Id_Profesor,$Id_Alumno_users)
		{
			$paneles_acordion='';
			if($Notas!= NULL)
			{
				$i=0;
				foreach ($asignatura as $asi) 
				{
					$i++;
					$in="";
					if($i===1){$in="in";}

					$paneles_acordion.='
							<div class="panel" style="background-color:rgba(137, 137, 137, 1)">
                                <div class="panel-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse'.$i.$Id_Alumno.'">
                                        '.$asi->Nombre.'
                                    </a>
                                </div>
                                <div id="collapse'.$i.$Id_Alumno.'" class="panel-collapse '.$in.' collapse" style="height: auto;">
                                    <div class="panel-body">
                                       '.set_notas_acordion($set_notas_cursos,$Notas,$Id_Alumno,$asi->Id_Cha).'
                                    </div>
                                </div>
                            </div>';
					
				}

			}
			else
			{
				$paneles_acordion='<h3>No hay evaluaciones registradas</h3>';
			}
			$paneles_acordion=' <div class="panel-group" id="accordion'.$Id_Alumno.'">'.$paneles_acordion.'</div>';
			$modal_acordion=acordion_modal($paneles_acordion,$Id_Alumno,$nombre);
			$modal_hoja_vida=hvida_modal($Hoja_Vida,$Id_Alumno,$nombre);
			$modal_anotacion=anotaciones_modal($Id_Alumno_users,$Id_Profesor);
			return $modal_acordion.$modal_hoja_vida.$modal_anotacion;
		}

		function set_notas_acordion($set_notas_cursos,$Notas=NULL,$Id_Alumno,$Id_Cha)
		{
			if($set_notas_cursos!=NULL)
			{
				$row="";
				$count_set_nota=0;
				$count_re=0;
            	$options="";
            	$td_respaldo="";
            	$td="";
            	foreach ($set_notas_cursos as $s) 
            	{
            		if($Id_Cha==$s->Id_Cha)
            		{	            		     		
	            		$nota=""; 
		            	foreach ($Notas as $n) 
		            	{	 		            		         		
		            		if($Id_Alumno==$n->Alumno_Id && $s->Id==$n->Id_Set)
		            		{		            			
		            			$nota=$n->Nota;	   			
		            		}	          		
		            	}
		            	$count_set_nota++;
		            	$row.='
		            		<tr colo>
		            			<td style="color:#ffffff">'.$count_set_nota.'</td>
		            			<td style="color:#ffffff">'.$s->Tipo.'</td>
		            			<td style="color:#ffffff">'.$s->Fecha.'</td>
		            			<td style="color:#ffffff">'.$nota.'</td>
		            		</tr>';
	            	}
            	}
            	$td_respaldo="";
				$code='	<table class="table table-hover ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Tipo</th>
                                <th>Fecha</th>
                                <th>Nota</th>
                            </tr>
                            </thead>
                            <tbody>'.$row.'</tbody>
                        </table>';
            	return $code;
			}
			else
			{
				return "<h3> No hay notas programadas</h3>";
			}
		}

		function acordion_modal($acordion,$Id_Alumno,$nombre)
		{
			$btn=' <button type="button" class="btn btn-success" data-toggle="modal" data-target="#notas_alumno_'.$Id_Alumno.'">Notas</button>
                        <div id="notas_alumno_'.$Id_Alumno.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Notas de '.$nombre.'</h4>
                                    </div>
                                    <div class="modal-body">
                                    '.$acordion.'
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    </div>

                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>';
			return $btn;
		}

		function hvida_modal($hoja,$Id_Alumno,$nombre_alumno)
		{
			if($hoja!=NULL)
			{
				$rows="";
				$i=0;
				foreach ($hoja as $q) 
				{	
					if($q->Alumno_Id=$Id_Alumno)
					{
						if($q->Tipo)
						{
							$tipo='<span class="badge badge-success">Positiva</span>';
						}
						else
						{
							$tipo='<span class="badge badge-danger">Negativa</span>';
						}
						$i++;
						$rows.='
			            		<tr colo>
			            			<td style="color:#ffffff">'.$i.'</td>
			            			<td style="color:#ffffff">'.name($q->first_name,$q->last_name).'</td>
			            			<td style="color:#ffffff">'.$q->Date.'</td>
			            			<td style="color:#ffffff">'.$q->Descripcion.'</td>
			            			<td style="color:#ffffff">'.$tipo.'</td>
			            		</tr>';
		            }

				}
				$tabla='<table class="table table-hover" style="background-color:rgba(137, 137, 137, 1)">
                            <thead>
                            <tr>
                                <th>#</th>                                
                                <th>Profesor</th>
                                <th>Comentario</th>
                                <th>Tipo</th>
                            </tr>
                            </thead>
                            <tbody>'.$rows.'</tbody>
                        </table>';
				$btn=' <button type="button" class="btn btn-info" data-toggle="modal" data-target="#hoja_alumno_'.$Id_Alumno.'">Hoja de vida</button>
	                        <div id="hoja_alumno_'.$Id_Alumno.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	                            <div class="modal-dialog">
	                                <div class="modal-content">

	                                    <div class="modal-header">
	                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                                        <h4 class="modal-title" id="myModalLabel">Notas de '.$nombre_alumno.'</h4>
	                                    </div>
	                                    <div class="modal-body">
	                                    '.$tabla.'
	                                    </div>
	                                    <div class="modal-footer">
	                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	                                    </div>

	                                </div><!-- /.modal-content -->
	                            </div><!-- /.modal-dialog -->
	                        </div>';
				return $btn;
			}
			else
			{
				return "";
			}
		}

		function anotaciones_modal($Id_Alumno,$Id_profesor)
		{
			$codigo='<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#anotar_alumno_'.$Id_Alumno.'">Anotación</button>
			<div id="anotar_alumno_'.$Id_Alumno.'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel2">Anotaciones Hoja de Vida</h4>
                                    </div>
                                    <div class="modal-body"> 
                                        <div style="height:120px">
                                            <h4>Comentario</h4>
                                            <textarea rows="4" class="form-control" name="comentario_anotacion'.$Id_Alumno.'" required></textarea>
                                        </div>
                                        <div style="height:40px">
                                            <h4>Tipo</h4>
                                            <select  name="tipo_anotacion'.$Id_Alumno.'" class="selectpicker" data-style="btn-default" tabindex="-1" id="simple-colored">
                                                                <option value="0">Negativa</option>
                                                                <option value="1">Postivia</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cierrar</button>
                                        <button type="button" name="Save_alumno" id="'.$Id_Alumno.'" class="btn btn-primary save_anotacion">Guardar</button>
                                    </div>

                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                </div>';
                return $codigo;
		}
	}
?>