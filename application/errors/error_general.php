
<!DOCTYPE html>
<html>
<?php $url="/tesis/assets/white/";?>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>Error 404 - Pagina no encontrada</title>
    <link href="<?=$url?>css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="<?=$url?>img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script src="<?=$url?>lib/jquery/jquery-2.0.3.min.js"> </script>
	<script src="<?=$url?>lib/jquery-pjax/jquery.pjax.js"></script>
    <script src="<?=$url?>lib/parsley/parsley.js"> </script>
    <script src="<?=$url?>lib/backbone/underscore-min.js"></script>
    <script src="<?=$url?>js/settings.js"> </script>
</head>
<body>
<div class="single-widget-container error-page">
    <section class="widget transparent widget-404">
        <div class="body">
            <div class="row">
                	<div id="container">
						<h2 style="color:white"><?php echo $heading; ?></h2>
						<h3 style="color:white"><?php echo $message; ?></h3>
					</div>  
            </div>
        </div>
    </section>    
</div>
</body>
</html>