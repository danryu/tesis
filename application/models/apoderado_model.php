<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Apoderado_model extends CI_MODEL {
 
    function __construct()
    {
    	parent::__construct();
    	$this->load->database();
    	$this->load->library('security_lib');
    	$this->load->library('whatsapp');
    	$this->load->library('tw_msg');
    	$this->load->library('ion_auth');
    }
    public function get_id_apoderado($id_user){
        try
        {
            $this->db->select('*');  
            $this->db->from('users');  
            $this->db->join('apoderado',  'users.id = apoderado.Usuario_Id', 'INNER');
            $this->db->where('apoderado.Usuario_Id',$id_user);
            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $q=$query->row()->Id;
                $r=array('bool'=>TRUE,'query'=>$q);
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ENCONTRO APODERADO');
                return $r;
            }

        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR APODERADO');
            return $r;
        }
    }
    public function get_id_alumno($id_user){
        try
        {
            $this->db->select('*');  
            $this->db->from('users');  
            $this->db->join('alumno',  'users.id = alumno.Usuario_Id', 'INNER');
            $this->db->where('alumno.Usuario_Id',$id_user);
            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $q=$query->row()->Id;
                $r=array('bool'=>TRUE,'query'=>$q);
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
                return $r;
            }

        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ALUMNOS');
            return $r;
        }
    }
    public function get_alumnos($id_apoderado){
        try
        {
            $this->db->select('alumno.Id as Alumno_Id, users.Id as Usuario_Id, users.first_name, users.last_name');  
            $this->db->from('users');  
            $this->db->join('alumno',  'users.id = alumno.Usuario_Id', 'INNER');
            $this->db->where('alumno.Apoderado_Id', $id_apoderado);
            $this->db->where('alumno.hide', false);
            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $r=array('bool'=>TRUE,'one'=>$query->row(),'query'=>$query->result());
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
                return $r;
            }

        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ALUMNOS');
            return $r;
        }
    }
    public function get_alumnos_and_cursos($id_apoderado)
    {
        try
        {
            $this->db->select('users.first_name, users.last_name, curso.Grado, curso.Letra,alumno.Id as Alumno_Id, users.id as Users_Id');  
            $this->db->from('users');  
            $this->db->join('alumno',  'users.id = alumno.Usuario_Id', 'INNER');
            $this->db->join('curso_has_alumno',  'curso_has_alumno.Alumno_Id = alumno.Id', 'INNER');
            $this->db->join('curso',  'curso.id = curso_has_alumno.Curso_Id', 'INNER');
            $this->db->where('alumno.Apoderado_Id', $id_apoderado);
            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $q=$query->result();
                $r=array('bool'=>TRUE,'query'=>$q,'cantidad'=>$query->num_rows());
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
                return $r;
            }

        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ALUMNOS');
            return $r;
        }
    }
    public function get_asistencia_alumno($id_alumno){
        try
        {
            $this->db->select('*');  
            $this->db->from('alumno');  
            $this->db->join('asistencia',  'alumno.Id = asistencia.Alumno_Id', 'INNER');
            $this->db->where('alumno.Id = $id_alumno');
            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $q=$query->result();
                $r=array('bool'=>TRUE,'query'=>$q);
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
                return $r;
            }

        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ALUMNOS');
            return $r;
        }
    }
    public function get_profesores_alumno($id_alumno){
        try
        {
            $this->db->SELECT('profesor.Id,users.first_name, users.last_name, users.email, asignatura.Nombre as materia,profesor_has_asignatura.Curso_has_asignatura_Id'); 
            $this->db->FROM('alumno'); 
            $this->db->JOIN('curso_has_alumno', 'alumno.Id = curso_has_alumno.Alumno_Id');
            $this->db->JOIN('curso_has_asignatura', 'curso_has_alumno.Curso_Id = curso_has_asignatura.Curso_Id');
            $this->db->JOIN('profesor_has_asignatura', 'curso_has_asignatura.Id = profesor_has_asignatura.Curso_has_Asignatura_Id');
            $this->db->JOIN('profesor', 'profesor_has_asignatura.Profesor_Id = profesor.Id');
            $this->db->JOIN('users', 'profesor.Usuario_Id = users.id');
            $this->db->JOIN('asignatura', 'curso_has_asignatura.Asignatura_Id = asignatura.Id');
            $this->db->WHERE('alumno.Id', $id_alumno);
            $this->db->WHERE('profesor_has_asignatura.hide', false);
            $this->db->group_by('profesor_has_asignatura.Curso_has_Asignatura_Id'); 

            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $q=$query->result();
                $r=array('bool'=>TRUE,'query'=>$q);
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON PROFESORES 1');
                return $r;
            }

        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR PROFESORES 2');
            return $r;
        }
    }
    public function verificar_alumno_apoderado($id_alumno,$id_apoderado)
    {
        $this->db->select('*');  
        $this->db->from('alumno');  
        $this->db->join('apoderado',  'apoderado.Id = alumno.Apoderado_Id', 'INNER');
        $this->db->join('users', 'users.Id = apoderado.Usuario_id', 'INNER');
        $this->db->where('alumno.Id = '.$id_alumno);
        $this->db->where('users.Id = '.$id_usuario);
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //ACADEMIA
    public function get_notas_alumno($id_alumno)
    {
        $this->db->select('asignatura.Nombre, asignatura.Id as Asignatura_Id,set_nota.Id as set_nota_Id, set_nota.Fecha, set_nota.Tipo, set_nota.Registrada, nota.Nota');  
        $this->db->from('alumno');  
        $this->db->join('curso_has_alumno',  'alumno.Id = curso_has_alumno.Alumno_Id', 'INNER');
        $this->db->join('curso_has_asignatura', 'curso_has_alumno.Curso_Id = curso_has_asignatura.Curso_Id', 'INNER');       
        $this->db->join('asignatura','asignatura.Id = curso_has_asignatura.Asignatura_Id','INNER');
        $this->db->join('set_nota','set_nota.Curso_has_asignatura_Id = curso_has_asignatura.Id','INNER');
        $this->db->join('nota','nota.Set_nota_Id = set_nota.Id','INNER');
        $this->db->where('alumno.Id',$id_alumno);
        $this->db->where('nota.Alumno_Id',$id_alumno);
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            $q=$query->result();
            $r=array('bool'=>TRUE,'query'=>$q);
            return $r;
        }
        else
        {
            $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
            return $r;
        }
    }
    public function get_asignaturas_alumno($id_alumno)
    {
        $this->db->select('asignatura.Nombre, asignatura.Id as Asignatura_Id');  
        $this->db->from('alumno');  
        $this->db->join('curso_has_alumno',  'alumno.Id = curso_has_alumno.Alumno_Id', 'INNER');
        $this->db->join('curso_has_asignatura', 'curso_has_alumno.Curso_Id = curso_has_asignatura.Curso_Id', 'INNER');       
        $this->db->join('asignatura','asignatura.Id = curso_has_asignatura.Asignatura_Id','INNER');
        $this->db->where('alumno.Id',$id_alumno);
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            $q=$query->result();
            $r=array('bool'=>TRUE,'query'=>$q);
            return $r;
        }
        else
        {
            $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
            return $r;
        }
    }
    public function get_set_notas_alumno($id_alumno)
    {
        $this->db->select('asignatura.Id as Asignatura_Id, set_nota.Fecha,set_nota.Id as set_nota_Id, set_nota.Tipo, set_nota.Registrada');  
        $this->db->from('alumno');  
        $this->db->join('curso_has_alumno',  'alumno.Id = curso_has_alumno.Alumno_Id', 'INNER');
        $this->db->join('curso_has_asignatura', 'curso_has_alumno.Curso_Id = curso_has_asignatura.Curso_Id', 'INNER');       
        $this->db->join('asignatura','asignatura.Id = curso_has_asignatura.Asignatura_Id','INNER');
        $this->db->join('set_nota','set_nota.Curso_has_asignatura_Id = curso_has_asignatura.Id','INNER');
        $this->db->where('alumno.Id',$id_alumno);
        $this->db->order_by("set_nota.Fecha", "asc");
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            $q=$query->result();
            $r=array('bool'=>TRUE,'query'=>$q);
            return $r;
        }
        else
        {
            $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
            return $r;
        }
    }
    public function get_curso_has_asignatura($id_alumno)
    {
        $this->db->select('curso_has_asignatura.Id');  
        $this->db->from('alumno');  
        $this->db->join('curso_has_alumno',  'alumno.Id = curso_has_alumno.Alumno_Id', 'INNER');
        $this->db->join('curso_has_asignatura', 'curso_has_alumno.Curso_Id = curso_has_asignatura.Curso_Id', 'INNER');    
        $this->db->where('alumno.Id',$id_alumno);
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            $r=array('bool'=>TRUE,'query'=>$query->row()->Id);
            return $r;
        }
        else
        {
            $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON CURSOS');
            return $r;
        }
    }
    public function get_promedio_nota_alumno($id_alumno)
    {
        if(is_numeric($id_alumno))
        {
            $this->db->select('set_nota.Id as set_nota_Id, nota.Nota');  
            $this->db->from('set_nota');  
            $this->db->join('nota','nota.Set_nota_Id = set_nota.Id','INNER');
            $this->db->join('curso_has_asignatura', 'set_nota.Curso_has_asignatura_Id = curso_has_asignatura.Id ', 'INNER');    
            $this->db->join('curso_has_alumno',  'curso_has_alumno.Curso_Id = curso_has_asignatura.Curso_Id', 'INNER');
            $this->db->where('curso_has_alumno.Alumno_Id',$id_alumno);
            $this->db->where('set_nota.Registrada',true);

            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $q=$query->result();
                $r=array('bool'=>TRUE,'query'=>$q);
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON PROMEDIOS');
                return $r;
            }
        }
        else
        {
             $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON CURSOS');
                return $r;
        }        
    }
    public function get_hojaVida_alumno($id_alumno)
    {
        try
        {
            $this->db->select('users.first_name as NombreProfesor, users.last_name as ApellidoProfesor, hoja_vida.Descripcion, hoja_vida.Tipo, hoja_vida.Date');  
            $this->db->from('alumno');  
            $this->db->join('hoja_vida',  'alumno.Id = hoja_vida.Alumno_Id', 'INNER');
            $this->db->join('profesor',  'profesor.Id = hoja_vida.Profesor_Id', 'INNER');
            $this->db->join('users',  'profesor.Usuario_Id = users.id', 'INNER');
            $this->db->where('alumno.Id',$id_alumno);
            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $q=$query->result();
                $r=array('bool'=>TRUE,'query'=>$q);
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
                return $r;
            }

        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ALUMNOS');
            return $r;
        }
    }
    public function get_porfe_jefe_alumno($id_alumno)
    {
        try
        {
            $this->db->SELECT('profesor.Id,users.first_name, users.last_name, users.email'); 
            $this->db->FROM('alumno'); 
            $this->db->JOIN('curso_has_alumno', 'alumno.Id = curso_has_alumno.Alumno_Id');
            $this->db->JOIN('curso', 'curso.Id = curso_has_alumno.Curso_Id');
            $this->db->JOIN('profesor', 'curso.Profesor_Id = profesor.Id');
            $this->db->JOIN('users', 'profesor.Usuario_Id = users.id');
            $this->db->WHERE('alumno.Id', $id_alumno);
            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $q=$query->row();
                $r=array('bool'=>TRUE,'query'=>$q);
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON PROFESORES 1');
                return $r;
            }

        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR PROFESORES 2');
            return $r;
        }
    }
    public function get_horario_alumno($id_alumno)
    {
        $this->db->select('horario.Id, curso_has_alumno.Curso_Id, curso_has_asignatura.Id as cha_Id,asignatura.Nombre, horario.Temporada, horario.Año, horario.Dia,bloques.Hora_Inicio,bloques.Hora_Fin');  
        $this->db->from('alumno');  
        $this->db->join('curso_has_alumno',  'alumno.Id = curso_has_alumno.Alumno_Id', 'INNER');
        $this->db->join('curso_has_asignatura', 'curso_has_alumno.Curso_Id = curso_has_asignatura.Curso_Id', 'INNER');    
        $this->db->join('asignatura', 'curso_has_asignatura.Asignatura_Id = asignatura.Id', 'INNER');   
        $this->db->join('horario', 'curso_has_asignatura.Id = horario.Curso_has_Asignatura_Id', 'INNER');   
        $this->db->join('bloques', 'bloques.Id = horario.Bloques_Id', 'INNER');   
        $this->db->where('alumno.Id',$id_alumno);        
        $query = $this->db->get();             
        if($query->num_rows() > 0 )
        {
            //var_dump($query->result());
            $r=array('bool'=>TRUE,'query'=>$query->result());
            return $r;
        }
        else
        {
            $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
            return $r;
        }
    }
    //DASHBOARD
    public function get_reuniones_alumno($id_alumno,$hoy=null)
    {
        $this->db->select('reuniones.Fecha, reuniones.Motivo');  
        $this->db->from('reuniones');  
        $this->db->join('curso',  'curso.Id = reuniones.Curso_Id', 'INNER');
        $this->db->join('curso_has_alumno', 'curso_has_alumno.Curso_Id = curso.Id', 'INNER');
        $this->db->join('alumno', 'alumno.Id = curso_has_alumno.Alumno_Id','INNER');
        $this->db->where('alumno.Id',$id_alumno);
        if($hoy || is_null($hoy))
            $this->db->where('reuniones.Fecha >= CURDATE()');
        $this->db->order_by("Fecha", "desc");
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            $q=$query->result();
            $r=array('bool'=>TRUE,'query'=>$q);
            return $r;
        }
        else
        {
            $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON REUNIONES');
            return $r;
        }
    }
    public function get_pruebas_news($id_alumno)
    {
        $this->db->select('set_nota.Fecha, set_nota.Tipo, asignatura.Nombre,set_nota.Registrada');  
        $this->db->from('alumno');  
        $this->db->join('curso_has_alumno',  'alumno.Id = curso_has_alumno.Alumno_Id', 'INNER');
        $this->db->join('curso_has_asignatura', 'curso_has_alumno.Curso_Id = curso_has_asignatura.Curso_Id', 'INNER');
        $this->db->join('asignatura', 'curso_has_asignatura.Asignatura_Id = asignatura.Id', 'INNER');
        $this->db->join('set_nota', 'set_nota.Curso_has_asignatura_Id = curso_has_asignatura.Id','INNER');
        //$this->db->join('nota','nota.Set_nota_Id = set_nota.Id','INNER');
        $this->db->where('alumno.Id',$id_alumno);
         $this->db->where('set_nota.Fecha >= CURDATE()');
         $this->db->where('set_nota.Registrada',false);
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            $q=$query->result();
            $r=array('bool'=>TRUE,'query'=>$q);
            return $r;
        }
        else
        {
            $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
            return $r;
        }
    }
    public function get_bloques_horario()
    {
        $this->db->select('bloques.Hora_Inicio,bloques.Hora_Fin');  
        $this->db->from('bloques');  
        $this->db->where('bloques.hide',0);
        $this->db->order_by("bloques.Hora_Inicio", "ASC");
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            $q=$query->result();
            $r=array('bool'=>TRUE,'query'=>$q);
            return $r;
        }
        else
        {
            $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON REUNIONES');
            return $r;
        }
    }
    public function get_notificacion_news($id_alumno,$id_ap)
    {
        
    }

}