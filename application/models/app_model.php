    <?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class App_model extends CI_MODEL {
 
    function __construct()
    {
    	parent::__construct();
    	$this->load->database();
        $this->load->model('security_model');
        $this->load->model('profesor_model');
        $this->load->library('tw_msg');
        $this->load->library('app');
    	$this->load->library('security_lib');
    	$this->load->library('send_msg');

    }
    function inbox_load($Id)
    {
    	try
    	{
            $contNew=0;
            $contAll=0;
    		if(isset($Id))
    		{
	    		$this->db->select('users.first_name,users.last_name, mensaje.Id as Id,Titulo,Contenido,Fecha,mensaje.hide,Destino_id,Origen_id,visto');  
				$this->db->from('mensaje');  
				$this->db->join('origen_destino',  'mensaje.Id = Mensaje_Id', 'INNER');
                $this->db->join('users',  'users.id = origen_destino.Origen_id', 'INNER');
				$this->db->where('origen_destino.Destino_id',$Id);
                $this->db->order_by("mensaje.Id","desc"); 
				$query = $this->db->get();
                if($query->num_rows()>0)
                {
    	    		$query=$query->result();
                    foreach ($query as $row) 
                    {
                        if(!$row->hide)
                        {
                            if(!$row->visto)
                            {
                                $contNew++;
                            }
                            $contAll++;
                        }
                    }
    	    		$r=array('bool'=>TRUE,'query'=>$query,'n_new'=>$contNew,'n_all'=>$contAll);
    	    		return $r;
                }
                else
                {
                    $r=array('bool'=>FALSE,'msg'=> 'No tiene Mensajes');
                    return $r;
                }
    		}
    		else
    		{
    			$r=array('bool'=>FALSE,'msg'=>'No hay usuario');
				return $r;
    		}
    	}
    	catch(Exception $e)
    	{
    		$r=array('bool'=>FALSE,'msg'=>'Error en la busqueda de mensajes');
			return $r;
    	}
    }
    function my_alert($Id_Message,$Id_User)
    {

        try
        {
            if(!is_null($Id_User) && !is_null($Id_Message))
            {
                $this->db->select('mensaje.Id as IdMensaje,Titulo,Contenido, origen_destino.Id as IdOD, Destino_id,Origen_id,visto'); 
                //$this->db->select('*');                
                $this->db->from('mensaje');                  
                $this->db->join('origen_destino',  'mensaje.Id =Mensaje_Id', 'INNER');
                $this->db->where('origen_destino.Destino_id',$Id_User);
                $this->db->where('mensaje.Id', $Id_Message);
                $query = $this->db->get();
                //echo $Id_User.'|'.$Id_Message;
                if($query->num_rows()>0)
                {
                    $query=$query->result(); 
                    $r = array('bool' => true, 'query'=>$query);
                    return $r;  
                }
                else
                {
                    $r = array('bool' => false, 'msg'=>'Error en Usuario o Mensaje' );
                    return $r;
                }
            }
            else
            {
                $r = array('bool' => false, 'msg'=>'Error en Usuario o Mensaje' );
                return $r;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }
    }
    function change_see($Id_Destino,$Id_Mensaje)//ID Origen_Destino
    {
        try
        {          
            if($Id_Mensaje!=NULL || $Id_Message != "")  
            {
                $visto=array('visto'=>'1');
                $this->db->where('Mensaje_Id', $Id_Mensaje);
                $this->db->where('Destino_id', $Id_Destino);
                $this->db->update('origen_destino', $visto);
                $r = array('bool' => true, 'msg'=> 'cambio realizado');
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'Escriba una ID de mensaje');
                return $r;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }
    } 
    function check_see($Id_Destino,$Id_Mensaje)
    {
                $this->db->select('visto');                
                $this->db->from('origen_destino'); 
                $this->db->where('Destino_id',$Id_Destino);
                $this->db->where('Mensaje_Id', $Id_Mensaje);
                 $this->db->where('visto', '0');
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query=$query->result(); 
                    $r = array('bool' => true, 'query'=>$query);
                    return $r;  
                }
                else
                {
                    $r = array('bool' => false, 'msg'=>'Error en Usuario o Mensaje' );
                    return $r;
                }
    }
    function save_alert($mensaje)
    {        
        try
        {
            if(isset($mensaje))
            {
                $this->db->insert('mensaje',$mensaje);
                $id=mysql_insert_id();
                $r = array('bool' => true, 'id'=> $id);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'Error en la bd');
                return $r;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }
    }    
    function find_curso($Id_Curso,$user_type)
    {
        try
        {
            $Agno=date('Y');
            if(isset($Id_Curso) && isset($user_type))
            {

                switch ($user_type) 
                {    
                    case 'apoderado':
                            $this->db->select('users.Id,users.email,users.cellphone,users.Id_twitter');  
                            $this->db->from('users');  
                            $this->db->join('apoderado',  'users.Id = apoderado.Usuario_id', 'INNER');
                            $this->db->join('alumno',  'apoderado.Id = alumno.Apoderado_Id', 'INNER');
                            $this->db->join('curso_has_alumno',  'curso_has_alumno.Alumno_id = alumno.Id', 'INNER');
                            $this->db->join('curso',  'curso_has_alumno.Curso_Id = curso.Id', 'INNER');
                            $this->db->where('curso.Id',$Id_Curso);
                            $this->db->where('curso_has_alumno.Año',$Agno);
                            $query = $this->db->get();
                            if($query->num_rows()>0)
                            {
                                $query=$query->result(); 
                                $r = array('bool' => true, 'query'=> $query);
                                return $r;
                            }
                            else
                            {
                                $r = array('bool' => false, 'msg'=> 'No hay Apoderados');
                                return $r; 
                            }
                            break;

                    case 'alumno':                        
                            $this->db->select('users.Id,users.email,users.cellphone,users.Id_twitter');  
                            $this->db->from('users');  
                            $this->db->join('alumno',  'alumno.Usuario_id = users.Id', 'INNER');
                            $this->db->join('curso_has_alumno',  'curso_has_alumno.Alumno_id = alumno.Id', 'INNER');
                            $this->db->join('curso',  'curso_has_alumno.Curso_Id = curso.Id', 'INNER');
                            $this->db->where('curso.Id',$Id_Curso);
                            $this->db->where('curso_has_alumno.Año',$Agno);
                            $query = $this->db->get();
                            if($query->num_rows()>0)
                            {
                                $query=$query->result(); 
                                $r = array('bool' => true, 'query'=> $query);
                                return $r;
                            }
                            else
                            {
                                $r = array('bool' => false, 'msg'=> 'No hay alumnos');
                                return $r; 
                            }
                            break;

                    default:
                            $r = array('bool' => false, 'msg'=> 'No seleccionado');
                            return $r; 
                            break;
                    
                }
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'falta información');
                return $r;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }
    }
    function save_OD($Id_Curso,$id_origen,$id_msg,$user_type,$lvl=NULL,$mensaje,$title)
    {
        try
        {
            if(isset($Id_Curso) && isset($id_origen) && isset($id_msg) && isset($user_type) && isset($mensaje))
            {
                $users=$this->find_curso($Id_Curso,$user_type);
                if($users['bool'])
                {
                    $users=$users['query'];
                    foreach ($users as $q) 
                    {
                        $var=array('Destino_id'=>$q->Id,'Origen_id'=>$id_origen,'Mensaje_Id'=>$id_msg,'visto'=>0);
                        $this->db->insert('origen_destino',$var);
                        $this->send_alert($q->Id,$q->email,$q->cellphone,$id_msg,$lvl,$mensaje,$title,$q->Id_twitter);
                    }
                    $r = array('bool' => false, 'msg'=> 'Mensajes Enviados');
                    return $r;
                }
                else
                {
                    $r = array('bool' => false, 'msg'=> 'No se encontraron usuarios');
                    return $r;
                }
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'falta información');
                return $r;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }
    }    
    function send_alert($id_user,$correo,$cellphone,$id_mensaje,$nivel,$mensaje,$title,$Id_twitter)
    {
        try
        {    
            $funca=$this->app->mandrill($correo,$mensaje,$title);
            if($nivel==2)
            {
                //API CORREO + Whatsapp
                $user=$this->load_user($id_user,"Apoderado");
                $user=$user['query'];
                $mensaje=$title.'-'.$mensaje;                  
                $r=$this->send_msg->twitter($Id_twitter,$mensaje);                

            }
            if($nivel==3)
            {
                //new_alert
                $var = array('type' =>'new_alert' , 'to'=>$cellphone);
                $r=$this->tw_msg->send($var);
            }
            if($nivel==4)
            {
                //Api correo + whatsapp + SMS => FORCE
                // $datos = array('' => , );
                $r=$this->whatsapp->send_wsp($datos);
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }
    }
    private function load_user($id_user,$type=NULL)
    {
        try
        {   
            if($type!=NULL)
            {
                $this->db->select('users.Id,users.email,users.Id_twitter,users.cellphone,apoderado.Wsp,apoderado.Sms');  
                $this->db->from('users');  
                $this->db->join('apoderado',  'users.Id = apoderado.Usuario_Id', 'INNER');
                $this->db->where('users.Id',$id_user);
                $query = $this->db->get();
                if($query->num_rows() > 0 )
                {
                    $q=$query->row();
                    $r=array('bool'=>TRUE,'query'=>$q);
                    return $r;
                }
            }
            else
            {
                $users = $this->db->get_where('users',array('Id' =>$id_user));
                return $users;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }
    }
    function load_curso($var)
    {
        $Agno=date("Y-01-01"); 
        try
        {
            if(isset($var['Id_User']))
            {
                $final=NULL;
                $Id_User=$var['Id_User'];
                $fr=$this->security_model->find_rol($Id_User);
                if($fr['bool'])
                {
                    $query=$fr['query'];
                    foreach ($query as $row) 
                    {
                        $group=$row->group_id;
                        if($group==1)//Cargar todos los cursos [ADMIN]
                        {
                            $admin = $this->db->get_where('curso',array('Agno' =>$Agno));
                            if($admin->num_rows()>0)
                            {
                                $admin = $admin->result();
                                $final['bool_admin_cursos']=TRUE;
                                $final['query_admin_cursos']=$admin;                                
                            }
                            else
                            {
                                $final['bool_admin']=FALSE;
                            }
                            $this->db->select('*');                
                            $this->db->from('profesor');                  
                            $this->db->join('users',  'users.Id =Usuario_Id', 'INNER');
                            $this->db->where('users.active',1);
                            $query = $this->db->get();
                            if($query->num_rows()>0)
                            {
                                $query = $query->result();
                                $final['bool_admin_profes']=TRUE;
                                $final['query_admin_profes']=$query;
                            }
                            else
                            {

                            }
                        }
                        if($group==4)//Cargar todos los cursos [Profesor]                 
                        {
                            $cp=$this->profesor_model->load_cursos($Id_User);                            
                            if($cp['bool'])
                            {
                                $final['bool_profe']=TRUE;
                                $final['query_profe']=$cp['query'];
                            }
                            else
                            {
                                $final['bool_profe']=FALSE;
                            }
                            $cpj=$this->profesor_model->load_cursos_jefe($Id_User);
                            if($cpj['bool'])
                            {
                                $final['bool_jefe']=TRUE;
                                $final['query_jefe']=$cpj['query'];
                            }
                            else
                            {
                                $final['bool_profe']=FALSE;
                            }
                        }
                    }
                    if($final!=null)
                    {
                        $r = array('bool' => true, 'query'=> $final);
                        return $r;
                    }
                    else
                    {
                        $r = array('bool' => false, 'msg'=> 'Error al cargar cursos');
                        return $r;
                    }
                    
                }
                else
                {
                    $r = array('bool' => false, 'msg'=> 'No se encontraron roles para el usuario');
                    return $r;
                }
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'Falta usuario ID');
                return $r;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }    
    }   
     
   
}
?>