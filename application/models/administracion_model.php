<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Administracion_model extends CI_MODEL {
 
    function __construct()
    {
    	parent::__construct();
    	$this->load->database();
    	$this->load->library('security_lib');
    	$this->load->library('whatsapp');
    	$this->load->library('tw_msg');
    	$this->load->library('ion_auth');

    }
    //PREPARADO PARA LA DEMOSTRACION
    private function password_gen() 
    {
    	$this->security_lib->generator();
    	return $pass;
    }
    public function get_number($nombre_tabla){
    	try
    	{
    		$this->db->select('*');  
			$this->db->from($nombre_tabla);
			$this->db->where('hide',0);
			$query = $this->db->get();
			if($query->num_rows() > 0 )
			{
				return $query->num_rows();
			}
			else
			{
    			return 0;
			}
    	}
    	catch(Exception $ex){
    		return 0;
    	}
    }
    public function verificar_datos($correo,$celular){
    	$contrasegna = $this->security_lib->generator();
        $this->db->select("");
        $this->db->from('users');
        $this->db->where('email',$correo);
        $this->db->like('cellphone',$celular,'before');
        $query = $this->db->get();

        if($query->num_rows() > 0){
        	$q=$query->row();
        	//$contrasegna = "1234";
        	try{
	    		$exito = $this->ion_auth->update($q->id, array('password'=>$contrasegna,'activation_code'=>NULL));
	    		if($exito){
	    			$r=array('bool'=>TRUE, 'query'=>$q);
		        	$varSms = array('type' =>'new_pass','to'=>$q->cellphone,'pass'=>$contrasegna );
		    		$SmsSend = $this->tw_msg->send($varSms);
		        	return $r;
	    		}
	    		else{
	    			redirect('auth/forgot_password?cambio_no_exitoso');
	    		}
        	}
        	catch(Exception $ex){
        		redirect('auth/forgot_password?problema_bd');
        	}
        	
        }
        else{
        	$r=array('bool'=>FALSE,'msg'=>'INGRESO DATOS INCORRECTOS');
        	return $r;
        }
    }
    public function get_log(){
    	try
    	{
    		$this->db->select('log.Descripcion as Descripcion, log.Tabla as Tabla, users.first_name as Nombre, users.last_name as Apellido');  
			$this->db->from('log');
			$this->db->order_by("log.Id","desc");
			$this->db->join('users','log.Usuario_id = users.id','INNER');
			$this->db->limit('5');
			$query = $this->db->get();
			if($query->num_rows() > 0 )
			{
				$q=$query->result();
				$r=array('bool'=>TRUE,'query'=>$q);
				return $r;
			}
			else
			{
				$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON LOGS','query'=>null);
    			return $r;
			}
		}
		catch(Exception $ex)
		{

		}
    }
    
    // ***********  APODERADO **********
    	public function get_nombre_apoderado($id_apoderado){
    		try{
	    		if(is_numeric($id))
	    		{
		    		$this->db->select('*');  
					$this->db->from('users');  
					$this->db->join('apoderado',  'users.Id = apoderado.Usuario_Id', 'INNER');
					$this->db->where('apoderado.Id',$id);
					$query = $this->db->get();
					if($query->num_rows() > 0 )
					{
						$q=$query->row();
						$r=array('bool'=>TRUE,'query'=>$q);
						return $r;
					}
					else
					{
						$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON APODERADOS','query'=>null);
		    			return $r;
					}
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'INGRESE UNA ID NUMERICA','query'=>null);
		    			return $r;
				}}

		    	catch(Exception $e)
		    	{
		    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR APODERADOS','query'=>null);
		    		return $r;
		    	}
	    	
	    }
	    public function add_apoderado($datos)
	    {
	    	$Rol_Id=4;//ROL ID DE APODERADO
	    	$id_user=NULL;
	    	$tabla='apoderado';
	    	//$contrasegna="123456789";
	    	//$contrasegna=$this->password_gen();
	    	try
	    	{
	    		if($this->existe_user($datos['rut']))
		    	{
		    		$explode = explode('/',$datos['f_nacimiento']);
	       			$dia = $explode[0];
	       			$mes = $explode[1];
	       			$año = $explode[2];
	       			$fecha = $mes.'/'.$dia.'/'.$año;		    		
	       			$newformat = date('Y/m/d',strtotime($fecha));
	       			$cellphone_ = '569'.$datos['t_celular'];
	       			$contrasegna = $datos['password'];
		    		$user_add = $this->ion_auth->register($datos['rut'], $contrasegna, $datos['correo'], array('sex' => $datos['sexo'], 'f_nacimiento' => $newformat, 'phone' => $datos['t_fijo'], 'cellphone' => $cellphone_, 'first_name' => $datos['nombre'], 'last_name' => $datos['apellido']), array(2));	
					if($user_add != FALSE)
			    	{
			    		$id_user=$user_add;
			    		if($id_user!==NULL)
			    		{
			    			try
			    			{
				    			$comuna=$datos['comuna'];
						    	$_datos_alumno=array(	'Comuna_id'=>$comuna,//$comuna, //Original Comuna_id : probando el try_catch
						    							'Domicilio'=>$datos['domicilio'],
						    							'Usuario_Id'=>$id_user,
						    							'Wsp'=>$datos['wsp'],
						    							'hide'=>0,
						    							'Sms'=>$datos['sms']);
					    		$this->db->insert($tabla,$_datos_alumno);
					    		$varSms = array('type' =>'password','to'=>$cellphone_,'pass'=>$contrasegna );
					    		$SmsSend = $this->tw_msg->send($varSms);
					    		if($SmsSend['bool'])
					    		{
					    			$r=array('bool'=>TRUE,'msg'=>'Funciona');
					    			return $r;
					    		}
					    		else
					    		{

					    			$r=array('bool'=>FALSE,'msg'=>'Error al enviar sms Apoerado');
					    			return $r;
					    		}
				    		}
				    		catch(Exception $e)
				    		{
				    			$this->ion_auth->delete_user($id_user);
				    			$r=array('bool'=>FALSE,'msg'=>'Error al crear Datos opcionales Apoderado');
					    		return $r;
				    		}	
				    	}
			    		else
			    		{

			    			$r=array('bool'=>FALSE,'msg'=>'Error de base de datos-> Usuario - Apoderado');
			    			return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
			    		}
			    	}
			    	else
			    	{
			    		$r=array('bool'=>FALSE,'msg'=>'Error al crear Usuario');
			    		return $r;
			    	}
		    	}
		    	else
		    	{

		    		$r=array('bool'=>FALSE,'msg'=>'Ya existe apoderado');
		    		return $r;
		    	}
	    	}
	    	catch(Exception $e)
	    	{
	    		if($id_user!==NULL)
			    {
			    	$this->ion_auth->delete_user($id_user);	
			    }
	    		$r=array('bool'=>FALSE,'msg'=>'Error del sistema de registro');
		    	return $r;
	    	}
	    }
	    public function all_Horario($id=NULL){
	    	try
	    	{
	    		$this->db->select('horario.Id as Id, horario.Temporada, horario.Año, horario.Dia, horario.Bloques_Id, horario.Curso_has_Asignatura_Id, bloques.Hora_Inicio, bloques.Hora_Fin, asignatura.Nombre, curso.Tipo, curso.Grado, curso.Letra');  
				$this->db->from('horario');
				$this->db->join('bloques','bloques.Id = horario.Bloques_Id','INNER');
				$this->db->join('curso_has_asignatura', 'curso_has_asignatura.Id = horario.Curso_has_Asignatura_Id','INNER');
				$this->db->join('curso','curso.Id = curso_has_asignatura.Curso_Id', 'INNER');
				$this->db->join('asignatura','asignatura.Id = curso_has_asignatura.Asignatura_Id','INNER');
				if($id != NULL){
					$this->db->where('horario.Id',$id);
				}
				$this->db->where('horario.hide',0);
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					if($id!=NULL){
						$q=$query->row();
					}
					else{
						$q=$query->result();
					}
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON HORARIO','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR HORARIO','query'=>null);
	    		return $r;
	    	}
	    }
	    public function all_Asignatura($id=NULL){
	    	try
	    	{
	    		$this->db->select('*');  
				$this->db->from('asignatura');
				if($id != NULL){
					$this->db->where('Id',$id);
				}
				$this->db->where('hide',0);
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					if($id != NULL){
						$q=$query->row();
					}
					else{
						$q=$query->result();
					}
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ASIGNATURA','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ASIGNATURA','query'=>null);
	    		return $r;
	    	}
	    }
	    public function all_Reuniones($id_profesor=NULL){
	    	try
	    	{
	    		$this->db->select('reuniones.Id as Id, reuniones.Fecha, reuniones.Curso_Id, reuniones.Hora, reuniones.Motivo');  
				$this->db->from('reuniones');
				$this->db->join('curso', 'reuniones.Curso_Id = curso.Id','INNER');
				if($id_profesor != NULL){
					$this->db->where('curso.Profesor_Id',$id_profesor);
				}
				$this->db->where('reuniones.hide',0);
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					$q=$query->result();
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ASIGNATURA','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ASIGNATURA','query'=>null);
	    		return $r;
	    	}
	    }
	    public function all_Profesor_Asignatura($id=NULL){
	    	try
	    	{
	    		$this->db->select('profesor_has_asignatura.Id as Id, users.first_name, users.last_name, profesor_has_asignatura.Profesor_Id, profesor_has_asignatura.Curso_has_Asignatura_Id, asignatura.Nombre,curso.Grado, curso.Tipo, curso.Letra');  
				$this->db->from('profesor_has_asignatura');
				$this->db->join('profesor','profesor.Id = profesor_has_asignatura.Profesor_Id');
				$this->db->join('users','profesor.Usuario_Id = users.Id','INNER');
				$this->db->join('curso_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id = curso_has_asignatura.Id','INNER');
				$this->db->join('curso','curso_has_asignatura.Curso_Id = curso.Id','INNER');
				$this->db->join('asignatura','curso_has_asignatura.Asignatura_Id = asignatura.Id','INNER'); 
				if($id != NULL){
					$this->db->where('profesor_has_asignatura.Id',$id);
				}
				$this->db->where('profesor_has_asignatura.hide',0);
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					if($id != NULL){
						$q=$query->row();
					}
					else{
						$q=$query->result();
					}
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ASOCIACIONES DE PROFESOR CON ASIGNATURA 1','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ASOCIACIONES DE PROFESOR CON ASIGNATURA 2','query'=>null);
	    		return $r;
	    	}
	    }
	    public function all_Curso_Asignatura($id=NULL){
	    	try
	    	{
	    		$this->db->select('curso_has_asignatura.Id as Id, Curso_Id, Asignatura_Id, curso.Tipo, curso.Grado, curso.Letra, asignatura.Nombre, curso.Agno');  
				$this->db->from('curso_has_asignatura');
				$this->db->join('curso','curso_has_asignatura.Curso_Id = curso.Id','INNER');
				$this->db->join('asignatura','curso_has_asignatura.Asignatura_Id = asignatura.Id','INNER');
				if($id != NULL){
					$this->db->where('curso_has_asignatura.Id',$id);
				}
				$this->db->where('curso_has_asignatura.hide',0);
				$this->db->order_by('Tipo','asc');
				$this->db->order_by('Grado','asc');
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					if($id != NULL){
						$q=$query->row();
					}
					else{
						$q=$query->result();
					}
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ASOCIACIONES DE CURSO CON ASIGNATURA','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ASOCIACIONES DE CURSO CON ASIGNATURA','query'=>null);
	    		return $r;
	    	}
	    }
	    public function all_Curso_Alumno($id=NULL){
	    	try
	    	{
	    		$this->db->select('curso_has_alumno.Año,curso_has_alumno.Id as Id, curso_has_alumno.Curso_Id, curso_has_alumno.Alumno_Id, users.first_name, users.last_name, curso.Tipo, curso.Grado, curso.Letra');  
				$this->db->from('curso_has_alumno');
				$this->db->join('curso','curso_has_alumno.Curso_Id = curso.Id','INNER');
				$this->db->join('alumno','curso_has_alumno.Alumno_Id = alumno.Id','INNER');
				$this->db->join('users','users.Id = alumno.Usuario_Id','INNER');  
				if($id != NULL){
					$this->db->where('curso_has_alumno.Id',$id);
				}
				$this->db->where('curso_has_alumno.hide',0);
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					if($id != NULL){
						$q = $query->row();
					}
					else{
						$q=$query->result();
					}
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ASOCIACIONES DE CURSO CON ALUMNO','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ASOCIACIONES DE CURSO CON ALUMNO','query'=>null);
	    		return $r;
	    	}
	    }
	    public function all_Curso_Alumno2($id_alumno=NULL){
	    	try
	    	{
	    		$this->db->select('curso_has_alumno.Año,curso_has_alumno.Id as Id, curso_has_alumno.Curso_Id, curso_has_alumno.Alumno_Id, users.first_name, users.last_name, curso.Tipo, curso.Grado, curso.Letra');  
				$this->db->from('curso_has_alumno');
				$this->db->join('curso','curso_has_alumno.Curso_Id = curso.Id','INNER');
				$this->db->join('alumno','curso_has_alumno.Alumno_Id = alumno.Id','INNER');
				$this->db->join('users','users.Id = alumno.Usuario_Id','INNER');  
				if($id_alumno != NULL){
					$this->db->where('curso_has_alumno.Alumno_Id',$id_alumno);	
				}
				$this->db->where('curso_has_alumno.hide',0);
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					if($id_alumno != NULL){
						$q = $query->row();
					}
					else{
						$q=$query->result();
					}
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ASOCIACIONES DE CURSO CON ALUMNO','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ASOCIACIONES DE CURSO CON ALUMNO','query'=>null);
	    		return $r;
	    	}
	    }
	    public function all_Bloques($id=NULL){
	    	try
	    	{
	    		$this->db->select('*');  
				$this->db->from('bloques');
				if($id != NULL){
					$this->db->where('Id',$id);
				}
				$this->db->where('hide',0);
				$this->db->order_by('Hora_Inicio','asc');

				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					if($id != NULL){
						$q=$query->row();
					}
					else{
						$q=$query->result();
					}
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{

					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON BLOQUES','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR BLOQUES','query'=>null);
	    		return $r;
	    	}
	    }
	    public function all_Apoderado($activo=TRUE,$id=NULL)
	    {
	    	try
	    	{
	    		$this->db->select('*');  
				$this->db->from('users');  
				$this->db->join('apoderado', 'users.id = apoderado.Usuario_Id', 'INNER');
				$this->db->join('comunas', 'apoderado.Comuna_Id = comunas.codigoInterno','INNER');
				if($id != NULL){
					$this->db->where('apoderado.Id',$id);
				}
				$this->db->where('hide',0);
				if($activo){
					$this->db->where('active','1');
				}
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					if($id != NULL)
					{
						$q=$query->row();
					}
					else
					{
						$q=$query->result();
					}
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON APODERADOS','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR APODERADOS','query'=>null);
	    		return $r;
	    	}
	    }	    
	    public function last_cinco_apoderados()
	    {
	    	try
	    	{
	    		$this->db->select('*');  
				$this->db->from('users');  
				$this->db->join('apoderado', 'users.id = apoderado.Usuario_Id', 'INNER');
				$this->db->where('hide',0);
				$this->db->order_by('users.id', 'DESC');
				$this->db->limit('5');
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					$q=$query->result();
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON APODERADOS','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR APODERADOS','query'=>null);
	    		return $r;
	    	}
	    }
	     public function last_cinco_alumno()
	    {
	    	try
	    	{
	    		$this->db->select('*');  
				$this->db->from('users');  
				$this->db->join('alumno', 'users.id = alumno.Usuario_Id', 'INNER');
				$this->db->where('hide',0);
				$this->db->order_by('users.id', 'DESC');
				$this->db->limit('5');
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					$q=$query->result();
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON APODERADOS','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR APODERADOS','query'=>null);
	    		return $r;
	    	}
	    }
	    public function last_cinco_profesor()
	    {
	    	try
	    	{
	    		$this->db->select('*');  
				$this->db->from('users');  
				$this->db->join('profesor', 'users.id = profesor.Usuario_Id', 'INNER');
				$this->db->where('hide',0);
				$this->db->order_by('users.id', 'DESC');
				$this->db->limit('5');
				$query = $this->db->get();
				if($query->num_rows() > 0 )
				{
					$q=$query->result();
					$r=array('bool'=>TRUE,'query'=>$q);
					return $r;
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON APODERADOS','query'=>null);
	    			return $r;
				}

	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR APODERADOS','query'=>null);
	    		return $r;
	    	}
	    }
	    public function ver_Apoderado($id)
	    {
	    	try
	    	{

	    		if(is_numeric($id))
	    		{
		    		$this->db->select('users.first_name, users.last_name, users.username, users.f_nacimiento, users.phone, users.cellphone, users.email, apoderado.Comuna_id, apoderado.Domicilio, apoderado.Wsp, apoderado.Sms');  
					$this->db->from('users');  
					$this->db->join('apoderado',  'users.Id = apoderado.Usuario_Id', 'INNER');
					$this->db->where('apoderado.Usuario_Id',$id);
					$query = $this->db->get();
					if($query->num_rows() > 0 )
					{
						$q=$query->row();
						$r=array('bool'=>TRUE,'query'=>$q);
						return $r;
					}
					else
					{
						$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON APODERADOS','query'=>null);

		    			return $r;

					}
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'INGRESE UNA ID NUMERICA','query'=>null);
		    			return $r;
				}
	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR APODERADOS','query'=>null);
	    		return $r;
	    	}
	    }
	    public function ver_Alumno($id)
	    {
	    	try
	    	{
	    		if(is_numeric($id))
	    		{
		    		$this->db->select('*');  
					$this->db->from('users');  
					$this->db->join('alumno',  'users.id = alumno.Usuario_id', 'INNER');
					$this->db->where('users.id',$id);
					$query = $this->db->get();
					if($query->num_rows() > 0 )
					{
						$q=$query->row();
						$r=array('bool'=>TRUE,'query'=>$q);
						return $r;
					}
					else
					{
						$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS','query'=>null);

		    			return $r;

					}
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'INGRESE UNA ID NUMERICA','query'=>null);
		    			return $r;
				}
	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ALUMNOS','query'=>null);
	    		return $r;
	    	}
	    }
	    public function edit_apoderado($datos)
	    {
	    	$Rol_Id=2;//ROL ID DE APODERADO
	    	$tabla='apoderado';
	    	$_datos_usuario=array();
	    	try
	    	{
	    		if($this->existe_user_edit($datos['rut'],$datos['Id_Us']))
		    	{
		    		$_datos_apoderado=array('Comuna_Id'=>$datos['comuna'],
					    				'Domicilio'=>$datos['domicilio'],
					    				'Wsp'=>$datos['wsp'],
					    				'Sms'=>$datos['sms']);
		    		$this->db->where('Id', $datos['Id_Ap']);
	       			$this->db->update('apoderado', $_datos_apoderado);
	       			$check=$datos['cam_pass'];
	       			if($check)
	       			{
	       				$_datos_usuario['Password']=$datos['password'];
	       			}
	       			else
	       			{
	       				$pass_cryp=$this->security_lib->cryp_pass($datos['password']);
	       				$_datos_usuario['Password']=$pass_cryp;

	       			}
	       			$explode = explode('/',$datos['f_nacimiento']);
	       			$dia = $explode[0];
	       			$mes = $explode[1];
	       			$año = $explode[2];
	       			$fecha = $mes.'/'.$dia.'/'.$año;		    		
	       			$newformat = date('Y/m/d',strtotime($fecha));
		    		$user_add = $this->ion_auth->update($datos['Id_Us'], array('username' => $datos['rut'], 'email' => $datos['correo'], 'sex' => $datos['sexo'], 'f_nacimiento' => $newformat, 'phone' => $datos['t_fijo'], 'cellphone' => $datos['t_celular'], 'first_name' => $datos['nombre'], 'last_name' => $datos['apellido']));
		    		var_dump($user_add);
		    		//$this->db->where('Id', $datos['Id_Us']);
	       			//$this->db->update('usuario', $_datos_usuario);		
	       			$r=array('bool'=>TRUE);
		    		return $r;    		
		    	}
		    	else
		    	{
		    		$r=array('bool'=>FALSE,'msg'=>'Ya existe APODERADO','query'=>null);
		    		return $r;
		    	}
	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR USUARIO','query'=>null);
		    	return $r;
	    	}
	    }
    //************ FIN APODERADO *********
    // ***********  ALUMNO **********
	    public function add_Alumno($datos)
	    {
	        //Grupo 3
	        $tabla='alumno';
	    	$id_user=NULL;
	        //$contrasegna=$this->password_gen();
	        //$contrasegna="123456789";
	        try
	        {
	            if($this->existe_user($datos['rut']))
	            {
	            	$explode = explode('/',$datos['f_nacimiento']);
	       			$dia = $explode[0];
	       			$mes = $explode[1];
	       			$año = $explode[2];
	       			$fecha = $mes.'/'.$dia.'/'.$año;		    		
	       			$newformat = date('Y/m/d',strtotime($fecha));
	       			$cellphone_ = '569'.$datos['t_celular'];
	       			//$contrasegna=$datos['password'];
	            	$user_add = $this->ion_auth->register($datos['rut'], $datos['password'], $datos['correo'], array('sex' => $datos['sexo'], 'f_nacimiento' => $newformat, 'phone' => $datos['t_fijo'], 'cellphone' => $cellphone_, 'first_name' => $datos['nombre'], 'last_name' => $datos['apellido']), array(3));

	                if($user_add)
	                {
	                    $id_user=$user_add;
	                    if($id_user!==NULL)
	                    {

	                        $_datos_alumno=array(   'Tipo_Sangre'=>$datos['t_sangre'],
	                                                'Alergias'=>$datos['alergias'],
	                                                'Usuario_Id'=>$id_user,
	                                                'hide'=>0,
	                                                'Apoderado_Id'=>$datos['Apoderado_Id']);

	                        $this->db->insert($tabla,$_datos_alumno);
	                        $varSms = array('type' =>'password','to'=>$cellphone_,'pass'=>$datos['password']);
				    		$SmsSend = $this->tw_msg->send($varSms);
				    		if($SmsSend['bool'])
				    		{
				    			$r=array('bool'=>TRUE,'msg'=>'Funciona');
				    			return $r;
				    		}
				    		else
				    		{

				    			$r=array('bool'=>FALSE,'msg'=>'Error al enviar el mensaje');
				    			return $r;
				    		}
	                    }
	                    else
	                    {
	                        $r=array('bool'=>FALSE,'msg'=>'Error al crear Datos opcionales del Alumno');
	                        return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
	                    }
	                }
	                else
	                {
	                    $r=array('bool'=>FALSE,'msg'=>'Error al crear USUARIO');
	                    return $r;
	                }
	            }
	            else
	            {
	                $r=array('bool'=>FALSE,'msg'=>'Ya existe ALUMNO');
	                return $r;
	            }
	        }
	        catch(Exception $e)
	        {
	            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR USUARIO');
	            return $r;
	        }
	    }
	    public function all_Alumno($id = NULL)
	    {
	        try
	        {
	            $this->db->select('*');  
	            $this->db->from('users');  
	            $this->db->join('alumno',  'users.Id = alumno.Usuario_Id', 'INNER');
	            $this->db->where('hide',0);
	            if($id != NULL){
	            	$this->db->where('alumno.id',$id);
	            }
	            $query = $this->db->get();
	            if($query->num_rows() > 0 )
	            {
	                if($id != NULL){
	            		$q=$query->row();
	            	}
	            	else{
	                	$q=$query->result();
	                }
	                $r=array('bool'=>TRUE,'query'=>$q);
	                return $r;
	            }
	            else
	            {
	                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS','query'=>null);
	                return $r;
	            }

	        }
	        catch(Exception $e)
	        {
	            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR ALUMNOS','query'=>null);
	            return $r;
	        }
	    }
	    public function edit_alumno($datos)
	    {
	    	$Rol_Id=2;//ROL ID DE APODERADO
	    	$tabla='alumno';
	    	$_datos_usuario=array();
	    	try
	    	{
	    		if($this->existe_user_edit($datos['rut'],$datos['Id_Us']))
		    	{
		    		$_datos_apoderado=array('Tipo_Sangre'=>$datos['t_sangre'],
					    				'Alergias'=>$datos['alergias'],
					    				'Apoderado_Id'=>$datos['Apoderado_Id']);
		    		$this->db->where('Id', $datos['Id_Al']);
	       			$this->db->update('alumno', $_datos_apoderado);
	       			$explode = explode('/',$datos['f_nacimiento']);
	       			$dia = $explode[0];
	       			$mes = $explode[1];
	       			$año = $explode[2];
	       			$fecha = $mes.'/'.$dia.'/'.$año;		    		
	       			$newformat = date('Y/m/d',strtotime($fecha));
		    		$user_add = $this->ion_auth->update($datos['Id_Us'], array('username' => $datos['rut'], 'email' => $datos['correo'], 'sex' => $datos['sexo'], 'f_nacimiento' => $newformat, 'phone' => $datos['t_fijo'], 'cellphone' => $datos['t_celular'], 'first_name' => $datos['nombre'], 'last_name' => $datos['apellido']));
		    		//var_dump($user_add);
		    		//$this->db->where('Id', $datos['Id_Us']);
	       			//$this->db->update('usuario', $_datos_usuario);		
	       			$r=array('bool'=>TRUE);
		    		return $r;    		
		    	}
		    	else
		    	{
		    		$r=array('bool'=>FALSE,'msg'=>'Ya existe ALUMNO','query'=>null);
		    		return $r;
		    	}
	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR USUARIO','query'=>null);
		    	return $r;
	    	}
	    }
    //************ FIN ALUMNO *********
	// ***********  PROFESOR **********
	    public function add_Profesor($datos)
	    {
	        $Rol_Id=4;//ROL ID DE PROFESOR
	        $tabla='profesor';
	        $id_user=NULL;
	        //$contrasegna = "123456789";
	        //$contrasegna=$this->password_gen();
	        try
	        {
	            if($this->existe_user($datos['rut']))
	            {
	            	$explode = explode('/',$datos['f_nacimiento']);
	       			$dia = $explode[0];
	       			$mes = $explode[1];
	       			$año = $explode[2];
	       			$fecha = $mes.'/'.$dia.'/'.$año;		    		
	       			$newformat = date('Y/m/d',strtotime($fecha));
	       			$cellphone_ = '569'.$datos['t_celular'];
	       			$contrasegna = $datos['password'];
	            	$user_add = $this->ion_auth->register($datos['rut'], $contrasegna, $datos['correo'], array('sex' => $datos['sexo'], 'f_nacimiento' => $newformat, 'phone' => $datos['t_fijo'], 'cellphone' => $cellphone_, 'first_name' => $datos['nombre'], 'last_name' => $datos['apellido']), array(4));
	                //$pass_cryp=$this->security_lib->cryp_pass($datos['password']);
	                /*$_datos_usuario=array(  'Nombre'=>$datos['nombre'],
	                                        'Apellido'=>$datos['apellido'],
	                                        'Rut'=>$datos['rut'],
	                                        'Fnacimiento'=>$newformat,
	                                        'Telefono_fijo'=>$datos['t_fijo'],
	                                        'Telefono_celular'=>$datos['t_celular'],
	                                        'Password'=>$pass_cryp,
	                                        'Rol_Id'=>$Rol_Id,
	                                        'Email'=>$datos['correo']);
	            
	                $user_add=$this->add_user($_datos_usuario); */
	                
	                if($user_add)
	                {
	                    $id_user=$user_add;
	                    if($id_user!==NULL)
	                    {
	                        $_datos_profesor=array(   'Profesion'=>$datos['profesion'],'Usuario_Id'=>$id_user,'hide'=>0);

	                        $this->db->insert($tabla,$_datos_profesor);
	                        $varSms = array('type' =>'password','to'=>$cellphone_,'pass'=>$contrasegna );
				    		$SmsSend = $this->tw_msg->send($varSms);
				    		if($SmsSend['bool'])
				    		{
				    			$r=array('bool'=>TRUE,'msg'=>'Funciona');
				    			return $r;
				    		}
				    		else
				    		{
				    			$r=array('bool'=>FALSE,'msg'=>'Error al crear Apoderado sms wsp');
				    			return $r;
				    		}
	                    }
	                    else
	                    {
	                        $r=array('bool'=>FALSE,'msg'=>'Error al crear Profesor');
	                        return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
	                    }
	                }
	                else
	                {
	                    $r=array('bool'=>FALSE,'msg'=>'Error al crear USUARIO');
	                    return $r;
	                }
	            }
	            else
	            {
	                $r=array('bool'=>FALSE,'msg'=>'Ya existe Profesor');
	                return $r;
	            }
	        }
	        catch(Exception $e)
	        {
	            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR USUARIO');
	            return $r;
	        }
	    }
	    public function all_Profesor($id=NULL)
	    {
	        try
	        {
	            $this->db->select('*');  
	            $this->db->from('users');  
	            $this->db->join('profesor',  'users.Id = profesor.Usuario_Id', 'INNER');
	            $this->db->where('active','1');
	            if($id != NULL){
	            	$this->db->where('users.id',$id);
	            }
	            $this->db->where('hide',0);
	            $query = $this->db->get();
	            if($query->num_rows() > 0 )
	            {
	            	if($id != NULL){
	            		$q=$query->row();
	            	}
	            	else{
	                	$q=$query->result();
	                }
	                $r=array('bool'=>TRUE,'query'=>$q);
	                return $r;
	            }
	            else
	            {
	                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON PROFESORS','query'=>null);
	                return $r;
	            }

	        }
	        catch(Exception $e)
	        {
	            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR PROFESORS','query'=>null);
	            return $r;
	        }
	    }
	    public function all_Profesor2($id=NULL)
	    {
	        try
	        {
	            $this->db->select('*');  
	            $this->db->from('users');  
	            $this->db->join('profesor',  'users.Id = profesor.Usuario_Id', 'INNER');
	            $this->db->where('active','1');
	            if($id != NULL){
	            	$this->db->where('profesor.Id',$id);
	            }
	            $this->db->where('hide',0);
	            $query = $this->db->get();
	            if($query->num_rows() > 0 )
	            {
	            	if($id != NULL){
	            		$q=$query->row();
	            	}
	            	else{
	                	$q=$query->result();
	                }
	                $r=array('bool'=>TRUE,'query'=>$q);
	                return $r;
	            }
	            else
	            {
	                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON PROFESORS','query'=>null);
	                return $r;
	            }

	        }
	        catch(Exception $e)
	        {
	            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR PROFESORS','query'=>null);
	            return $r;
	        }
	    }
	    public function get_id_Curso($id_profesor=NULL)
	    {
	        try
	        {
	            $this->db->select('*');  
	            $this->db->from('curso');
	            if($id_profesor != NULL){
	            	$this->db->where("Profesor_Id",$id_profesor);
	            } 
	            $query = $this->db->get();
	            if($query->num_rows() > 0 )
	            {
            		$q=$query->row();
	                $r=array('bool'=>TRUE,'query'=>$q);
	                return $r;
	            }
	            else
	            {
	                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON CURSOS','query'=>null);
	                return $r;
	            }

	        }
	        catch(Exception $e)
	        {
	            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR CURSO','query'=>null);
	            return $r;
	        }
	    }
	    public function all_Curso($id=NULL)
	    {
	        try
	        {
	            $this->db->select('*');  
	            $this->db->from('curso');
	            $this->db->where('hide',0); 
	            if($id != NULL){
	            	$this->db->where("Id",$id);
	            } 
	            $query = $this->db->get();
	            if($query->num_rows() > 0 )
	            {
	            	if($id != NULL){
	            		$q=$query->row();
	            	}
	            	else{
	            	    $q=$query->result();
	            	}
	                $r=array('bool'=>TRUE,'query'=>$q);
	                return $r;
	            }
	            else
	            {
	                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON CURSOS','query'=>null);
	                return $r;
	            }

	        }
	        catch(Exception $e)
	        {
	            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR CURSO','query'=>null);
	            return $r;
	        }
	    }
	    public function ver_profesor($id)
	    {
	    	try
	    	{

	    		if(is_numeric($id))
	    		{
		    		$this->db->select('*');  
					$this->db->from('users');  
					$this->db->join('profesor',  'users.Id = profesor.Usuario_Id', 'INNER');
					$this->db->where('profesor.Id',$id);
					$query = $this->db->get();
					if($query->num_rows() > 0 )
					{
						$q=$query->row();
						$r=array('bool'=>TRUE,'query'=>$q);
						return $r;
					}
					else
					{
						$r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARO PROFESOR','query'=>null);

		    			return $r;

					}
				}
				else
				{
					$r=array('bool'=>FALSE,'msg'=>'INGRESE UNA ID NUMERICA','query'=>null);
		    			return $r;
				}
	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CARGAR PROFESORES','query'=>null);
	    		return $r;
	    	}
	    }
	    public function edit_profesor($datos)
	    {
	    	$Rol_Id=2;//ROL ID DE APODERADO
	    	$tabla='profesor';
	    	$_datos_usuario=array();
	    	try
	    	{
	    		if($this->existe_user_edit($datos['rut'],$datos['Id_Us']))
		    	{
		    		$_datos_apoderado=array('Profesion'=>$datos['profesion']);
		    		$this->db->where('Id', $datos['Id_Pr']);
	       			$this->db->update('profesor', $_datos_apoderado);
	       			$check=$datos['cam_pass'];
	       			if($check)
	       			{
	       				$_datos_usuario['Password']=$datos['password'];
	       			}
	       			else
	       			{
	       				$pass_cryp=$this->security_lib->cryp_pass($datos['password']);
	       				$_datos_usuario['Password']=$pass_cryp;
	       			}
	       			$explode = explode('/',$datos['f_nacimiento']);
	       			$dia = $explode[0];
	       			$mes = $explode[1];
	       			$año = $explode[2];
	       			$fecha = $mes.'/'.$dia.'/'.$año;		    		
	       			$newformat = date('Y/m/d',strtotime($fecha));
			    	$user_add = $this->ion_auth->update($datos['Id_Us'], array('username' => $datos['rut'], 'email' => $datos['correo'], 'sex' => $datos['sexo'], 'f_nacimiento' => $newformat, 'phone' => $datos['t_fijo'], 'cellphone' => $datos['t_celular'], 'first_name' => $datos['nombre'], 'last_name' => $datos['apellido']));	
	       			$r=array('bool'=>TRUE);
		    		return $r;    		
		    	}
		    	else
		    	{
		    		$r=array('bool'=>FALSE,'msg'=>'Ya existe APODERADO','query'=>null);
		    		return $r;
		    	}
	    	}
	    	catch(Exception $e)
	    	{
	    		$r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR USUARIO','query'=>null);
		    	return $r;
	    	}
	    }
    //************ FIN PROFESOR *********  
    public function comunas()
    {
    	try
    	{
    		$query = $this->db->get('comunas');
    		 if($query->num_rows() > 0 )
    		 {
    		 	 $r=$query->result();
    		 	 return $r;
    		 }
        
    	}
    	catch(Exception $e)
    	{
    		return NULL;
    	}
    }
    
    public function add_curso($datos){  
    	//Curso para alumnos
        $tabla='curso';
        try
        {            
        	$newformat = date('Y-m-d');
            $_datos_curso=array(  	'Tipo'=>$datos['tipo'],
                                    'Grado'=>$datos['grado'],
                                    'Letra'=>$datos['letra'],
                                    'Profesor_Id'=>$datos['profesor_id'],
                                    'Agno'=>$newformat,
                                    'hide'=>0);
            $exito = $this->db->insert('curso',$_datos_curso);
            
            if($exito)
            {
                if($exito!==NULL)
                {
                	$r=array('bool'=>TRUE);
                    return $r;
                }
                else
                {
                    $r=array('bool'=>FALSE,'msg'=>'Error al crear CURSO','query'=>null);
                    return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
                }
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'Error al crear CURSO','query'=>null);
                return $r;
            }
            
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR CURSO','query'=>null);
            return $r;
        }
    }
    public function profesor_asignatura_add($datos){
    	//Curso para alumnos
        $tabla='profesor_has_asignatura';
        
        try
        {            
            $_datos_profesor_asigntura=array(  	'Profesor_Id'=>$datos['Profesor_Id'],
                                    			'Curso_has_Asignatura_Id'=>$datos['Curso_has_Asignatura_Id']);
            $exito = $this->db->insert($tabla,$_datos_profesor_asigntura);
            
            if($exito)
            {
                if($exito!==NULL)
                {
                	$r=array('bool'=>TRUE);
                    return $r;
                }
                else
                {
                    $r=array('bool'=>FALSE,'msg'=>'Error al crear la ASOCIACIÓN ENTRE PROFESOR Y ASIGNATURA','query'=>null);
                    return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
                }
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'Error al crear ASOCIACIÓN ENTRE PROFESOR Y ASIGNATURA','query'=>null);
                return $r;
            }
            
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR ASOCIACIÓN ENTRE PROFESOR Y ASIGNATURA','query'=>null);
            return $r;
        }
    }
    public function curso_asignatura_add($datos){
    	//Curso para alumnos
        $tabla='curso_has_asignatura';
        
        try
        {            
            $_datos_curso_asigntura=array(  	'Curso_Id'=>$datos['Curso_Id'],
                                    'Asignatura_Id'=>$datos['Asignatura_Id']);
            $exito = $this->db->insert($tabla,$_datos_curso_asigntura);
            
            if($exito)
            {
                if($exito!==NULL)
                {
                	$r=array('bool'=>TRUE);
                    return $r;
                }
                else
                {
                    $r=array('bool'=>FALSE,'msg'=>'Error al crear la ASOCIACIÓN ENTRE CURSO Y ASIGNATURA','query'=>null);
                    return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
                }
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'Error al crear ASOCIACIÓN ENTRE CURSO Y ASIGNATURA','query'=>null);
                return $r;
            }
            
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR ASOCIACIÓN ENTRE CURSO Y ASIGNATURA','query'=>null);
            return $r;
        }
    }
    public function horario_add($datos){
    	//Curso para alumnos
        $tabla='horario';
        $agno=date('Y', strtotime($datos['Año']));
        try
        {            
            $_datos_=array(  	'Temporada'=>$datos['Temporada'],
            					'Año'=>$agno,
            					'Dia'=>$datos['Dia'],
                                'Curso_has_Asignatura_Id'=>$datos['Curso_Asignatura_Id'],
                                'Bloques_Id'=>$datos['Bloque_Id']);
            $exito = $this->db->insert($tabla,$_datos_);
            
            if($exito)
            {
                if($exito!==NULL)
                {
                	$r=array('bool'=>TRUE);
                    return $r;
                }
                else
                {
                    $r=array('bool'=>FALSE,'msg'=>'Error al crear el HORARIO','query'=>null);
                    return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
                }
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'Error al crear HORARIO','query'=>null);
                return $r;
            }
            
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR HORARIO','query'=>null);
            return $r;
        }
    }
    public function curso_alumno_add($datos){
    	//Curso para alumnos
        $q = $this->all_Curso_Alumno2($datos['Curso_Id'],$datos['Alumno_Id']);
    	$agno=date('Y');
    	//var_dump($q['query']->Año);
    	//exit;
    	if($q['query']->Año == $agno){
    		$r=array('bool'=>FALSE,'msg'=>'El alumno ya ha sido registrado en un curso este año','query'=>null);
            return $r;
    	}
    	else{
	        $tabla='curso_has_alumno';
	        $agno=date('Y');
	        try
	        {            
	            $_datos_=array(  	'Curso_Id'=>$datos['Curso_Id'],
				                                    'Alumno_Id'=>$datos['Alumno_Id'],
				                                    'Año'=>$agno);
	            $exito = $this->db->insert($tabla,$_datos_);
	            
	            if($exito)
	            {
	                if($exito!==NULL)
	                {
	                	$r=array('bool'=>TRUE);
	                    return $r;
	                }
	                else
	                {
	                    $r=array('bool'=>FALSE,'msg'=>'Error al crear la ASOCIACIÓN ENTRE CURSO Y ALUMNO','query'=>null);
	                    return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
	                }
	            }
	            else
	            {
	                $r=array('bool'=>FALSE,'msg'=>'Error al crear ASOCIACIÓN ENTRE CURSO Y ALUMNO','query'=>null);
	                return $r;
	            }
	            
	        }
	        catch(Exception $e)
	        {
	            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR ASOCIACIÓN ENTRE CURSO Y ALUMNO','query'=>null);
	            return $r;
	        }
    	}
    }
    public function add_asignatura($datos){
    	//Curso para alumnos
        $tabla='asignatura';
        try
        {            
            $_datos_asignatura=array(  	'Nombre'=>$datos['Nombre'],
	                                    'Descripcion'=>$datos['Descripcion'],
	                                    'hide'=>0,
	                                    'Hora_Total'=>$datos['Hora_Total']);
            $exito = $this->db->insert($tabla,$_datos_asignatura);
            
            if($exito)
            {
                if($exito!==NULL)
                {
                	$r=array('bool'=>TRUE);
                    return $r;
                }
                else
                {
                    $r=array('bool'=>FALSE,'msg'=>'Error al crear ASIGNATURA','query'=>null);
                    return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
                }
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'Error al crear ASIGNATURA','query'=>null);
                return $r;
            }
            
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR ASIGNATURA','query'=>null);
            return $r;
        }
    }
    public function add_bloque($datos){  
    	//Curso para alumnos
        $tabla='bloques';
        //INSERT INTO `tesis`.`bloques` (`Id`, `Hora_Inicio`, `Hora_Fin`, `hide`) VALUES (NULL, '08:30:00', '09:45:00', NULL);
        try
        {            
        	$newformat = date('H:i',strtotime($datos['hora_inicio']));
        	$newformat2 = date('H:i',strtotime($datos['hora_fin']));
            $_datos_bloque=array(  	'Hora_Inicio'=>$newformat,
                                    'Hora_Fin'=>$newformat2);
            $exito = $this->db->insert('bloques',$_datos_bloque);
            
            if($exito)
            {
                if($exito!==NULL)
                {
                	$r=array('bool'=>TRUE);
                    return $r;
                }
                else
                {
                    $r=array('bool'=>FALSE,'msg'=>'Error al crear BLOQUE','query'=>null);
                    return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
                }
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'Error al crear BLOQUE','query'=>null);
                return $r;
            }
            
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR BLOQUE','query'=>null);
            return $r;
        }
    }
    private function ver_user($id)
    {
    	try
    	{
    		$query = $this->db->get_where('usuario',array('Id' => $id));
    		 if($query->num_rows() > 0 )
    		 {
    		 	 return $query->row();
    		 }
        
    	}
    	catch(Exception $e)
    	{
    		return NULL;
    	}
    }
    private function existe_user($rut)
    {
    	try
    	{
    		$query = $this->db->get_where('users',array('username' => $rut));
    		 if($query->num_rows() > 0 )
    		 {
    		 	 return false;
    		 }
    		 else
    		 {
    		 	return true;
    		 }
        
    	}
    	catch(Exception $e)
    	{
    		return true;
    	}
    }
    private function existe_user_edit($rut,$Id)
    {
    	try
    	{
    		$query = $this->db->get_where('users',array('username' => $rut));
    		 if($query->num_rows() > 0 )
    		 {
    		 	$query=$query->row();
    		 	if($query->id==$Id)
    		 	{
    		 	 	return true;
    		 	}
    		 	else
    		 	{
    		 		return false;
    		 	}
    		 }
    		 else
    		 {
    		 	return true;
    		 }
        
    	}
    	catch(Exception $e)
    	{
    		return true;
    	}
    }
    private function add_user($datos)
    {
    	try
    	{
    		$this->db->insert('users',$datos);
    		return true;
    	}
    	catch(Exception $e)
    	{
    		return false;
    	}
    }
    private function edit_user()
    {
    	
    }
    private function delete_user()
    {
    	
    }
    public function agregar_tabla($nombre_tabla,$datos){
        try
        {            
            $exito = $this->db->insert($nombre_tabla,$datos);
            
            if($exito)
            {
                if($exito!==NULL)
                {
                	$r=array('bool'=>TRUE);
                    return $r;
                }
                else
                {
                    $r=array('bool'=>FALSE,'msg'=>'Error al crear el '.$nombre_tabla,'query'=>null);
                    return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
                }
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'Error al crear '.$nombre_tabla,'query'=>null);
                return $r;
            }
            
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'ERROR AL CREAR '.$nombre_tabla,'query'=>null);
            return $r;
        }
    }
    public function editar_tabla($id,$nombre_tabla, $datos = array()){
    	try{
    		$this->db->where('Id', $id);
    		$exito = $this->db->update($nombre_tabla,$datos);            
            if($exito)
            {
                if($exito!==NULL)
                {
                	$r=array('bool'=>TRUE);
                    return $r;
                }
                else
                {
                    $r=array('bool'=>FALSE,'msg'=>'Error al crear el '.$nombre_tabla,'query'=>null);
                    return $r; // Cambiar respuesta array('bool'=>true,'msg'=>'MENSAJE')
                }
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'Error al crear '.$nombre_tabla,'query'=>null);
                return $r;
            }
    	}
    	catch(Exception $ex){
    		$r=array('bool'=>FALSE,'msg'=>'Error al crear '.$nombre_tabla,'query'=>null);
            return $r;
    	}
    }
}
?>