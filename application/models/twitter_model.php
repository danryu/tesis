<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Twitter_model extends CI_MODEL {
 
    function __construct()
    {
    	parent::__construct();
    	$this->load->database();
    	$this->load->library('security_lib');
    	$this->load->library('tw_msg');
    	$this->load->library('ion_auth');
    	$this->load->model(array('security_model','ion_auth_model'));

 	}
   	public function verificar_twitter($User_Id)
    {
    	if(isset($User_Id))
    	{
    		 	$this->db->select('Id_twitter');                
                $this->db->from('users'); 
                $this->db->where('Id',$User_Id);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->row();
                    if(is_null($query->Id_twitter))
                    {
                    	$r = array('bool' => true);
                    	return $r;
                    }
                    else
                    {
                    	$r = array('bool' => false);
                    	return $r;
                    }                    
                }
                else
                {
                    $r = array('bool' => false, 'msg'=> 'Error al buscar twitter');
                    return $r;
                }
    	}
    	else
    	{
    		$r =array('bool'=> false, 'msg'=>'No hay Id de Usuario');
    	}
    }
    public function save_twitter_Id($Id_twitter,$User_Id)
    {
    	if(isset($Id_twitter) && isset($User_Id))
    	{
            if($this->twitter_usado($Id_twitter))
            {
                $data = array('Id_twitter'=>$Id_twitter);
                $this->db->where('id',$User_Id);
                $this->db->update('users', $data); 
                $r = array('bool' => true);
                return $r;
            }
    		else
            {
                $r = array('bool' => false,'msg'=>'El twitter que intenta asociar, ya se encuentra en uso');
                return $r;
            }
    	}
    	else
    	{
    		$r = array('bool' => false, 'msg'=>'Id Twitter o Usuario incompleto');
    		return $r;
    	}
    }
    public function delete_twitter_Id($User_Id)
    {
        if(isset($User_Id))
        {
            //var_dump($Id_twitter);
            $data = array('Id_twitter'=>NULL);
            $this->db->where('id',$User_Id);
            $this->db->update('users', $data); 
            $r = array('bool' => true);
            return $r;
        }
        else
        {
            $r = array('bool' => false);
            return $r;
        }
    }
    public function login_twitter($Id_twitter)
    {
    	if(isset($Id_twitter))
    	{
    		 	$this->db->select('username, email, id, password, active, last_login, Id_twitter');                
                $this->db->from('users'); 
                $this->db->where('Id_twitter',$Id_twitter);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $user = $query->row();
                    $this->ion_auth_model->set_session($user);
                    $this->ion_auth_model->set_special_session($user->id);
					$this->ion_auth_model->update_last_login($user->id);
                    $r = array('bool' => true);
                    return $r;    
                }
                else
                {
                    $r = array('bool' => false, 'msg'=> 'No se ha encontrado usuario asociado');
                    return $r;
                }
    	}
    	else
    	{
    		$r =array('bool'=> false, 'msg'=>'No se ha encontrado Twitter ID');
    	}
    }
    public function get_username()
	{	
	}
    private function twitter_usado($twitter_Id)
    {
        if(isset($twitter_Id))
        {
                $this->db->select('Id_twitter');                
                $this->db->from('users'); 
                $this->db->where('Id_twitter',$twitter_Id);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    return false;                    
                }
                else
                {
                   return true;
                }
        }
        else
        {
            return false;
        }
    }
}?>