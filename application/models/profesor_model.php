<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Profesor_model extends CI_MODEL 
{
 
 	//private $_Agno=date("Y-01-01"); 
    function __construct()
    {
    	parent::__construct();
    	$this->load->database();
        $this->load->model('security_model');
    	$this->load->library('security_lib');
    }
    public function load_cursos($Id_User,$Agno=NULL)
    {
    	try
    	{
    		if(is_null($Agno)){$Agno=date("Y");}
    		if(isset($Id_User))
    		{
		    	$this->db->select('curso.Grado, curso.Id, curso.Letra,curso.Agno');                
		        $this->db->from('curso');                          
		        $this->db->join('curso_has_asignatura','curso.Id =curso_has_asignatura.Curso_Id', 'INNER');
		        $this->db->join('profesor_has_asignatura','curso_has_asignatura.Id = profesor_has_asignatura.Curso_has_Asignatura_Id','INNER');
		        $this->db->where('Agno',$Agno);
		        $this->db->where('profesor_has_asignatura.Profesor_Id', $Id_User);
                $this->db->group_by("curso.Id"); 
		        $query = $this->db->get();
		        if($query->num_rows()>0)
		        {
		            $query = $query->result();
		            $r = array('bool' => true, 'query'=> $query);
            		return $r;
		        }
		        else
		        {
		        	$r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
            		return $r;
		        }
	    	}
	    	else
	    	{
	    		$r = array('bool' => false, 'msg'=> 'Error en la bd');
            	return $r;
	    	}
    	}
    	catch(Exception $e)
    	{
    		$r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
    	}
    }
    public function load_cursos_jefe($Id_User,$Agno=NULL)
    {
		try
    	{
    		if(is_null($Agno)){$Agno=date("Y-01-01");}
    		if(isset($Id_User))
    		{				
				$this->db->select('curso.Grado, curso.Id, curso.Letra,curso.Agno');                
                $this->db->from('curso');   
                $this->db->join('profesor','profesor.Id = curso.Profesor_Id','INNER');
                $this->db->join('users','users.Id = profesor.Usuario_Id','INNER');
                $this->db->where('Agno',$Agno);
                $this->db->where('users.Id', $Id_User);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();
                    //var_dump($query);
                    $r = array('bool' => true, 'query'=> $query);
            		return $r;
                }
                else
                {
                	$r = array('bool' => false, 'msg'=> 'No es profesor Jefe');
            		return $r;
                }   
            }
	    	else
	    	{
	    		$r = array('bool' => false, 'msg'=> 'Error en la bd');
            	return $r;
	    	}
    	}
    	catch(Exception $e)
    	{
    		$r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
    	}             	
    }
    public function verificar_curso($Id_User,$id_curso,$Agno=NULL)
    {
        try
        {
                if(!isset($id_curso) && !isset($Id_User)){$r = array('bool' => false, 'msg'=> 'Falta información');return $r;}
                if(is_null($Agno)){$Agno=date("Y-01-01");}
                $this->db->select('curso.Id');                
                $this->db->from('curso');                          
                $this->db->join('curso_has_asignatura','curso.Id =Curso_Id', 'INNER');
                $this->db->join('asignatura','asignatura.Id =curso_has_asignatura.Asignatura_Id','INNER');
                $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id = curso_has_asignatura.Id','INNER');
                $this->db->join('profesor','profesor.Id = profesor_has_asignatura.Profesor_Id','INNER');
                $this->db->join('users','users.Id = Usuario_Id','INNER');
                $this->db->where('Agno',$Agno);
                $this->db->where('users.Id', $Id_User);
                $this->db->where('curso.Id', $id_curso);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();
                    //var_dump($query);
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
                    return $r;
                }
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'Error al verificar curso');
            return $r;
        }
    }
    public function verificar_curso_Jefe($Id_Profesor,$id_curso,$Agno=NULL)
    {
        try
        {
                if(!isset($id_curso) && !isset($Id_Profesor)){$r = array('bool' => false, 'msg'=> 'Falta información');return $r;}
                if(is_null($Agno)){$Agno=date("Y");}
                $this->db->select('curso.Id');                
                $this->db->from('curso');
                $this->db->where('Agno',$Agno);
                $this->db->where('curso.Profesor_Id', $Id_Profesor);
                $this->db->where('curso.Id', $id_curso);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();
                    //var_dump($query);
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
                    return $r;
                }
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'Error al verificar curso');
            return $r;
        }
    }
    public function get_asignaturas($Id_User,$Id_Curso,$Agno=NULL)
    {
        try
        {
                if(!isset($id_curso) && !isset($Id_User)){$r = array('bool' => false, 'msg'=> 'Falta información');return $r;}
                if(is_null($Agno)){$Agno=date("Y");}
                $this->db->select('asi.Id,asi.Nombre,asi.Descripcion,asi.hora_total,curso_has_asignatura.Id as cha_Id,curso_has_asignatura.Cantidad_Notas,curso_has_asignatura.Cantidad_Hora_Semana as cha_horas');                
                $this->db->from('asignatura asi');   
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Asignatura_Id = asi.Id', 'INNER');
                $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id', 'INNER');
                $this->db->join('profesor','profesor_has_asignatura.Profesor_Id = profesor.Id','INNER');
                $this->db->join('users','users.Id = profesor.Usuario_Id','INNER');
                //$this->db->where('Agno',$Agno);
                $this->db->where('users.Id', $Id_User);
                $this->db->where('curso_has_asignatura.Curso_Id', $Id_Curso);
                $this->db->where('asi.hide', 0);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();
                    //var_dump($query);
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
                    return $r;
                }
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'Error al verificar curso');
            return $r;
        }
    }
    public function get_apoderados_curso($Id_curso,$Agno=NULL)
    {
                
                if(is_null($Agno)){$Agno=date("Y-01-01");}
                $this->db->select('users.Id,users.username,users.first_name,users.last_name,users.email,users.cellphone,users.phone,users.Id_twitter');                
                $this->db->from('users');                          
                $this->db->join('apoderado','apoderado.Usuario_id =users.Id', 'INNER');
                $this->db->join('alumno','alumno.Apoderado_id =apoderado.Id','INNER');
                $this->db->join('curso_has_alumno','curso_has_alumno.Alumno_id = alumno.Id','INNER');
                $this->db->join('curso','curso.Id = curso_has_alumno.Curso_Id','INNER');
                $this->db->where('curso.Id', $Id_curso);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();
                    //var_dump($query);
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
                    return $r;
                }                    
    }
    public function get_apoderados_curso_nota($Id_curso,$Id_set,$Agno=NULL)
    {
                
                if(is_null($Agno)){$Agno=date("Y-01-01");}
                $this->db->select('nota.Nota,asignatura.Nombre,users.Id,users.username,users.first_name,users.last_name,users.email,users.cellphone,users.phone,users.Id_twitter');                
                $this->db->from('users');                          
                $this->db->join('apoderado','apoderado.Usuario_id =users.Id', 'INNER');
                $this->db->join('alumno','alumno.Apoderado_id =apoderado.Id','INNER');
                $this->db->join('nota','nota.Alumno_Id =alumno.Id','INNER');
                $this->db->join('set_nota','set_nota.Id =nota.Set_nota_Id','INNER');
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Id = set_nota.Curso_has_Asignatura_Id','INNER');
                $this->db->join('asignatura','curso_has_asignatura.Asignatura_Id = asignatura.Id','INNER');
                $this->db->join('curso_has_alumno','curso_has_alumno.Alumno_id = alumno.Id','INNER');
                $this->db->join('curso','curso.Id = curso_has_alumno.Curso_Id','INNER');
                $this->db->where('curso.Id', $Id_curso);
                $this->db->where('nota.Set_nota_Id', $Id_set);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
                    return $r;
                }                    
    }
    public function get_alumnos_curso($Id_curso,$Agno=NULL)
    {
                if(is_null($Agno)){$Agno=date("Y-01-01");}
                $this->db->select('users.Id,users.username,users.first_name,users.last_name,users.email,alumno.Id as Id_Alumno');                
                $this->db->from('users');   
                $this->db->join('alumno','alumno.Usuario_id =users.Id','INNER');
                $this->db->join('curso_has_alumno','curso_has_alumno.Alumno_id = alumno.Id','INNER');
                $this->db->join('curso','curso.Id = curso_has_alumno.Curso_Id','INNER');
                $this->db->where('curso.Id', $Id_curso);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $cantidad=$query->num_rows();
                    $query = $query->result();
                    $r = array('bool' => true, 'query'=> $query,'Cantidad'=>$cantidad);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
                    return $r;
                }  
    }
    public function load_asg($Id_User,$Id_Curso)
    {
                $this->db->select('asignatura.Id,asignatura.Nombre');                
                $this->db->from('asignatura');   
                
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Asignatura_Id =asignatura.Id','INNER');
                $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
                $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
                $this->db->join('users','profesor.Usuario_id =users.Id','INNER');
                $this->db->where('curso_has_asignatura.Curso_Id', $Id_Curso);
                $this->db->where('users.Id', $Id_User);

                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();
                   
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
                    return $r;
                } 
    }
    public function get_foros_curso($Id_User,$Id_Curso)
    {
                $Agno=date('Y');
                $this->db->select('foro.Titulo,foro.Comentario,foro.Curso_has_Asignatura_Id as Id_cha,foro.Fecha,foro.Id,foro.Estado');                
                $this->db->from('foro');   
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Id =foro.Curso_has_Asignatura_Id','INNER');
                $this->db->join('curso','curso.Id =curso_has_asignatura.Curso_Id','INNER');
                $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
                $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
                $this->db->join('users','profesor.Usuario_id =users.Id','INNER');
                $this->db->where('curso.Id', $Id_Curso);
                $this->db->where('curso.Agno', $Agno);
                $this->db->where('users.Id', $Id_User);  
                $this->db->where('foro.hide', 0);               
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();              
                    //var_dump($query);     
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                    return $r;
                } 
    }
    public function get_foros_asg($Id_User,$Id_Cha)
    {
        try
        {
                $Agno=date('Y');
                $this->db->select('foro.Titulo,foro.Comentario,foro.Curso_has_Asignatura_Id as Id_cha,foro.Fecha,foro.Id,foro.Estado');                
                $this->db->from('foro');   
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Id =foro.Curso_has_Asignatura_Id','INNER');
                $this->db->join('curso','curso.Id =curso_has_asignatura.Curso_Id','INNER');
                $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
                $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
                $this->db->join('users','profesor.Usuario_id =users.Id','INNER');
                $this->db->where('curso_has_asignatura.Id', $Id_Cha);
                $this->db->where('curso.Agno', $Agno);
                $this->db->where('users.Id', $Id_User);  
                $this->db->where('foro.hide', 0);               
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();             
                    
                        $r = array('bool' => true, 'query'=> $query);
                        return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                    return $r;
                } 
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                    return $r;

        }
    }
    public function verificar_asg($Id_User,$Id_cha)
    {

                $this->db->select('asignatura.Id,asignatura.Nombre');                
                $this->db->from('asignatura');   
                
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Asignatura_Id =asignatura.Id','INNER');
                $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
                $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
                $this->db->join('users','profesor.Usuario_id =users.Id','INNER');
                $this->db->where('curso_has_asignatura.Id', $Id_cha);
                $this->db->where('users.Id', $Id_User);                
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();                   
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                    return $r;
                } 
    }
    public function create_foro($foro,$Id_User)
    {
        if(isset($foro) && isset($Id_User))
        {
            $verificar_asg=$this->verificar_asg($Id_User,$foro['Curso_has_Asignatura_Id']);
            if($verificar_asg['bool'])
            {
                $foro['hide']=0;
                $foro['Fecha']= date('Y-m-d H:i:s');
                $foro['Estado']= true;
                $this->db->insert('foro',$foro);
                $r = array('bool' => true, 'msg'=> 'Foro Guardado correctamente');
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> $verificar_asg['msg']);
                return $r;
            }
        }
        else
        {

            $r = array('bool' => false, 'msg'=> 'Datos no ingresado');
            return $r;
        }
    }
    public function get_foro($Id_foro)
    {
        if(isset($Id_foro))
        {
            $this->db->select('foro.Id,foro.Titulo,foro.Comentario,foro.Fecha,foro.Estado,users.first_name,users.last_name,users.id as Id_User');                
            $this->db->from('foro');   
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id =foro.Curso_has_Asignatura_Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
            $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
            $this->db->join('users','profesor.Usuario_id =users.Id','INNER');
            $this->db->where('foro.Id', $Id_foro);     
            $this->db->where('foro.Hide', false);          
            $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->row();                   
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                    return $r;
                } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
            return $r;
        }
    }
    public function get_respuesta_foro($Id_foro)
    {
        if(isset($Id_foro))
        {
            $this->db->select('respuestas_foro.Id, respuestas_foro.Comentario, respuestas_foro.Fecha, users.Id as Id_User, users.first_name, users.last_name');                
            $this->db->from('respuestas_foro');   
            $this->db->join('foro','respuestas_foro.Foro_Id = foro.Id','INNER');
            $this->db->join('users','respuestas_foro.Users_id = users.Id','INNER');
            $this->db->where('foro.Id', $Id_foro);                
            $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();                   
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {
                    $r = array('bool' => false, 'msg'=> 'No hay mensajes en foro');
                    return $r;
                } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta ID FORO');
            return $r;
        } 
    }
    public function save_comentario_foro($Id_foro,$Id_User,$Comentario)
    {
        try
        {
            if(isset($Id_User) && isset($Comentario) && isset($Id_foro))
            {
                if(is_numeric($Id_User) && is_numeric($Id_foro))
                {
                    $r=true;//$this->verificar($Id_foro,$Id_User);
                    $Fecha=date('Y-m-d H:i:s');
                    if($r)
                    {
                        $datos = array( 'Comentario' => $Comentario, 
                                        'Fecha'=>$Fecha,
                                        'Foro_Id'=>$Id_foro,
                                        'Users_Id'=>$Id_User,
                                        'hide'=>false);
                        $this->db->insert('respuestas_foro',$datos);
                        $r = array('bool' => true, 'msg'=> 'respuesta Guardada correctamente');
                        return $r;
                    }
                    else
                    {   
                        $r = array('bool' => false, 'msg'=> 'No tiene permitido publicar en este foro');
                        return $r;
                    }

                }
                else
                {
                    $r = array('bool' => false, 'msg'=> 'Foro Guardado correctamente');
                    return $r;
                }
                
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'Foro Guardado correctamente');
                return $r;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function verificar_alumno_foro($Id_User,$Id_foro)
    {
        if(isset($Id_foro) && isset($Id_User))
        {
            $agno = date('Y');
            $this->db->select('foro.Id');                
            $this->db->from('foro');   
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id =foro.Curso_has_Asignatura_Id','INNER');
            $this->db->join('curso','curso.Id =curso_has_asignatura.Curso_Id','INNER');
            $this->db->join('curso_has_alumno','curso.Id =curso_has_alumno.Curso_Id ','INNER');
            $this->db->join('alumno','alumno.Id =curso_has_alaumno.Alumno_Id','INNER');
            $this->db->join('users','alumno.Usuario_id =users.Id','INNER');
            $this->db->where('foro.Id', $Id_foro);
            $this->db->where('users.Id', $Id_User);   
            $this->db->where('curso.Año',$agno);             
            $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();                   
                    $r = array('bool' => true);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                    return $r;
                } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
            return $r;
        }
    }
    public function verificar_profesor_foro($Id_User,$Id_foro)
    {
        if(isset($Id_foro) && isset($Id_User))
        {
            $this->db->select('foro.Id,curso_has_asignatura.Curso_Id');                
            $this->db->from('foro');   
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id =foro.Curso_has_Asignatura_Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
            $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
            $this->db->join('users','profesor.Usuario_id =users.Id','INNER');
            $this->db->where('foro.Id', $Id_foro);
            $this->db->where('users.Id', $Id_User);                
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row();                   
                $r = array('bool' => true,'query'=>$query);
                return $r;
            }
            else
            {

               $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
            return $r;
        }
    }
    public function change_state_foro($Id_foro)
    {
        try
        {
            if($this->see_state_foro($Id_foro)){$estado=false;}else{$estado=true;}
            $data = array('Estado' => $estado);
            $this->db->where('Id', $Id_foro);
            $this->db->update('foro', $data); 
            $r=array('bool'=>true,'Estado'=>$estado);
            return $r;
        }
        catch(Exception $e)
        {
            $r=array('bool'=>false);
            return $r;
        }
    }
    public function update_set_nota($Nota,$Alumno_Id,$Id_Set)
    {
        try
        {
            $data = array('Nota' => $Nota);
            $this->db->where('Alumno_Id', $Alumno_Id);
            $this->db->where('Set_nota_Id', $Id_Set);
            $this->db->update('nota', $data); 
            $r=array('bool'=>true);
            return $r;
        }
        catch(Exception $e)
        {
            $r=array('bool'=>false);
            return $r;
        }
    }
    private function see_state_foro($Id_foro)
    {
        $this->db->select('foro.Estado');                
        $this->db->from('foro');               
        $this->db->where('foro.Id', $Id_foro);               
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $query = $query->row();
            $r=$query->Estado;
            return $r;
        }
    }
    public function get_reglamento_notas()
    {
            $this->db->select('*');                
            $this->db->from('reglamento_notas');               
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'Error notas');
                return $r;
            }         
    }
    public function get_tipos_evaluaciones()
    {
            $this->db->select('*');                
            $this->db->from('tipos_evaluacion');               
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No existen tipos de evaluación registrados');
                return $r;
            }         
    }
    public function get_Profesor_Id($User_Id)
    {
        if(is_numeric($User_Id))
        {
            $this->db->select('Id');                
            $this->db->from('profesor'); 
            $this->db->where('Usuario_id',$User_Id);         
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row();       

                $r = array('bool' => true, 'Id'=> $query->Id);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No existen tipos de evaluación registrados');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false);
            return $r;
        }
    }
    public function get_Alumno_Id($UsuarioId)
    {
        if(isset($UsuarioId))
        {
            $this->db->select('alumno.Id');                
            $this->db->from('alumno'); 
            $this->db->where('alumno.Usuario_id',$UsuarioId);        
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row();       

                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No existen tipos de evaluación registrados');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false);
            return $r;
        } 
    }
    public function get_Id_Nota($Id_Set,$Id_Alumno)
    {
        if(isset($Id_Set) && isset($Id_Alumno))
        {
            $this->db->select('nota.Id,nota.Nota');                
            $this->db->from('nota'); 
            $this->db->where('Alumno_Id',$Id_Alumno);  
            $this->db->where('Set_nota_Id',$Id_Set);         
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row();       

                $r = array('bool' => true, 'Nota'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No existen tipos de evaluación registrados');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false);
            return $r;
        } 
    }
    public function get_Id_curso($Id_Cha)
    {
        if(isset($Id_Cha))
        {
            $this->db->select('curso_has_asignatura.Curso_Id');                
            $this->db->from('curso_has_asignatura'); 
            $this->db->where('Id',$Id_Cha);          
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row();       

                $r = array('bool' => true, 'Id'=> $query->Curso_Id);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No existen tipos de evaluación registrados');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false);
            return $r;
        } 
    }
    public function get_curso_asistencia($Id_Curso)
    {
        if(isset($Id_Curso))
        {
            $this->db->select('*');                
            $this->db->from('curso'); 
            $this->db->where('Id',$Id_Curso);          
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row();       

                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No existen tipos de evaluación registrados');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false);
            return $r;
        } 
    }
    public function get_asignatura_asistencia($Id_Cha)
    {
        if(isset($Id_Cha))
        {
            $this->db->select('asignatura.Nombre');                
            $this->db->from('asignatura'); 
             $this->db->join('curso_has_asignatura','curso_has_asignatura.Asignatura_Id =asignatura.Id','INNER');
            $this->db->where('curso_has_asignatura.Id',$Id_Cha);          
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row();  

                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No existen tipos de evaluación registrados');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false);
            return $r;
        } 
    }
    public function save_cambio_nota($Nota_nueva,$Nota_vieja,$Comentario,$Id_Nota)
    {
        try
        {
            if(isset($Nota_nueva)&&isset($Nota_vieja)&&isset($Comentario)&&isset($Id_Nota))
            {
                $fecha=date('Y-m-d');
                   $datos=array('N_Nueva'=>$Nota_nueva,
                                'N_Anterior'=>$Nota_vieja,
                                'Justificacion'=>$Comentario,
                                'Fecha'=>$fecha,
                                'Nota_Id'=>$Id_Nota,
                                'hide'=>false);
                      
                            $this->db->insert('cambiar_nota',$datos);
                            $r = array('bool' => true, 'msg'=> 'Anotacion guardada');
                            return $r;                             
                
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'Anotacion guardada correctamente');
                return $r;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function save_anotaciones($save)
    {
        try
        {
            if(isset($save))
            {
                    $profesor_Id=$this->get_profesor_Id($save['Profesor_Id']);
                    if($profesor_Id['bool'])
                    {
                        $save['Profesor_Id']=$profesor_Id['Id'];
                        $p=true;//VERIFICAR PROFESOR ASIGNATURA
                        $r=true;//VERIFICAR ALUMNO EN CURSO
                        $Fecha=date('Y-m-d H:i:s');
                        if($r)
                        {
                            $save['Date']=$Fecha;
                            $save['hide']=false;
                            $this->db->insert('hoja_vida',$save);
                            $r = array('bool' => true, 'msg'=> 'Anotacion guardada');
                            return $r;
                        }
                        else
                        {   
                            $r = array('bool' => false, 'msg'=> 'No tiene permitido anotar al alumno');
                            return $r;
                        }
                    }
                    else
                    {
                        $r = array('bool' => false, 'msg'=> 'Anotacion guardada correctamente');
                        return $r;
                    }                   
                
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'Anotacion guardada correctamente');
                return $r;
            }
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function get_Cha($Id_User,$Id_Cha)
    {
       if(isset($Id_User) && isset($Id_Cha))
        {
            $this->db->select('cha.Id, cha.Curso_Id,cha.Asignatura_Id,cha.Cantidad_Notas,cha.Cantidad_Hora_Semana');                
            $this->db->from('curso_has_asignatura cha');   
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =cha.Id','INNER');
            $this->db->join('profesor','profesor.Id = profesor_has_asignatura.Profesor_Id','INNER');
            $this->db->join('users','profesor.Usuario_id = users.Id','INNER');
            $this->db->where('cha.Id', $Id_Cha);      
            $this->db->where('users.Id', $Id_User); 
             $this->db->where('cha.hide', 0);           
            $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->row();                   
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {
                    $r = array('bool' => false, 'msg'=> 'No hay Asignatura registrada');
                    return $r;
                } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta ID CHA');
            return $r;
        } 
    }
    public function guarda_asistencia($rows)
    {
        try
        {
            $this->db->insert_batch('asistencia',$rows);
            $r = array('bool' => true);
            return $r;
           
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function save_setting_nota($rows)
    {
        try
        {
            $this->db->insert_batch('set_nota',$rows);
            $r = array('bool' => true);
            return $r;
           
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function edit_setting_nota($Id_set_nota,$data)
    {
        try
        {
            $this->db->where('Id', $Id_set_nota);
            $this->db->update('set_nota', $data); 
            $r = array('bool' => true);
            return $r;           
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function delete_setting_nota($Id_Cha)
    {
        $this->db->where('Curso_has_asignatura_Id', $Id_Cha);
        $this->db->delete('set_nota'); 
    }    
    public function update_Notas_Curso_has_Asignatura($Id_Cha,$Cantidad_Notas)
    {
        try
        {
            $data = array('Cantidad_Notas' => $Cantidad_Notas);
            $this->db->where('Id', $Id_Cha);
            $this->db->update('curso_has_asignatura', $data); 
            $r=array('bool'=>true);
            return $r;
        }
        catch(Exception $e)
        {
            $r=array('bool'=>false);
            return $r;
        }
    }
    public function get_notas_curso($Id_Profesor,$Id_C)
    {
        if(isset($Id_C) && isset($Id_Profesor))
        {
            $this->db->select('curso_has_asignatura.Id');                
            $this->db->from('nota');   
            $this->db->join('set_nota','nota.Set_nota_Id=set_nota.Id','INNER');
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id=set_nota.Curso_has_asignatura_Id','INNER');
            $this->db->join('curso','curso.Id=curso_has_asignatura.Curso_Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
            $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
            $this->db->join('curso_has_alumno','curso.Id = curso_has_alumno.Curso_Id','INNER');
            $this->db->join('alumno','alumno.Id = curso_has_alumno.Alumno_Id','INNER');
            $this->db->join('users','alumno.Usuario_id =users.Id','INNER');
            $this->db->where('profesor.Id', $Id_Profesor);   
            $this->db->where('curso.Id', $Id_C);             
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
            return $r;
        }
    }
    public function get_set_notas_curso($Id_Profesor,$Id_C)
    {
        if(isset($Id_C) && isset($Id_Profesor))
        {
            $this->db->select('set_nota.Id,set_nota.Curso_has_asignatura_Id as Id_Cha,set_nota.Tipo,set_nota.Fecha, set_nota.Registrada');                
            $this->db->from('set_nota');   
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id=set_nota.Curso_has_asignatura_Id','INNER');
            $this->db->join('curso','curso.Id=curso_has_asignatura.Curso_Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
            $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
            $this->db->where('profesor.Id', $Id_Profesor);   
            $this->db->where('curso.Id', $Id_C);    
            $this->db->order_by("set_nota.Fecha", "asc");
          //  $this->db->where('set_nota.Registrada', false);           
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
            return $r;
        }
    }
    public function get_registrar_nota($Id_set_nota,$Id_Profesor)
    {
    }
    public function get_curso_set_nota($Id_set_nota,$Id_Profesor)
    {
        if(isset($Id_set_nota) && isset($Id_Profesor))
        {
            $this->db->select('curso.Id,set_nota.Fecha');                
            $this->db->from('curso');   
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Curso_Id=curso.Id','INNER');
            $this->db->join('set_nota','set_nota.Curso_has_asignatura_Id=curso_has_asignatura.Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
            $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
            $this->db->where('profesor.Id', $Id_Profesor);   
            $this->db->where('set_nota.Id', $Id_set_nota);            
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row();                   
                $r = array('bool' => true, 'Id'=> $query->Id,'Fecha'=> $query->Fecha);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }
    }
    public function save_notas($rows)
    {
        try
        {
            $this->db->insert_batch('nota',$rows);
            $r = array('bool' => true);
            return $r;           
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function revisado_set_nota($Id)
    {
        try
        {
            $data = array('Registrada' => true);
            $this->db->where('Id', $Id);
            $this->db->update('set_nota', $data); 
            $r=array('bool'=>true);
            return $r;           
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function get_notas_alumnos($Id_Profesor,$Id_Curso)
    {
        if(isset($Id_Curso) && isset($Id_Profesor))
        {
            $this->db->select('users.Id as Id_U,users.username,users.first_name,users.last_name,curso_has_asignatura.Cantidad_Notas,set_nota.Id as Id_set,nota.Nota,nota.Id as Nota_Id,curso_has_asignatura.Id as Id_Cha');                
            $this->db->from('nota');   
            $this->db->join('set_nota','set_nota.Id=nota.Set_nota_Id','INNER');
            $this->db->join('curso_has_asignatura','set_nota.Curso_has_asignatura_Id=curso_has_asignatura.Id','INNER');
            $this->db->join('curso','curso.Id=curso_has_asignatura.Curso_Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');            
            $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
            $this->db->join('curso_has_alumno','curso.Id =curso_has_alumno.Curso_Id','INNER');
            $this->db->join('alumno','alumno.Id = curso_has_alumno.Alumno_Id','INNER');
            $this->db->join('users','users.Id = alumno.Usuario_id','INNER');
            $this->db->where('profesor.Id', $Id_Profesor);   
            $this->db->where('curso.Id', $Id_Curso); 
            $this->db->where('alumno.Id`=`nota.Alumno_Id');                   
            $this->db->order_by("users.last_name", "asc"); 
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();  
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }    
    }
    public function get_Hoja_vida($Id_Curso)
    {
        if(isset($Id_Curso))
        {
            $this->db->select('hoja_vida.Id,hoja_vida.Tipo,hoja_vida.Descripcion,hoja_vida.Alumno_Id,hoja_vida.Date,users.first_name,users.last_name');                
            $this->db->from('hoja_vida');   
            $this->db->join('profesor','profesor.Id =hoja_vida.Profesor_Id','INNER');   
            $this->db->join('curso_has_alumno','curso_has_alumno.Alumno_Id =hoja_vida.Alumno_Id','INNER'); 

            $this->db->join('users','users.Id = profesor.Usuario_id','INNER');  
            $this->db->where('curso_has_alumno.Curso_Id', $Id_Curso);            
            $this->db->order_by("users.last_name", "asc"); 
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }  
    }
    public function get_notas_curso_jefe($Id_Curso)
    {
        if(isset($Id_Curso))
        {
            $this->db->select('nota.Nota,nota.Alumno_Id,asignatura.Id,set_nota.Tipo,set_nota.Id as Id_Set,curso_has_asignatura.Cantidad_Notas');                
            $this->db->from('nota');   
            $this->db->join('set_nota','set_nota.Id =nota.Set_nota_Id','INNER');  
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id =set_nota.Curso_has_asignatura_Id','INNER');     
            $this->db->join('asignatura','asignatura.Id =curso_has_asignatura.Asignatura_Id','INNER');  
            $this->db->where('curso_has_asignatura.Curso_Id', $Id_Curso);  
            $this->db->order_by("set_nota.Fecha", "asc");
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }  
    }
    public function get_curso_jefe($Id_Curso,$Id_Profesor)
    {
        if(isset($Id_Curso) && isset($Id_Profesor))
        {
            $this->db->select('*');                
            $this->db->from('curso'); 
            $this->db->where('curso.Id', $Id_Curso);  
            $this->db->where('curso.Profesor_Id', $Id_Profesor);  
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }
    }
    public function save_reunion($Id_Curso,$Fecha,$Hora,$Motivo)
    {
        try
        {
            $data=array('Fecha'=>$Fecha,'Curso_Id'=>$Id_Curso,'Motivo'=>$Motivo,'Hora'=>$Hora,'hide'=>false);
            $this->db->insert('reuniones',$data);
            $r = array('bool' => true);
            return $r;           
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function edit_reunion($Id_reunion,$Fecha,$Hora,$Motivo)
    {
        try
        {
            $data=array('Fecha'=>$Fecha,'Motivo'=>$Motivo,'Hora'=>$Hora,'hide'=>false);
            $this->db->where('Id', $Id_reunion);
            $this->db->update('reuniones', $data); 
            $r = array('bool' => true);
            return $r;           
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }
    public function get_reuniones($Id_Curso)
    {
        if(isset($Id_Curso))
        {
            $this->db->select('*');                
            $this->db->from('reuniones'); 
            $this->db->where('reuniones.Curso_Id', $Id_Curso);  
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }
    }
    public function get_asignaturas_jefe($Id_Curso,$Agno=NULL)
    {
        try
        {
                if(!isset($Id_Curso)){$r = array('bool' => false, 'msg'=> 'Falta información');return $r;}
                if(is_null($Agno)){$Agno=date("Y");}
                $this->db->select('asi.Nombre,asi.Id as Id_Asi,curso_has_asignatura.Id as Id_Cha,curso_has_asignatura.Cantidad_Notas,users.first_name,users.last_name,users.Id,users.email');                
                $this->db->from('asignatura asi');                          
                
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Asignatura_Id = asi.Id', 'INNER');
                $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id', 'INNER');
                $this->db->join('profesor','profesor_has_asignatura.Profesor_Id = profesor.Id','INNER');
                $this->db->join('users','users.Id =profesor.Usuario_Id','INNER');
                $this->db->where('curso_has_asignatura.Curso_Id', $Id_Curso);
                $this->db->where('asi.hide', 0);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();
                    //var_dump($query);
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
                    return $r;
                }
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'Error al verificar curso');
            return $r;
        }
    }
    public function delete_reunion($id)
    {
        $this->db->where('Id', $id);
        $this->db->delete('reuniones');
        $r = array('bool' => true);
        return $r;  

    }
    // DASHBOARD
    public function get_all_reuniones($Id_Profesor)
    {
        if(is_numeric($Id_Profesor))
        {
            $this->db->select('*');                
            $this->db->from('reuniones'); 
            $this->db->join('curso','curso.Id=reuniones.Curso_Id','INNER');
            $this->db->where('curso.Profesor_Id', $Id_Profesor);  
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }
    }
    public function get_all_evaluaciones($Id_Profesor)
    {
        if(is_numeric($Id_Profesor))
        {
            $this->db->select('curso.Grado,curso.Letra,set_nota.Fecha,set_nota.Tipo,asignatura.Nombre');                
            $this->db->from('set_nota'); 
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id=set_nota.Curso_has_asignatura_Id','INNER');
            $this->db->join('asignatura','asignatura.Id=curso_has_asignatura.Asignatura_Id','INNER');
            $this->db->join('curso','curso.Id=curso_has_asignatura.Curso_Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_asignatura_Id=set_nota.Curso_has_asignatura_Id','INNER');

            $this->db->where('curso.Profesor_Id', $Id_Profesor);  
            $this->db->order_by("set_nota.Fecha", "asc");
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }
    }

    public function get_prox_prueba($Id_Profesor)
    {
        if(is_numeric($Id_Profesor))
        {
            $this->db->select('curso.Grado,curso.Letra,set_nota.Fecha,set_nota.Tipo,asignatura.Nombre');                
            $this->db->from('set_nota'); 
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id=set_nota.Curso_has_asignatura_Id','INNER');
            $this->db->join('asignatura','asignatura.Id=curso_has_asignatura.Asignatura_Id','INNER');
            $this->db->join('curso','curso.Id=curso_has_asignatura.Curso_Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_asignatura_Id=curso_has_asignatura.Id','INNER');

            $this->db->where('profesor_has_asignatura.Profesor_Id', $Id_Profesor);  
            $this->db->where('set_nota.Registrada', false); 
            $this->db->order_by("set_nota.Fecha", "asc");
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }
    }
    public function todos_usuarios()
    {
         $this->db->select('*');  
         $this->db->from('users');
         $query = $this->db->get();
         return $query;
    }
    public function get_debe_prueba($Id_Profesor)
    {
        if(is_numeric($Id_Profesor))
        {
            $this->db->select('curso.Grado,curso.Letra,set_nota.Fecha,set_nota.Tipo,asignatura.Nombre');                
            $this->db->from('set_nota'); 
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id=set_nota.Curso_has_asignatura_Id','INNER');
            $this->db->join('asignatura','asignatura.Id=curso_has_asignatura.Asignatura_Id','INNER');
            $this->db->join('curso','curso.Id=curso_has_asignatura.Curso_Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_asignatura_Id=set_nota.Curso_has_asignatura_Id','INNER');

            $this->db->where('profesor_has_asignatura.Profesor_Id', $Id_Profesor);  
            $this->db->where('set_nota.Registrada', false); 
            $this->db->where('set_nota.hide', false); 
            $this->db->order_by("set_nota.Fecha", "asc");
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'Falta información');
            return $r;
        }
    }
    //MI CURSO
    public function get_alumno($Id_alumno,$Id_curso)
    {
        $this->db->select('users.first_name,users.last_name,users.email,alumno');                
        $this->db->from('users'); 
        $this->db->join('curso','curso.Id=reuniones.Curso_Id','INNER');
        $this->db->where('curso.Profesor_Id', $Id_Profesor);  
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $query = $query->result();                   
            $r = array('bool' => true, 'query'=> $query);
            return $r;
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
            return $r;
        } 
    }
    public function show_alumno($Id_alumno,$Id_curso,$Id_Profesor)
    {
        $this->db->select('users.username,users.first_name,users.last_name,users.email,users.sex, users.f_nacimiento, users.cellphone, users.phone, alumno.Tipo_Sangre, alumno.Alergias');                
        $this->db->from('users'); 
        $this->db->join('alumno','alumno.Usuario_id=users.id','INNER');
        $this->db->join('curso_has_alumno','curso_has_alumno.Alumno_Id=alumno.Id','INNER');
        $this->db->join('curso','curso_has_alumno.Curso_Id=curso.Id','INNER');
        $this->db->where('users.id', $Id_alumno);  
        $this->db->where('curso.id', $Id_curso);  
        $this->db->where('curso.Profesor_Id', $Id_Profesor);  
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $query = $query->row();                   
            $r = array('bool' => true, 'query'=> $query);
            return $r;
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
            return $r;
        } 
    }
    public function get_all_notas($Id_Curso)
    {
        $this->db->select('users.id as User_Id,alumno.Id as Alumno_Id,nota.Nota,set_nota.Curso_has_Asignatura_Id as Id_Cha, nota.Set_nota_Id');                
        $this->db->from('users'); 
        $this->db->join('alumno','alumno.Usuario_id=users.id','INNER');
        $this->db->join('curso_has_alumno','curso_has_alumno.Alumno_Id=alumno.Id','INNER');
        $this->db->join('curso','curso_has_alumno.Curso_Id=curso.Id','INNER');
        $this->db->join('nota','alumno.Id=nota.Alumno_Id','INNER');
        $this->db->join('set_nota','nota.Set_nota_Id=set_nota.Id','INNER');
        $this->db->where('curso_has_alumno.Curso_Id', $Id_Curso);  
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $query = $query->result();                   
            $r = array('bool' => true, 'query'=> $query);
            return $r;
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
            return $r;
        } 
    }
    public function get_all_set_notas($Id_Curso)
    {
        $this->db->select('set_nota.Id,set_nota.Curso_has_Asignatura_Id as Id_Cha,set_nota.Fecha,set_nota.Tipo');                
        $this->db->from('set_nota'); 
        $this->db->join('curso_has_asignatura','curso_has_asignatura.Id=set_nota.Curso_has_Asignatura_Id','INNER');
        $this->db->where('curso_has_asignatura.Curso_Id', $Id_Curso);
        $this->db->order_by("set_nota.Fecha", "asc");  
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $query = $query->result();                   
            $r = array('bool' => true, 'query'=> $query);
            return $r;
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay nota asociada');
            return $r;
        } 
    }
    public function edit_foro($Id_foro,$data)
    {
        try
        {
            $this->db->where('Id', $Id_foro);
            $this->db->update('foro', $data); 
            $r = array('bool' => true);
            return $r;           
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error de sistema de guardado =>'.$e);
            return $r;
        }
    }

}
