<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Alumno_model extends CI_MODEL 
{
 
 	//private $_Agno=date("Y-01-01"); 
    function __construct()
    {
    	parent::__construct();
    	$this->load->database();
        $this->load->model('security_model');
    	$this->load->library('security_lib');
    }
    function get_Alumno_Id2($User_Id)
    {
        if(is_numeric($User_Id))
        {
            $this->db->select('Id');                
            $this->db->from('alumno'); 
            $this->db->where('Id',$User_Id);         
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row(); 
                $r = array('bool' => true, 'Id'=> $query->Id);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay alumno registrado');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false, 'msg'=> 'No hay alumno registrado');
            return $r;
        }
    }
    function get_Alumno_Id($User_Id)
    {
    	if(is_numeric($User_Id))
        {
            $this->db->select('Id');                
            $this->db->from('alumno'); 
            $this->db->where('Usuario_id',$User_Id);         
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row(); 
                $r = array('bool' => true, 'Id'=> $query->Id);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay alumno registrado');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false, 'msg'=> 'No hay alumno registrado');
            return $r;
        }
    }
    function get_Curso_Id($Alumno_Id,$Fecha=NULL)
    {
    	if(is_numeric($Alumno_Id))
        {
        	if($Fecha==NULL){$Fecha=date('Y');}
            $this->db->select('curso.Id,curso.Grado,curso.Letra,curso.Tipo,curso.Profesor_Id');                
            $this->db->from('curso_has_alumno'); 
            $this->db->join('curso','curso.Id =curso_has_alumno.Curso_Id','INNER');       
            $this->db->where('Alumno_Id',$Alumno_Id);   
            $this->db->where('curso.Agno',$Fecha);          
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row(); 
                $r = array('bool' => true, 'Id'=> $query->Id, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'Alumno NO registrado');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false, 'msg'=> 'Error al ingresar alumno');
            return $r;
        }
    }
    function get_Hoja_Vida($Alumno_Id)
    {
    	if(is_numeric($Alumno_Id))
        {
        	$fecha=date('Y');
            $this->db->select('*');                
            $this->db->from('hoja_vida'); 
            $this->db->where('Alumno_Id',$Alumno_Id);   
            $this->db->where('Date >',$fecha);    
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result(); 
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay anotaciones registradas');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false, 'msg'=> 'Error al ingresar ID de Alumno');
            return $r;
        }
    }
    function get_Docentes_Asignatura($Alumno_Id,$Fecha=NULL)
    {
    	if(isset($Alumno_Id))
        {
        	if($Fecha==NULL){$Fecha=date('Y');}
            $this->db->select('users.Id, users.first_name, users.last_name, users.email, asignatura.Nombre');                
            $this->db->from('users');   
            $this->db->join('profesor','profesor.Usuario_id =users.Id','INNER');            
            $this->db->join('profesor_has_asignatura','profesor.Id = profesor_has_asignatura.Profesor_Id','INNER');
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id = profesor_has_asignatura.Curso_has_Asignatura_Id','INNER');
            $this->db->join('asignatura','asignatura.Id =curso_has_asignatura.Asignatura_Id','INNER');
            $this->db->join('curso','curso.Id =curso_has_asignatura.Curso_Id','INNER');
            $this->db->join('curso_has_alumno','curso_has_asignatura.Curso_Id =curso_has_alumno.Curso_Id','INNER');    
            $this->db->join('alumno','alumno.Id =curso_has_alumno.Alumno_Id','INNER');     	
            $this->db->where('curso.Agno', $Fecha); 
            $this->db->where('alumno.Id', $Alumno_Id);  
            $this->db->where('curso_has_asignatura.hide',0);
            $this->db->where('profesor_has_asignatura.hide',0);
            $this->db->where('curso_has_alumno.hide',0);
            $this->db->where('profesor.hide',0);             
            $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();                   
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay docentes');
                    return $r;
                } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay docentes');
            return $r;
        }
    }
    function get_Mis_Evaluaciones($Alumno_Id){

            $this->db->select('users.Id, users.first_name, users.last_name, users.email, asignatura.Nombre, set_nota.Fecha, set_nota.Tipo, set_nota.Id as id_nota_i');                
            $this->db->from('users');   
            $this->db->join('alumno','alumno.Usuario_Id = users.id','INNER');
            $this->db->join('curso_has_alumno','curso_has_alumno.Alumno_Id = alumno.Id','INNER');
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Curso_Id = curso_has_alumno.Curso_Id','INNER');
            $this->db->join('set_nota','set_nota.Curso_has_Asignatura_Id = curso_has_asignatura.Id','INNER');
            $this->db->join('asignatura','curso_has_asignatura.Asignatura_Id = asignatura.Id','INNER'); 
            $this->db->where('alumno.Id', $Alumno_Id);  
            $this->db->where('curso_has_asignatura.hide',0); 
            //$this->db->order_by('asignatura.Id','asc');
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {

                $r = array('bool' => false, 'msg'=> 'No hay docentes');
                return $r;
            } 
        
    }
    function get_Ramos($Alumno_Id){
        $this->db->select('asignatura.Nombre, asignatura.hora_total');                
        $this->db->from('users');   
        $this->db->join('alumno','alumno.Usuario_Id = users.id','INNER');
        $this->db->join('curso_has_alumno','curso_has_alumno.Alumno_Id = alumno.Id','INNER');
        $this->db->join('curso_has_asignatura','curso_has_asignatura.Curso_Id = curso_has_alumno.Curso_Id','INNER');
        $this->db->join('asignatura','curso_has_asignatura.Asignatura_Id = asignatura.Id','INNER'); 
        $this->db->where('alumno.Id', $Alumno_Id);  
        $this->db->where('curso_has_asignatura.hide',0); 
        //$this->db->order_by('asignatura.Id','asc');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $query = $query->result();                   
            $r = array('bool' => true, 'query'=> $query);
            return $r;
        }
        else
        {

            $r = array('bool' => false, 'msg'=> 'No hay docentes');
            return $r;
        }
    }
    function get_Mis_Notas($Alumno_Id,$Fecha=NULL)
    {
        if(isset($Alumno_Id))
        {
            
            $this->db->select('users.Id, users.first_name, users.last_name, users.email, asignatura.Nombre, nota.Nota, nota.Set_nota_Id ');                
            $this->db->from('users');   
            $this->db->join('alumno','alumno.Usuario_id = users.id','INNER');
            $this->db->join('nota','nota.Alumno_Id = alumno.Id','INNER');
            $this->db->join('set_nota','set_nota.Id = nota.Set_nota_Id','INNER');
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id = set_nota.Curso_has_Asignatura_Id','INNER');
            $this->db->join('curso','curso_has_asignatura.Curso_Id = curso.Id','INNER');
            $this->db->join('asignatura','curso_has_asignatura.Asignatura_Id = asignatura.Id','INNER');
            
            $this->db->where('alumno.Id', $Alumno_Id);  
            $this->db->where('curso_has_asignatura.hide',0); 
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {

                $r = array('bool' => false, 'msg'=> 'No hay docentes','query'=> null);
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay docentes','query'=> null);
            return $r;
        }
    }
    function verificar_alumno_foro($Id_U,$Id_Foro)
    {
        if(isset($Id_Foro) && isset($Id_U))
        {
            $this->db->select('foro.Id');                
            $this->db->from('foro');   
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id =foro.Curso_has_Asignatura_Id','INNER');
            $this->db->join('curso_has_alumno','curso_has_alumno.Curso_Id =curso_has_asignatura.Curso_Id','INNER');
            $this->db->join('alumno','alumno.Id =curso_has_alumno.Alumno_Id','INNER');
            $this->db->join('users','alumno.Usuario_id = users.Id','INNER');
            $this->db->where('foro.Id', $Id_Foro);
            $this->db->where('users.Id', $Id_U);                
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true);
                return $r;
            }
            else
            {

               $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay Asignaturas asignadas');
            return $r;
        }
    }
    function get_Docentes_Asignatura_ByGroup($Alumno_Id,$Fecha=NULL)
    {
    	if(isset($Alumno_Id))
        {
        	if($Fecha==NULL){$Fecha=date('Y');}
            $this->db->select('users.Id');                
            $this->db->from('users');   
            $this->db->join('profesor','profesor.Usuario_id =users.Id','INNER');
            $this->db->join('profesor_has_asignatura','profesor.Id = profesor_has_asignatura.Profesor_Id','INNER');
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Asignatura_Id = profesor_has_asignatura.Asignatura_Id','INNER');
			$this->db->join('asignatura','asignatura.Id =curso_has_asignatura.Asignatura_Id','INNER');            
            $this->db->join('curso','curso.Id =curso_has_asignatura.Curso_Id','INNER');          
            $this->db->join('curso_has_alumno','curso.Id =curso_has_alumno.Curso_Id','INNER');    
            $this->db->join('alumno','alumno.Id =curso_has_alumno.Alumno_Id','INNER');     	
            $this->db->where('curso.Agno', $Fecha); 
            $this->db->where('alumno.Id', $Alumno_Id);
            $this->db->group_by("users.Id");           
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay docentes');
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay docentes');
            return $r;
        }
    }
    function get_fechas_eventos($id_usuario){
      $query = $this->db->query('SELECT asignatura.Nombre,horario.Dia,horario.Año, bloques.Hora_Inicio, bloques.Hora_Fin FROM horario join bloques on horario.Bloques_Id = bloques.Id JOIN curso_has_asignatura on curso_has_asignatura.Id = horario.Curso_has_Asignatura_Id join curso_has_alumno on curso_has_alumno.Curso_Id = curso_has_asignatura.Curso_Id join alumno on alumno.Id = curso_has_alumno.Alumno_Id join asignatura on curso_has_asignatura.Asignatura_Id = asignatura.Id WHERE alumno.Id = '.$id_usuario);
      $data = "";
      foreach ($query->result() as $key) {
          $titulo = $key->Nombre;
          $fecha_i = $key->Hora_Inicio;
          $fecha_f = $key->Hora_Fin;
          $agno = $key->Año;
          $foo = strtotime($fecha_i);
          $foo2 = strtotime($fecha_f); 
          $horaI = date('H',$foo);
          $minutoI = date('i',$foo);
          $horaF = date('H',$foo2);
          $minutoF = date('i',$foo2);
          //$comentario = $key->comentario;
          $dia = strtotime('07/01/2015');
          $dia_final = strtotime('07/22/2015');
          $dia_aux = $dia;
          if($key->Dia == "Lunes"){
            $data = $data."{ title: '".$titulo."',
                           start: new Date(y,m,d, ".$horaI.", ".$minutoI."),
                           end: new Date(y,m,d, ".$horaF.", ".$minutoF."), 
                           allDay: false,
                           dow: [1,4]
                         },";
                     }

        }
        return $data;
   }
   function get_bloques(){
        $this->db->select('bloques.Hora_Inicio, bloques.Hora_Fin');                
        $this->db->from('bloques');   
        $this->db->order_by('bloques.Hora_Inicio','asc');
        $this->db->where('Id >',1);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $query = $query->result();                   
            $r = array('bool' => true, 'query'=> $query);
            return $r;
        }
        else
        {

            $r = array('bool' => false, 'msg'=> 'No hay bloques');
            return $r;
        }
   }
   function get_asistencia($Alumno_Id){
        if(isset($Alumno_Id))
        {            
            $this->db->select('users.Id, users.first_name, users.last_name, users.email, asignatura.Nombre, curso.Letra, curso.Grado, curso.Tipo, asistencia.presente, asistencia.Fecha_asistencia,horario.Dia');                
            $this->db->from('users');   
            $this->db->join('alumno','alumno.Usuario_id = users.id','INNER');
            $this->db->join('curso_has_alumno','curso_has_alumno.Alumno_Id = alumno.Id');
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Curso_Id = curso_has_alumno.Curso_Id');
            $this->db->join('asistencia','asistencia.Curso_has_Asignatura_Id = curso_has_asignatura.Id');
            $this->db->join('curso','curso.Id = curso_has_alumno.Curso_Id');
            $this->db->join('asignatura','asignatura.Id = curso_has_asignatura.Asignatura_Id');
            $this->db->join('horario','horario.Curso_has_Asignatura_Id = curso_has_asignatura.Id');
            /*$this->db->join('asistencia','asistencia.Alumno_Id = alumno.Id','INNER');
            $this->db->join('curso_has_asignatura','curso_has_asignatura.Id = asistencia.Curso_has_Asignatura_Id','INNER');
            $this->db->join('curso','curso_has_asignatura.Curso_Id = curso.Id','INNER');
            $this->db->join('asignatura','curso_has_asignatura.Asignatura_Id = asignatura.Id','INNER');*/            
            $this->db->where('alumno.Id', $Alumno_Id);   
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->result();                   
                $r = array('bool' => true, 'query'=> $query);
                return $r;
            }
            else
            {

                $r = array('bool' => false, 'msg'=> 'No hay docentes','query'=> null);
                return $r;
            } 
        }
        else
        {
            $r = array('bool' => false, 'msg'=> 'No hay docentes','query'=> null);
            return $r;
        }
   }
   function get_horario(){
        $this->db->select('bloques.Hora_Inicio, bloques.Hora_Fin,horario.Dia,asignatura.Nombre');                
        $this->db->from('horario');
        $this->db->join('bloques','bloques.Id = horario.Bloques_Id','INNER');   
        $this->db->join('curso_has_asignatura','curso_has_asignatura.Id = horario.Curso_has_Asignatura_Id','INNER');
        $this->db->join('asignatura','curso_has_asignatura.Asignatura_Id = asignatura.Id','INNER');
        $this->db->order_by('bloques.Hora_Inicio','asc');
        //$this->db->where('bloques.Id >',1);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $query = $query->result();                   
            $r = array('bool' => true, 'query'=> $query);
            return $r;
        }
        else
        {

            $r = array('bool' => false, 'msg'=> 'No hay bloques');
            return $r;
        } 
   }
    function get_Asignaturas_Curso($Alumno_Id,$Fecha=NULL)
    {
    	try
        {
                if(!isset($Alumno_Id)){$r = array('bool' => false, 'msg'=> 'Falta información');return $r;}
                if(is_null($Fecha)){$Fecha=date("Y");}
                $this->db->select('asi.Id,asi.Nombre,asi.Descripcion,asi.hora_total,curso_has_asignatura.Id as cha_Id');                
                $this->db->from('asignatura asi');                          
               
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Asignatura_Id = asi.Id', 'INNER');
                $this->db->join('curso','curso_has_asignatura.Curso_Id = curso.Id', 'INNER');
                $this->db->join('curso_has_alumno','curso_has_alumno.Curso_Id = curso.Id', 'INNER');
                //$this->db->join('curso_has_alumno','curso_has_alumno.Curso_Id = curso.Id', 'INNER');
                $this->db->where('curso.Agno',$Fecha);
                $this->db->where('curso_has_alumno.Alumno_Id', $Alumno_Id);
                $this->db->where('asi.hide', 0);
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();
                    //var_dump($query);
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay cursos asignado');
                    return $r;
                }
        }
        catch(Exception $e)
        {
            $r=array('bool'=>FALSE,'msg'=>'Error de sistema Asignaturas');
            return $r;
        }
    }
    public function get_foros_curso2($Id_User,$Id_Curso)
    {
                $Agno=date('Y');
                $this->db->select('foro.Titulo,foro.Comentario,foro.Curso_has_Asignatura_Id as Id_cha,foro.Fecha,foro.Id,foro.Estado');                
                $this->db->from('foro');   
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Id =foro.Curso_has_Asignatura_Id','INNER');
                $this->db->join('curso','curso.Id =curso_has_asignatura.Curso_Id','INNER');
                $this->db->join('profesor_has_asignatura','profesor_has_asignatura.Curso_has_Asignatura_Id =curso_has_asignatura.Id','INNER');
                $this->db->join('profesor','profesor.Id =profesor_has_asignatura.Profesor_Id','INNER');
                $this->db->join('users','profesor.Usuario_id =users.Id','INNER');
                $this->db->where('curso.Id', $Id_Curso);
                $this->db->where('curso.Agno', $Agno);
                $this->db->where('users.Id', $Id_User);  
                $this->db->where('foro.hide', 0);               
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();              
                    //var_dump($query);     
                    $r = array('bool' => true, 'query'=> $query);
                    return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay Foros asignadas');
                    return $r;
                } 
    }
    public function get_foros_curso($Id_User,$Id_Cha)
    {
        try
        {
                $Agno=date('Y');
                $this->db->select('foro.Titulo,foro.Comentario,foro.Curso_has_Asignatura_Id as Id_cha,foro.Fecha,foro.Id,foro.Estado');                
                $this->db->from('foro');   
                $this->db->join('curso_has_asignatura','curso_has_asignatura.Id = foro.Curso_has_Asignatura_Id','INNER');
                $this->db->join('curso','curso.Id =curso_has_asignatura.Curso_Id','INNER');
                $this->db->join('curso_has_alumno','curso_has_alumno.Curso_Id =curso.Id','INNER');
                $this->db->join('alumno','alumno.Id =curso_has_alumno.Alumno_Id','INNER');
                
                $this->db->where('curso_has_asignatura.Curso_Id', $Id_Cha);
                $this->db->where('curso.Agno', $Agno);
                $this->db->where('alumno.Id', $Id_User);  
                $this->db->where('foro.hide', 0);               
                $query = $this->db->get();
                if($query->num_rows()>0)
                {
                    $query = $query->result();             
                    
                        $r = array('bool' => true, 'query'=> $query);
                        return $r;
                }
                else
                {

                    $r = array('bool' => false, 'msg'=> 'No hay Foros registrados');
                    return $r;
                } 
        }
        catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la busqueda de foros');
                    return $r;

        }
    }

    //Academia
    public function get_apoderado($Id_alumno)
    {
        if(is_numeric($Id_alumno))
        {
            $this->db->select('Id');                
            $this->db->from('alumno'); 
            $this->db->join('apoderado','apoderado.Id =alumno.Apoderado_Id','INNER');       
            $this->db->where('Id',$User_Id);         
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
                $query = $query->row(); 
                $r = array('bool' => true, 'Id'=> $query->Id);
                return $r;
            }
            else
            {
                $r = array('bool' => false, 'msg'=> 'No hay alumno registrado');
                return $r;
            } 
        }
        else
        {
            $r=array('bool'=>false, 'msg'=> 'No hay alumno registrado');
            return $r;
        }
    }
    public function get_alumno($Id_alumno)
    {
        if(is_numeric($Id_alumno))
        {
            $this->db->select('alumno.Id as Alumno_Id, users.Id as Usuario_Id, users.first_name, users.last_name');  
            $this->db->from('users');  
            $this->db->join('alumno',  'users.id = alumno.Usuario_Id', 'INNER');
            $this->db->where('alumno.id', $Id_alumno);
            $this->db->where('alumno.hide', false);
            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
                $r=array('bool'=>TRUE,'one'=>$query->row());
                return $r;
            }
            else
            {
                $r=array('bool'=>FALSE,'msg'=>'NO SE ECONTRARON ALUMNOS');
                return $r;
            }
        }
        else
        {
            $r=array('bool'=>FALSE,'msg'=>'Error en Id');
            return $r;
            
        }

            
    }
}
?>