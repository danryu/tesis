<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Security_model extends CI_MODEL {
 
    function __construct()
    {
    	parent::__construct();
    	$this->load->database();
    	$this->load->library('security_lib');
    }
    public function cargarUser($password,$rut)
    {
    	try
    	{
	    	if(isset($password)&& isset($rut))
	    	{

	    		$query=$this->ver_user($rut);
	    		if($query!==NULL)
	    		{
	    			;
	    			$paswordAC=$password;
	    			$paswordDB=$query->Password;
	    		 	$result=$this->security_lib->verify_pass($paswordAC,$paswordDB);
	    		 	
	    		 	if($result)
	    		 	{
	    		 		$resultado=array(	'Id'=>$query->Id,
	    		 							'Nombre'=>$query->Nombre,
				    						'Apellido'=>$query->Apellido,
				    						'Rut'=>$query->Rut,
				    						'Fnacimiento'=>$query->Fnacimiento,
				    						'Telefono_fijo'=>$query->Telefono_fijo,
				    						'Telefono_celular'=>$query->Telefono_celular,
				    						'Rol_Id'=>$query->Rol_Id,
				   							'Email'=>$query->Email);	
	    		 		return $resultado;
	    		 	}
	    		 	else
	    		 	{
	    		 		echo "chao";
	    		 		return false;
	    		 	}
	    		}
	    	}
	    	else
	    	{
	    		return false;
	    	}
    	}
    	catch(Exception $e)
    	{
    		return false;
    	}
    }
    private function ver_user($rut)
    {
    	try
    	{
    		$query = $this->db->get_where('usuario',array('Rut' => $rut));
    		 if($query->num_rows() > 0 )
    		 {
    		 	 return $query->row();
    		 }
        
    	}
    	catch(Exception $e)
    	{
    		return NULL;
    	}
    }
    public function find_rol($Id)
    {
    	try
    	{
    		if(isset($Id))
    		{
    			$query = $this->db->get_where('users_groups',array('user_id' => $Id));
	    		 if($query->num_rows() > 0 )
	    		 {
	    		 	$query=$query->result();   		 	
	    		 	$r=array('bool'=>true,'query'=>$query);
	    		 	return $r;
	    		 }
	    		 else
	    		 {
	    		 	$r = array('bool' => false, 'msg'=> 'Error en la bd');
	            	return $r;
	    		 }
    		}
    		else    			
	        {
	            $r = array('bool' => false, 'msg'=> 'Error en la bd');
	            return $r;
	        }

    	}
    	catch(Exception $e)
        {
            $r = array('bool' => false, 'msg'=> 'Error en la bd','e'=>$e);
            return $r;
        }
    }
}
?>