-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 27-04-2015 a las 21:54:13
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `tesis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
`Id` int(11) NOT NULL,
  `Tipo_Sangre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alergias` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Apoderado_Id` int(11) NOT NULL,
  `hide` tinyint(1) DEFAULT NULL,
  `Usuario_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apoderado`
--

CREATE TABLE IF NOT EXISTS `apoderado` (
`Id` int(11) NOT NULL,
  `Comuna` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Domicilio` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Wsp` tinyint(1) NOT NULL,
  `Sms` tinyint(1) NOT NULL,
  `hide` tinyint(1) NOT NULL,
  `Usuario_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo`
--

CREATE TABLE IF NOT EXISTS `archivo` (
`Id` int(11) NOT NULL,
  `Visible` tinyint(1) NOT NULL,
  `Enlace` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Curso_has_Asignatura_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignatura`
--

CREATE TABLE IF NOT EXISTS `asignatura` (
`Id` int(11) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `hora_total` int(11) DEFAULT NULL,
  `hide` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE IF NOT EXISTS `asistencia` (
`Id` int(11) NOT NULL,
  `Fecha_asistencia` datetime NOT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `Asignatura_Id` int(11) NOT NULL,
  `presente` tinyint(1) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bloques`
--

CREATE TABLE IF NOT EXISTS `bloques` (
`Id` int(11) NOT NULL,
  `Hora_Inicio` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Hora_Fin` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambiar_nota`
--

CREATE TABLE IF NOT EXISTS `cambiar_nota` (
`Id` int(11) NOT NULL,
  `N_Nueva` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `N_Anterior` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Justificacion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Fecha` date NOT NULL,
  `Nota_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
`Id` int(11) NOT NULL,
  `Tipo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Grado` int(11) NOT NULL,
  `Letra` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Profesor_Id` int(11) NOT NULL,
  `Agno` date NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso_has_alumno`
--

CREATE TABLE IF NOT EXISTS `curso_has_alumno` (
  `Curso_Id` int(11) NOT NULL,
  `Alumno_Id` int(11) NOT NULL,
`Id` int(11) NOT NULL,
  `Año` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso_has_asignatura`
--

CREATE TABLE IF NOT EXISTS `curso_has_asignatura` (
  `Curso_Id` int(11) NOT NULL,
  `Asignatura_Id` int(11) NOT NULL,
`Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE IF NOT EXISTS `documentos` (
`Id` int(11) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Enlace` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Respuestas_Foro_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foro`
--

CREATE TABLE IF NOT EXISTS `foro` (
`Id` int(11) NOT NULL,
  `Titulo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Comentario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Curso_has_Asignatura_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hoja_vida`
--

CREATE TABLE IF NOT EXISTS `hoja_vida` (
`Id` int(11) NOT NULL,
  `Tipo` tinyint(1) NOT NULL,
  `Descripcion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE IF NOT EXISTS `horario` (
`Id` int(11) NOT NULL,
  `Temporada` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Año` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Bloques_Id` int(11) NOT NULL,
  `Curso_has_Asignatura_Id` int(11) NOT NULL,
  `Dia` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `justificacion`
--

CREATE TABLE IF NOT EXISTS `justificacion` (
`Id` int(11) NOT NULL,
  `Motivo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Asistencia_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `Registro` datetime NOT NULL,
  `IP` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
`Id` int(11) NOT NULL,
  `Descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Tabla` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Usuario_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
`id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE IF NOT EXISTS `mensaje` (
`Id` int(11) NOT NULL,
  `Titulo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Contenido` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota`
--

CREATE TABLE IF NOT EXISTS `nota` (
`Id` int(11) NOT NULL,
  `Nota` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `Asignatura_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `origen_destino`
--

CREATE TABLE IF NOT EXISTS `origen_destino` (
`Id` int(11) NOT NULL,
  `Mensaje_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL,
  `Destino_id` int(11) unsigned NOT NULL,
  `Origen_id` int(11) unsigned NOT NULL,
  `visto` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta`
--

CREATE TABLE IF NOT EXISTS `pregunta` (
`Id` int(11) NOT NULL,
  `Texto` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Prueba_Id` int(11) NOT NULL,
  `A_1` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `A_2` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `A_3` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `A_4` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `A_5` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `Correcta` int(11) DEFAULT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE IF NOT EXISTS `profesor` (
`Id` int(11) NOT NULL,
  `Profesion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hide` tinyint(1) DEFAULT NULL,
  `Usuario_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor_has_asignatura`
--

CREATE TABLE IF NOT EXISTS `profesor_has_asignatura` (
  `Profesor_Id` int(11) NOT NULL,
  `Asignatura_Id` int(11) NOT NULL,
`Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prueba`
--

CREATE TABLE IF NOT EXISTS `prueba` (
`Id` int(11) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` date NOT NULL,
  `Curso_has_Asignatura_Id` int(11) NOT NULL,
  `Exigencia` double NOT NULL,
  `Visible` tinyint(1) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas_foro`
--

CREATE TABLE IF NOT EXISTS `respuestas_foro` (
`Id` int(11) NOT NULL,
  `Comentario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Foro_Id` int(11) NOT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuesta_alumno`
--

CREATE TABLE IF NOT EXISTS `respuesta_alumno` (
`Id` int(11) NOT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `Respuesta` int(11) NOT NULL,
  `Pregunta_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reuniones`
--

CREATE TABLE IF NOT EXISTS `reuniones` (
`Id` int(11) NOT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Curso_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, 'p10KUE1YE/B5D/h6HfqKre', 1268889823, 1430164409, 1, 'Admin', 'istrator', '0'),
(2, '127.0.0.1', 'juan perez', '$2y$08$P5O4m2aDZIutE/gcH43MP.VuDERM8IqOi1gVhJ0NkePLeTKRDSsgC', NULL, '1@1.com', NULL, NULL, NULL, '/aoBqwOtF.mjCxgcjbG.d.', 1429563120, 1429561187, 1, 'juan', 'perez', '123123'),
(3, '127.0.0.1', 'alfonso naguian', '$2y$08$SgcB3blfOY5maIVM7/Yz7uyfnIPGCB.6NbKCi5NukeME5d/Wufm1G', NULL, 'danryu@live.cl', NULL, '6UELbnviy2sVy5E8ULtGVe36411ff9f1d19644e8', 1429562118, NULL, 1429561336, 1430164445, 1, 'Alfonso', 'Naguian', '987654321');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(6, 3, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
 ADD PRIMARY KEY (`Id`), ADD KEY `fk_Alumno_Apoderado1_idx` (`Apoderado_Id`), ADD KEY `fk_alumno_users1_idx` (`Usuario_id`);

--
-- Indices de la tabla `apoderado`
--
ALTER TABLE `apoderado`
 ADD PRIMARY KEY (`Id`), ADD KEY `fk_apoderado_users1_idx` (`Usuario_id`);

--
-- Indices de la tabla `archivo`
--
ALTER TABLE `archivo`
 ADD PRIMARY KEY (`Id`), ADD KEY `fk_Archivo_Curso_has_Asignatura1_idx` (`Curso_has_Asignatura_Id`);

--
-- Indices de la tabla `asignatura`
--
ALTER TABLE `asignatura`
 ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Alumno_Id`,`Asignatura_Id`), ADD KEY `fk_Asistencia_Alumno1_idx` (`Alumno_Id`), ADD KEY `fk_Asistencia_Asignatura1_idx` (`Asignatura_Id`);

--
-- Indices de la tabla `bloques`
--
ALTER TABLE `bloques`
 ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `cambiar_nota`
--
ALTER TABLE `cambiar_nota`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Nota_Id`), ADD KEY `fk_Cambiar_Nota_Nota1_idx` (`Nota_Id`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Profesor_Id`), ADD KEY `fk_Curso_Profesor1_idx` (`Profesor_Id`);

--
-- Indices de la tabla `curso_has_alumno`
--
ALTER TABLE `curso_has_alumno`
 ADD PRIMARY KEY (`Id`), ADD KEY `fk_Curso_has_Alumno_Alumno1_idx` (`Alumno_Id`), ADD KEY `fk_Curso_has_Alumno_Curso1_idx` (`Curso_Id`);

--
-- Indices de la tabla `curso_has_asignatura`
--
ALTER TABLE `curso_has_asignatura`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Curso_Id`,`Asignatura_Id`), ADD KEY `fk_Curso_has_Asignatura_Asignatura1_idx` (`Asignatura_Id`), ADD KEY `fk_Curso_has_Asignatura_Curso1_idx` (`Curso_Id`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Respuestas_Foro_Id`), ADD KEY `fk_Documentos_Respuestas_Foro1_idx` (`Respuestas_Foro_Id`);

--
-- Indices de la tabla `foro`
--
ALTER TABLE `foro`
 ADD PRIMARY KEY (`Id`), ADD KEY `fk_Foro_Curso_has_Asignatura1_idx` (`Curso_has_Asignatura_Id`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hoja_vida`
--
ALTER TABLE `hoja_vida`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Alumno_Id`), ADD KEY `fk_Hoja_Vida_Alumno1_idx` (`Alumno_Id`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
 ADD PRIMARY KEY (`Id`), ADD KEY `fk_Horario_Bloques1_idx` (`Bloques_Id`), ADD KEY `fk_Horario_Curso_has_Asignatura1_idx` (`Curso_has_Asignatura_Id`);

--
-- Indices de la tabla `justificacion`
--
ALTER TABLE `justificacion`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Asistencia_Id`), ADD KEY `fk_Justificacion_Asistencia1_idx` (`Asistencia_Id`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
 ADD PRIMARY KEY (`Id`), ADD KEY `fk_log_users1_idx` (`Usuario_id`);

--
-- Indices de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
 ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `nota`
--
ALTER TABLE `nota`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Alumno_Id`,`Asignatura_Id`), ADD KEY `fk_Nota_Alumno1_idx` (`Alumno_Id`), ADD KEY `fk_Nota_Asignatura1_idx` (`Asignatura_Id`);

--
-- Indices de la tabla `origen_destino`
--
ALTER TABLE `origen_destino`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index5` (`Mensaje_Id`), ADD KEY `fk_Origen_Destino_Mensaje1_idx` (`Mensaje_Id`), ADD KEY `fk_origen_destino_users1_idx` (`Destino_id`), ADD KEY `fk_origen_destino_users2_idx` (`Origen_id`);

--
-- Indices de la tabla `pregunta`
--
ALTER TABLE `pregunta`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Prueba_Id`), ADD KEY `fk_Pregunta_Prueba1_idx` (`Prueba_Id`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
 ADD PRIMARY KEY (`Id`), ADD KEY `fk_profesor_users1_idx` (`Usuario_id`);

--
-- Indices de la tabla `profesor_has_asignatura`
--
ALTER TABLE `profesor_has_asignatura`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Profesor_Id`,`Asignatura_Id`), ADD KEY `fk_Profesor_has_Asignatura_Asignatura1_idx` (`Asignatura_Id`), ADD KEY `fk_Profesor_has_Asignatura_Profesor1_idx` (`Profesor_Id`);

--
-- Indices de la tabla `prueba`
--
ALTER TABLE `prueba`
 ADD PRIMARY KEY (`Id`), ADD KEY `fk_Prueba_Curso_has_Asignatura1_idx` (`Curso_has_Asignatura_Id`);

--
-- Indices de la tabla `respuestas_foro`
--
ALTER TABLE `respuestas_foro`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Foro_Id`,`Alumno_Id`), ADD KEY `fk_Respuestas_Foro_Foro1_idx` (`Foro_Id`), ADD KEY `fk_Respuestas_Foro_Alumno1_idx` (`Alumno_Id`);

--
-- Indices de la tabla `respuesta_alumno`
--
ALTER TABLE `respuesta_alumno`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Alumno_Id`), ADD KEY `fk_Respuesta_Alumno_Alumno1_idx` (`Alumno_Id`), ADD KEY `fk_Respuesta_Alumno_Pregunta1_idx` (`Pregunta_Id`);

--
-- Indices de la tabla `reuniones`
--
ALTER TABLE `reuniones`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Curso_Id`), ADD KEY `fk_Reuniones_Curso1_idx` (`Curso_Id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_groups`
--
ALTER TABLE `users_groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`), ADD KEY `fk_users_groups_users1_idx` (`user_id`), ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `apoderado`
--
ALTER TABLE `apoderado`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `archivo`
--
ALTER TABLE `archivo`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `asignatura`
--
ALTER TABLE `asignatura`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `asistencia`
--
ALTER TABLE `asistencia`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bloques`
--
ALTER TABLE `bloques`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cambiar_nota`
--
ALTER TABLE `cambiar_nota`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `curso_has_alumno`
--
ALTER TABLE `curso_has_alumno`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `curso_has_asignatura`
--
ALTER TABLE `curso_has_asignatura`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `foro`
--
ALTER TABLE `foro`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `hoja_vida`
--
ALTER TABLE `hoja_vida`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `justificacion`
--
ALTER TABLE `justificacion`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `mensaje`
--
ALTER TABLE `mensaje`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `nota`
--
ALTER TABLE `nota`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `origen_destino`
--
ALTER TABLE `origen_destino`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `pregunta`
--
ALTER TABLE `pregunta`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `profesor_has_asignatura`
--
ALTER TABLE `profesor_has_asignatura`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `prueba`
--
ALTER TABLE `prueba`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `respuestas_foro`
--
ALTER TABLE `respuestas_foro`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `respuesta_alumno`
--
ALTER TABLE `respuesta_alumno`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reuniones`
--
ALTER TABLE `reuniones`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `users_groups`
--
ALTER TABLE `users_groups`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
ADD CONSTRAINT `fk_Alumno_Apoderado1` FOREIGN KEY (`Apoderado_Id`) REFERENCES `apoderado` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_alumno_users1` FOREIGN KEY (`Usuario_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `apoderado`
--
ALTER TABLE `apoderado`
ADD CONSTRAINT `fk_apoderado_users1` FOREIGN KEY (`Usuario_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `archivo`
--
ALTER TABLE `archivo`
ADD CONSTRAINT `fk_Archivo_Curso_has_Asignatura1` FOREIGN KEY (`Curso_has_Asignatura_Id`) REFERENCES `curso_has_asignatura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `asistencia`
--
ALTER TABLE `asistencia`
ADD CONSTRAINT `fk_Asistencia_Alumno1` FOREIGN KEY (`Alumno_Id`) REFERENCES `alumno` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Asistencia_Asignatura1` FOREIGN KEY (`Asignatura_Id`) REFERENCES `asignatura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cambiar_nota`
--
ALTER TABLE `cambiar_nota`
ADD CONSTRAINT `fk_Cambiar_Nota_Nota1` FOREIGN KEY (`Nota_Id`) REFERENCES `nota` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `curso`
--
ALTER TABLE `curso`
ADD CONSTRAINT `fk_Curso_Profesor1` FOREIGN KEY (`Profesor_Id`) REFERENCES `profesor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `curso_has_alumno`
--
ALTER TABLE `curso_has_alumno`
ADD CONSTRAINT `fk_Curso_has_Alumno_Alumno1` FOREIGN KEY (`Alumno_Id`) REFERENCES `alumno` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Curso_has_Alumno_Curso1` FOREIGN KEY (`Curso_Id`) REFERENCES `curso` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `curso_has_asignatura`
--
ALTER TABLE `curso_has_asignatura`
ADD CONSTRAINT `fk_Curso_has_Asignatura_Asignatura1` FOREIGN KEY (`Asignatura_Id`) REFERENCES `asignatura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Curso_has_Asignatura_Curso1` FOREIGN KEY (`Curso_Id`) REFERENCES `curso` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `documentos`
--
ALTER TABLE `documentos`
ADD CONSTRAINT `fk_Documentos_Respuestas_Foro1` FOREIGN KEY (`Respuestas_Foro_Id`) REFERENCES `respuestas_foro` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `foro`
--
ALTER TABLE `foro`
ADD CONSTRAINT `fk_Foro_Curso_has_Asignatura1` FOREIGN KEY (`Curso_has_Asignatura_Id`) REFERENCES `curso_has_asignatura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `hoja_vida`
--
ALTER TABLE `hoja_vida`
ADD CONSTRAINT `fk_Hoja_Vida_Alumno1` FOREIGN KEY (`Alumno_Id`) REFERENCES `alumno` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `horario`
--
ALTER TABLE `horario`
ADD CONSTRAINT `fk_Horario_Bloques1` FOREIGN KEY (`Bloques_Id`) REFERENCES `bloques` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Horario_Curso_has_Asignatura1` FOREIGN KEY (`Curso_has_Asignatura_Id`) REFERENCES `curso_has_asignatura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `justificacion`
--
ALTER TABLE `justificacion`
ADD CONSTRAINT `fk_Justificacion_Asistencia1` FOREIGN KEY (`Asistencia_Id`) REFERENCES `asistencia` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `log`
--
ALTER TABLE `log`
ADD CONSTRAINT `fk_log_users1` FOREIGN KEY (`Usuario_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `nota`
--
ALTER TABLE `nota`
ADD CONSTRAINT `fk_Nota_Alumno1` FOREIGN KEY (`Alumno_Id`) REFERENCES `alumno` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Nota_Asignatura1` FOREIGN KEY (`Asignatura_Id`) REFERENCES `asignatura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `origen_destino`
--
ALTER TABLE `origen_destino`
ADD CONSTRAINT `fk_Origen_Destino_Mensaje1` FOREIGN KEY (`Mensaje_Id`) REFERENCES `mensaje` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_origen_destino_users1` FOREIGN KEY (`Destino_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_origen_destino_users2` FOREIGN KEY (`Origen_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pregunta`
--
ALTER TABLE `pregunta`
ADD CONSTRAINT `fk_Pregunta_Prueba1` FOREIGN KEY (`Prueba_Id`) REFERENCES `prueba` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `profesor`
--
ALTER TABLE `profesor`
ADD CONSTRAINT `fk_profesor_users1` FOREIGN KEY (`Usuario_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `profesor_has_asignatura`
--
ALTER TABLE `profesor_has_asignatura`
ADD CONSTRAINT `fk_Profesor_has_Asignatura_Asignatura1` FOREIGN KEY (`Asignatura_Id`) REFERENCES `asignatura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Profesor_has_Asignatura_Profesor1` FOREIGN KEY (`Profesor_Id`) REFERENCES `profesor` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `prueba`
--
ALTER TABLE `prueba`
ADD CONSTRAINT `fk_Prueba_Curso_has_Asignatura1` FOREIGN KEY (`Curso_has_Asignatura_Id`) REFERENCES `curso_has_asignatura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `respuestas_foro`
--
ALTER TABLE `respuestas_foro`
ADD CONSTRAINT `fk_Respuestas_Foro_Alumno1` FOREIGN KEY (`Alumno_Id`) REFERENCES `alumno` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Respuestas_Foro_Foro1` FOREIGN KEY (`Foro_Id`) REFERENCES `foro` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `respuesta_alumno`
--
ALTER TABLE `respuesta_alumno`
ADD CONSTRAINT `fk_Respuesta_Alumno_Alumno1` FOREIGN KEY (`Alumno_Id`) REFERENCES `alumno` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Respuesta_Alumno_Pregunta1` FOREIGN KEY (`Pregunta_Id`) REFERENCES `pregunta` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reuniones`
--
ALTER TABLE `reuniones`
ADD CONSTRAINT `fk_Reuniones_Curso1` FOREIGN KEY (`Curso_Id`) REFERENCES `curso` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users_groups`
--
ALTER TABLE `users_groups`
ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
