-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-07-2015 a las 04:56:46
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `tesis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
  `Id` int(11) NOT NULL,
  `Tipo_Sangre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Alergias` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Apoderado_Id` int(11) NOT NULL,
  `hide` tinyint(1) DEFAULT NULL,
  `Usuario_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`Id`, `Tipo_Sangre`, `Alergias`, `Apoderado_Id`, `hide`, `Usuario_id`) VALUES
(2, 'Tipo AB', 'ZAPATOS', 19, NULL, 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apoderado`
--

CREATE TABLE IF NOT EXISTS `apoderado` (
  `Id` int(11) NOT NULL,
  `Domicilio` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Wsp` tinyint(1) NOT NULL,
  `Sms` tinyint(1) NOT NULL,
  `hide` tinyint(1) NOT NULL,
  `Usuario_id` int(11) unsigned NOT NULL,
  `Comuna_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `apoderado`
--

INSERT INTO `apoderado` (`Id`, `Domicilio`, `Wsp`, `Sms`, `hide`, `Usuario_id`, `Comuna_id`) VALUES
(19, 'Luxemburgo 123', 0, 0, 0, 14, 0),
(20, '9876512', 0, 0, 0, 18, 0),
(21, 'Osorno 123', 0, 0, 0, 23, 2),
(22, 'Santander', 0, 0, 0, 24, 295),
(23, 'tumblr', 0, 0, 0, 25, 315),
(24, 'prueba2', 0, 1, 0, 26, 213),
(25, '123123', 0, 1, 0, 34, 346),
(26, 'asdasd', 0, 1, 0, 35, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo`
--

CREATE TABLE IF NOT EXISTS `archivo` (
  `Id` int(11) NOT NULL,
  `Visible` tinyint(1) NOT NULL,
  `Enlace` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Curso_has_Asignatura_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignatura`
--

CREATE TABLE IF NOT EXISTS `asignatura` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `hora_total` int(11) DEFAULT NULL,
  `hide` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `asignatura`
--

INSERT INTO `asignatura` (`Id`, `Nombre`, `Descripcion`, `hora_total`, `hide`) VALUES
(1, 'Matematicas', 'masmdamsdasdhjahbfjkasd', 45, '0'),
(2, 'Asig 2', 'lorem', 12, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE IF NOT EXISTS `asistencia` (
  `Id` int(11) NOT NULL,
  `Fecha_asistencia` datetime NOT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `Asignatura_Id` int(11) NOT NULL,
  `presente` tinyint(1) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bloques`
--

CREATE TABLE IF NOT EXISTS `bloques` (
  `Id` int(11) NOT NULL,
  `Hora_Inicio` time DEFAULT NULL,
  `Hora_Fin` time DEFAULT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambiar_nota`
--

CREATE TABLE IF NOT EXISTS `cambiar_nota` (
  `Id` int(11) NOT NULL,
  `N_Nueva` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `N_Anterior` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Justificacion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Fecha` date NOT NULL,
  `Nota_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comunas`
--

CREATE TABLE IF NOT EXISTS `comunas` (
  `codigoInterno` int(11) unsigned NOT NULL DEFAULT '0',
  `nombre` varchar(255) DEFAULT NULL,
  `padre` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comunas`
--

INSERT INTO `comunas` (`codigoInterno`, `nombre`, `padre`) VALUES
(346, 'ALTO HOSPICIO', 1),
(296, 'CAMINA', 1),
(297, 'COLCHANE', 1),
(3, 'HUARA', 1),
(2, 'IQUIQUE', 1),
(4, 'PICA', 1),
(5, 'POZO ALMONTE', 1),
(7, 'ANTOFAGASTA', 2),
(10, 'CALAMA', 2),
(298, 'MARIA ELENA', 2),
(8, 'MEJILLONES', 2),
(300, 'OLLAGÜE', 2),
(301, 'SAN PEDRO DE ATACAMA', 2),
(299, 'SIERRA GORDA', 2),
(9, 'TALTAL', 2),
(6, 'TOCOPILLA', 2),
(302, 'ALTO DEL CARMEN', 3),
(14, 'CALDERA', 3),
(11, 'CHAÑARAL', 3),
(13, 'COPIAPO', 3),
(12, 'DIEGO DE ALMAGRO', 3),
(17, 'FREIRINA', 3),
(18, 'HUASCO', 3),
(15, 'TIERRA AMARILLA', 3),
(16, 'VALLENAR', 3),
(22, 'ANDACOLLO', 4),
(31, 'CANELA', 4),
(29, 'COMBARBALA', 4),
(21, 'COQUIMBO', 4),
(30, 'ILLAPEL', 4),
(20, 'LA HIGUERA', 4),
(19, 'LA SERENA', 4),
(33, 'LOS VILOS', 4),
(26, 'MONTE PATRIA', 4),
(25, 'OVALLE', 4),
(24, 'PAIHUANO', 4),
(27, 'PUNITAQUI', 4),
(28, 'RIO HURTADO', 4),
(32, 'SALAMANCA', 4),
(23, 'VICUÑA', 4),
(44, 'ALGARROBO', 5),
(56, 'CABILDO', 5),
(67, 'CALLE LARGA', 5),
(46, 'CARTAGENA', 5),
(40, 'CASABLANCA', 5),
(63, 'CATEMU', 5),
(340, 'CONCON', 5),
(45, 'EL QUISCO', 5),
(47, 'EL TABO', 5),
(51, 'HIJUELAS', 5),
(41, 'ISLA DE PASCUA', 5),
(321, 'JUAN FERNANDEZ', 5),
(50, 'LA CALERA', 5),
(49, 'LA CRUZ', 5),
(59, 'LA LIGUA', 5),
(53, 'LIMACHE', 5),
(65, 'LLAY LLAY', 5),
(66, 'LOS ANDES', 5),
(52, 'NOGALES', 5),
(54, 'OLMUE', 5),
(62, 'PANQUEHUE', 5),
(57, 'PAPUDO', 5),
(55, 'PETORCA', 5),
(36, 'PUCHUNCAVI', 5),
(61, 'PUTAENDO', 5),
(48, 'QUILLOTA', 5),
(38, 'QUILPUE', 5),
(35, 'QUINTERO', 5),
(68, 'RINCONADA', 5),
(42, 'SAN ANTONIO', 5),
(69, 'SAN ESTEBAN', 5),
(60, 'SAN FELIPE', 5),
(64, 'SANTA MARIA', 5),
(43, 'SANTO DOMINGO', 5),
(34, 'VALPARAISO', 5),
(39, 'VILLA ALEMANA', 5),
(37, 'VIÑA DEL MAR', 5),
(58, 'ZAPALLAR', 5),
(132, 'CHEPICA', 6),
(125, 'CHIMBARONGO', 6),
(110, 'CODEGUA', 6),
(114, 'COINCO', 6),
(113, 'COLTAUCO', 6),
(112, 'DOÑIHUE', 6),
(107, 'GRANEROS', 6),
(139, 'LA ESTRELLA', 6),
(116, 'LAS CABRAS', 6),
(136, 'LITUECHE', 6),
(129, 'LOLOL', 6),
(106, 'MACHALI', 6),
(122, 'MALLOA', 6),
(134, 'MARCHIGUE', 6),
(126, 'NANCAGUA', 6),
(138, 'NAVIDAD', 6),
(120, 'OLIVAR', 6),
(130, 'PALMILLA', 6),
(133, 'PAREDONES', 6),
(131, 'PERALILLO', 6),
(115, 'PEUMO', 6),
(118, 'PICHIDEGUA', 6),
(137, 'PICHILEMU', 6),
(127, 'PLACILLA', 6),
(135, 'PUMANQUE', 6),
(123, 'QUINTA DE TILCOCO', 6),
(105, 'RANCAGUA', 6),
(121, 'RENGO', 6),
(119, 'REQUINOA', 6),
(124, 'SAN FERNANDO', 6),
(111, 'SAN FRANCISCO DE MOSTAZAL', 6),
(117, 'SAN VICENTE', 6),
(128, 'SANTA CRUZ', 6),
(166, 'CAUQUENES', 7),
(167, 'CHANCO', 7),
(161, 'COLBUN', 7),
(157, 'CONSTITUCION', 7),
(155, 'CUREPTO', 7),
(140, 'CURICO', 7),
(158, 'EMPEDRADO', 7),
(144, 'HUALAÑE', 7),
(145, 'LICANTEN', 7),
(159, 'LINARES', 7),
(162, 'LONGAVI', 7),
(154, 'MAULE', 7),
(147, 'MOLINA', 7),
(164, 'PARRAL', 7),
(152, 'PELARCO', 7),
(320, 'PELLUHUE', 7),
(153, 'PENCAHUE', 7),
(143, 'RAUCO', 7),
(165, 'RETIRO', 7),
(149, 'RIO CLARO', 7),
(141, 'ROMERAL', 7),
(148, 'SAGRADA FAMILIA', 7),
(151, 'SAN CLEMENTE', 7),
(156, 'SAN JAVIER', 7),
(341, 'SAN RAFAEL', 7),
(150, 'TALCA', 7),
(142, 'TENO', 7),
(146, 'VICHUQUEN', 7),
(163, 'VILLA ALEGRE', 7),
(160, 'YERBAS BUENAS', 7),
(303, 'ANTUCO', 8),
(198, 'ARAUCO', 8),
(180, 'BULNES', 8),
(208, 'CABRERO', 8),
(201, 'CAÑETE', 8),
(344, 'CHIGUAYANTE', 8),
(168, 'CHILLAN', 8),
(342, 'CHILLAN VIEJO', 8),
(175, 'COBQUECURA', 8),
(186, 'COELEMU', 8),
(170, 'COIHUECO', 8),
(188, 'CONCEPCION', 8),
(202, 'CONTULMO', 8),
(194, 'CORONEL', 8),
(197, 'CURANILAHUE', 8),
(185, 'EL CARMEN', 8),
(193, 'FLORIDA', 8),
(192, 'HUALQUI', 8),
(210, 'LAJA', 8),
(199, 'LEBU', 8),
(200, 'LOS ALAMOS', 8),
(204, 'LOS ANGELES', 8),
(195, 'LOTA', 8),
(214, 'MULCHEN', 8),
(212, 'NACIMIENTO', 8),
(213, 'NEGRETE', 8),
(174, 'NINHUE', 8),
(184, 'PEMUCO', 8),
(191, 'PENCO', 8),
(169, 'PINTO', 8),
(171, 'PORTEZUELO', 8),
(215, 'QUILACO', 8),
(206, 'QUILLECO', 8),
(182, 'QUILLON', 8),
(172, 'QUIRIHUE', 8),
(187, 'RANQUIL', 8),
(176, 'SAN CARLOS', 8),
(178, 'SAN FABIAN', 8),
(177, 'SAN GREGORIO DE ÑIQUEN', 8),
(181, 'SAN IGNACIO', 8),
(179, 'SAN NICOLAS', 8),
(343, 'SAN PEDRO DE LA PAZ', 8),
(211, 'SAN ROSENDO', 8),
(205, 'SANTA BARBARA', 8),
(196, 'SANTA JUANA', 8),
(189, 'TALCAHUANO', 8),
(203, 'TIRUA', 8),
(190, 'TOME', 8),
(173, 'TREHUACO', 8),
(209, 'TUCAPEL', 8),
(207, 'YUMBEL', 8),
(183, 'YUNGAY', 8),
(216, 'ANGOL', 9),
(235, 'CARAHUE', 9),
(220, 'COLLIPULLI', 9),
(230, 'CUNCO', 9),
(225, 'CURACAUTIN', 9),
(305, 'CURARREHUE', 9),
(221, 'ERCILLA', 9),
(229, 'FREIRE', 9),
(232, 'GALVARINO', 9),
(238, 'GORBEA', 9),
(231, 'LAUTARO', 9),
(240, 'LONCOCHE', 9),
(226, 'LONQUIMAY', 9),
(218, 'LOS SAUCES', 9),
(223, 'LUMACO', 9),
(304, 'MELIPEUCO', 9),
(234, 'NUEVA IMPERIAL', 9),
(345, 'PADRE LAS CASAS', 9),
(233, 'PERQUENCO', 9),
(237, 'PITRUFQUEN', 9),
(242, 'PUCON', 9),
(236, 'PUERTO SAAVEDRA', 9),
(217, 'PUREN', 9),
(219, 'RENAICO', 9),
(227, 'TEMUCO', 9),
(306, 'TEODORO SCHMIDT', 9),
(239, 'TOLTEN', 9),
(222, 'TRAIGUEN', 9),
(224, 'VICTORIA', 9),
(228, 'VILCUN', 9),
(241, 'VILLARRICA', 9),
(277, 'ANCUD', 10),
(265, 'CALBUCO', 10),
(270, 'CASTRO', 10),
(280, 'CHAITEN', 10),
(271, 'CHONCHI', 10),
(262, 'COCHAMO', 10),
(276, 'CURACO DE VELEZ', 10),
(279, 'DALCAHUE', 10),
(268, 'FRESIA', 10),
(269, 'FRUTILLAR', 10),
(281, 'FUTALEUFU', 10),
(308, 'HUALAIHUE', 10),
(267, 'LLANQUIHUE', 10),
(264, 'LOS MUERMOS', 10),
(263, 'MAULLIN', 10),
(255, 'OSORNO', 10),
(282, 'PALENA', 10),
(261, 'PUERTO MONTT', 10),
(258, 'PUERTO OCTAY', 10),
(266, 'PUERTO VARAS', 10),
(274, 'PUQUELDON', 10),
(260, 'PURRANQUE', 10),
(256, 'PUYEHUE', 10),
(272, 'QUEILEN', 10),
(273, 'QUELLON', 10),
(278, 'QUEMCHI', 10),
(275, 'QUINCHAO', 10),
(259, 'RIO NEGRO', 10),
(307, 'SAN JUAN DE LA COSTA', 10),
(257, 'SAN PABLO', 10),
(285, 'AYSEN', 11),
(287, 'CHILE CHICO', 11),
(286, 'CISNES', 11),
(289, 'COCHRANE', 11),
(284, 'COYHAIQUE', 11),
(309, 'GUAITECAS', 11),
(312, 'LAGO VERDE', 11),
(310, 'O´HIGGINS', 11),
(288, 'RIO IBAÑEZ', 11),
(311, 'TORTEL', 11),
(316, 'LAGUNA BLANCA', 12),
(319, 'NAVARINO', 12),
(292, 'PORVENIR', 12),
(317, 'PRIMAVERA', 12),
(291, 'PUERTO NATALES', 12),
(290, 'PUNTA ARENAS', 12),
(314, 'RIO VERDE', 12),
(315, 'SAN GREGORIO', 12),
(318, 'TIMAUKEL', 12),
(313, 'TORRES DEL PAINE', 12),
(109, 'ALHUE', 13),
(103, 'BUIN', 13),
(99, 'CALERA DE TANGO', 13),
(333, 'CERRILLOS', 13),
(324, 'CERRO NAVIA', 13),
(76, 'COLINA', 13),
(75, 'CONCHALI', 13),
(83, 'CURACAVI', 13),
(338, 'EL BOSQUE', 13),
(89, 'EL MONTE', 13),
(328, 'ESTACION CENTRAL', 13),
(334, 'HUECHURABA', 13),
(330, 'INDEPENDENCIA', 13),
(87, 'ISLA DE MAIPO', 13),
(96, 'LA CISTERNA', 13),
(93, 'LA FLORIDA', 13),
(97, 'LA GRANJA', 13),
(327, 'LA PINTANA', 13),
(92, 'LA REINA', 13),
(78, 'LAMPA', 13),
(71, 'LAS CONDES', 13),
(332, 'LO BARNECHEA', 13),
(337, 'LO ESPEJO', 13),
(325, 'LO PRADO', 13),
(323, 'MACUL', 13),
(94, 'MAIPU', 13),
(90, 'MARIA PINTO', 13),
(88, 'MELIPILLA', 13),
(91, 'ÑUÑOA', 13),
(339, 'PADRE HURTADO', 13),
(104, 'PAINE', 13),
(336, 'PEDRO AGUIRRE CERDA', 13),
(85, 'PEÑAFLOR', 13),
(322, 'PEÑALOLEN', 13),
(101, 'PIRQUE', 13),
(72, 'PROVIDENCIA', 13),
(82, 'PUDAHUEL', 13),
(100, 'PUENTE ALTO', 13),
(79, 'QUILICURA', 13),
(81, 'QUINTA NORMAL', 13),
(329, 'RECOLETA', 13),
(77, 'RENCA', 13),
(98, 'SAN BERNARDO', 13),
(335, 'SAN JOAQUIN', 13),
(102, 'SAN JOSE DE MAIPO', 13),
(95, 'SAN MIGUEL', 13),
(108, 'SAN PEDRO', 13),
(326, 'SAN RAMON', 13),
(70, 'SANTIAGO CENTRO', 13),
(73, 'SANTIAGO OESTE', 13),
(84, 'SANTIAGO SUR', 13),
(86, 'TALAGANTE', 13),
(80, 'TIL-TIL', 13),
(331, 'VITACURA', 13),
(244, 'CORRAL', 14),
(248, 'FUTRONO', 14),
(251, 'LA UNION', 14),
(254, 'LAGO RANCO', 14),
(249, 'LANCO', 14),
(247, 'LOS LAGOS', 14),
(246, 'MAFIL', 14),
(245, 'MARIQUINA', 14),
(243, 'VALDIVIA', 14),
(250, 'PANGUIPULLI', 14),
(252, 'PAILLACO', 14),
(253, 'RIO BUENO', 14),
(1, 'ARICA', 15),
(295, 'CAMARONES', 15),
(293, 'GENERAL LAGOS', 15),
(294, 'PUTRE', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
  `Id` int(11) NOT NULL,
  `Tipo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Grado` int(11) NOT NULL,
  `Letra` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Profesor_Id` int(11) NOT NULL,
  `Agno` year(4) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`Id`, `Tipo`, `Grado`, `Letra`, `Profesor_Id`, `Agno`, `hide`) VALUES
(1, '123123', 7, 'A', 2, 2015, 0),
(4, '123', 8, 'b', 1, 2015, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso_has_alumno`
--

CREATE TABLE IF NOT EXISTS `curso_has_alumno` (
  `Curso_Id` int(11) NOT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `Id` int(11) NOT NULL,
  `Año` year(4) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `curso_has_alumno`
--

INSERT INTO `curso_has_alumno` (`Curso_Id`, `Alumno_Id`, `Id`, `Año`, `hide`) VALUES
(1, 2, 1, 2015, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso_has_asignatura`
--

CREATE TABLE IF NOT EXISTS `curso_has_asignatura` (
  `Curso_Id` int(11) NOT NULL,
  `Asignatura_Id` int(11) NOT NULL,
  `Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL,
  `Cantidad_Notas` int(11) NOT NULL DEFAULT '0',
  `Cantidad_Hora_Semana` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `curso_has_asignatura`
--

INSERT INTO `curso_has_asignatura` (`Curso_Id`, `Asignatura_Id`, `Id`, `hide`, `Cantidad_Notas`, `Cantidad_Hora_Semana`) VALUES
(1, 1, 1, 0, 4, 3),
(1, 2, 2, 0, 0, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE IF NOT EXISTS `documentos` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Enlace` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Respuestas_Foro_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foro`
--

CREATE TABLE IF NOT EXISTS `foro` (
  `Id` int(11) NOT NULL,
  `Titulo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Comentario` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Curso_has_Asignatura_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `foro`
--

INSERT INTO `foro` (`Id`, `Titulo`, `Comentario`, `Curso_has_Asignatura_Id`, `hide`, `Fecha`, `Estado`) VALUES
(36, 'Ejemplo', 'Ejemplo de Foro 1', 1, 0, '2015-07-04 04:56:39', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrador'),
(2, 'Apoderado', 'Usuario general sin privilegios'),
(3, 'Alumno', 'Usuario con privilegios simples'),
(4, 'Profesor', 'Usuario con mas privilegios'),
(5, 'Profesor Jefe', 'Usuario con aún mas privilegios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hoja_vida`
--

CREATE TABLE IF NOT EXISTS `hoja_vida` (
  `Id` int(11) NOT NULL,
  `Tipo` tinyint(1) NOT NULL,
  `Descripcion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `hide` tinyint(1) NOT NULL,
  `Profesor_Id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hoja_vida`
--

INSERT INTO `hoja_vida` (`Id`, `Tipo`, `Descripcion`, `Alumno_Id`, `Date`, `hide`, `Profesor_Id`) VALUES
(1, 0, '0', 2, '2015-06-25 01:08:47', 0, 6),
(5, 1, 'Hola Mundo', 2, '2015-06-25 01:13:43', 0, 6),
(6, 0, 'El pendejo qlo se porta mal', 2, '2015-06-25 01:14:23', 0, 6),
(7, 0, 'Hola Mundo', 2, '2014-06-25 10:37:31', 0, 6),
(8, 0, '12', 2, '2014-06-25 10:51:30', 0, 6),
(9, 1, 'asd', 2, '2015-06-25 10:52:24', 0, 6),
(10, 0, 'qsdasd', 2, '2015-06-26 00:21:07', 0, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE IF NOT EXISTS `horario` (
  `Id` int(11) NOT NULL,
  `Temporada` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Año` year(4) DEFAULT NULL,
  `Bloques_Id` int(11) NOT NULL,
  `Curso_has_Asignatura_Id` int(11) NOT NULL,
  `Dia` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `justificacion`
--

CREATE TABLE IF NOT EXISTS `justificacion` (
  `Id` int(11) NOT NULL,
  `Motivo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Asistencia_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `Registro` datetime NOT NULL,
  `IP` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Id` int(11) NOT NULL,
  `Descripcion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Tabla` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Usuario_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE IF NOT EXISTS `mensaje` (
  `Id` int(11) NOT NULL,
  `Titulo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Contenido` varchar(400) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` datetime NOT NULL,
  `hide` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `mensaje`
--

INSERT INTO `mensaje` (`Id`, `Titulo`, `Contenido`, `Fecha`, `hide`) VALUES
(1, 'Ejemplo 1', 'asdasdasd', '2015-05-03 00:00:00', NULL),
(2, 'Ejemplo 1', 'asdasdasd', '2015-05-03 00:00:00', NULL),
(3, 'demo', 'Demo!', '2015-05-03 00:00:00', NULL),
(4, 'demo', 'Demo!', '2015-05-03 00:00:00', NULL),
(5, 'demo', 'Demo!', '2015-05-03 00:00:00', NULL),
(6, '123123', '123123123', '2015-05-03 00:00:00', NULL),
(7, '123123', '123123123', '2015-05-03 00:00:00', NULL),
(8, '123123', '123123123', '2015-05-03 00:00:00', NULL),
(9, '123', '123123', '2015-05-03 00:00:00', NULL),
(10, '123', '123123', '2015-05-03 00:00:00', NULL),
(11, '123123', '<ol><li>123123</li></ol>', '2015-05-03 00:00:00', NULL),
(12, '123123', '123123', '2015-05-03 00:00:00', NULL),
(13, '123123', '123123', '2015-05-03 00:00:00', NULL),
(14, '234234', '234234', '2015-05-03 00:00:00', NULL),
(15, '234234', '234234', '2015-05-03 00:00:00', NULL),
(16, '123', '123123', '2015-05-03 00:00:00', NULL),
(17, '123123', 'sadgsdg', '2015-05-03 00:00:00', NULL),
(18, '123123', '3123123', '2015-05-03 00:00:00', NULL),
(19, '123123', '123123', '2015-05-03 00:00:00', NULL),
(20, '123123', '123123', '2015-05-03 00:00:00', NULL),
(21, '123', '123123', '2015-05-03 00:00:00', NULL),
(22, '123123', '123123123', '2015-05-03 00:00:00', NULL),
(23, 'Asdas', 'asdasd', '2015-05-03 00:00:00', NULL),
(24, 'Asdas', 'asdasd', '2015-05-03 00:00:00', NULL),
(25, '123', '123123', '2015-05-03 00:00:00', NULL),
(26, '123', '123123', '2015-05-03 00:00:00', NULL),
(27, '123123', '123123', '2015-05-03 00:00:00', NULL),
(28, '123123', '123123', '2015-05-03 00:00:00', NULL),
(29, '123123', '123123', '2015-05-03 00:00:00', NULL),
(30, '123', '123', '2015-05-03 00:00:00', NULL),
(31, 'Asd', 'asdasd', '2015-05-03 00:00:00', NULL),
(32, '123', '123123', '2015-05-03 00:00:00', NULL),
(33, '123', '123123', '2015-05-03 00:00:00', NULL),
(34, '123123', '123123123', '2015-05-03 00:00:00', NULL),
(35, '123123123123', '123123123123', '2015-05-03 00:00:00', NULL),
(36, '123123', '123123123', '2015-05-03 00:00:00', NULL),
(37, '123123', '123123', '2015-05-03 00:00:00', NULL),
(38, '123123', '123123', '2015-05-03 00:00:00', NULL),
(39, '123123', '123123', '2015-05-03 00:00:00', NULL),
(40, '123123', '123123123123', '2015-05-03 00:00:00', NULL),
(41, '123123', '123123123', '2015-05-03 00:00:00', NULL),
(42, '1231231', '123123', '2015-05-03 00:00:00', NULL),
(43, '1231231', '123123', '2015-05-03 00:00:00', NULL),
(44, '123123', '1231231231', '2015-05-03 00:00:00', NULL),
(45, '123123', '1231231231', '2015-05-03 00:00:00', NULL),
(46, 'a', 'asd', '2015-05-03 00:00:00', NULL),
(47, '123', '123123', '2015-05-04 00:00:00', NULL),
(48, '123', '123123', '2015-05-04 00:00:00', NULL),
(49, '123123', '123123', '2015-05-04 00:00:00', NULL),
(50, '123123', '123123', '2015-05-04 00:00:00', NULL),
(51, '123123', '123123123', '2015-05-04 00:00:00', NULL),
(52, '123123', '123123', '2015-05-04 00:00:00', NULL),
(53, '123123', '123123123', '2015-05-04 00:00:00', NULL),
(54, '123123', '123123123', '2015-05-04 00:00:00', NULL),
(55, '123123', '123123123', '2015-05-04 00:00:00', NULL),
(56, '123123', '123123', '2015-05-04 00:00:00', NULL),
(57, '123123123', '123123123', '2015-05-04 00:00:00', NULL),
(58, '123123123', '1123123123', '2015-05-04 00:00:00', NULL),
(59, '123123123', '1123123123', '2015-05-04 00:00:00', NULL),
(60, '123123', '123123', '2015-05-04 00:00:00', NULL),
(61, '123123', '123123123', '2015-05-04 00:00:00', NULL),
(62, '123123', '1231231', '2015-05-04 00:00:00', NULL),
(63, '123123', '123123123', '2015-05-04 00:00:00', NULL),
(64, 'Mensaje de prueba Mandrill', '<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nec imperdiet turpis. Maecenas ac neque <b>malesuada, egestas odio sed</b>, iaculis elit. Etiam dolor ligula, vulputate non convallis vitae, mollis a mauris.&nbsp;<br><ul><li>asdasdasd</li><li>asdasdasd</li></ul><ol><li>asdasdasdasd</li><li>asasdasdasd</li></ol><i>Suspendisse placerat nibh sed ultricies malesuada. Nulla eget dolo', '2015-05-04 00:00:00', NULL),
(65, '123123', '123123123', '2015-05-04 00:00:00', NULL),
(66, '123123123', '123123123', '2015-05-04 00:00:00', NULL),
(67, '123123', '123123123', '2015-05-04 00:00:00', NULL),
(68, 'Hola', 'Hola', '2015-05-04 00:00:00', NULL),
(69, '12', '123123', '2015-05-04 00:00:00', NULL),
(70, '123', '123', '2015-05-04 00:00:00', NULL),
(71, '1231', '123123', '2015-05-04 00:00:00', NULL),
(72, 'Hola Longi', '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occa', '2015-05-04 00:00:00', NULL),
(73, '123123', 'Hola', '2015-05-04 00:00:00', NULL),
(74, '123123', '123123123', '2015-05-04 00:00:00', NULL),
(75, '123123', 'asdasdasdasd', '2015-05-04 00:00:00', NULL),
(76, 'Mensaje Prueba', 'Hola mundo', '2015-05-04 00:00:00', NULL),
(77, 'Mensaje Prueba', 'Hola mundo', '2015-05-04 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota`
--

CREATE TABLE IF NOT EXISTS `nota` (
  `Id` int(11) NOT NULL,
  `Nota` double DEFAULT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `Set_nota_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `origen_destino`
--

CREATE TABLE IF NOT EXISTS `origen_destino` (
  `Id` int(11) NOT NULL,
  `Mensaje_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL,
  `Destino_id` int(11) unsigned NOT NULL,
  `Origen_id` int(11) unsigned NOT NULL,
  `visto` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `origen_destino`
--

INSERT INTO `origen_destino` (`Id`, `Mensaje_Id`, `hide`, `Destino_id`, `Origen_id`, `visto`) VALUES
(1, 13, 0, 22, 14, 0),
(2, 14, 0, 22, 14, 0),
(3, 15, 0, 22, 14, 0),
(4, 16, 0, 22, 14, 0),
(5, 17, 0, 18, 14, 0),
(6, 23, 0, 18, 14, 0),
(7, 25, 0, 22, 14, 0),
(8, 26, 0, 22, 14, 0),
(9, 27, 0, 18, 14, 0),
(10, 29, 0, 18, 14, 0),
(11, 34, 0, 18, 14, 0),
(12, 35, 0, 18, 14, 0),
(13, 36, 0, 14, 14, 1),
(14, 37, 0, 14, 14, 1),
(15, 38, 0, 14, 14, 1),
(16, 39, 0, 14, 14, 1),
(17, 40, 0, 14, 14, 1),
(18, 41, 0, 14, 14, 1),
(19, 42, 0, 14, 14, 1),
(20, 43, 0, 14, 14, 1),
(21, 44, 0, 14, 14, 1),
(22, 45, 0, 14, 14, 1),
(23, 46, 0, 14, 14, 1),
(24, 47, 0, 18, 14, 0),
(25, 48, 0, 18, 14, 0),
(26, 49, 0, 18, 14, 0),
(27, 50, 0, 18, 14, 0),
(28, 51, 0, 18, 14, 0),
(29, 52, 0, 18, 14, 0),
(30, 53, 0, 18, 14, 0),
(31, 54, 0, 18, 14, 0),
(32, 55, 0, 18, 14, 0),
(33, 56, 0, 18, 14, 0),
(34, 57, 0, 18, 14, 0),
(35, 58, 0, 18, 14, 0),
(36, 59, 0, 18, 14, 0),
(37, 60, 0, 18, 14, 0),
(38, 61, 0, 18, 14, 0),
(39, 62, 0, 17, 14, 0),
(41, 63, 0, 18, 14, 0),
(42, 64, 0, 17, 14, 0),
(43, 64, 0, 18, 14, 0),
(44, 65, 0, 17, 14, 0),
(45, 65, 0, 18, 14, 0),
(46, 66, 0, 17, 14, 0),
(47, 66, 0, 18, 14, 0),
(48, 67, 0, 17, 14, 0),
(49, 67, 0, 18, 14, 0),
(50, 68, 0, 18, 14, 0),
(51, 69, 0, 17, 14, 0),
(52, 69, 0, 18, 14, 0),
(53, 70, 0, 18, 14, 0),
(54, 71, 0, 17, 32, 0),
(55, 72, 0, 17, 36, 0),
(56, 73, 0, 18, 14, 0),
(57, 74, 0, 17, 14, 0),
(58, 75, 0, 17, 14, 0),
(59, 75, 0, 18, 14, 0),
(60, 76, 0, 17, 14, 0),
(61, 76, 0, 18, 14, 0),
(62, 77, 0, 17, 14, 0),
(63, 77, 0, 18, 14, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta`
--

CREATE TABLE IF NOT EXISTS `pregunta` (
  `Id` int(11) NOT NULL,
  `Texto` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Prueba_Id` int(11) NOT NULL,
  `A_1` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `A_2` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `A_3` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `A_4` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `A_5` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `Correcta` int(11) DEFAULT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE IF NOT EXISTS `profesor` (
  `Id` int(11) NOT NULL,
  `Profesion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hide` tinyint(1) DEFAULT NULL,
  `Usuario_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`Id`, `Profesion`, `hide`, `Usuario_id`) VALUES
(1, NULL, NULL, 0),
(2, 'matematico', NULL, 30),
(4, 'vago', NULL, 32),
(5, '123', NULL, 33),
(6, 'Vago', NULL, 14),
(7, 'Vago', NULL, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor_has_asignatura`
--

CREATE TABLE IF NOT EXISTS `profesor_has_asignatura` (
  `Profesor_Id` int(11) NOT NULL,
  `Asignatura_Id` int(11) NOT NULL,
  `Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `profesor_has_asignatura`
--

INSERT INTO `profesor_has_asignatura` (`Profesor_Id`, `Asignatura_Id`, `Id`, `hide`) VALUES
(6, 1, 1, 0),
(6, 2, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prueba`
--

CREATE TABLE IF NOT EXISTS `prueba` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Fecha` datetime NOT NULL,
  `Fecha_evaluacion` datetime NOT NULL,
  `Curso_has_Asignatura_Id` int(11) NOT NULL,
  `Exigencia` double NOT NULL,
  `Visible` tinyint(1) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regiones`
--

CREATE TABLE IF NOT EXISTS `regiones` (
  `codigo` int(11) NOT NULL,
  `nombre` char(255) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `activo` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `regiones`
--

INSERT INTO `regiones` (`codigo`, `nombre`, `orden`, `activo`) VALUES
(14, 'Región de Los Ríos', 0, 1),
(13, 'Región Metropolitana', 0, 1),
(12, 'Región de Magallanes y la Antártica Chilena', 0, 1),
(11, 'Región de Aysén del General Carlos Ibáñez del Campo', 0, 1),
(10, 'Región de Los Lagos', 0, 1),
(9, 'Región de la Araucanía', 0, 1),
(8, 'Región del Bío-Bío', 0, 1),
(7, 'Región del Maule', 0, 1),
(6, 'Región del Libertador General Bernardo O Higgins', 0, 1),
(5, 'Región de Valparaiso', 0, 1),
(4, 'Región de Coquimbo', 0, 1),
(3, 'Región de Atacama', 0, 1),
(2, 'Región de Antofagasta', 0, 1),
(1, 'Región de Tarapacá', 0, 1),
(15, 'Región de Arica y Parinacota', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reglamento_notas`
--

CREATE TABLE IF NOT EXISTS `reglamento_notas` (
  `Id` int(11) NOT NULL,
  `Cantidad_Horas` int(11) NOT NULL,
  `Notas_minima` int(11) NOT NULL,
  `Notas_maxima` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `reglamento_notas`
--

INSERT INTO `reglamento_notas` (`Id`, `Cantidad_Horas`, `Notas_minima`, `Notas_maxima`) VALUES
(1, 2, 4, 5),
(2, 3, 4, 6),
(3, 4, 6, 7),
(4, 5, 6, 7),
(5, 6, 7, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas_foro`
--

CREATE TABLE IF NOT EXISTS `respuestas_foro` (
  `Id` int(11) NOT NULL,
  `Comentario` mediumtext COLLATE utf8_spanish_ci,
  `Foro_Id` int(11) NOT NULL,
  `Users_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL,
  `Fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `respuestas_foro`
--

INSERT INTO `respuestas_foro` (`Id`, `Comentario`, `Foro_Id`, `Users_Id`, `hide`, `Fecha`) VALUES
(28, '123123', 15, 14, 0, '2015-06-17 21:44:14'),
(29, 'asdasdasdasdasdasdasd', 15, 14, 0, '2015-06-17 21:44:23'),
(30, 'asdasdasasdasdasd', 15, 14, 0, '2015-06-17 21:44:35'),
(31, 'Hola Mundo Jueves', 15, 14, 0, '2015-06-19 01:03:58'),
(32, 'Hola Jueves', 19, 14, 0, '2015-06-19 01:05:27'),
(33, 'Hola Mundo LUnes', 15, 14, 0, '2015-06-22 21:36:52'),
(34, 'Hola asdasd <i>asdasdasd&nbsp;<br><ol><li>asdasd</li><li><b>asdasd</b></li></ol></i><b><i></i></b>', 15, 14, 0, '2015-06-22 21:37:41'),
(35, '<b></b>La&nbsp;<b><a href="https://es.wikipedia.org/wiki/Arquitectura_de_Barcelona" target="" rel="">arquitectura de Barcelona</a></b>&nbsp;ha tenido una evolución paralela a la del resto de la&nbsp;<a href="https://es.wikipedia.org/wiki/Arte_de_Catalu%C3%B1a" target="" rel="">arquitectura catalana</a>&nbsp;y&nbsp;<a href="https://es.wikipedia.org/wiki/Arquitectura_de_Espa%C3%B1a" target="" rel="">española</a>, y ha seguido de forma diversa las múltiples tendencias que se han ido produciendo en el contexto de la&nbsp;<a href="https://es.wikipedia.org/wiki/Historia_del_arte" target="" rel="">historia del arte</a>&nbsp;occidental. A lo largo de su<a href="https://es.wikipedia.org/wiki/Historia_de_Barcelona" target="" rel="">historia</a>,&nbsp;<a href="https://es.wikipedia.org/wiki/Barcelona" target="" rel="">Barcelona</a>&nbsp;ha acogido diversas culturas y civilizaciones, que han aportado su concepto del arte y han dejado su legado para la posteridad, desde los primeros pobladores&nbsp;<a href="https://es.wikipedia.org/wiki/%C3%8Dbero" target="" rel="">íberos</a>, pasando por los colonizadores&nbsp;<a href="https://es.wikipedia.org/wiki/Antigua_Roma" target="" rel="">romanos</a>, los&nbsp;<a href="https://es.wikipedia.org/wiki/Visigodo" target="" rel="">visigodos</a>&nbsp;y un breve período<a href="https://es.wikipedia.org/wiki/Islam" target="" rel="">islámico</a>, hasta el surgimiento en la&nbsp;<a href="https://es.wikipedia.org/wiki/Edad_Media" target="" rel="">Edad Media</a>&nbsp;del arte, la lengua y la cultura catalana, con una primera época de esplendor para el<a href="https://es.wikipedia.org/wiki/Arte_de_Catalu%C3%B1a" target="" rel="">arte catalán</a>, en que el&nbsp;<a href="https://es.wikipedia.org/wiki/Rom%C3%A1nico" target="" rel="">románico</a>&nbsp;y el&nbsp;<a href="https://es.wikipedia.org/wiki/Arte_g%C3%B3tico" target="" rel="">gótico</a>&nbsp;fueron períodos muy fructíferos para el desarrollo artístico de la región.<span>Durante la&nbsp;<a href="https://es.wikipedia.org/wiki/Edad_Moderna" target="" rel="">Edad Moderna</a>, época en que la ciudad condal se vinculó a la&nbsp;<a href="https://es.wikipedia.org/wiki/Monarqu%C3%ADa_Hisp%C3%A1nica" target="" rel="">Monarquía Hispánica</a>, los principales estilos fueron el&nbsp;<a href="https://es.wikipedia.org/wiki/Renacimiento" target="" rel="">Renacimiento</a>&nbsp;y el&nbsp;<a href="https://es.wikipedia.org/wiki/Barroco" target="" rel="">Barroco</a>, desarrollados a partir de las propuestas provenientes de los países difusores de estos estilos, principalmente<a href="https://es.wikipedia.org/wiki/Italia" target="" rel="">Italia</a>&nbsp;y&nbsp;<a href="https://es.wikipedia.org/wiki/Francia" target="" rel="">Francia</a>. Estos estilos fueron aplicados con diversas variantes locales, y si bien algunos autores afirman que no fue un período especialmente esplendoroso en el devenir artístico de la ciudad, la calidad de las obras estuvo en consonancia con la del conjunto del estado, mientras que en cantidad fue un período bastante productivo, aunque la mayor parte de las realizaciones no haya llegado a la actualidad.</span>', 15, 14, 0, '2015-06-22 21:38:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuesta_alumno`
--

CREATE TABLE IF NOT EXISTS `respuesta_alumno` (
  `Id` int(11) NOT NULL,
  `Alumno_Id` int(11) NOT NULL,
  `Respuesta` int(11) NOT NULL,
  `Pregunta_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reuniones`
--

CREATE TABLE IF NOT EXISTS `reuniones` (
  `Id` int(11) NOT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Curso_Id` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `set_nota`
--

CREATE TABLE IF NOT EXISTS `set_nota` (
  `Id` int(11) NOT NULL,
  `Curso_has_asignatura_Id` int(11) NOT NULL,
  `Tipo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Fecha` date NOT NULL,
  `hide` tinyint(1) NOT NULL,
  `Registrada` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `set_nota`
--

INSERT INTO `set_nota` (`Id`, `Curso_has_asignatura_Id`, `Tipo`, `Fecha`, `hide`, `Registrada`) VALUES
(25, 1, '123', '2015-07-07', 0, 0),
(26, 1, '123', '2015-07-07', 0, 0),
(27, 1, '123', '2015-07-07', 0, 0),
(28, 1, '123', '2015-07-07', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_evaluacion`
--

CREATE TABLE IF NOT EXISTS `tipos_evaluacion` (
  `Id` int(11) NOT NULL,
  `Tipo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Descripcion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `hide` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipos_evaluacion`
--

INSERT INTO `tipos_evaluacion` (`Id`, `Tipo`, `Descripcion`, `hide`) VALUES
(1, 'Prueba Online', 'Prueba realizada por el sistema ', 0),
(2, 'Prueba Escrita', 'Prueba Escrita', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `f_nacimiento` date NOT NULL,
  `sex` varchar(45) NOT NULL,
  `cellphone` varchar(45) NOT NULL,
  `Id_twitter` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `phone`, `f_nacimiento`, `sex`, `cellphone`, `Id_twitter`) VALUES
(14, '127.0.0.1', 'administrator', '$2y$08$PoQlr5xcfV0FKJxo3HFk4ua3Gn7ijqGVABBiaGbf2ue49rnU0/rki', NULL, 'admin@admin.com', 'activado', NULL, NULL, NULL, 1268889823, 1436045405, 1, 'Admin1', 'Admin2', '56972363570', '2015-04-27', '1', '56972363570', NULL),
(17, '127.0.0.1', '1-2', '$2y$08$EqGtXrLFsh.w3Cwfy1c8FOgewgcd6q81mOSUhrH26/1H0mEmIETbu', NULL, 'max.soto@jampsur.com', NULL, NULL, NULL, NULL, 1430240401, 1430320291, 1, 'Alfonso', 'Naguian', '56972363570', '0000-00-00', '', '', NULL),
(18, '127.0.0.1', '1-3', '$2y$08$RMN8tbPPpZukB1DqItiVju7yryCH4pc1DH/njTNvpLzDFBMiAlwjm', NULL, 'maxsotov@gmail.com', 'activado', NULL, NULL, NULL, 1430241027, 1433562114, 1, 'Apoderado', 'San', '56972363570', '0000-00-00', '', '', NULL),
(19, '127.0.0.1', '1-333', '0', NULL, '0', NULL, NULL, NULL, NULL, 1430495932, NULL, 1, 'Alfonso', 'Naguian', '0', '1970-01-01', '1', '0', NULL),
(20, '127.0.0.1', '1-3333', '$2y$08$EFfkgx.tP/cxVQcBK0.l5.06Zlow959GXsmLGR2d4IEoNfDwXcKQ2', NULL, '0', NULL, NULL, NULL, NULL, 1430496197, NULL, 1, 'Alfonso', 'Naguian', '0', '1970-01-01', '2', '0', NULL),
(21, '127.0.0.1', '1-33333', '$2y$08$NO5ogjpu2vmO9z9AYG8tEu5CQvqUAk0Iubys61/..f1MVI0P57HgK', NULL, 'aa.cc23@gmail.com', NULL, NULL, NULL, NULL, 1430496527, NULL, 1, 'Alfonso', 'Naguian', '98765432', '2015-02-09', '2', '56972363570', NULL),
(22, '127.0.0.1', '1-55555', '$2y$08$LevnV57tJmhXmmsKk0T7C.HwL7c/lnAy5EKhPlBCusd5orRtyqeHm', NULL, 'RR.SS@GMAIL.COM', NULL, NULL, NULL, NULL, 1430497202, NULL, 1, 'Rodrigo', 'Soto', '123123123', '2015-02-11', '1', '56972363570', NULL),
(23, '127.0.0.1', '12-12', '$2y$08$D.AV.JdmXgTt5ge1o0tNDezMSqlPR4qnOwW/Ua8mXD8vRK6kccare', NULL, 'roberto.man@gmail.com', NULL, NULL, NULL, NULL, 1430501976, NULL, 1, 'roberto', 'man', '56972363570', '0000-00-00', '', '', NULL),
(24, '127.0.0.1', '123-123', '$2y$08$haJX8to9SjpAR8BJyNA6HeKZLBA2ScRSWVxj6MD0U5w3a5TGxa64S', NULL, 'prueba1@gmail.com', NULL, NULL, NULL, NULL, 1430502404, NULL, 1, 'prueba1', 'ta', '98765432', '2015-05-06', '2', '56972363570', NULL),
(25, '127.0.0.1', '1234-1234', '$2y$08$tFcLupa3KAzSIt96XDNaSOqp44oQqkzoA4gsUv39.azIQ7f1CAk2e', NULL, 'max.soto1@gmail.com', NULL, NULL, NULL, NULL, 1430502638, NULL, 1, 'Max', 'Soto1', '98765432', '2014-12-16', '1', '56972363570', NULL),
(26, '127.0.0.1', '1331-1331', '$2y$08$LWirNpFF7mSfM7VjtyvTKOCsztV9/PYbEn/5QPnJPo..G9GmdktzW', NULL, 'prueba2@gmail.com', NULL, NULL, NULL, NULL, 1430503619, NULL, 1, 'prueba2', 'asdasd', '1231231231', '2015-08-29', '2', '56972363570', NULL),
(27, '127.0.0.1', '19.19', '$2y$08$1hQi/2HGMDoaS3ZrT2lyHO.XlJwElvft07oCVWD1fPu/OjPr81SDq', NULL, 'vardoc@gmail.com', NULL, NULL, NULL, NULL, 1430598464, NULL, 1, 'vardoc', 'mia', '0987654', '1970-01-01', '2', '56972363570', NULL),
(28, '127.0.0.1', '20-20', '$2y$08$krt1Ow6yegx6pbCwpQVLpe/UhhLpKheBwQxcCgmWVLZOL2GZUI.ya', NULL, 'bmoya@gmail.com', NULL, NULL, NULL, NULL, 1430598707, NULL, 1, 'Barbara', 'Moya', '8615231', '1970-01-01', '1', '56972363570', NULL),
(29, '127.0.0.1', '20-21', '$2y$08$FOqe69soLbXgxW3aJME8xOcjFN/gC8QmFh3U/RorN4dmAkHsHsJtq', NULL, 'b.moya@gmail.com', NULL, NULL, NULL, NULL, 1430598756, NULL, 1, 'Barbara', 'Moya', '1231231', '1970-01-01', '1', '56972363570', NULL),
(30, '127.0.0.1', '200-200', '$2y$08$IFoq6mNNLyt5DnvKb819cewjhvU0j7GrMlVgJDJIeUsQMHMy8goHy', NULL, 'prueba100@gmail.com', NULL, NULL, NULL, NULL, 1430599280, NULL, 1, 'prueba100', '200', '132123', '1970-01-01', '1', '56972363570', NULL),
(32, '::1', '18869937-5', '$2y$08$w2TEskEFkysx/RVDzwhJ6OJZtez9fpW3zMUTS9RFxdBWpsljJ3B6O', NULL, 'max.soto@inacapmail.cl', NULL, NULL, NULL, NULL, 1430704812, 1430704866, 1, 'Max Eliseo', 'Soto Valdebenito', '123456789', '1970-01-01', '2', '56972363570', NULL),
(33, '::1', '1asd123', '$2y$08$pNeHSGHK1Qmu2TC4ZioTIe3gFyMKc1dVj4Wa2/IlWijrhHHgbuun2', NULL, 'maxsoto@1.com', NULL, NULL, NULL, NULL, 1430728295, NULL, 1, 'maxsoto', '123123', '123123123', '1970-01-01', '2', '56972363570', NULL),
(34, '::1', '123123', '$2y$08$R1SdjD30RvWJQmBJax8im.jHaMMfYImGFkk4/NSOwYrxFv4/kdfzW', NULL, '123@123.com', NULL, NULL, NULL, NULL, 1430728359, NULL, 1, '123123', '123123', '123123123', '2015-04-28', '2', '56972363570', NULL),
(35, '::1', '123123a', '$2y$08$lpILvQkcwsxsBJ3I4F7jsumLir1/52JNH0ceT2I3sSCb6KNTSMKe6', NULL, '123@321.com', NULL, NULL, NULL, NULL, 1430728742, 1430729123, 1, '123', '123', '12123', '2015-05-04', '2', '56972363570', NULL),
(36, '::1', '12346789', '$2y$08$0alHCorIlCJ/nY4/YhzfTuvARaX2xawu1lMbq3VCNRUmnybENRo82', NULL, 'ricardo.mansilla01@outlook.com', 'activado', NULL, NULL, NULL, 1430766457, 1430771361, 1, 'Profe 1', 'Profe 1', '123456789', '2015-05-04', '2', '56972363570', NULL),
(37, '::1', '19-19123', '$2y$08$VwNTE6Ij9Drud1t3cmGY/uEVFC1X6ckW/tklYFlGPgkY/L6KcazCi', NULL, 'psalinas1234@gmail.com', 'activado', NULL, NULL, NULL, 1430771137, 1430771274, 1, 'Pablo', 'Salinas', '2353765', '1939-09-13', '2', '56972363570', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 14, 1),
(17, 15, 2),
(18, 16, 2),
(19, 17, 2),
(20, 18, 2),
(21, 19, 3),
(22, 20, 3),
(23, 21, 3),
(24, 22, 3),
(25, 23, 2),
(26, 24, 2),
(27, 25, 2),
(28, 26, 2),
(29, 27, 4),
(30, 28, 4),
(31, 29, 4),
(32, 30, 4),
(33, 31, 4),
(34, 32, 4),
(35, 33, 4),
(36, 34, 2),
(37, 35, 2),
(38, 36, 4),
(39, 37, 4),
(40, 14, 2),
(48, 14, 5),
(49, 14, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_Alumno_Apoderado1_idx` (`Apoderado_Id`), ADD KEY `fk_alumno_users1_idx` (`Usuario_id`);

--
-- Indices de la tabla `apoderado`
--
ALTER TABLE `apoderado`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_apoderado_users1_idx` (`Usuario_id`), ADD KEY `fk_apoderado_comunas1_idx` (`Comuna_id`);

--
-- Indices de la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_Archivo_Curso_has_Asignatura1_idx` (`Curso_has_Asignatura_Id`);

--
-- Indices de la tabla `asignatura`
--
ALTER TABLE `asignatura`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Alumno_Id`,`Asignatura_Id`), ADD KEY `fk_Asistencia_Alumno1_idx` (`Alumno_Id`), ADD KEY `fk_Asistencia_Asignatura1_idx` (`Asignatura_Id`);

--
-- Indices de la tabla `bloques`
--
ALTER TABLE `bloques`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `cambiar_nota`
--
ALTER TABLE `cambiar_nota`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Nota_Id`), ADD KEY `fk_Cambiar_Nota_Nota1_idx` (`Nota_Id`);

--
-- Indices de la tabla `comunas`
--
ALTER TABLE `comunas`
  ADD PRIMARY KEY (`codigoInterno`), ADD KEY `fk_comunas_regiones1_idx` (`padre`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Profesor_Id`), ADD KEY `fk_Curso_Profesor1_idx` (`Profesor_Id`);

--
-- Indices de la tabla `curso_has_alumno`
--
ALTER TABLE `curso_has_alumno`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_Curso_has_Alumno_Alumno1_idx` (`Alumno_Id`), ADD KEY `fk_Curso_has_Alumno_Curso1_idx` (`Curso_Id`);

--
-- Indices de la tabla `curso_has_asignatura`
--
ALTER TABLE `curso_has_asignatura`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Curso_Id`,`Asignatura_Id`), ADD KEY `fk_Curso_has_Asignatura_Asignatura1_idx` (`Asignatura_Id`), ADD KEY `fk_Curso_has_Asignatura_Curso1_idx` (`Curso_Id`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Respuestas_Foro_Id`), ADD KEY `fk_Documentos_Respuestas_Foro1_idx` (`Respuestas_Foro_Id`);

--
-- Indices de la tabla `foro`
--
ALTER TABLE `foro`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_Foro_Curso_has_Asignatura1_idx` (`Curso_has_Asignatura_Id`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hoja_vida`
--
ALTER TABLE `hoja_vida`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_Hoja_Vida_Alumno1_idx` (`Alumno_Id`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_Horario_Bloques1_idx` (`Bloques_Id`), ADD KEY `fk_Horario_Curso_has_Asignatura1_idx` (`Curso_has_Asignatura_Id`);

--
-- Indices de la tabla `justificacion`
--
ALTER TABLE `justificacion`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Asistencia_Id`), ADD KEY `fk_Justificacion_Asistencia1_idx` (`Asistencia_Id`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_log_users1_idx` (`Usuario_id`);

--
-- Indices de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_Nota_Alumno1_idx` (`Alumno_Id`), ADD KEY `fk_Nota_Asignatura1_idx` (`Set_nota_Id`);

--
-- Indices de la tabla `origen_destino`
--
ALTER TABLE `origen_destino`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `Id` (`Id`), ADD KEY `fk_Origen_Destino_Mensaje1_idx` (`Mensaje_Id`), ADD KEY `fk_origen_destino_users1_idx` (`Destino_id`), ADD KEY `fk_origen_destino_users2_idx` (`Origen_id`);

--
-- Indices de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Prueba_Id`), ADD KEY `fk_Pregunta_Prueba1_idx` (`Prueba_Id`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_profesor_users1_idx` (`Usuario_id`);

--
-- Indices de la tabla `profesor_has_asignatura`
--
ALTER TABLE `profesor_has_asignatura`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Profesor_Id`,`Asignatura_Id`), ADD KEY `fk_Profesor_has_Asignatura_Asignatura1_idx` (`Asignatura_Id`), ADD KEY `fk_Profesor_has_Asignatura_Profesor1_idx` (`Profesor_Id`);

--
-- Indices de la tabla `prueba`
--
ALTER TABLE `prueba`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_Prueba_Curso_has_Asignatura1_idx` (`Curso_has_Asignatura_Id`);

--
-- Indices de la tabla `regiones`
--
ALTER TABLE `regiones`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `reglamento_notas`
--
ALTER TABLE `reglamento_notas`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `respuestas_foro`
--
ALTER TABLE `respuestas_foro`
  ADD PRIMARY KEY (`Id`), ADD KEY `fk_Respuestas_Foro_Foro1_idx` (`Foro_Id`), ADD KEY `fk_Respuestas_Foro_Alumno1_idx` (`Users_Id`);

--
-- Indices de la tabla `respuesta_alumno`
--
ALTER TABLE `respuesta_alumno`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index4` (`Alumno_Id`), ADD KEY `fk_Respuesta_Alumno_Alumno1_idx` (`Alumno_Id`), ADD KEY `fk_Respuesta_Alumno_Pregunta1_idx` (`Pregunta_Id`);

--
-- Indices de la tabla `reuniones`
--
ALTER TABLE `reuniones`
  ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `index3` (`Curso_Id`), ADD KEY `fk_Reuniones_Curso1_idx` (`Curso_Id`);

--
-- Indices de la tabla `set_nota`
--
ALTER TABLE `set_nota`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `tipos_evaluacion`
--
ALTER TABLE `tipos_evaluacion`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_users_groups_users1_idx` (`user_id`), ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `apoderado`
--
ALTER TABLE `apoderado`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `archivo`
--
ALTER TABLE `archivo`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `asignatura`
--
ALTER TABLE `asignatura`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bloques`
--
ALTER TABLE `bloques`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cambiar_nota`
--
ALTER TABLE `cambiar_nota`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `curso_has_alumno`
--
ALTER TABLE `curso_has_alumno`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `curso_has_asignatura`
--
ALTER TABLE `curso_has_asignatura`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `foro`
--
ALTER TABLE `foro`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `hoja_vida`
--
ALTER TABLE `hoja_vida`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `justificacion`
--
ALTER TABLE `justificacion`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT de la tabla `nota`
--
ALTER TABLE `nota`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `origen_destino`
--
ALTER TABLE `origen_destino`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `profesor_has_asignatura`
--
ALTER TABLE `profesor_has_asignatura`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `prueba`
--
ALTER TABLE `prueba`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reglamento_notas`
--
ALTER TABLE `reglamento_notas`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `respuestas_foro`
--
ALTER TABLE `respuestas_foro`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `respuesta_alumno`
--
ALTER TABLE `respuesta_alumno`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reuniones`
--
ALTER TABLE `reuniones`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `set_nota`
--
ALTER TABLE `set_nota`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `tipos_evaluacion`
--
ALTER TABLE `tipos_evaluacion`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
ADD CONSTRAINT `fk_Alumno_Apoderado1` FOREIGN KEY (`Apoderado_Id`) REFERENCES `apoderado` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_alumno_users1` FOREIGN KEY (`Usuario_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
