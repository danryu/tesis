function jsRemoveWindowLoad() { $("#WindowLoad").remove(); }
 
function jsShowWindowLoad(mensaje) {
    if (mensaje === undefined) mensaje = "Procesando la información<br>Espere por favor";
    height = 20;
    var ancho = 999999999999;
    var alto = 999999999999;
    if (window.innerWidth == undefined) ancho = window.screen.width;
    else ancho = window.innerWidth;
    if (window.innerHeight == undefined) alto = window.screen.height;
    else alto = window.innerHeight;

    var heightdivsito = alto/2 - parseInt(height)/2;
    var loader = '<div style=";margin: 25% 50%;"><label style="color: white; position: absolute; text-align: center; font-weight: bold; font-size: 16px; margin-top: -40px; margin-left: -40px;">Cargando</label><div class="loader-inner ball-clip-rotate-multiple" style="margin: 25% 50%;"><div></div><div></div></div></div>';
        div = document.createElement("div");
        div.id = "WindowLoad"
        $("body").append(div);
        input = document.createElement("input");
        input.id = "focusInput";
        input.type = "text"
        $("#WindowLoad").append(input);
        $("#WindowLoad").css({
                              "position": "fixed", 
                              "top": "0px",
                              "left":"0px",
                              "z-index":"3200",
                              "filter":"alpha(opacity=65)",
                              "-moz-opacity":"65",
                              "opacity":"0.65",
                              "background":"#999",
                              "width":"100%",
                              "height":"100%"
                            }); 
        $("#focusInput").focus();
        $("#focusInput").hide();
        $("#WindowLoad").html(loader);
 
}